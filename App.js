import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import reducer from './app/Reducer';
import Router from './router';
import logger from 'redux-logger';
import codePush from "react-native-code-push";
import thunk from "redux-thunk";

import { Sentry } from 'react-native-sentry';
import * as Config from "./Config";
import NotificationService from "./app/user/notification/NotificationService";

Sentry.config(Config.SENTRY_DSN, {
  deactivateStacktraceMerging: true
}).install();

NotificationService.configure();

console.disableYellowBox = true;
const store = createStore(reducer, applyMiddleware(logger, thunk));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router/>
      </Provider>
    );
  }
}

codePush.getUpdateMetadata().then((update) => {
  if (update) {
    Sentry.setVersion(update.appVersion + '-codepush:' + update.label);
  }
});

App = codePush(App);

module.exports = App;
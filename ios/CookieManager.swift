//
//  CookieManager.swift
//  SmallcaseIos
//
//  Created by Shashank M on 23/01/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

import Foundation

@objc(CookieManager)
class CookieManager: NSObject {
  
  @objc func clearCookies() -> Void {
    // Remove all cache
    URLCache.shared.removeAllCachedResponses()
    
    // Delete any associated cookies
    if let cookies = HTTPCookieStorage.shared.cookies {
      for cookie in cookies {
        HTTPCookieStorage.shared.deleteCookie(cookie)
      }
    }
  }
}

//
//  CookieManagerBridge.m
//  SmallcaseIos
//
//  Created by Shashank M on 23/01/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

// CalendarManagerBridge.m
#import "React/RCTBridgeModule.h"
#import "React/RCTBridge.h"
#import "React/RCTEventDispatcher.h"

@interface RCT_EXTERN_MODULE(CookieManager, NSObject)

RCT_EXTERN_METHOD(clearCookies)

@end

import { AppRegistry, Text, TextInput } from 'react-native';
import App from './App';

console.ignoredYellowBox = ['Remote debugger'];

Text.defaultProps.allowFontScaling=false
TextInput.defaultProps.allowFontScaling=false

AppRegistry.registerComponent('SmallcaseIos', () => App);

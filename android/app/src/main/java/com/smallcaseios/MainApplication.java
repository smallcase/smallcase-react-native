package com.smallcaseios;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.clevertap.react.CleverTapPackage;
import com.kevinejohn.RNMixpanel.RNMixpanel;
import com.robinpowered.react.Intercom.IntercomPackage;
import io.sentry.RNSentryPackage;
import io.sentry.RNSentryPackage;
import com.apsl.versionnumber.RNVersionNumberPackage;
import com.horcrux.svg.SvgPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import io.sentry.RNSentryPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.robinpowered.react.Intercom.IntercomPackage;
import com.wix.interactable.Interactable;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.github.wuxudong.rncharts.MPAndroidChartPackage;
import io.sentry.RNSentryPackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.robinpowered.react.Intercom.IntercomPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.microsoft.codepush.react.CodePush;
import com.apsl.versionnumber.RNVersionNumberPackage;
import com.github.wuxudong.rncharts.MPAndroidChartPackage;
import com.wix.interactable.Interactable;
import com.horcrux.svg.SvgPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new CleverTapPackage(),
            new RNMixpanel(),
            new IntercomPackage(),
            new RNSentryPackage(),
            new RNSentryPackage(),
            new RNVersionNumberPackage(),
            new SvgPackage(),
            new RNSpinkitPackage(),
            new RNSentryPackage(),
            new ReactNativePushNotificationPackage(),
            new LinearGradientPackage(),
            new IntercomPackage(),
            new Interactable(),
            new RNGestureHandlerPackage(),
            new MPAndroidChartPackage(),
            new RNSentryPackage(),
            new ReactNativePushNotificationPackage(),
            new IntercomPackage(),
            new RNSpinkitPackage(),
            new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
            new RNVersionNumberPackage(),
            new MPAndroidChartPackage(),
            new Interactable(),
            new SvgPackage(),
            new LinearGradientPackage(),
            new RNGestureHandlerPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}

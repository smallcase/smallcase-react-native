import React, {Component} from 'react';
import {StyleSheet, Image, TouchableHighlight, View, Text} from 'react-native';
import {connect} from 'react-redux';

import {
  Scene,
  Router,
  Actions
} from 'react-native-router-flux';
import Landing from './app/onboarding/LandingContainer';
import AllInvestmentsComponent from './app/investments/invested/AllInvestmentsComponent';
import InvestmentDetailsComponent from './app/investments/details/InvestmentDetailsComponent';
import LoginKite from './app/onboarding/login/LoginContainer';
import Info from './app/extra/Info';
import ShowNews from './app/investments/details/NewsComponent';
import ReviewOrder from "./app/order/ReviewOrder";
import AllOrders from './app/order/AllOrders';
import {Routes} from "./app/util/Utility";
import OrderBatches from "./app/order/batches/OrderBatchesComponent";
import StockOrderDetails from "./app/order/components/StockOrderDetails";
import OrderStatus from "./app/order/OrderStatus";
import SipComponent from "./app/investments/sip/SipComponent";
import AccountComponent from "./app/account/AccountComponent"
import Fees from "./app/account/Fees"
import Notifications from "./app/account/Notifications"
import About from "./app/account/About"
import ReviewRebalance from "./app/investments/rebalance/ReviewRebalance"
import CustomizeRebalance from "./app/investments/rebalance/CustomizeRebalance"
import Manage from "./app/investments/manage/Manage"
import ExitedSmallcaseComponent from "./app/investments/exited/ExitedSmallcaseComponent";
import LoginWithLeprechaunComponent from './app/onboarding/LoginWithLeprechaunComponent';

class InvestmentsIcon extends Component {
  render() {
    let color = this.props.focused
      ? this.props.activeTintColor
      : this.props.inactiveTintColor;

    return (
      <Image source={require('./assets/investments.png')} style={{tintColor: color}}/>
    );
  }
}

class ProfileIcon extends Component {

  render() {
    const { notificationsActive } = this.props;
    let color = this.props.focused
      ? this.props.activeTintColor
      : this.props.inactiveTintColor;


    return (
      <View>
        {(notificationsActive.notification && notificationsActive.notification.notificationsRead) && (<View
          style={{alignItems: 'flex-end', justifyContent: 'flex-end', marginLeft: 18, marginTop: 6, marginBottom: -6}}>
          <View style={styles.number}/>
        </View>)
        }
        <Image source={require('./assets/profile.png')} style={{tintColor: color}}/>

      </View>
    );
  }
}

const mapStateToProps = state => ({ notificationsActive: (state.notificationReducer) });

const ProfileIconWithBadge = connect(mapStateToProps, null)(ProfileIcon);

// const mapStateToProps = state => {return {badgeCount: state.notifications.count};};
// const ConnectedYourTabIcon = connect(mapStateToProps)(ProfileIcon);
// ...

let router = ({dispatch}) => {
  return (
    <Router uriPrefix={'smallcase.com'}>
      <Scene key="key" dispatch={dispatch}>
        <Scene key={Routes.LANDING} hideNavBar component={Landing} initial/>
        <Scene key={Routes.LOGIN_WITH_LEPRECHAUN} hideNavBar component={LoginWithLeprechaunComponent}/>
        <Scene key={Routes.INVESTMENT_DETAILS} backTitle={'Back'} navigationBarStyle={styles.navBar}
               title={'Investment Details'} component={InvestmentDetailsComponent} onEnter={InvestmentDetailsComponent.onEnter}/>
        <Scene key={Routes.LOGIN} navigationBarStyle={styles.navBar} title={'Login With Kite'}
               component={LoginKite}/>
        <Scene key={Routes.INFO} navigationBarStyle={styles.navBar} component={Info}/>
        <Scene key={Routes.REVIEW_ORDER} navigationBarStyle={styles.navBar} component={ReviewOrder}
               title={'Review Order'}/>
        <Scene key={Routes.SHOW_NEWS} navigationBarStyle={styles.navBar} title={'News'} backTitle={'Back'}
               component={ShowNews}/>
        <Scene key={Routes.ALL_ORDERS} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'Orders'}
               component={AllOrders}
               back={true} onEnter={AllOrders.onEnter}/>
        <Scene key={Routes.ORDER_BATCHES} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'All Batches'}
               component={OrderBatches}/>
        <Scene key={Routes.ORDER_DETAILS} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'Order Details'}
               component={StockOrderDetails}/>
        <Scene key={Routes.SIP} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'Set up Sip'}
               component={SipComponent}/>
        <Scene key={Routes.ORDER_STATUS} panHandlers={null} navigationBarStyle={styles.navBar} title={'Order Status'}
               left={() => null} component={OrderStatus}/>
        <Scene key={Routes.REVIEW_REBALANCE} navigationBarStyle={styles.navBar} title={'Review Rebalance'}
               component={ReviewRebalance} leftTitle={"Cancel"} onLeft={() => Actions.pop()} />
        <Scene key={Routes.CUSTOMIZE_REBALANCE} navigationBarStyle={styles.navBar} title={'Customize Rebalance'}
               component={CustomizeRebalance} panHandlers={null} leftTitle={"Cancel"} onLeft={CustomizeRebalance.onPressCancel}/>
        <Scene key={Routes.FEES} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'Fees'} component={Fees}/>
        <Scene key={Routes.NOTIFICATIONS} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'Notifications'}
               component={Notifications} onRight={null} rightTitle={null} onEnter={Notifications.onEnter}/>
        <Scene key={Routes.ABOUT} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'About'}
               component={About}/>
        <Scene key={Routes.MANAGE} hideNavBar title={'Manage'} component={Manage}
               panHandlers={null} />
        <Scene key={Routes.ROOT} hideNavBar panHandlers={null} tabs={true} activeTintColor={'rgb(31, 122, 224)'}
               inactiveTintColor={'rgb(83, 91, 98)'}
               showLabel={true} swipeEnabled={false} animationEnabled={false}
               tabBarPosition={"bottom"} tabBarStyle={styles.tabBar}>
          <Scene key={Routes.INVESTMENTS} navigationBarStyle={styles.navBar} back={false} title={'Investments'}
                 component={AllInvestmentsComponent}
                 icon={InvestmentsIcon} tabBarLabel={"Investments"} onEnter={AllInvestmentsComponent.onEnter}/>
          <Scene key={Routes.ACCOUNT} navigationBarStyle={styles.navBar} title={'Account'} back={false}
                 component={AccountComponent} icon={ProfileIconWithBadge} onEnter={AccountComponent.onEnter} notifs = {AccountComponent.unreadCount}/>
        </Scene>
        <Scene key={Routes.EXITED_SMALLCASES} backTitle={'Back'} navigationBarStyle={styles.navBar} title={'Exited'}
               component={ExitedSmallcaseComponent}/>
      </Scene>
    </Router>
  );
};

router = connect()(router);

const styles = StyleSheet.create({
  navBar: {
    backgroundColor: 'white'
  },
  tabBar: {
    backgroundColor: 'white',
    shadowOffset: {width: 10, height: 10,},
    shadowColor: 'black',
    shadowOpacity: 1.0,
  },
  number: {
    height: 6,
    width: 6,
    borderRadius: 100,
    backgroundColor: 'rgb(255, 89, 66)',
    justifyContent: 'center',
    alignItems: 'center',

  },
  badgeText: {
    fontSize: 12,
    color: 'white',
    fontWeight: 'bold'
  }
});

export default router;
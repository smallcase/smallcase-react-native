// Base URLs
export const BASE_URL_PROD = 'https://api.smallcase.com/';
export const BASE_URL_DEV = 'https://api-dev.smallcase.com/';
export const AUTH_BASE_URL_PROD = 'https://auth.smallcase.com/';
export const AUTH_BASE_URL_DEV = 'https://auth-dev.smallcase.com/';

// OnBoarding
export const BROKER_LOGIN = 'auth/brokerLogin';
export const CREATE_ACCOUNT = 'auth/applyForAccount';
export const GET_REQUEST_TOKEN = 'auth/getRequestToken';
export const LOGIN_SMALLCASE = 'auth/smallcase';

// User smallcase
export const TOTAL_INVESTMENT = 'user/sc/investments/total';
export const INVESTED_SMALLCASES = 'user/sc/investments';
export const INVESTED_SMALLCASE = 'user/sc/investments/smallcase';
export const EXITED_SMALLCASES = 'user/sc/investments/exited';
export const INVESTED_SMALLCASE_DETAILS = 'user/sc/investments/smallcase';
export const GET_USER_FUNDS = 'user/sc/funds';
export const GET_USER_DETAILS = 'user/sc/getUser';
export const CREATE_SIP = 'user/sc/actions/sip/create';
export const GET_SIP_DETAILS = 'user/sc/actions/sip/get';
export const EDIT_SIP = 'user/sc/actions/sip/manage';
export const END_SIP = 'user/sc/actions/sip/end';
export const SKIP_SIP = 'user/sc/actions/sip/ignore';
export const REGISTER_FOR_PUSH_NOTIFICATION = 'user/sc/registerDevice';
export const DE_REGISTER_FOR_PUSH_NOTIFICATION = 'user/sc/deregisterDevice';
export const PAYOUT_DATE = 'market/lbesPayoutDate';

// Market
export const GET_STOCK_PRICE = 'market/price';
export const GET_STOCK_INFO_AND_HISTORICAL = 'market/stocks/getInfoAndHistorical';
export const GET_STOCK_PRICE_AND_CHANGE = 'market/priceAndChange';
export const GET_MARKET_STATUS = 'market/status';
export const STOCKS_SEARCH = 'market/search';
export const STOCKS_SIMILAR = 'market/stocks/similar';
export const STOCK_HISTORICAL = 'market/historical';

// News
export const GET_NEWS = 'news/getNews';

// Orders
export const GET_ORDERS = 'user/sc/orders';
export const PLACE_ORDERS = 'user/sc/placeOrders';
export const REPAIR_BATCH = 'user/sc/fixBatch';
export const NO_ORDERS = 'user/sc/actions/rebalance/noOrders';
export const SKIP_REBALANCE = 'user/sc/actions/rebalance/cancel';
export const CANCEL_ORDER = 'user/sc/cancelBatch';

// Profile
export const GET_FEES = 'user/sc/fees';
export const GET_NOTIFICATIONS = 'user/sc/notifications';
export const NOTIFICATION_READ = 'user/sc/notifications/read';
export const NOTIFICATIONS_READALL = 'user/sc/notifications/readAll';

// Smallcases
export const SMALLCASE_DETAILS = 'smallcases/smallcase';
export const AWI_STATUS = 'smallcases/awi/status';
export const SMALLCASE_HISTORICAL = 'smallcases/historical';
export const SMALLCASE_LITE_DETAILS = 'smallcases/smallcaseLite';
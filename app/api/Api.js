import * as Config from "../../Config";

export default class Api {

  static getHeaders = (jwt, csrf) => {
    if (jwt && csrf) {
      return {
        'Cookie': 'jwt=JWT ' + jwt,
        'x-csrf-Token': csrf,
        'Content-Type': 'application/json'
      };
    }
    return {
      'Content-Type': 'application/json'
    };
  };

  static getConfig(jwt, csrf) {
    return {
      method: 'GET',
      headers: this.getHeaders(jwt, csrf)
    };
  }

  static postConfig(body, jwt, csrf) {
    return {
      method: 'POST',
      headers: this.getHeaders(jwt, csrf),
      body: JSON.stringify(body)
    };
  }

  static get({ route, jwt, csrf, params, baseUrl }) {
    route = route + (params ? ('?' + params) : '');
    return this.request(baseUrl, route, this.getConfig(jwt, csrf));
  }

  static post({ route, jwt, csrf, body, baseUrl }) {
    return this.request(baseUrl, route, this.postConfig(body, jwt, csrf));
  }

  static request(baseUrl, route, config) {
    if (Config.loggingEnabled) {
      console.log("Route: " + route);
      console.log(config);
    }
    return fetch((baseUrl ? baseUrl : Config.BASE_URL) + route, config)
      .then(response => {
        if (Config.loggingEnabled) console.log(response);
          if (!response.ok) return { success: false, status: response.status, data: JSON.parse(response._bodyText).data }
        return response.json()
      })
      .then(responseJson => {
        if (!responseJson.success) {
          return Promise.reject(responseJson);
        }
        return Promise.resolve(responseJson);
      })
      .catch((error) => {
        return Promise.reject({ success: false, status: null, data: error });
      });
  }
}
import {GET_FEES, GET_NOTIFICATIONS, GET_USER_DETAILS, GET_USER_FUNDS, NOTIFICATION_READ, NOTIFICATIONS_READALL} from "../api/ApiRoutes";
import Api from "../api/Api";
import {SmallcaseImageURLs} from "../util/Utility";

export const getUserFees = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: GET_FEES, jwt: jwt, csrf: csrf})
        .then((response) => resolve(response))
        .catch((error) => reject(error));
  })
};

export const getUserNotifications = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: GET_NOTIFICATIONS, jwt: jwt, csrf: csrf})
        .then((response) => resolve(response.data))
        .catch((error) => reject(error));
  })
};

export const markNotificationRead = (jwt, csrf, notificationId) => {
  const body = {
    notificationId : notificationId,
  };
  return new Promise((resolve, reject) => {
    Api.post({route: NOTIFICATION_READ, jwt, csrf, body})
        .then((response) => resolve(response))
        .catch((error) => reject(error));
  });
};

export const markNotificationsReadAll = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.post({route: NOTIFICATIONS_READALL, jwt, csrf})
        .then((response) => resolve(response))
        .catch((error) => reject(error));
  });
};

export const getUserDetails = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: GET_USER_DETAILS, jwt: jwt, csrf: csrf})
        .then((response) => resolve(response.data.broker))
        .catch((error) => reject(error));
  })
};

export const getImageURL = (scid) => {
  return SmallcaseImageURLs.NON_CREATED + "80/" + scid + ".png"
};
import React, {PureComponent} from 'react';
import {
  Text, View, Image, StyleSheet, TouchableHighlight
} from 'react-native';
import Divider from "../component/Divider"
import {AnalyticsSource, Routes} from "../util/Utility";
import {Actions} from "react-native-router-flux";
import VersionNumber from "react-native-version-number";
import AnalyticsManager from "../analytics/AnalyticsManager";

export default class About extends PureComponent {

  termsOfService = () => {
    Actions.push(Routes.INFO, {content: 'terms'});
  };

  privacy = () => {
    Actions.push(Routes.INFO, {content: 'privacy'});
  };

  disclosures = () => {
    Actions.push(Routes.INFO, {content: 'disclosures'});
  };

  componentDidMount() {
    AnalyticsManager.track('Viewed About', null, [AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])

  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../../assets/smallcaseLogo.png')} style={styles.image}/>
        <Text style={styles.text}>Version {VersionNumber.appVersion}</Text>
        <Text style={[styles.text, {marginBottom: 40, width: 260}]}>smallcase is the easiest way to invest in portfolios
          of stocks</Text>
        <Row text={"How It Works"} onClick={this.termsOfService}/>
        <Row text={"Privacy"} onClick={this.privacy}/>
        <Row text={"Disclosures"} onClick={this.disclosures}/>
      </View>
    );
  }

}
class Row extends PureComponent {
  render() {
    const {text, onClick} = this.props;

    return (
      <TouchableHighlight onPress={onClick}>
        <View style={styles.touchArea}>
          <View style={styles.row}>
            <Text style={styles.rowText}>{text}</Text>
            <Image source={require('../../assets/chevronLeft.png')} style={styles.rightIcon}/>
          </View>
          <Divider dividerStyles={{marginTop: 12}}/>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  image: {
    width: 42,
    height: 48,
    marginTop: 32,
    marginBottom: 16,
    alignSelf: 'center'
  },
  text: {
    color: 'rgb(129, 135, 140)',
    lineHeight: 20,
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 8,
    alignSelf: 'center'
  },
  row: {
    flexDirection: 'row',
  },
  rightIcon: {
    marginRight: 16,
    height: 18,
    width: 18,
    transform: [{rotate: '180deg'}],
    tintColor: '#81878C'
  },
  rowText: {
    flex: 1,
    fontSize: 16,
    color: 'rgb(47, 54, 63)'
  },
  touchArea: {
    flexDirection: 'column',
    paddingTop: 18,
    paddingLeft: 16,
    backgroundColor: 'white'
  }


});
import React, {PureComponent} from 'react';
import {
  Alert, AsyncStorage,
  Image,
  Linking,
  NativeModules,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import {connect} from "react-redux";
import {AnalyticsSource, getFormattedCurrency, Routes, SnackBarTime, Styles} from "../util/Utility";
import SmallcaseButton from "../component/SmallcaseButton";
import Divider from "../component/Divider";
import {Actions} from "react-native-router-flux"
import {ADD_FUNDS_URL, EM_DASH} from "../constants/Constants";
import {getUserDetails, getUserNotifications} from "./AccountService";
import {getUserFunds} from "../user/UserService";
import Intercom from 'react-native-intercom';
import {logOutUser} from "../user/UserService";
import {userLoggedIn} from "../onboarding/LandingActions";
import {getJwt} from "../helper/LoginHelper";
import * as types from "../constants/Constants";
import {BROKER_LOGIN} from "../api/ApiRoutes";
import * as Config from "../../Config";
import AnalyticsManager from "../analytics/AnalyticsManager";
import {updateNotificationStatus} from "../user/notification/NotificationAction";
import {SnackBarMessages} from "../util/Strings";

const FAQ_URL = "https://zerodha-help.smallcase.com/smallcase-for-ios";

class AccountComponent extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      userName: "",
      userId: "",
      funds: EM_DASH,
      unreadCount: 0,
      refreshing: false,
      userAvatarUrl: null
    };

    this.fetchData = this.fetchData.bind(this);
  }

  componentWillReceiveProps = (newProps) => {
    if (newProps.loginReducer && (newProps.loginReducer.requestToken !== this.props.loginReducer.requestToken)) {
      this.fetchAuthDetails(newProps);
    }
  }

  componentDidMount() {
    this.fetchData();
    AnalyticsManager.track('Viewed Account', null, [AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])
  }

  fetchAuthDetails = (newProps) => {
    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {});
        this.fetchData();
      } else {
        this.snackbar.show({
          title: SnackBarMessages.LOGIN_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    });
  }

  fetchData() {
    const {jwt, csrf} = this.props;

    getUserDetails(jwt, csrf)
      .then((response) => {
        this.setState({userName: response.userName, userId: response.userId, userAvatarUrl: response.avatarUrl});
      })
      .catch((error) => console.log(error));

    getUserFunds(jwt, csrf)
      .then((funds) => this.setState({funds, refreshing: false}))
      .catch((error) => console.log(error));

    getUserNotifications(jwt, csrf)
      .then((notifications) => {
        let unreadCount = 0;
        notifications.forEach((notif) => {
          if (notif.read === false) {
            unreadCount++;
          }
        });
        this.setState({unreadCount: unreadCount});
        this.props.dispatch(updateNotificationStatus(unreadCount));
      })
      .catch((error) => console.log(error));
  };

  static onEnter() {
    const accountInstance = Actions.refs[Routes.ACCOUNT].getWrappedInstance();
    accountInstance.fetchData();
  };

  openNotifications = () => {
    Actions.push(Routes.NOTIFICATIONS);
  };

  openOrders = () => {
    Actions.push(Routes.ALL_ORDERS,
      {
        analytics: {
          accessedFrom :'Orders',
          sectionTag : 'N/A',

        }
      });
  };

  openFees = () => {
    Actions.push(Routes.FEES ,{
      analytics: {
        accessedFrom :'Orders',
        sectionTag : 'N/A',
      }
    });
  };

  openFAQ = () => {
    Linking.canOpenURL(FAQ_URL).then(supported => {
      if (supported) {
        Linking.openURL(FAQ_URL);
        AnalyticsManager.track('Viewed FAQs',{accessedFrom:'Account'},[AnalyticsSource.CLEVER_TAP,AnalyticsSource.INTERCOM,AnalyticsSource.MIXPANEL])
      } else {
        console.log("Couldn't open url");
      }
    });
  };

  openChat = () => {
    AnalyticsManager.track('Opened Chat',null,[AnalyticsSource.CLEVER_TAP,AnalyticsSource.INTERCOM,AnalyticsSource.MIXPANEL])
    Intercom.displayMessageComposer();
  };

  logout = () => {
    const {jwt, csrf} = this.props;
    Alert.alert('', 'Are you sure you want to logout?',
      [
        {text: 'Stay'},
        {
          text: 'Leave', onPress: () => {
            logOutUser(jwt, csrf)
          }
        }
      ],
      {cancelable: false}
    );
  };

  about = () => {
    Actions.push(Routes.ABOUT);
  };


  addFundsOrLogin = () => {
    if (this.state.funds === EM_DASH) {
      Actions.push(Routes.LOGIN);
      return;
    }
    AnalyticsManager.track('Proceeded To Adding Funds',
      {
        accessedFrom:"Account",
        sectionTag: 'N/A',
        amountConfirmed: 'N/A',
        minInvestAmount: 'N/A',
        smallcaseSource: 'N/A',
        smallcaseType: 'N/A',
        smallcaseName: 'N/A',
      },
      [AnalyticsSource.MIXPANEL,AnalyticsSource.CLEVER_TAP]);
    Linking.canOpenURL(ADD_FUNDS_URL).then(supported => {
      if (supported) {
        Linking.openURL(ADD_FUNDS_URL);
      } else {
        console.log("Couldn't open url");
      }
    });
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    this.fetchData();
  }

  render() {
    const {userName, userId, funds, unreadCount, refreshing, userAvatarUrl} = this.state;

    return (
      <ScrollView
        style={{flex: 1}}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={this.onRefresh}
          />
        }
      >
        <View style={styles.container}>
          <View style={styles.userContainer}>
            {userAvatarUrl ?
              <Image style={styles.userAvatar} source={{uri: userAvatarUrl}}/> :
              <View style={styles.imagePlaceholder}>
                <Text style={styles.placeholderText}>{userName ? userName.charAt(0).toUpperCase() : ''}</Text>
              </View>
            }
            <View style={styles.userDetails}>
              <Text style={styles.username}>{userName.capitalize(" ")}</Text>
              <Text style={styles.kite}>Kite ID: {userId}</Text>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View>
                  <Text style={styles.funds}>Funds</Text>
                  <Text style={styles.amount}>{getFormattedCurrency(funds)}</Text>
                </View>
                <SmallcaseButton buttonStyles={styles.button} onClick={this.addFundsOrLogin}>
                  <Text style={styles.buttonText}>{funds === EM_DASH? 'Login to Refresh' : 'Add Funds'}</Text>
                </SmallcaseButton>
              </View>
            </View>
          </View>
          <View style={styles.activityContainer}>
            <Text style={styles.title}>Activity</Text>
            <Row text={"Notifications"} number={unreadCount} iconsource={require('../../assets/notification.png')}
                 color={{tintColor: 'rgb(31, 122, 224)'}} onClick={this.openNotifications}/>
            <Row text={"Orders"} iconsource={require('../../assets/orders.png')}
                 color={{tintColor: 'rgb(31, 122, 224)'}} onClick={this.openOrders}/>
            <Row text={"Fees"} iconsource={require('../../assets/fees.png')}
                 color={{tintColor: 'rgb(31, 122, 224)'}} onClick={this.openFees} isLast={true}/>
          </View>
          <View style={styles.supportContainer}>
            <Text style={styles.title}>Support</Text>
            <Row text={"FAQs"} iconsource={require('../../assets/help.png')} color={{tintColor: '#535B62'}}
                 onClick={this.openFAQ}/>
            <Row text={"Chat with Support"} iconsource={require('../../assets/chat.png')}
                 color={{tintColor: '#535B62'}} onClick={this.openChat} isLast={true}/>
          </View>
          <View style={styles.otherContainer}>
            <Text style={styles.title}>Other</Text>
            <Row text={"About"} iconsource={require('../../assets/info.png')}
                 color={{tintColor: '#535B62'}} onClick={this.about}/>
            <Row text={"Log Out"} iconsource={require('../../assets/exit.png')}
                 color={{tintColor: '#D82F44'}} onClick={this.logout}/>
          </View>
        </View>
      </ScrollView>
    );
  }
}

class Row extends PureComponent {
  render() {

    const {iconsource, text, number, color, onClick, isLast} = this.props;

    let numberView = (
      <View style={styles.number}>
        <Text style={styles.numberText}>{number}</Text>
      </View>
    );

    let view = number > 0 ? numberView : null;

    return (
      <TouchableHighlight onPress={onClick}>
        <View style={{flexDirection: 'column', paddingTop: 18, backgroundColor: 'white'}}>
          <View style={styles.row}>
            <Image source={iconsource} style={[styles.icon, color]}/>
            <Text style={styles.rowText}>{text}</Text>
            {view}
            <Image source={require('../../assets/chevronLeft.png')} style={styles.rightIcon}/>
          </View>
          <Divider dividerStyles={{marginTop: 12, marginHorizontal: isLast ? -16 : 0}}/>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: "white",
    flex: 1
  },
  userContainer: {
    flexDirection: 'row',
    padding: 16,
    backgroundColor: 'rgb(249, 250, 252)',
    height: 144
  },
  activityContainer: {
    flexDirection: 'column',
    paddingLeft: 16,
  },
  supportContainer: {
    flexDirection: 'column',
    paddingLeft: 16,
  },
  otherContainer: {
    flexDirection: 'column',
    paddingLeft: 16,
  },
  userDetails: {
    flex: 1,
    flexDirection: 'column',
  },
  kite: {
    color: 'rgb(83, 91, 98)',
    fontSize: 14,
    marginBottom: 24,
  },
  username: {
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 8,
  },
  funds: {
    color: 'rgb(129, 135, 140)',
    fontSize: 14,
    marginBottom: 8,
  },
  amount: {
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 8,
  },
  userImage: {
    height: 48,
    width: 48,
    marginRight: 16,
  },
  imagePlaceholder: {
    height: 48,
    width: 48,
    borderRadius: 100,
    backgroundColor: Styles.BLUE,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 16,
  },
  placeholderText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  userAvatar: {
    width: 48,
    height: 48,
    borderRadius: 24,
    marginRight: 16
  },
  button: {
    flex: 1,
    height: 36,
    paddingHorizontal: 16,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: Styles.BLUE_ALPHA,
    backgroundColor: 'white',
    marginRight: 8,
    marginTop: 8,
    alignSelf: 'flex-end'
  },
  buttonText: {
    color: Styles.BLUE,
    fontWeight: 'bold',
    fontSize: 13,
  },
  title: {
    color: 'rgb(47, 54, 63)',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 24,
    marginBottom: 6,
  },
  row: {
    flexDirection: 'row',
  },
  number: {
    height: 20,
    width: 20,
    borderRadius: 100,
    backgroundColor: 'rgb(255, 89, 66)',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 24,
  },
  icon: {
    height: 24,
    width: 24,
    marginRight: 10,
  },
  rightIcon: {
    marginRight: 16,
    height: 18,
    width: 18,
    transform: [{rotate: '180deg'}],
    tintColor: '#81878C'
  },
  rowText: {
    flex: 1,
    fontSize: 16,
    color: 'rgb(47, 54, 63)'
  },
  numberText: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold'
  },

});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  unreadCount : state,
});

module.exports = connect(mapStateToProps, null, null, {withRef: true})(AccountComponent);

import React, {PureComponent} from 'react';
import {ActivityIndicator, FlatList, Image, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {getImageURL, getUserNotifications, markNotificationRead, markNotificationsReadAll} from "./AccountService";
import {connect} from "react-redux";
import Divider from "../component/Divider"
import {
  AnalyticsSource,
  getBottomBarPaddingBottom,
  getBottomBarPaddingTop,
  getSmallcaseImage,
  getTimeAgoDate,
  Routes
} from "../util/Utility";
import {Actions} from "react-native-router-flux";
import SmallcaseImage from "../component/SmallcaseImage"
import {logOutUser} from "../user/UserService";
import AnalyticsManager from "../analytics/AnalyticsManager";
import ErrorState from "../component/ErrorState";
import Loader from "../component/Loader";

const Types = {
  ORDERUPDATE: 'ORDERUPDATE',
  REBALANCEUPDATE: 'REBALANCEUPDATE',
  PLAYEDOUTUPDATE: 'PLAYEDOUTUPDATE',
  SIPUPDATE: 'SIPUPDATE',
  ALERT: "ALERT",
};

class Notifications extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      empty: true,
      notifications: [],
      isFetching: true,
      hasError: false,
      unreadCount: 0,
    };

    this.refresh = this.refresh.bind(this);
  }

  componentDidMount() {

    AnalyticsManager.track('Viewed Notifications',
      {
        accessedFrom: 'Account',
        sectionTag: 'N/A',
      },
      [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]
    )

    this.refresh();
  }

  refresh() {
    const {jwt, csrf} = this.props;

    getUserNotifications(jwt, csrf)
      .then((notifications) => {
        if (notifications.length > 0) {
          this.setState({notifications: notifications, empty: false, isFetching: false,hasError: false});
          this.setRightButton(notifications, jwt, csrf);
        } else {
          this.setState({empty: true, isFetching: false,hasError: false});
        }

      })
      .catch((error) => {
        if (error.data && error.data.status === 401) {
          logOutUser(jwt, csrf);
          return;
        }

        this.setState({hasError: true, isFetching: false});

      })
  }

  static onEnter() {
    const notificationsInstance = Actions.refs[Routes.NOTIFICATIONS].getWrappedInstance();
    notificationsInstance.refresh();
  };

  setRightButton = (notifications, jwt, csrf) => {
    let unreadCount = 0;
    notifications.forEach((notification) => {
      if (notification.read === false) {
        unreadCount++;
      }
    });

    this.setState({unreadCount: unreadCount});
    if (unreadCount > 0) {
      Actions.refresh({rightTitle: "Read All", onRight: () => this.readAll(jwt, csrf)})
    }


  };

  readAll = (jwt, csrf) => {
    this.setState({unreadCount: 0});


    markNotificationsReadAll(jwt, csrf)
      .then((response) => {
        Actions.refresh({rightTitle: null, onRight: null});
        this.refresh();
      })
      .catch((error) => console.log(error));
  };

  handlePress = (item, index) => {
    let {jwt, csrf} = this.props;
    let {notifications} = this.state;

    switch (item.type) {
      case Types.ORDERUPDATE :
        const smallcase = {
          name: item.header,
          source: item.source,
          iscid: item.iscid,
          scid: item.scid,
          smallcaseImage: getSmallcaseImage(item.scid, item.source, '80')
        };
        Actions.push(Routes.ORDER_BATCHES, {smallcase});
        break;

      case Types.SIPUPDATE :
        Actions.replace(Routes.INVESTMENTS);
        break;

      case Types.REBALANCEUPDATE :
        Actions.replace(Routes.INVESTMENTS);
        break;

      case Types.PLAYEDOUTUPDATE :
        break;

      case Types.ALERT :
        // do nothing
        break;
    }
    if (!item.read) {
      markNotificationRead(jwt, csrf, item._id)
        .then((response) => {
          let newItem = Object.assign({}, item);
          newItem.read = true;
          notifications[index] = newItem;
          this.setState({notifications: notifications});
        })
        .catch((error) => console.log(error));
      this.setState({unreadCount: this.state.unreadCount - 1})


    }
  };

  renderItem = ({item, index}) => {

    let imageView = null;
    switch (item.type) {
      case Types.ORDERUPDATE :
        imageView = (
          <SmallcaseImage imageUrl={getImageURL(item.scid)} source={item.source} imageStyles={styles.image} textSize={5} />);
        break;
      case Types.REBALANCEUPDATE :
        imageView = (<Image source={require('../../assets/rebalance.png')} style={styles.image}/>);
        break;
      case Types.SIPUPDATE :
        imageView = (<Image source={require('../../assets/sipSmall.png')} style={styles.image}/>);
        break;
      case Types.PLAYEDOUTUPDATE :
        imageView = (<Image source={require('../../assets/playedOut.png')} style={styles.image}/>);
        break;
      case Types.ALERT :
        imageView = (<Image source={require('../../assets/info.png')} style={styles.image}/>);
        break;
    }

    let color = (item.read === true ? {color: 'rgba(129, 135, 140, 0.7)'} : {color: 'rgb(31, 122, 224)'});
    let weightStyle = (item.read === true ? null : {fontWeight: 'bold'});

    return (
      <TouchableHighlight onPress={() => this.handlePress(item, index)}>
        <View style={{backgroundColor: 'white'}}>
          <View style={styles.listItem}>
            {imageView}
            <View style={{flexDirection: 'column', flex: 1}}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', flex: 1}}>
                <Text style={[styles.header, weightStyle]} numberOfLines={1}>{item.header}</Text>
                <Text style={[styles.time, color]}>{getTimeAgoDate(item.time)}</Text>
              </View>
              <Text style={styles.text}>{item.text}</Text>
            </View>
          </View>
          <Divider/>
        </View>
      </TouchableHighlight>
    );
  };

  render() {

    let {empty, notifications, isFetching,hasError} = this.state;

    if (isFetching) return (<Loader loaderStyles={{marginTop: 64}} />)
    if (hasError) return (<ErrorState refresh={this.refresh}/>);
    let listView = (
      <FlatList
        style={styles.list}
        data={notifications}
        renderItem={this.renderItem}
        keyExtractor={(item) => item._id}
        extraData={this.state}
      />
    );

    let emptyView = (
      <View style={styles.empty}>
        <Image source={require("../../assets/notificationsEmptyState.png")} style={styles.emptyImage}/>
        <Text style={styles.emptyText}>You don't have any notifications yet</Text>
      </View>
    );

    let view = empty ? emptyView : listView;

    return (
      <View style={styles.container}>
        {view}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
    paddingBottom: getBottomBarPaddingBottom()
  },
  list: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 8,
    paddingLeft: 16,
  },
  listItem: {
    flexDirection: 'row',
    paddingTop: 16,
    paddingBottom: 16,
    height: 64
  },
  image: {
    height: 32,
    width: 32,
    marginRight: 16,
  },
  header: {
    fontSize: 14,
    color: 'rgb(47, 54, 63)',
    marginBottom: 2,
    flex: 1,
    marginRight: 16
  },
  text: {
    fontSize: 12,
    color: 'rgb(129, 135, 140)',
  },
  time: {
    fontSize: 11,
    marginRight: 16,
  },
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'white'
  },
  emptyText: {
    fontSize: 16,
    color: 'rgb(129, 135, 140)',
  },
  emptyImage: {
    height: 186,
    width: 265,
  },

});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps, null, null, {withRef: true})(Notifications);
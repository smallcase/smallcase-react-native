import React, {PureComponent} from 'react';
import {ActivityIndicator, FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {getImageURL, getUserFees} from "./AccountService";
import Divider from "../component/Divider";
import {connect} from "react-redux";
import SmallcaseImage from "../component/SmallcaseImage";
import {AnalyticsSource, getFormattedCurrency, getReadableDate, OrderLabel} from "../util/Utility";
import {logOutUser} from "../user/UserService";
import AnalyticsManager from "../analytics/AnalyticsManager";
import ErrorState from "../component/ErrorState";
import Loader from "../component/Loader";

class Fees extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      smallcaseLedger: [],
      empty: true,
      isFetching: true,
      hasError: false,
    }

  }

  componentDidMount() {

    let {jwt, csrf} = this.props;

    getUserFees(jwt, csrf)
      .then(response => {
        if (response.data.smallcaseLedger.length > 0) {
          this.setState({smallcaseLedger: response.data.smallcaseLedger, empty: false, isFetching: false});
        } else {
          this.setState({empty: true, isFetching: false});
        }
      })
      .catch((error) => {
        if (error.data && error.data.status === 401) {
          logOutUser(jwt, csrf);
          return;
        }
        this.setState({hasError: true, isFetching: false});
      })
    AnalyticsManager.track('Viewed Fees', this.props.analytics, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
  }

  getPaymentPrefix = (orderLabel) => {
    if (!orderLabel) return "Bought on";

    switch (orderLabel) {
      case OrderLabel.BUY:
        return "Bought on";

      case OrderLabel.INVESTMORE:
        return "Invested more on";

      case OrderLabel.MANAGE:
        return "Managed on";

      case OrderLabel.REBALANCE:
        return "Rebalanced on";

      case OrderLabel.SIP:
        return "SIP on";

      default:
        return "Charged on";
    }
  }

  renderItem = ({item, index}) => {
    return (
      <View>
        <View style={styles.listItem}>
          <SmallcaseImage imageUrl={getImageURL(item.scid)} source={item.source} imageStyles={styles.image}/>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{item.name}</Text>
            <Text style={styles.date}>{this.getPaymentPrefix(item.label)} {getReadableDate(item.completedDate)}</Text>
          </View>
          <View style={styles.feesContainer}>
            <Text style={styles.fees}>Fees</Text>
            <Text style={styles.amount}>{getFormattedCurrency(item.amount)}</Text>
          </View>
        </View>
        <Divider/>
      </View>
    );
  };

  render() {
    let {smallcaseLedger, empty, isFetching, hasError} = this.state;

    if (isFetching) return (<Loader loaderStyles={{marginTop: 64}}/>)
    if (hasError) return (<ErrorState refresh={getUserFees}/>);
    let listView = (
      <FlatList
        style={styles.list}
        data={smallcaseLedger}
        renderItem={this.renderItem}
        keyExtractor={(item, index) => index}/>
    );

    let emptyView = (
      <View style={styles.empty}>
        <Image source={require("../../assets/feesEmptyState.png")} style={styles.emptyImage}/>
        <Text style={styles.emptyTitle}>Fees for your smallcase appears here</Text>
        <Text style={styles.emptyText}>A convenience fees is applied each time you buy a smallcase. Fees are updated
          here at the end of the buy day.</Text>
      </View>
    );

    let view = empty ? emptyView : listView;

    return (
      <View style={styles.container}>
        {view}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  list: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 8,
    paddingLeft: 16,
    backgroundColor: 'white'
  },
  listItem: {
    flexDirection: 'row',
    paddingTop: 24,
    paddingBottom: 24,
    height: 96
  },
  titleContainer: {
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
    marginRight: 16
  },
  feesContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    marginRight: 16,
    justifyContent: 'center'
  },
  image: {
    height: 48,
    width: 48,
    marginRight: 8,
  },
  title: {
    fontSize: 16,
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
  },
  date: {
    color: 'rgb(129, 135, 140)',
    fontSize: 13,
    marginTop: 4
  },
  fees: {
    color: 'rgb(129, 135, 140)',
    fontSize: 13,
  },
  amount: {
    fontSize: 18,
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
  },
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'white'
  },
  emptyImage: {
    height: 200,
    width: 360,
    marginBottom: 16,
  },
  emptyTitle: {
    color: 'rgb(47, 54, 63)',
    fontSize: 16,
    lineHeight: 24,
    marginBottom: 8,
    textAlign: 'center'
  },
  emptyText: {
    fontSize: 14,
    lineHeight: 20,
    color: 'rgb(129, 135, 140)',
    marginHorizontal: 24,
    textAlign: 'center'
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
});

module.exports = connect(mapStateToProps)(Fees);
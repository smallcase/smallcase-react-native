import {connect} from 'react-redux';
import LandingComponent from './LandingComponent';

const mapStateToProps = (state) => ({...state});

const mapDispatchToProps = (dispatch) => ({dispatch});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LandingComponent);
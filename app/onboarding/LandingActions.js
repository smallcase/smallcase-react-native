
import * as types from '../constants/Constants';
import {
  BROKER_LOGIN,
  CREATE_ACCOUNT,
} from '../api/ApiRoutes';
import { getJwt } from '../helper/LoginHelper';
import Api from "../api/Api";
import * as Config from "../../Config";

const handleAccountRequestResponse = (data, cb) => {
  if (!data) {
    cb(null);
    return;
  }

  if (data.redirect) {
    cb({
      redirect: data.redirect,
      title: 'Thank you for your information',
      description: 'You can now continue to open up your account on Zerodha by setting up a password. Do keep your PAN number handy to get started',
      button: 'CONTINUE TO ZERODHA',
      emoji: '🙌',
    });
    return;
  }

  switch (data.status) {
    case 'EXISTINGCLIENT':
      cb({
        title: 'Looks like you already have a Zerodha account',
        description: 'You can log in to smallcase with your Zerodha Kite credentials',
      });
      return;

    case "ALREADYREGISTERED":
    case "DUPLICATEPRIORITY":
    case "DUPLICATELEAD":
      cb({
        redirect: 'https://zerodha.com/account/',
        title: 'Looks like you have already signed up for a Zerodha account earlier.',
        description: 'In case you don\'t remember your password to check status, click the \'Forgot password\' link on the Application Status page to receive the same in your email',
        button: 'CHECK APPLICATION STATUS',
        emoji: '🙇',
      });
      return;

    default:
      cb(null);
      return;
  }
}

export const loginWithLeprechaun = (leprechaunToken, cb) => {
  const body = {broker: 'kite-leprechaun', reqToken: leprechaunToken, app: 'platform'};

  getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, cb);
  return {type: types.LOGIN_LEPRECHAUN};
};

export const userLoggedIn = (jwt, csrf) => {
  return {type: types.USER_LOGGED_IN, jwt, csrf};
};

export const createAccount = (name, email, phone, heardFrom, cb) => {
  const body = {name, phone, email, channel: heardFrom};

  Api.post({ route: CREATE_ACCOUNT, body })
    .then((response) => {
      handleAccountRequestResponse(response.data, cb);
    })
    .catch((error) => {
      handleAccountRequestResponse(error.data, cb);
    });

  return {type: types.CREATE_ACCOUNT, name, email, phone, heardFrom};
}

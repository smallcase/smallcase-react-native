import {CREATE_ACCOUNT, LOGIN_LEPRECHAUN, USER_LOGGED_IN} from '../constants/Constants';

const landingReducer = (state = {}, action) => {
  switch (action.type) {
    case CREATE_ACCOUNT:
      return {
        ...state,
      }

    case LOGIN_LEPRECHAUN:
      return {
        ...state,
      }

    case USER_LOGGED_IN:
      return {
        ...state,
        jwt: action.jwt,
        csrf: action.csrf,
        notificationRead: true
      }

    default:
      return state
  }
}

export default landingReducer
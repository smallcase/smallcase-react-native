import React, {Component} from 'react';
import {
  View,
  TextInput,
  Text,
  AsyncStorage,
  Alert
} from 'react-native';
import SmallcaseButton from "../component/SmallcaseButton";
import * as types from "../constants/Constants";
import {loginWithLeprechaun, userLoggedIn} from "./LandingActions";
import {Actions} from "react-native-router-flux";
import {Routes, Styles} from "../util/Utility";
import {connect} from "react-redux";

class LoginWithLeprechaunComponent extends Component {

  constructor() {
    super();
    this.state = {
      leprechaun: ''
    };
  }

  loginWithLeprechaun = () => {
    this.props.dispatch(loginWithLeprechaun(this.state.leprechaun, (jwt, csrf) => {
      if (!jwt || !csrf) {
        Alert.alert('Error!', 'Failed to login', [{text: 'OK'}], {cancelable: false});
        return;
      }
      AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {});
      this.props.dispatch(userLoggedIn(jwt, csrf));
      if (jwt) Actions.replace(Routes.INVESTMENTS, {jwt, csrf});
    }));
  }

  render() {
    return (
      <View style={{margin: 36, justifyContent: 'center', alignItems: 'center'}}>
        <TextInput style={{height: 45, width: 200, borderWidth: 1, borderRadius: 4, borderColor: Styles.DESCRIPTION_TEXT_COLOR, marginTop: 64}}
                   onChangeText={(leprechaun) => this.setState({leprechaun})} />
        <SmallcaseButton buttonStyles={{width: 160, marginTop: 24}} onClick={this.loginWithLeprechaun}><Text>Confirm</Text></SmallcaseButton>
      </View>
    );
  }
}

module.exports = connect()(LoginWithLeprechaunComponent);
import {SET_REQUEST_TOKEN} from "../../constants/Constants";

const loginReducer = (state = {}, action) => {
  switch (action.type) {
    case SET_REQUEST_TOKEN:
      return {
        ...state,
        requestToken: action.requestToken
      }

    default:
      return state
  }
}

export default loginReducer
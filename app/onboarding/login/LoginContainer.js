import { connect } from 'react-redux';
import LoginComponent from './LoginComponent';

const mapStateToProps = (state) => ({ ...state });

const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginComponent);
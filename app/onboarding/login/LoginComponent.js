import React, {Component} from 'react';
import {
  WebView,
  ActivityIndicator,
  View
} from 'react-native';
import {addRequestToken} from "./LoginActions";
import {Actions} from 'react-native-router-flux';
import * as Config from "../../../Config";
import Loader from "../../component/Loader";

const URL = require("url-parse");
const kiteUrl = 'https://kite.zerodha.com/connect/login?api_key=' + Config.KITE_TOKEN;

export default class LoginComponent extends Component {

  constructor(props) {
    super();
    this.state= {
      showLoader: true
    }
  }

  isSmallCaseUrl = (url) => new URL(url, '', true).hostname.includes('smallcase');

  extractRequestToken = (url) => new URL(url, '', true).query.request_token;

  interceptLoadRequest = (value) => {
    if (this.isSmallCaseUrl(value.url)) {
      const reqToken = this.extractRequestToken(value.url);
      if (reqToken) this.props.dispatch(addRequestToken(reqToken));
      Actions.pop();
      return false;
    }
    return true;
  }

  hideLoader = () => this.setState({ showLoader: false });

  render() {
    let loader = null;
    if (this.state.showLoader) {
      loader = (<Loader loaderStyles={{ marginTop: 64 }} />);
    }

    return (
      <View style={{flex: 1}}>
        {loader}
        <WebView
          style={{flex: 1, backgroundColor: 'transparent'}}
          source={{uri: kiteUrl}}
          onLoadEnd={this.hideLoader}
          onShouldStartLoadWithRequest={this.interceptLoadRequest}
        />
      </View>
    );
  }
}
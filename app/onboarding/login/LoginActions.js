import { SET_REQUEST_TOKEN } from '../../constants/Constants';

export const addRequestToken = (requestToken) => {
  return {type: SET_REQUEST_TOKEN, requestToken};
}
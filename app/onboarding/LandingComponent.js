import React, {Component} from 'react';
import {AsyncStorage, Dimensions, StyleSheet, View,Platform,Linking} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {userLoggedIn} from './LandingActions';
import PropTypes from 'prop-types';
import {AUTH_INFO} from "../constants/Constants";
import {TabViewAnimated, TabViewPagerScroll} from "react-native-tab-view";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import {AnalyticsSource, getScreenSize, Routes} from "../util/Utility";
import {getUserDetails} from "../user/UserActions";
import AnalyticsManager from "../analytics/AnalyticsManager";

const Screen = getScreenSize();

export default class Landing extends Component {

  constructor(props) {
    super();
    this.state = {
      isLogin: true,
      index: 0,
      routes: [{key: '1'}, {key: '2'}],
    }

    AsyncStorage.getItem(AUTH_INFO, (error, result) => {
      const loginObject = JSON.parse(result);
      if (result) {
        props.dispatch(getUserDetails(loginObject.jwt, loginObject.csrf));
        props.dispatch(userLoggedIn(loginObject.jwt, loginObject.csrf));
        Actions.replace(Routes.ROOT, {jwt: loginObject.jwt, csrf: loginObject.csrf});
      }
    });
  }

  handleIndexChange = (index) => this.setState({index});

  renderScene = ({route}) => {
    let props = {
      ...this.props,
      toggle: this.toggle,
      dispatch: this.props.dispatch,
    }
    switch (route.key) {
      case '1':
        return (<Login {...props} />);

      case '2':
        return (<SignUp {...props} />);

      default:
        return null;
    }
  }

  toggle = () => {
    if (this.state.index) {

    } else {
      AnalyticsManager.track('Clicked SignUp', null, [AnalyticsSource.MIXPANEL,AnalyticsSource.CLEVER_TAP]);

    }

    this.setState({index: this.state.index === 0 ? 1 : 0});
  }

  renderHeader = () => (<View/>);

  renderPager = props => (
      <TabViewPagerScroll
          {...props}
          swipeEnabled={false}
      />
  );

  componentDidMount() {

      if (Platform.OS === 'android') {
          Linking.getInitialURL().then(url => {
              //this.navigate(url);
          });
      } else {
          Linking.addEventListener('url', this.handleOpenURL);
      }

  }
    componentWillUnmount() {
        Linking.removeEventListener('url', this.handleOpenURL);
    }

    handleOpenURL = (event) => {
        //this.navigate(event.url);
    }

    navigate = (url) => {
        // const { navigate } = this.props.navigation;
        //
        // const route = url.replace(/.*?:\/\//g, '');
        // const id = route.match(/\/([^\/]+)\/?$/)[1];
        // const routeName = route.split('/')[0];
        // if (routeName === 'people') {
        //     navigate('People', { id, name: 'chris' })
        // }
    }

  render = () => (
      <TabViewAnimated
          style={styles.sectionHeader}
          navigationState={this.state}
          renderScene={this.renderScene}
          renderHeader={this.renderHeader}
          onIndexChange={this.handleIndexChange}
          initialLayout={Screen}
          renderPager={this.renderPager}
          useNativeDriver
      />
  );
}

Landing.propTypes = {
  dispatch: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
  sectionHeader: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: 'white'
  },
});
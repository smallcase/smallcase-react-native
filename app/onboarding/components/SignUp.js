import React, {Component} from "react";
import {
  ActionSheetIOS,
  ActivityIndicator,
  Image,
  Keyboard,
  Linking,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View
} from "react-native";
import {AnalyticsSource, SnackBarTime, Styles} from "../../util/Utility";
import {createAccount} from "../LandingActions";
import SmallcaseButton from "../../component/SmallcaseButton";
import SnackBar from "../../component/SnackBar";
import {IPAD_PRO_SMALLEST, IPHONE_FIVE_AND_SMALLER} from "../../constants/Constants";
import {MediaQueryStyleSheet} from "react-native-responsive";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";

const NAME_ERROR = 'name_error';
const EMAIL_ERROR = 'email_error';
const PHONE_ERROR = 'phone_error';
const NO_ERROR = 'no_error';
const heardFrom = ['Social Media', 'News/Reviews/Investing Blogs', 'Zerodha/Kite Mailers', 'Friends', 'Others'];

export default class SignUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      signUpFormError: NO_ERROR,
      signUpRequestProcessing: false,
      signUpResponse: null,
      heardFrom: 'Eg: Friends',
      focus: ''
    }
  }

  createAccountClicked = (dispatch) => {
    let {name, email, phone, heardFrom} = this.state;
    if (this.isInvalidName(name)) {
      this.setState({signUpFormError: NAME_ERROR});
      return;
    }

    if (this.isInvalidEmail(email)) {
      this.setState({signUpFormError: EMAIL_ERROR});
      return;
    }

    if (this.isInvalidPhoneNumber(phone)) {
      this.setState({signUpFormError: PHONE_ERROR});
      return;
    }

    AnalyticsManager.track('Submitted Sign Up Form', null, [AnalyticsSource.MIXPANEL,AnalyticsSource.CLEVER_TAP]);
    this.setState({signUpRequestProcessing: true, signUpFormError: NO_ERROR});
    dispatch(createAccount(name, email, phone, heardFrom, (response) => {
      this.setState({signUpRequestProcessing: false, signUpResponse: response});
      if (!response) {
        this.snackbar.show({
          title: SnackBarMessages.SIGN_UP_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    }));
  }

  isInvalidName = (name) => {
    return !name || !(/^[a-zA-Z\s]+$/.test(name));
  };

  isInvalidEmail = (email) => {
    return !email || !(/[\w\.-]+[@][\w]+[\.][\w]+$/.test(email));
  };

  isInvalidPhoneNumber = (phone) => {
    return !phone || phone.length !== 10 || !(/^\d+$/.test(phone))
  }

  showHeardFrom = () => {
    ActionSheetIOS.showActionSheetWithOptions({options: heardFrom}, (index) => {
      this.setState({heardFrom: heardFrom[index]});
    });
  }

  redirectUser = () => {
    const url = this.state.signUpResponse.redirect;
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  setCurrentFocus = (focus) => {
    this.setState({focus});
  }

  removeFocus = () => {
    Keyboard.dismiss();
    this.setCurrentFocus('');
  }

  render() {
    let {heardFrom, signUpResponse, signUpRequestProcessing, signUpFormError, email, name, phone, focus} = this.state;
    let {dispatch, toggle} = this.props;
    let signUpButtonContent = signUpRequestProcessing ? (
      <Loader color='white'/>
    ) : (<Text style={styles.signUp}>Sign Up for Zerodha</Text>);
    let signUpForm = (
      <TouchableWithoutFeedback onPress={this.removeFocus}>
        <View>
          <Text style={styles.note}>Note: We are currently live with Zerodha trading & demat account. Open an account below</Text>
          <Text style={[styles.inputHeaders, {marginTop: 22}]}>Full Name</Text>
          <TextInput
            style={[styles.signUpField, {borderColor: focus === 'name' ? 'rgba(31, 122, 224, 0.5)' : 'rgba(83, 91, 98, 0.25)'}]}
            placeholder='Eg: Ashok Kumar'
            returnKeyType='next'
            blurOnSubmit={false}
            editable={!signUpRequestProcessing}
            onSubmitEditing={(event) => this.refs.email.focus()}
            onFocus={this.setCurrentFocus.bind(this, 'name')}
            placeholderTextColor='rgb(129, 135, 140)'
            onChangeText={(name) => this.setState({name})}/>
          {signUpFormError === NAME_ERROR ? (<Text style={styles.errorText}>Invalid name</Text>) : null}
          <Text style={styles.inputHeaders}>Email</Text>
          <TextInput
            ref='email'
            style={[styles.signUpField, {borderColor: focus === 'email' ? 'rgba(31, 122, 224, 0.5)' : 'rgba(83, 91, 98, 0.25)'}]}
            keyboardType='email-address'
            onSubmitEditing={(event) => this.refs.phone.focus()}
            returnKeyType='next'
            autoCapitalize='none'
            blurOnSubmit={false}
            editable={!signUpRequestProcessing}
            onFocus={this.setCurrentFocus.bind(this, 'email')}
            placeholder='Eg: example@gmail.com'
            placeholderTextColor='rgb(129, 135, 140)'
            onChangeText={(email) => this.setState({email})}/>
          {signUpFormError === EMAIL_ERROR ? (<Text style={styles.errorText}>Invalid email</Text>) : null}
          <Text style={styles.inputHeaders}>Mobile Number</Text>
          <TextInput
            ref='phone'
            style={[styles.signUpField, {borderColor: focus === 'phone' ? 'rgba(31, 122, 224, 0.5)' : 'rgba(83, 91, 98, 0.25)'}]}
            keyboardType='number-pad'
            returnKeyType='done'
            blurOnSubmit={true}
            maxLength={10}
            editable={!signUpRequestProcessing}
            onFocus={this.setCurrentFocus.bind(this, 'phone')}
            placeholderTextColor='rgb(129, 135, 140)'
            placeholder='Phone'
            onChangeText={(phone) => this.setState({phone})}/>
          {signUpFormError === PHONE_ERROR ? (<Text style={styles.errorText}>Invalid phone number</Text>) : null}
          <Text style={styles.inputHeaders}>How did you hear about us?</Text>
          <TouchableWithoutFeedback disabled={signUpRequestProcessing} onPress={this.showHeardFrom.bind(this)}>
            <View style={styles.heardFromContainer}>
              <Text
                style={[styles.heardFromText, {color: heardFrom === 'Eg: Friends' ? 'rgb(129, 135, 140)' : 'rgb(83, 91, 98)'}]}>{heardFrom}</Text>
              <Image style={styles.heardFromArrow} source={require('../../../assets/iconArrowDownGrey.png')}/>
            </View>
          </TouchableWithoutFeedback>
          <SmallcaseButton disabled={signUpRequestProcessing || name === '' || email === '' || phone === ''}
                           onClick={this.createAccountClicked.bind(this, dispatch)} buttonStyles={[styles.kiteLoginButton, {
            backgroundColor: 'rgb(31, 122, 224)',
            marginHorizontal: 0,
            marginTop: 24,
          }]}>
            {signUpButtonContent}
          </SmallcaseButton>
          <SnackBar onRef={ref => this.snackbar = ref}/>
        </View>
      </TouchableWithoutFeedback>
    );

    if (signUpResponse) {
      signUpForm = (
        <View>
          <Text style={{fontSize: 32, marginTop: 32}}>{signUpResponse.emoji}</Text>
          <Text style={styles.signUpResponseTitle}>{signUpResponse.title}</Text>
          <Text style={styles.signUpResponseDescription}>{signUpResponse.description}</Text>
          {signUpResponse.redirect ?
            (<SmallcaseButton
              disabled={signUpRequestProcessing}
              onClick={this.redirectUser.bind(this)}
              buttonStyles={[styles.kiteLoginButton, {
                backgroundColor: 'rgb(31, 122, 224)',
                marginHorizontal: 0,
                marginTop: 32
              }]}>
              <Text style={styles.signUp}>{signUpResponse.button}</Text>
            </SmallcaseButton>) : null}
        </View>
      );
    }

    return (
      <View style={{flex: 1, backgroundColor: 'white', paddingHorizontal: 40}}>
        <Image style={styles.signUpSmallcaseImage} source={require('../../../assets/logoWithTag.png')}/>
        {signUpForm}
        <View style={styles.loginLink}>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text>Already have an account?</Text>
            <TouchableWithoutFeedback disabled={signUpRequestProcessing} onPress={() => toggle()}>
              <View><Text style={styles.loginHere}> Log In here</Text></View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  signUp: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  kiteLoginButton: {
    marginBottom: 16,
    marginHorizontal: 40,
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    marginTop: 4
  },
  inputHeaders: {
    marginTop: 16,
    fontSize: 12,
    color: 'rgb(83, 91, 98)'
  },
  loginLink: {
    paddingBottom: '15%',
    justifyContent: 'flex-end'
  },
  signUpField: {
    borderRadius: 4,
    borderWidth: 1,
    height: 36,
    marginTop: 12,
    paddingLeft: 16,
    fontSize: 14,
  },
  signUpSmallcaseImage: {
    marginLeft: 0,
    marginTop: 64,
    height: 35
  },
  signUpResponseTitle: {
    color: 'rgb(83, 91, 98)',
    fontSize: 20,
    lineHeight: 20 * 1.5,
    marginTop: 16,
    fontWeight: 'bold'
  },
  signUpResponseDescription: {
    color: Styles.SECONDARY_TEXT_COLOR,
    lineHeight: 20 * 1.1,
    marginTop: 8
  },
  note: {
    marginTop: 24,
    color: Styles.SECONDARY_TEXT_COLOR,
    fontSize: 14,
    lineHeight: 14 * 1.5
  },
  loginHere: {
    color: 'rgb(31, 122, 224)'
  },
  heardFromText: {
    marginLeft: 16,
    flex: 1
  },
  heardFromArrow: {
    height: 7,
    width: 10,
    marginHorizontal: 16
  },
  heardFromContainer: {
    height: 36,
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 4,
    borderWidth: 1,
    marginTop: 12,
    borderColor: 'rgba(83, 91, 98, 0.25)',
  },
  createAccountContainer: {
    backgroundColor: '#2f363f',
    justifyContent: 'center',
    height: 45
  },
  createAccount: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
}, {
  [IPAD_PRO_SMALLEST]: {
    signUpSmallcaseImage: {
      marginTop: '30%',
      height: 35,
      alignSelf: 'center'
    },
    note: {
      marginTop: 32,
      alignSelf: 'center',
      textAlign: 'center',
      width: 314
    },
    signUpField: {
      width: 314,
      alignSelf: 'center'
    },
    inputHeaders: {
      width: 320,
      alignSelf: 'center'
    },
    heardFromContainer: {
      width: 314,
      alignSelf: 'center'
    },
    loginLink: {
      paddingBottom: '30%',
      justifyContent: 'flex-end'
    },
    kiteLoginButton: {
      width: 300,
      alignSelf: 'center',
      marginTop: 32
    }
  },
  [IPHONE_FIVE_AND_SMALLER]: {
    note: {
      fontSize: 12,
      lineHeight: 12 * 1.5,
      marginTop: 60
    },
    signUpField: {
      height: 30,
    },
    heardFromContainer: {
      height: 30,
    },
    signUpSmallcaseImage: {
      display: 'none'
    }
  }
});
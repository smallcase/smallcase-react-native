import React, {Component} from "react";
import {ActivityIndicator, AsyncStorage, Image, Text, TouchableWithoutFeedback, View} from "react-native";
import {BROKER_LOGIN} from "../../api/ApiRoutes";
import {getJwt} from "../../helper/LoginHelper";
import * as types from "../../constants/Constants";
import {IPAD_PRO_SMALLEST, IPHONE_FIVE_AND_SMALLER} from "../../constants/Constants";
import {userLoggedIn} from "../LandingActions";
import {AnalyticsSource, Routes, SnackBarTime} from "../../util/Utility";
import LinearGradient from "react-native-linear-gradient";
import SmallcaseButton from "../../component/SmallcaseButton";
import SnackBar from "../../component/SnackBar";
import {MediaQueryStyleSheet} from "react-native-responsive";
import {Actions} from 'react-native-router-flux';
import {getUserDetails, setUserJwt} from "../../user/UserActions";
import * as Config from "../../../Config";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";

let leprechaunClicks = 0;

export default class Login extends Component {

  constructor() {
    super();
    this.state = {
      fetchingJwt: false
    }
    AnalyticsManager.track('Viewed Login Screen', null, [AnalyticsSource.MIXPANEL,AnalyticsSource.CLEVER_TAP]);
  }

  componentDidMount() {
    if (this.props.loggedOut) {
      this.snackbar.show({
        title: SnackBarMessages.SESSION_EXPIRED,
        duration: SnackBarTime.LONG,
      });
    }
  }

  componentWillReceiveProps(newProps) {
    if (!newProps.loginReducer || (newProps.loginReducer.requestToken === this.props.loginReducer.requestToken)) return;

    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    this.setState({fetchingJwt: true});
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      this.setState({fetchingJwt: false});
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {});
        Actions.replace(Routes.INVESTMENTS, {jwt, csrf});
      } else {
        this.snackbar.show({
          title: SnackBarMessages.LOGIN_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    });
  }

  loginUser = () => Actions.push(Routes.LOGIN);

  leprechaunLogin = () => {
    leprechaunClicks++;
    if (leprechaunClicks === 10) {
      Actions.push(Routes.LOGIN_WITH_LEPRECHAUN);
      leprechaunClicks = 0;
    }
  }

  render() {
    let {dispatch, toggle} = this.props;
    let {fetchingJwt} = this.state;

    let signUp = (
      <TouchableWithoutFeedback onPress={toggle}>
        <View style={styles.signUpLink}>
          <Text style={styles.signUpDescription}>Don't have a Zerodha account? </Text>
          <Text style={[styles.signUpDescription, {fontWeight: 'bold'}]}>Sign up here</Text>
        </View>
      </TouchableWithoutFeedback>
    );

    let login = (
      <Image style={styles.kiteLogo} source={require('../../../assets/kiteLogo.png')}/>
    );

    if (fetchingJwt) {
      signUp = null;
      login = (<Loader loaderStyles={{marginLeft: -12, marginRight: 12}} size={'small'} />);
    }

    return (
      <LinearGradient colors={['rgb(11, 97, 193)', 'rgb(31, 122, 224)']} style={{flex: 1}}>
        <TouchableWithoutFeedback onPress={this.leprechaunLogin.bind(this, dispatch)}>
          <Image style={styles.smallcaseLogo} source={require('../../../assets/logoWithTagWhite.png')}/>
        </TouchableWithoutFeedback>
        <Text style={styles.loginText}>Track your invested{"\n"}smallcases on the go</Text>
        <SmallcaseButton disabled={fetchingJwt} onClick={this.loginUser.bind(this, dispatch)}
                         buttonStyles={styles.kiteLoginButton}>
          {login}
          <Text style={styles.loginWithKite}>{fetchingJwt ? 'Logging in...' : 'Continue with Zerodha Kite'}</Text>
        </SmallcaseButton>
        {signUp}
        <SnackBar onRef={ref => this.snackbar = ref}/>
      </LinearGradient>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  kiteLoginButton: {
    marginBottom: 16,
    marginHorizontal: 40,
  },
  signUpLink: {
    paddingBottom: '15%',
    marginTop: 100,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  loginText: {
    fontSize: 28,
    marginTop: 140,
    marginLeft: 40,
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 16,
    lineHeight: 28 * 1.35,
    backgroundColor: 'transparent'
  },
  loginWithKite: {
    alignSelf: 'center',
    fontSize: 14,
  },
  kiteLogo: {
    alignSelf: 'center',
    marginRight: 12,
    height: 12,
    width: 18
  },
  signUpDescription: {
    backgroundColor: 'transparent',
    color: 'white',
    alignSelf: 'center'
  },
  smallcaseLogo: {
    height: 35,
    marginTop: 86,
    marginLeft: 40
  },
}, {
  [IPAD_PRO_SMALLEST]: {
    smallcaseLogo: {
      height: 35,
      marginTop: '30%',
      alignSelf: 'center',
      marginLeft: 0
    },
    loginText: {
      marginTop: 72,
      marginLeft: 0,
      color: 'white',
      lineHeight: 36,
      textAlign: 'center',
      alignSelf: 'center',
    },
    signUpLink: {
      paddingBottom: 0,
      marginTop: 64,
      alignSelf: 'center',
      flex: 0,
    },
    signUpDescription: {
      backgroundColor: 'transparent',
      color: 'white',
      fontWeight: 'bold',
      alignSelf: 'center',
      fontSize: 16
    },
    loginWithKite: {
      fontSize: 16,
    },
    kiteLoginButton: {
      width: 300,
      alignSelf: 'center',
      marginTop: 32
    },
  },
  [IPHONE_FIVE_AND_SMALLER]: {
    loginText: {
      fontSize: 24,
      marginTop: 72,
      lineHeight: 24 * 1.35
    },
    signUpLink: {
      flexDirection: 'column',
      justifyContent: 'flex-end',
      alignItems: 'flex-start'
    },
    signUpDescription: {
      alignSelf: 'flex-start',
      marginLeft: 40,
      marginTop: 2
    }
  }
});
import {combineReducers} from 'redux';
import landingReducer from './onboarding/LandingReducer';
import loginReducer from './onboarding/login/LoginReducer';
import userReducer from './user/UserReducer';
import notificationReducer from './user/notification/NotificationReducer'
import { LOGOUT_USER } from './constants/Constants';

const appReducer = combineReducers({
  landingReducer,
  loginReducer,
  userReducer,
  notificationReducer,
});

const rootReducer = (state = {}, actions) => {
  if (actions.type === LOGOUT_USER) {
    state = undefined;
  }
  return appReducer(state, actions);
}

export default rootReducer;
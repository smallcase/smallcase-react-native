import React, {Component} from 'react';
import {
  ActivityIndicator,
  Animated,
  Easing,
  Image,
  processColor,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  View
} from 'react-native';

const {width, height} = getScreenSize();
import SmallcaseImage from "../../component/SmallcaseImage";
import {
  AnalyticsSource,
  getBottomBarPaddingBottom,
  getFormattedCurrency, getScreenSize,
  SmallcaseSource, SnackBarTime,
  Styles
} from "../../util/Utility";
import Divider from "../../component/Divider";
import RadioButton from "../../component/RadioButton";
import {
  editSip,
  endSip,
  frequencyMap,
  getApiDate,
  getSmallcaseSipHistorical,
  getStockPrices,
  getStockSipHistorical,
  setUpSip,
  sipFrequency
} from "./SipService";
import {calculateMinInvestment} from "../../helper/SmallcaseHelper";
import moment from "moment";
import SmallcaseButton from "../../component/SmallcaseButton";
import DatePicker from "react-native-datepicker";
import {connect} from "react-redux";
import SnackBar from "../../component/SnackBar";
import {Actions} from "react-native-router-flux";
import EndSip from "./components/EndSip";
import update from "immutability-helper/index";
import SipHistorical from "./components/SipHistorical";
import SipSet from "./components/SipSet";
import ExpandCollapse from "../../component/ExpandCollapse";
import {EM_DASH} from "../../constants/Constants";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";

class SipComponent extends Component {

  constructor(props) {
    super();
    const nextDate = new Date();
    nextDate.setDate(nextDate.getDate() + 1);
    this.state = {
      minAmount: EM_DASH,
      date: (props.sip && props.sip.scheduledDate) ? moment(props.sip.scheduledDate) : moment(nextDate),
      frequency: (props.sip && props.sip.frequency) ? frequencyMap(props.sip.frequency, true) : sipFrequency.MONTHLY.display,
      isStartClicked: false,
      disableInteraction: false,
      confirmSip: false,
      isEdit: props.isEdit || false,
      showEndSip: false,
      sipValues: null,
      sipHistorical: {},
      legend: {
        enabled: false,
      },
      marker: {
        enabled: false
      },
      noData: false,
      showHistorical: false,
      startDate: '',
      endDate: '',
      defaultDuration: '',
      spinValue: new Animated.Value(0),
      spinStartDegree: '0deg',
      spinEndDegree: '180deg'
    };
    this.datePicker = null;
    this.minDate = moment(nextDate).toDate();
    this.previousFrequency = ''
  }
  // for analytics tracking

  componentDidMount() {
    this.fetchSipHistorical(this.state.frequency, this.state.date);
  }

  fetchSipHistorical = (frequency, date) => {
    const {smallcase, jwt, csrf} = this.props;
    const {isEdit} = this.state;
    const sids = [];
    smallcase.stocks.forEach((stock) => {
      if (stock.sid !== '-1') sids.push(stock.sid);
    });
    getStockPrices(jwt, csrf, sids)
      .then((prices) => {
        smallcase.stocks.forEach((stock) => stock.price = prices[stock.sid]);
        const result = calculateMinInvestment(smallcase.stocks.slice(1));
        if (!isEdit) {
          if (smallcase.source === SmallcaseSource.CREATED) {
            let sharesObj = {};
            smallcase.stocks.forEach((stock) => {
              sharesObj[stock.sid] = {shares: stock.shares};
            });
            getStockSipHistorical(jwt, csrf, sids, "3y", result.minAmount, frequency, date.toDate(), sharesObj)
              .then((response) => {
                this.highlightObj = response.highlightObj;
                this.setUpChart(response.sipData, response.defaultDuration);
              })
              .catch((error) => console.log(error));
          } else {
            getSmallcaseSipHistorical(jwt, csrf, smallcase.scid, result.minAmount, frequency, date.toDate())
              .then((response) => {
                this.highlightObj = response.highlightObj;
                this.setUpChart(response.sipData, response.defaultDuration);
              })
              .catch((error) => console.log(error));
          }
        }
        this.setState({minAmount: result.minAmount});
      })
      .catch((error) => console.log(error));
  }

  sipActionClicked = () => {
    if (this.state.isStartClicked) {
      const {jwt, csrf, smallcase} = this.props;
      const {frequency, date,startDate,minAmount} = this.state;
      setUpSip(jwt, csrf, smallcase.iscid, smallcase.scid, getApiDate(date), frequencyMap(frequency))
        .then((response) => {
          if (response.success) {
            this.setState({confirmSip: true, disableInteraction: false});
            AnalyticsManager.track('Started SIP',{
              iscid: smallcase.iscid,
              scid: smallcase.scid ,
              sipDate: this.state.date,
              frequency: frequency ,
              smallcaseSource: smallcase.source ,
              smallcaseType: 'N/A',
              startDate: startDate ,
              sipType: 'Manual' ,
              amountType: 'Minimum Investment',
              amount: minAmount ,
            },[AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])
            return;
          }
          this.snackbar.show({
            title: SnackBarMessages.SIP_SET_UP_FAILED,
            duration: SnackBarTime.LONG
          });
          this.setState({disableInteraction: false});
        })
        .catch((error) => console.log(error));
      this.setState({disableInteraction: true});
      return;
    }

    this.setState({isStartClicked: true});
  }

  changeFrequency = (frequency) => {
    if (this.state.disableInteraction) return;
    this.previousFrequency = this.state.frequency
    this.setState({frequency});
    this.fetchSipHistorical(frequency, this.state.date);
  }

  endSip = () => {
    const {jwt, csrf, smallcase} = this.props;
    const {frequency, date, minAmount} = this.state;
    endSip(jwt, csrf, smallcase.iscid)
      .then((response) => {
        if (response) {
          AnalyticsManager.track('Ended SIP',{
            amount: minAmount,
            frequency: frequency,
            iscid: smallcase.iscid,
            scid: smallcase.scid,
            sipDate: date,
            smallcaseName: smallcase.smallcaseName,
            smallcaseSource: smallcase.source,
            smallcaseType: 'N/A',
            sipType: 'Manual',
            amountType: 'Minimum Investment',

          },[AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
          Actions.pop();
          return;
        }
        this.snackbar.show({
          title: SnackBarMessages.SIP_END_FAILED,
          duration: SnackBarTime.LONG
        });
      });
  }

  editSip = () => {
    const {jwt, csrf, smallcase} = this.props;
    const {frequency, date,minAmount} = this.state;
    editSip(jwt, csrf, smallcase.iscid, smallcase.scid, getApiDate(date), frequencyMap(frequency))
      .then((response) => {
        if (response) {
          AnalyticsManager.track('Changed SIP Settings',
            {
              amountFrom: minAmount,
              amountTo: minAmount,
              frequencyFrom: this.previousFrequency ,
              frequencyTo: frequency,
              sipDateFrom: 'N/A',
              sipDateTo: date ,
              smallcaseName:smallcase.smallcaseName ,
              smallcaseSource:smallcase.source ,
              smallcaseType: 'N/A',
              sipTypeFrom: 'Manual',
              sipTypeTo: 'Manual',
              sipAmountTypeFrom: 'Minimum Investment',
              sipAmountTypeTo: 'Minimum Investment' ,
              scid: smallcase.scid ,
            },
            [AnalyticsSource.MIXPANEL,AnalyticsSource.CLEVER_TAP])
          Actions.pop();
          return;
        }
        this.snackbar.show({
          title: SnackBarMessages.EDIT_SIP_FAILED,
          duration: SnackBarTime.LONG
        });
      })
      .catch((error) => console.log(error));
  }

  showEndSipPopUp = () => this.setState({showEndSip: true});

  onEndSipClosed = () => {
    this.setState({showEndSip: false});


  }

  setUpChart = (sipValues, defaultDuration) => {
    this.setState(
      update(this.state, {
        sipHistorical: {
          $set: {
            dataSets: [{
              label: '',
              values: sipValues.smallcaseSipHistorical,
              config: {
                lineWidth: 3,
                drawCircles: false,
                highlightColor: processColor(Styles.SECONDARY_TEXT_COLOR),
                drawHorizontalHighlightIndicator: false,
                drawVerticalHighlightIndicator: true,
                color: processColor(Styles.BLUE),
                drawFilled: false,
                drawValues: false
              }
            }, {
              label: '',
              values: sipValues.userSipHistorical,
              config: {
                lineWidth: 3,
                drawCircles: false,
                highlightColor: processColor(Styles.SECONDARY_TEXT_COLOR),
                drawHorizontalHighlightIndicator: false,
                drawVerticalHighlightIndicator: true,
                color: processColor(Styles.GREEN),
                drawFilled: false,
                drawValues: false
              }
            }],
          }
        },
        xAxis: {
          $set: {
            enabled: false,
          }
        },
        yAxis: {
          $set: {
            left: {
              enabled: false
            },
            right: {
              enabled: false
            },
          }
        },
        axisRight: {
          $set: {
            enabled: false,
            drawLabels: false,
            drawAxisLine: false,
            drawGridLines: false
          }
        },
        sipValues: {
          $set: sipValues
        },
        startDate: {
          $set: moment(new Date(sipValues.userSipHistorical[0].x)).format('DD MMM, YYYY')
        },
        endDate: {
          $set: moment(new Date(sipValues.userSipHistorical[sipValues.userSipHistorical.length - 1].x)).format('DD MMM, YYYY')
        },
        defaultDuration: {
          $set: defaultDuration
        }
      })
    );
  }

  toggleHistorical = () => {
    this.state.spinValue.setValue(0);
    Animated.timing(
      this.state.spinValue, {
        toValue: 1,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();

    this.setState({
      showHistorical: !this.state.showHistorical,
      spinStartDegree: this.state.showHistorical ? '180deg' : '0deg',
      spinEndDegree: this.state.showHistorical ? '0deg' : '180deg'
    });
  }

  handleSelect = (event) => {
    const entry = event.nativeEvent;
    this.setState({
      sipValues: {
        ...this.state.sipValues,
        smallcaseAmount: this.highlightObj[entry.x].smallcase,
        userInvestmentAmount: this.highlightObj[entry.x].user
      },
      endDate: moment(new Date(entry.x)).format('DD MMM, YYYY')
    })
  }

  backToEdit = () => this.setState({isEdit: true, isStartClicked: false, confirmSip: false});

  getArrow = () => {
    const {spinValue, spinStartDegree, spinEndDegree} = this.state;
    const spin = spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [spinStartDegree, spinEndDegree]
    });

    return (
      <Animated.Image source={require('../../../assets/iconArrowDownBlue.png')}
                      style={[styles.seeHistoricalArrow, {transform: [{rotate: spin}]}]}/>
    );
  };

  render() {
    const {smallcase} = this.props;
    const {
      minAmount, date, frequency, isStartClicked, disableInteraction, confirmSip, isEdit, showEndSip, sipValues,
      showHistorical, startDate, endDate, defaultDuration, legend, marker, xAxis, yAxis, sipHistorical
    } = this.state;
    const formattedDate = moment(date).format('DD MMM, YYYY');

    let pAndL = 0;
    if (sipValues) {
      pAndL = ((sipValues.smallcaseAmount - sipValues.userInvestmentAmount) / sipValues.userInvestmentAmount) * 100;
    }

    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        {confirmSip ?
          <SipSet
            smallcase={smallcase}
            frequency={frequency}
            formattedDate={formattedDate}
            backToEdit={this.backToEdit}
          /> :
          (
            <View style={{flex: 1, backgroundColor: 'white'}}>
              <ScrollView style={{flex: 1}}>
                <View>
                  <View style={styles.smallcaseInfoContainer}>
                    <SmallcaseImage
                      imageUrl={smallcase.smallcaseImage}
                      source={smallcase.source}
                      imageStyles={styles.smallcaseImage}/>
                    <View style={{marginLeft: 16}}>
                      <Text style={styles.startingSip}>{isEdit ? 'Editing SIP in' : 'Starting SIP in'}</Text>
                      <Text style={styles.smallcaseName}>{smallcase.smallcaseName}</Text>
                    </View>
                  </View>
                  <View style={{paddingHorizontal: 16}}>
                    {isEdit ? null :
                      <TouchableWithoutFeedback onPress={this.toggleHistorical}>
                        <View>
                          <Text style={styles.historicalPerformance}>Historical Performance of SIP</Text>
                          <Text style={styles.sipDescription}>SIP helps you invest systematically & regularly.</Text>
                          <ExpandCollapse value={showHistorical && sipValues !== null} easing='easeInOut'>
                            <SipHistorical
                              startDate={startDate}
                              endDate={endDate}
                              defaultDuration={defaultDuration}
                              pAndL={pAndL}
                              frequency={frequency}
                              minAmount={minAmount}
                              sipValues={sipValues}
                              handleSelect={this.handleSelect}
                              legend={legend}
                              marker={marker}
                              xAxis={xAxis}
                              yAxis={yAxis}
                              sipHistorical={sipHistorical}
                            />
                          </ExpandCollapse>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={styles.seeHistorical}>
                              {showHistorical ? 'Hide historical performance' : 'See historical performance of SIP'}
                            </Text>
                            {this.getArrow()}
                          </View>
                          <Divider dividerStyles={styles.divider}/>
                        </View>
                      </TouchableWithoutFeedback>
                    }
                    <View style={styles.stepOneContainer}>
                      <View style={styles.circle}><Text style={styles.stepNumber}>1</Text></View>
                      <View style={{marginLeft: 10}}>
                        <Text style={[styles.stepTitle, {marginTop: 4}]}>Select Frequency</Text>
                        <RadioButton onSelected={() => this.changeFrequency(sipFrequency.WEEKLY.display)}
                                     containerStyles={{marginTop: 30}} title={sipFrequency.WEEKLY.display}
                                     isSelected={frequency === sipFrequency.WEEKLY.display}/>
                        <RadioButton onSelected={() => this.changeFrequency(sipFrequency.FORTNIGHTLY.display)}
                                     containerStyles={{marginTop: 24}} title={sipFrequency.FORTNIGHTLY.display}
                                     isSelected={frequency === sipFrequency.FORTNIGHTLY.display}/>
                        <RadioButton onSelected={() => this.changeFrequency(sipFrequency.MONTHLY.display)}
                                     containerStyles={{marginTop: 24}} title={sipFrequency.MONTHLY.display}
                                     isSelected={frequency === sipFrequency.MONTHLY.display}/>
                        <RadioButton onSelected={() => this.changeFrequency(sipFrequency.QUARTERLY.display)}
                                     containerStyles={{marginTop: 24}} title={sipFrequency.QUARTERLY.display}
                                     isSelected={frequency === sipFrequency.QUARTERLY.display}/>
                      </View>
                    </View>
                    <View style={styles.stepTwoContainer}>
                      <View style={styles.circle}><Text style={styles.stepNumber}>2</Text></View>
                      <View style={{flex: 1, marginLeft: 10}}>
                        <Text style={[styles.stepTitle, {marginTop: 4}]}>Select Amount</Text>
                        <Text
                          style={styles.minAmount}>{minAmount === EM_DASH ? minAmount : getFormattedCurrency(minAmount)}</Text>
                        <Divider dividerStyles={{marginTop: 6}}/>
                        <Text style={styles.minAmountText}>Minimum investment amount
                          is {minAmount === EM_DASH ? minAmount : getFormattedCurrency(minAmount)}</Text>
                      </View>
                    </View>
                    <View style={styles.stepThreeContainer}>
                      <View style={styles.circle}><Text style={styles.stepNumber}>3</Text></View>
                      <View style={{flex: 1, marginLeft: 10}}>
                        <Text style={[styles.stepTitle, {marginTop: 4}]}>Select Start Date</Text>
                        <TouchableWithoutFeedback onPress={() => this.datePicker.onPressDate()}>
                          <View style={styles.dateContainer}>
                            <Text style={styles.date}>{formattedDate}</Text>
                            <Image source={require('../../../assets/calendar.png')} style={styles.calendar}/>
                          </View>
                        </TouchableWithoutFeedback>
                        <Divider/>
                        <Text style={styles.marketHoliday}>If the SIP date is a market holiday, you will be notified on
                          the
                          next market day</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </ScrollView>
              {isEdit ?
                <View style={styles.actionsContainer}>
                  <SmallcaseButton onClick={this.showEndSipPopUp} buttonStyles={[styles.sipActionButton, {
                    borderWidth: 0.5,
                    borderColor: Styles.RED_ALPHA,
                    marginRight: 8
                  }]}>
                    <Text style={styles.endSip}>End SIP</Text>
                  </SmallcaseButton>
                  <SmallcaseButton onClick={this.editSip} buttonStyles={[styles.sipActionButton, {
                    backgroundColor: Styles.BLUE,
                    marginLeft: 8
                  }]}>
                    <Text style={styles.continueSip}>Save Changes</Text>
                  </SmallcaseButton>
                </View> :
                <View style={styles.actionsContainer}>
                  <Text style={styles.sip}>{'Investing min. amount ' + frequency + ', starting ' + formattedDate}</Text>
                  <SmallcaseButton disabled={disableInteraction} onClick={this.sipActionClicked}
                                   buttonStyles={[styles.startSipButton, {backgroundColor: isStartClicked ? Styles.BLUE : Styles.GREEN_SUCCESS}]}>
                    {disableInteraction ? <Loader size="small" color={'white'}/> :
                      <Text style={styles.startSip}>{isStartClicked ? 'Confirm SIP' : 'Start SIP'}</Text>}
                  </SmallcaseButton>
                </View>
              }
            </View>
          )
        }
        <DatePicker
          ref={ref => this.datePicker = ref}
          style={{position: 'absolute'}}
          date={date.toDate()}
          mode="date"
          format="DD MMM, YYYY"
          minDate={this.minDate}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          hideText={true}
          showIcon={false}
          duration={300}
          disabled={disableInteraction}
          onDateChange={(date) => {
            const newDate = moment(date);
            this.setState({date: newDate});
            this.fetchSipHistorical(this.state.frequency, newDate);
          }}
        />
        <EndSip endSip={this.endSip} showEndSip={showEndSip} endSipClosed={this.onEndSipClosed}/>
        <SnackBar onRef={ref => this.snackbar = ref}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  smallcaseImage: {
    height: 48,
    width: 48
  },
  smallcaseInfoContainer: {
    padding: 16,
    backgroundColor: 'rgb(249, 250, 251)',
    flexDirection: 'row'
  },
  startingSip: {
    fontWeight: 'bold',
    color: Styles.SECONDARY_TEXT_COLOR,
    marginTop: 4,
  },
  smallcaseName: {
    marginTop: 8,
    fontWeight: 'bold',
    fontSize: 18,
    flex: 1,
    marginRight: 36
  },
  historicalPerformance: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 24
  },
  sipDescription: {
    marginTop: 8,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  seeHistorical: {
    marginTop: 24,
    color: Styles.BLUE,
    fontWeight: 'bold'
  },
  seeHistoricalArrow: {
    marginLeft: 6,
    marginTop: 30
  },
  circle: {
    height: 28,
    width: 28,
    borderWidth: 2,
    borderRadius: 16,
    borderColor: Styles.BLUE,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 0,
    shadowOpacity: 0.09
  },
  stepNumber: {
    fontWeight: 'bold',
    color: Styles.BLUE
  },
  divider: {
    marginTop: 32,
    marginHorizontal: -16
  },
  stepTitle: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  stepOneContainer: {
    flexDirection: 'row',
    marginTop: 32
  },
  stepTwoContainer: {
    flexDirection: 'row',
    marginTop: 50
  },
  minAmount: {
    marginTop: 24,
    fontSize: 20,
    marginLeft: 8,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  stepThreeContainer: {
    flexDirection: 'row',
    marginTop: 56
  },
  calendar: {
    width: 18,
    height: 18
  },
  date: {
    flex: 1,
    marginLeft: 8
  },
  dateContainer: {
    flexDirection: 'row',
    paddingVertical: 10,
    marginTop: 14
  },
  marketHoliday: {
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR,
    marginTop: 8,
    marginBottom: 24,
    lineHeight: 16
  },
  actionsContainer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: 'rgb(221, 224, 228)',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'stretch',
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: getBottomBarPaddingBottom()
  },
  sip: {
    flex: 1,
    fontSize: 12,
    lineHeight: 18,
    color: Styles.SECONDARY_TEXT_COLOR,
    alignSelf: 'center'
  },
  startSipButton: {
    height: 48,
    paddingHorizontal: 40,
    marginLeft: 16,
  },
  startSip: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white'
  },
  minAmountText: {
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR,
    marginTop: 8
  },
  sipActionButton: {
    flexDirection: 'column',
    paddingVertical: 12,
    width: (width * 0.5) - 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  continueSip: {
    fontWeight: 'bold',
    color: 'white'
  },
  endSip: {
    fontWeight: 'bold',
    color: Styles.RED
  },
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps)(SipComponent);
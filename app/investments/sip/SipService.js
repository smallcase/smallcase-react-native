import Api from "../../api/Api";
import {
  CREATE_SIP,
  EDIT_SIP,
  END_SIP,
  GET_STOCK_PRICE,
  SMALLCASE_HISTORICAL,
  STOCK_HISTORICAL
} from "../../api/ApiRoutes";
import moment from "moment/moment";
import {BenchmarkId, BenchmarkType} from "../../util/Utility";
import {getHistoricalInIndexFormat, getSipFromHistorical} from "../../helper/SmallcaseHelper";
import {createHistoricalForStocks} from "../../helper/StockHelper";

export const getStockPrices = (jwt, csrf, sids) => {
  let params = '';
  for (let i = 0; i < sids.length; i++) {
    let sid = sids[i];
    params += 'stocks=' + sid + (i === sids.length - 1 ? '' : '&');
  }

  return new Promise((resolve, reject) => {
    Api.get({route: GET_STOCK_PRICE, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const setUpSip = (jwt, csrf, iscid, scid, scheduledDate, frequency) => {
  return new Promise((resolve, reject) => {
    Api.post({route: CREATE_SIP, jwt, csrf, body: {iscid, scid, scheduledDate, frequency}})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

export const editSip = (jwt, csrf, iscid, scid, scheduledDate, frequency) => {
  return new Promise((resolve, reject) => {
    Api.post({route: EDIT_SIP, jwt, csrf, body: {iscid, scid, scheduledDate, frequency}})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

const getSipValue = (smallcaseHistorical, id) => {
  if (smallcaseHistorical.length <= id) return smallcaseHistorical[smallcaseHistorical.length - 1].y;

  return smallcaseHistorical[id].y;
};

export const getSmallcaseSipHistorical = (jwt, csrf, scid, minInvestmentAmount, frequency, selectedDate) => {
  let params = 'scid=' + scid + '&benchmarkId=' + BenchmarkId.NSE + '&benchmarkType=' + BenchmarkType.INDEX;

  return new Promise((resolve, reject) => {
    Api.get({route: SMALLCASE_HISTORICAL, jwt, csrf, params})
      .then((response) => {
        const niftyHistorical = getHistoricalInIndexFormat(response.data.benchmark.points);
        const smallcaseHistorical = getHistoricalInIndexFormat(response.data.points);
        const sipData = getSipFromHistorical({smallcaseHistorical, niftyHistorical, minInvestmentAmount, frequency, selectedDate});
        let highlightObj = {};
        sipData.userSipHistorical.forEach((point, id) => {
          highlightObj[point.x] = {user: point.y, smallcase: getSipValue(sipData.smallcaseSipHistorical, id)};
        });
        resolve({sipData, highlightObj, defaultDuration: response.data.defaultDuration});
      })
      .catch((error) => reject(error));
  });
};

export const getStockSipHistorical = (jwt, csrf, sids, duration, minInvestmentAmount, frequency, selectedDate, sharesObj) => {
  let params = 'benchmarkType=' + BenchmarkType.INDEX;
  sids.forEach(sid => {
    params += '&stocks=' + sid;
  });
  params += '&stocks=' + BenchmarkId.NSE + '&duration=' + duration;

  return new Promise((resolve, reject) => {
    Api.get({route: STOCK_HISTORICAL, jwt, csrf, params})
      .then((response) => {
        let historicalList = [];
        sids.forEach((item) => historicalList.push(response.data[item]));
        historicalList.map((historical) => {
          historical.point.map((point) => {
            point.date = Date.parse(point.date);
          });
        });
        const niftyHistorical = getHistoricalInIndexFormat(response.data[BenchmarkId.NSE].point);
        const pointsList = createHistoricalForStocks(historicalList, sharesObj, niftyHistorical);
        const stockHistorical = getHistoricalInIndexFormat(pointsList);
        const sipData = getSipFromHistorical({smallcaseHistorical: stockHistorical, niftyHistorical, minInvestmentAmount, frequency, selectedDate});
        let highlightObj = {};
        sipData.userSipHistorical.forEach((point, id) => {
          highlightObj[point.x] = {user: point.y, smallcase: getSipValue(sipData.smallcaseSipHistorical, id)};
        });
        resolve({sipData, highlightObj, defaultDuration: duration});
      })
      .catch((error) => reject(error));
  });
};

export const endSip = (jwt, csrf, iscid) => {
  return new Promise((resolve, reject) => {
    Api.post({route: END_SIP, jwt, csrf, body: {iscid}})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const frequencyMap = (frequency, isApi) => {
  if (isApi) {
    switch (frequency) {
      case sipFrequency.WEEKLY.api:
        return sipFrequency.WEEKLY.display;

      case sipFrequency.MONTHLY.api:
        return sipFrequency.MONTHLY.display;

      case sipFrequency.FORTNIGHTLY.api:
        return sipFrequency.FORTNIGHTLY.display;

      case sipFrequency.QUARTERLY.api:
        return sipFrequency.QUARTERLY.display;
    }
  }

  switch (frequency) {
    case sipFrequency.WEEKLY.display:
      return sipFrequency.WEEKLY.api;

    case sipFrequency.FORTNIGHTLY.display:
      return sipFrequency.FORTNIGHTLY.api;

    case sipFrequency.MONTHLY.display:
      return sipFrequency.MONTHLY.api;

    case sipFrequency.QUARTERLY.display:
      return sipFrequency.QUARTERLY.api;

    default:
      return '';
  }
};

export const sipFrequency = {
  WEEKLY: {
    api: '7d',
    display: 'Weekly'
  },
  FORTNIGHTLY: {
    api: '14d',
    display: 'Fortnightly'
  },
  MONTHLY: {
    api: '1m',
    display: 'Monthly'
  },
  QUARTERLY: {
    api: '3m',
    display: 'Quarterly'
  }
};

export const getApiDate = (date) => moment(date).format('YYYY-MM-DD');

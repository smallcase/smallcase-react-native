import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet, Image
} from 'react-native';
import {Actions} from "react-native-router-flux";
import {Styles} from "../../../util/Utility";
import SmallcaseButton from "../../../component/SmallcaseButton";

export default class SipSet extends PureComponent {
  render() {
    const {smallcase, frequency, formattedDate, backToEdit} = this.props;

    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <Image style={styles.sipPlaced} source={require('../../../../assets/sipLarge.png')}/>
        <Text style={styles.sipStartedTitle}>Great, you have started an SIP in {smallcase.smallcaseName}!</Text>
        <Text style={styles.sipStartedDescription}>
          You have set up a
          <Text style={{fontWeight: 'bold'}}> {frequency} </Text>
          SIP for the
          <Text style={{fontWeight: 'bold'}}> minimum investment amount </Text>
          starting from
          <Text style={{fontWeight: 'bold'}}> {formattedDate} </Text>
        </Text>
        <View style={[styles.actionsContainer, {justifyContent: 'space-between'}]}>
          <SmallcaseButton onClick={backToEdit} buttonStyles={[styles.sipActionButton, {borderWidth: 0.5, borderColor: Styles.BLUE_ALPHA}]}>
            <Text style={styles.editSip}>Edit SIP</Text>
          </SmallcaseButton>
          <SmallcaseButton onClick={() => Actions.pop()} buttonStyles={[styles.sipActionButton, {backgroundColor: Styles.BLUE}]}>
            <Text style={styles.continueSip}>Continue</Text>
          </SmallcaseButton>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  sipPlaced: {
    marginTop: 64,
    height: 210,
    width: 170
  },
  sipStartedTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 24,
    marginHorizontal: 24,
    lineHeight: 26,
    textAlign: 'center'
  },
  sipStartedDescription: {
    marginTop: 8,
    marginHorizontal: 24,
    lineHeight: 22,
    textAlign: 'center'
  },
  actionsContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    paddingHorizontal: 16,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderTopColor: 'rgb(221, 224, 228)',
    backgroundColor: 'white'
  },
  sipActionButton: {
    flex: 0.5,
    marginVertical: 16,
    height: 48,
    width: 164
  },
  editSip: {
    fontWeight: 'bold',
    color: Styles.BLUE
  },
  continueSip: {
    fontWeight: 'bold',
    color: 'white'
  },
});

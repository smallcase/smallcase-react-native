import React, {PureComponent} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native';
import {getDefaultDurationText, getFormattedCurrency, Styles} from "../../../util/Utility";
import {LineChart} from "react-native-charts-wrapper";
import {EM_DASH} from "../../../constants/Constants";

export default class SipHistorical extends PureComponent {
  render() {
    const {startDate, endDate, defaultDuration, pAndL, minAmount, frequency, sipValues, handleSelect, legend,
      marker, xAxis, yAxis, sipHistorical} = this.props;

    return (
      <View style={styles.historicalContainer}>
        <Text style={styles.historicalDescription}>If you had invested the <Text style={{fontWeight: 'bold'}}>minimum investment
          amount</Text> ({getFormattedCurrency(minAmount)})<Text style={{fontWeight: 'bold'}}> {frequency.toLowerCase()} </Text>for the
          <Text style={{fontWeight: 'bold'}}> last {getDefaultDurationText(defaultDuration)}</Text>, your investment summary would be</Text>
        <View style={styles.returnsContainer}>
          <View style={{flex: 1}}>
            <Text style={styles.sipTitle}>Current Value</Text>
            <Text style={styles.sipValue}>{getFormattedCurrency(sipValues? sipValues.smallcaseAmount : EM_DASH)}</Text>
          </View>
          <View style={{flex: 1.2}}>
            <Text style={styles.sipTitle}>Total Investment</Text>
            <Text
              style={styles.sipValue}>{getFormattedCurrency(sipValues? sipValues.userInvestmentAmount : EM_DASH)}</Text>
          </View>
          <View>
            <Text style={styles.sipTitle}>Total Returns</Text>
            <Text
              style={[styles.sipValue, {color: pAndL < 0 ? Styles.RED : Styles.GREEN}]}>{pAndL.toFixed(2)}%</Text>
          </View>
        </View>
        <View>
          <View style={styles.legendContainer}>
            <View style={[styles.legendCircle, {backgroundColor: Styles.GREEN}]}/>
            <Text style={styles.legendTitle}>Investment</Text>
            <View style={[styles.legendCircle, {backgroundColor: Styles.BLUE, marginLeft: 24}]}/>
            <Text style={styles.legendTitle}>smallcase</Text>
          </View>
          <TouchableWithoutFeedback>
            <LineChart
              style={{height: 160, marginBottom: 24, marginTop: -24}}
              data={sipHistorical}
              chartDescription={{text: ''}}
              noDataText={'No data available'}
              legend={legend}
              marker={marker}
              xAxis={xAxis}
              yAxis={yAxis}
              drawGridBackground={false}
              drawBorders={false}
              touchEnabled={true}
              dragEnabled={true}
              scaleEnabled={false}
              scaleXEnabled={false}
              scaleYEnabled={false}
              pinchZoom={false}
              doubleTapToZoomEnabled={false}
              dragDecelerationEnabled={true}
              dragDecelerationFrictionCoef={0.99}
              keepPositionOnRotation={false}
              onSelect={handleSelect}
            />
          </TouchableWithoutFeedback>
          <Text style={styles.timeLine}>From {startDate} to {endDate}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  historicalContainer: {
    flexDirection: 'column',
    marginTop: 24
  },
  historicalDescription: {
    lineHeight: 22,
    color: Styles.DESCRIPTION_TEXT_COLOR
  },
  returnsContainer: {
    flexDirection: 'row',
    marginTop: 16
  },
  sipTitle: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  sipValue: {
    marginTop: 8,
    fontSize: 16,
    color: Styles.PRIMARY_TEXT_COLOR
  },
  legendContainer: {
    marginTop: 32,
    flexDirection: 'row'
  },
  legendCircle: {
    height: 12,
    width: 12,
    borderRadius: 6,
    marginTop: 2
  },
  legendTitle: {
    marginLeft: 6,
    fontSize: 13,
    color: Styles.DESCRIPTION_TEXT_COLOR
  },
  timeLine: {
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR,
    textAlign: 'center',
    alignItems: 'center'
  }
});

import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Animated,
  interpolate,
  Dimensions
} from 'react-native';
import Interactable from 'react-native-interactable';
import SmallcaseButton from "../../../component/SmallcaseButton";
import {getScreenSize, Styles} from "../../../util/Utility";

const Screen = getScreenSize();

export default class EndSip extends Component {

  constructor(props) {
    super(props);
    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
    this.endSip = null;
  }

  componentWillReceiveProps(newProps) {
    if (newProps.showEndSip !== this.props.showEndSip && newProps.showEndSip) {
      this.endSip.snapTo({index: 0});
    }
  }

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.endSipClosed();
    } else {
      this.blurPointerEvent = 'auto';
    }

    this.setState({endSipToggled: true});
  };

  dismissEndSip = () => this.endSip.snapTo({index: 1});

  render() {
    const {endSip} = this.props;
    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent} onPress={this.dismissEndSip}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - 275, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.endSip = ele}
          verticalOnly={true}
          snapPoints={[{y: Screen.height - 300}, {y: Screen.height}]}
          onSnap={this.handelSnap}
          boundaries={{top: Screen.height - 325}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          animatedValueY={this.deltaY}>
          <View style={styles.panel}>
            <Text style={styles.endSipTitle}>End SIP?</Text>
            <Text style={{lineHeight: 20}}>Once you end this SIP, you will no longer receive notifications to invest in this smallcase regularly.{'\n'}Are you sure you want to end this SIP?</Text>
            <View style={styles.actionsContainer}>
              <SmallcaseButton onClick={this.dismissEndSip} buttonStyles={styles.keepSipButton}><Text style={styles.keepSip}>Keep SIP</Text></SmallcaseButton>
              <SmallcaseButton onClick={endSip} buttonStyles={styles.endSipButton}><Text style={styles.endSip}>End SIP</Text></SmallcaseButton>
            </View>
          </View>
        </Interactable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  panel: {
    height: 400,
    backgroundColor: 'white',
    paddingHorizontal: 24
  },
  endSipTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 32,
    marginBottom: 8
  },
  actionsContainer: {
    flex: 1,
    marginVertical: 32,
    flexDirection: 'row'
  },
  keepSipButton: {
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: Styles.BLUE_ALPHA,
    height: 48,
    width: 154,
    marginRight: 8,
  },
  endSipButton: {
    borderRadius: 4,
    backgroundColor: Styles.RED_FAILURE,
    height: 48,
    width: 154,
    marginLeft: 8,
  },
  keepSip: {
    fontWeight: 'bold',
    color: Styles.BLUE
  },
  endSip: {
    fontWeight: 'bold',
    color: 'white'
  }
});

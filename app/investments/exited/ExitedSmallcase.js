import React, {PureComponent} from "react";
import {AnalyticsSource, getFormattedCurrency, Styles} from "../../util/Utility";
import {Text, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import SmallcaseImage from "../../component/SmallcaseImage";
import Divider from "../../component/Divider";
import ReturnsComponent from '../../component/ReturnsComponent'
import DateBox from '../../component/DateBoxComponent'
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {ReturnsComponentValueType} from "../../constants/Constants";

export default class ExitedSmallcase extends PureComponent {

  componentDidMount() {
    AnalyticsManager.track('Viewed Exited Investments', {
      sectionTag: 'N/A',
      accessedFrom: 'N/A'
    }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])
  }


  render() {
    const rowData = this.props.rowData;

    return (
      <TouchableWithoutFeedback>
        <View style={styles.investmentCard}>
          <View style={styles.investmentInfo}>
            <View style={styles.smallcaseHorizontalContainer}>
              <SmallcaseImage source={rowData.source} imageUrl={rowData.smallcaseImage}
                              imageStyles={styles.exitedSmallcaseImage} textSize={7}/>
              <View style={{flex: 1}}>
                <Text style={styles.smallcaseName} numberOfLines={1} ellipsizeMode='tail'>{rowData.smallcaseName}</Text>
                <Text style={styles.boughtOn}>{`Bought On ${rowData.boughtOn}`} </Text>
              </View>
            </View>
            <View style={styles.infoContainer}>
              <DateBox title="Exited On" date={rowData.soldOn}/>
              <ReturnsComponent
                topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Total Returns'}}
                topRight={{type: ReturnsComponentValueType.PERCENT, value: +rowData.pAndL.toFixed(2)}}
                bottomLeft={{type: ReturnsComponentValueType.AMOUNT, value: rowData.realizedInvestment}}
                containerStyles={{paddingTop: 2, alignItems: 'flex-start', marginLeft: 40}}
                bottomLeftStyles={{fontSize: 15}}
              />
            </View>
          </View>
          <Divider dividerStyles={{marginLeft: 16}}/>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  investmentCard: {
    backgroundColor: '#FFFFFF',
  },
  investmentInfo: {
    flexDirection: 'column',
    flex: 1,
    marginHorizontal: 16
  },
  smallcaseHorizontalContainer: {
    flexDirection: 'row'
  },
  exitedSmallcaseImage: {
    height: 40,
    width: 40,
    marginTop: 20,
    marginBottom: 20,
    marginRight: 16,
    borderRadius: 4
  },
  smallcaseName: {
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 20,
    marginRight: 16,
    letterSpacing: -0.1,
  },
  boughtOn: {
    fontSize: 13,
    marginTop: 4,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  realizedInvestment: {
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 20,
    letterSpacing: -0.1,
  },
  pAndL: {
    fontSize: 13,
    marginTop: 6,
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginBottom: 25,
  }
});

import React, {Component} from 'react';
import {
  FlatList,
  View,
  Text,
  ActivityIndicator,
  StyleSheet
} from 'react-native';
import ExitedSmallcase from "./ExitedSmallcase";
import {getExitedSmallcases} from "./ExitedSmallcaseService";
import {AnalyticsSource, getFormattedCurrency, Styles} from "../../util/Utility";
import {connect} from "react-redux";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import Loader from "../../component/Loader";

class ExitedSmallcaseComponent extends Component {

  constructor() {
    super();
    this.state = {
      fetchingExited: true,
      exitedSmallcases: [],
      totalReturns: null
    }
  }

  componentDidMount = () => {
    const {jwt, csrf} = this.props;
    getExitedSmallcases(jwt, csrf)
      .then((exitedSmallcases) => {
        let totalReturns = 0;
        exitedSmallcases.forEach(exitedSmallcase => totalReturns += exitedSmallcase.realizedInvestment);
        this.setState({fetchingExited: false, exitedSmallcases, totalReturns});
      })
      .catch((error) => console.log(error));

    AnalyticsManager.track('Viewed Exited Investments',{sectionTag: 'N/A',accessedFrom: 'Investment Details'},[AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL]);
  }

  renderExitedOverview = () => {
    const {exitedSmallcases, totalReturns} = this.state;
    return (
      <View style={styles.overviewContainer}>
        <Text style={styles.summary}>Your exited smallcases summary</Text>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.infoContainer}>
            <Text style={styles.infoTitle}>Total Exited</Text>
            <Text style={styles.infoDescription}>{exitedSmallcases.length}</Text>
          </View>
          <View style={[styles.infoContainer, {marginLeft: 58}]}>
            <Text style={styles.infoTitle}>Total Returns</Text>
            <Text style={[styles.infoDescription, {color: totalReturns > 0? Styles.GREEN : Styles.RED}]}>{getFormattedCurrency(totalReturns)}</Text>
          </View>
        </View>
      </View>
    );
  }

  renderExitedSmallcase = ({item}) => <ExitedSmallcase rowData={item}/>;

  render() {
    const {fetchingExited, exitedSmallcases} = this.state;

    if (fetchingExited) return (<Loader size='large' style={{ marginTop: 64 }} />);

    return (
      <FlatList
        style={{backgroundColor: 'white'}}
        data={exitedSmallcases}
        renderItem={this.renderExitedSmallcase}
        ListHeaderComponent={this.renderExitedOverview}
        keyExtractor={(item) => item.iscid}
        extraData={this.state}
      />
    );
  }
}

const styles = StyleSheet.create({
  overviewContainer: {
    backgroundColor: 'rgb(249, 250, 251)',
    paddingHorizontal: 16,
    paddingVertical: 24
  },
  summary: {
    color: Styles.PRIMARY_TEXT_COLOR,
    fontSize: 20,
    fontWeight: 'bold'
  },
  infoTitle: {
    color: Styles.SECONDARY_TEXT_COLOR
  },
  infoDescription: {
    fontSize: 20,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginTop: 8
  },
  infoContainer: {
    marginTop: 24
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
});

module.exports = connect(mapStateToProps)(ExitedSmallcaseComponent);

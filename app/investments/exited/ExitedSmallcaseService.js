import {getReadableDate, getSmallcaseImage} from "../../util/Utility";
import Api from "../../api/Api";
import {EXITED_SMALLCASES} from "../../api/ApiRoutes";


const sanitize = (value) => value || 0 ;
const getPresentableExitedSmallcases = (data) => {

  let exitedSmallcases = [];
  data.exitedSmallcases.map((item) => {
    exitedSmallcases.unshift({
      smallcaseName: item.name,
      boughtOn: getReadableDate(item.date),
      soldOn: getReadableDate(item.dateSold),
      smallcaseImage: getSmallcaseImage(item.scid, item.source, '80'),
      realizedInvestment: sanitize(item.returns.realizedReturns) + sanitize(item.returns.divReturns) + sanitize(item.returns.creditedDivReturns)
      + sanitize(item.returns.accruedDivReturns),
      pAndL: ((sanitize(item.returns.realizedReturns) + sanitize(item.returns.divReturns)) / sanitize(item.returns.realizedInvestment)) * 100,
      iscid: item._id,
      source: item.source
    })
  });
  return exitedSmallcases;
}

export const getExitedSmallcases = (jwt, csrf) => {

  return new Promise((resolve, reject) => {
    Api.get({route: EXITED_SMALLCASES, jwt, csrf})
      .then((response) => resolve(getPresentableExitedSmallcases(response.data)))
      .catch((error) => reject(error));
  });
}
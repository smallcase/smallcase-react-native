import React, {PureComponent} from "react";
import {Image, Text, View, StyleSheet, Linking} from "react-native";
import {Styles} from "../../util/Utility";
import SmallcaseButton from "../../component/SmallcaseButton";

const DISCOVER_SMALLCASE_URL = 'https://smallcase.zerodha.com/discover/all'

export default class EmptyState extends PureComponent {

  redirectToDiscover = () => {
    Linking.canOpenURL(DISCOVER_SMALLCASE_URL).then(supported => {
      if (supported) {
        Linking.openURL(DISCOVER_SMALLCASE_URL);
      } else {
        console.log("Couldn't open url");
      }
    });
  }

  render() {
    let title = '', description = '';
    let image = null;
    switch (this.props.type) {
      case 'awiInvested':
        title = 'Everyday is a beautiful day to start investing';
        description = 'Pick up your first smallcase today and start tracking it here';
        image = require('../../../assets/allInvestmentsEmpty.png');
        break;

      case 'invested':
        title = 'Diversify your investments';
        description = '90% of our users invest in other smallcases for higher diversification and returns';
        image = require('../../../assets/investmentsEmpty.png');
        break;
    }

    return (
      <View style={styles.emptyStateContainer}>
        <Image
          style={styles.investmentIllustration}
          resizeMode='contain'
          source={image}
        />
        <Text style={styles.emptyTitle}>{title}</Text>
        <Text style={[styles.emptyExitedDescription, {marginTop: 8}]}>{description}</Text>
        <SmallcaseButton buttonStyles={styles.discoverButton} onClick={this.redirectToDiscover}>
          <Text style={styles.discoverRedirect}>Discover smallcase</Text>
        </SmallcaseButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  emptyStateContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  emptyExitedDescription: {
    alignSelf: 'center',
    color: Styles.SECONDARY_TEXT_COLOR,
    lineHeight: 21,
    marginHorizontal: 36,
    textAlign: 'center'
  },
  investmentIllustration: {
    flex: 1,
    height: 200,
  },
  discoverRedirect: {
    color: 'white',
    fontWeight: 'bold'
  },
  emptyTitle: {
    marginTop: 16,
    fontSize: 16,
    marginHorizontal: 16,
    color: Styles.PRIMARY_TEXT_COLOR,
    alignItems: 'center'
  },
  discoverButton: {
    width: 184,
    height: 48,
    backgroundColor: Styles.BLUE,
    marginTop: 16,
    marginBottom: 24
  }
});
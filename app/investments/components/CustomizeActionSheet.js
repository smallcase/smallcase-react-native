import React, {PureComponent} from 'react';
import {
  TouchableWithoutFeedback,
  Text,
  View,
  Image,
  Linking,
} from 'react-native';
import Divider from "../../component/Divider";
import SmallcaseButton from "../../component/SmallcaseButton";
import {Actions} from "react-native-router-flux"
import {ADD_FUNDS_URL, IPHONE_FIVE_AND_SMALLER, NORMAL_PHONES} from "../../constants/Constants";
import {getBottomBarPaddingBottom, Routes, Styles, getScreenSize} from "../../util/Utility"
import {MediaQueryStyleSheet} from "react-native-responsive";

const {width} = getScreenSize();
export default class CustomizeActionSheet extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      fundsAvailable: props.fundsAvailable,
      disabled: props.disabled || false
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.fundsAvailable !== this.state.fundsAvailable) {
      this.setState({fundsAvailable: newProps.fundsAvailable});
    }

    if (newProps.disabled !== this.state.disabled) {
      this.setState({disabled: newProps.disabled});
    }
  }

  openFundsPage = () => {
    Linking.canOpenURL(ADD_FUNDS_URL).then(supported => {
      const {onFundsAvailableClicked} = this.props;
      if (supported) {
        onFundsAvailableClicked();
        Linking.openURL(ADD_FUNDS_URL);
      } else {
        console.log("Couldn't open url");
      }
    });
  };

  render() {
    const {leftFunc, rightFunc, noOrders, scene} = this.props;
    const {fundsAvailable} = this.state;

    let view;
    const disabled = (fundsAvailable && !this.state.disabled) ? null : {opacity: 0.3};
    const pointerEvents = (fundsAvailable && !this.state.disabled) ? 'auto' : 'none';

    const reviewView = (
      <View style={[styles.container, disabled]} pointerEvents={pointerEvents}>
        <SmallcaseButton buttonStyles={styles.leftButton} onClick={leftFunc}>
          <Text style={styles.ignore}>Skip Rebalance</Text>
        </SmallcaseButton>
        <SmallcaseButton buttonStyles={styles.confirmButton} onClick={rightFunc}>
          <Text style={styles.confirm}>Confirm Update</Text>
        </SmallcaseButton>
      </View>
    );

    const customizeView = (
      <View style={[styles.container, disabled]} pointerEvents={pointerEvents}>
        <SmallcaseButton buttonStyles={styles.leftButton} onClick={leftFunc}>
          <Text style={styles.reset}>Reset Changes</Text>
        </SmallcaseButton>
        <SmallcaseButton buttonStyles={styles.confirmButton} onClick={rightFunc}>
          <Text style={styles.confirm}>Confirm Changes</Text>
        </SmallcaseButton>
      </View>
    );

    const noOrdersView = (
      <View style={styles.noOrdersContainer}>
        <View style={styles.messageContainer}>
          <Image source={require('../../../assets/info.png')} style={styles.icon}/>
          <Text style={styles.text}>Looks like your smallcase is already up to date, so you can remove this update.
            You will be notified when it is next available.</Text>
        </View>
        <SmallcaseButton buttonStyles={styles.noOrdersButton} onClick={rightFunc}>
          <Text style={styles.confirm}>Remove Update</Text>
        </SmallcaseButton>
      </View>
    );

    const tooltip = (
      <TouchableWithoutFeedback onPress={this.openFundsPage}>
        <View style={styles.tooltipContainer}>
          <View style={styles.errorMessageContainer}>
            <Image source={require('../../../assets/error.png')} style={styles.errorIcon}/>
            <Text style={styles.text}>You do not have sufficient funds for this order. Please Add Funds to
              continue</Text>
          </View>
          <Text style={styles.fundsButton}>Add funds</Text>
        </View>
      </TouchableWithoutFeedback>
    );

    switch (scene) {
      case Routes.REVIEW_REBALANCE :
        view = reviewView;
        break;
      case Routes.CUSTOMIZE_REBALANCE :
      case Routes.MANAGE :
        view = customizeView;
        break;
    }

    if (noOrders) {
      view = noOrdersView;
    }

    const tooltipView = fundsAvailable ? null : tooltip;
    return (
      <View>
        <View>
          <Divider/>
          {view}
        </View>
        {tooltipView}
      </View>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    paddingTop: 16,
    paddingBottom: getBottomBarPaddingBottom()
  },
  leftButton: {
    backgroundColor: 'white',
    paddingVertical: 12,
    width: (width * 0.5) - 24,
    justifyContent: 'center',
    marginRight: 8,
    alignItems: 'center',
  },
  confirmButton: {
    backgroundColor: 'rgb(31, 122, 224)',
    paddingVertical: 12,
    width: (width * 0.5) - 24,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 8,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(31, 122, 224)',
  },
  reset: {
    color: Styles.RED,
    fontSize: 14,
    fontWeight: 'bold'
  },
  ignore: {
    color: 'rgb(216, 47, 68)',
    fontSize: 14,
    fontWeight: 'bold'
  },
  confirm: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  noOrdersContainer: {
    height: 175,
    padding: 16,
    alignItems: 'center'
  },
  messageContainer: {
    flexDirection: 'row',
    marginBottom: 24,
  },
  errorMessageContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  icon: {
    height: 24,
    width: 24,
    marginRight: 8,
    marginLeft: 16,
    tintColor: 'rgb(31, 122, 224)',
  },
  errorIcon: {
    height: 24,
    width: 24,
    marginRight: 8,
    padding: 2,
  },
  text: {
    fontSize: 14,
    lineHeight: 21,
    color: 'rgb(47, 54, 63)',
    marginRight: 16,
  },
  noOrdersButton: {
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48,
    width: 340,
  },
  fundsButton: {
    color: 'rgb(31, 122, 224)',
    fontSize: 14,
    fontWeight: 'bold',
    marginLeft: 32,
  },
  tooltipContainer: {
    borderRadius: 4,
    borderColor: 'rgba(255, 89, 66, 0.7)',
    borderWidth: 0.7,
    height: 96,
    padding: 16,
    width: 330,
    position: 'absolute',
    bottom: 68,
    right: 16,
    backgroundColor: 'white'
  }
});
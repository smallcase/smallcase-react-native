import React, {PureComponent} from "react";
import {
  Image,
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  SectionList,
  FlatList,
  Animated,
  interpolate,
  Easing, TouchableWithoutFeedback, ScrollView
} from "react-native";
import Search from "./Search";
import {
  fetchSimilarStocks, searchStocks,
} from "./ManageService";
import Divider from "../../component/Divider";
import SectorCard from "./SectorCard";
import TouchHighlight from "../../component/TouchHighlight";

export default class SearchPage extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      segments: [],
      accordionIndex: -1,
      searchStarted: false,  // input length > 0 ? true : false
      stocksSimilarToLastAdded: null,
    }
  }

  componentDidMount() {
    const {jwt, csrf, stocks, lastAddedStock} = this.props;

    fetchSimilarStocks(jwt, csrf, stocks)
      .then((response) => {

        response.forEach(sector => {
          sector.spinValue = new Animated.Value(0);
          sector.expanded = false;
          sector.startDeg = '180deg';
          sector.endDeg = '0deg';

          if (lastAddedStock) {
            if (lastAddedStock.sidInfo.sector === sector.sector) {
              let newStocks = [];
              sector.stocks.forEach(stock => newStocks.push(Object.assign({}, stock)));
              this.setState({stocksSimilarToLastAdded: newStocks});
            }
          }
        });

        this.setState({segments: response});
      })
      .catch((error) => console.log(error));
  }

  keyExtractor = (item, index) => {
    return index;
  };

  renderListHeader = () => {
    const {lastAddedStock} = this.props;

    let title = "";
    if (lastAddedStock) {
      title = "Based on"
    } else {
      title = "Similar to"
    }

    return (
      <View>
        <Text style={styles.listHeader}>{title} current constituents</Text>
      </View>
    );
  };

  setAccordionIndex = (index, currentSelectedIndex) => {
    const segments = [...this.state.segments];
    segments.forEach((sector, id) => {
      sector.expanded = id === index;
      if (id === currentSelectedIndex) {
        sector.startDeg = '0deg';
        sector.endDeg = '180deg';
      }
    });
    this.setState({accordionIndex: index, segments});
  };

  getSectorDetailsArrow = (sector) => {
    const spin = sector.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [sector.startDeg, sector.endDeg]
    });

    return (
      <Animated.Image style={[styles.stockDetailsArrow, {transform: [{rotate: spin}]}]}
                      source={require('../../../assets/chevronUp.png')}/>
    );
  };

  onSearch = (text) => {
    if (text === "") {
      this.setState({searchStarted: false, searchResults: undefined});
    } else {
      const {jwt, csrf} = this.props;

      searchStocks(jwt, csrf, text)
        .then(response => {
          this.setState({searchResults: response.data.searchResults, searchStarted: true});
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  mapThroughStocks = (stocks) => {
    if (!stocks) return;
    const {addStock} = this.props;
    const {searchStarted} = this.state;

    let symbol, name;
    return stocks.map((stock, index) => {
      if (searchStarted) {
        symbol = stock.stock.info.ticker;
        name = stock.stock.info.name;
      } else {
        symbol = stock.sid;
        name = stock.sidInfo.name;
      }
      return (
        <TouchHighlight key={index} onClick={addStock.bind(this, stock, false)}>
          <View style={styles.item} >
            <View style={{flexDirection: 'row'}}>
              <View style={styles.itemText}>
                <Text style={styles.stockSymbol}>{symbol}</Text>
                <Text style={styles.stockName}>{name}</Text>
              </View>
              <Image style={styles.itemButton} source={require('../../../assets/add.png')}/>
            </View>
            <Divider/>
          </View>
        </TouchHighlight>
      );
    });
  };


  renderSectors = ({item, index}) => {
    const {stocks} = this.props;
    const {accordionIndex} = this.state;

    return (
      <SectorCard
        item={item}
        index={index}
        getSectorDetailsArrow={this.getSectorDetailsArrow}
        stocks={stocks}
        renderSector={this.mapThroughStocks}
        setAccordionIndex={this.setAccordionIndex}
        currentSelected={accordionIndex}
      />
    )
  };

  renderItem = (item, index) => {
    if (!item) return null;

    const {addStock} = this.props;

    return (
      <TouchHighlight onClick={addStock.bind(this, item.item, true)}>
        <View style={styles.item}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.itemText}>
              <Text style={styles.stockSymbol}>{item.item.sid}</Text>
              <Text style={styles.stockName}>{item.item.stock.info.name}</Text>
            </View>
            <Image style={styles.itemButton} source={require('../../../assets/add.png')}/>
          </View>
          <Divider/>
        </View>
      </TouchHighlight>
    )
  };

  render() {
    const {onCancel, lastAddedStock} = this.props;
    const {segments, searchResults, searchStarted, stocksSimilarToLastAdded} = this.state;

    let stickyView = (
      <View>
        <Text style={styles.listHeader}>Because you added
          "{lastAddedStock ? lastAddedStock.sidInfo.ticker : null}"</Text>
        {stocksSimilarToLastAdded && !searchStarted ? this.mapThroughStocks(stocksSimilarToLastAdded) : null}
      </View>
    );

    let similarView = (
      <View style={{flex: 1}}>
        {lastAddedStock ? stickyView : null}
        <FlatList
          style={{flex: 1, backgroundColor: 'white'}}
          data={segments}
          extraData={this.state}
          renderItem={this.renderSectors}
          keyExtractor={this.keyExtractor}
          ListHeaderComponent={this.renderListHeader}/>
      </View>
    );

    let searchingView = (
      <View>
        <FlatList
          style={{backgroundColor: 'white'}}
          data={searchResults}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}/>
      </View>
    );

    let view = searchStarted ? searchingView : similarView;

    return (
      <View style={{flex: 1}}>
        <View style={styles.top}/>
        <View style={styles.searchContainer}>
          <Search
            searchStyles={styles.searchBar}
            autoFocus={true}
            placeholder={"Add stocks by name or ticker"}
            onChange={this.onSearch}
          />
          <TouchableHighlight style={{justifyContent: 'center', alignSelf: 'flex-end', marginBottom: 10}}
                              onPress={onCancel} underlayColor={'rgb(255, 255, 255)'}>
            <Text style={styles.cancel}>Cancel</Text>
          </TouchableHighlight>
        </View>
        <ScrollView>
          {view}
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  top: {
    height: 20,
    flexDirection: 'row',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 12
  },
  searchBar: {
    height: 36,
    width: 274,
  },
  cancel: {
    color: 'rgb(31, 122, 224)',
    fontSize: 16,
    textAlign: 'left',
  },
  listHeader: {
    fontSize: 18,
    color: 'rgb(47, 54, 63)',
    marginLeft: 16,
    marginBottom: 8,
    marginTop: 24,
    fontWeight: 'bold',
  },
  stockDetailsArrow: {
    width: 24,
    height: 24,
    marginRight: 16,
  },
  stockSymbol: {
    marginBottom: 6,
    marginTop: 12,
    fontSize: 14,
    fontWeight: 'bold',
    letterSpacing: 0.2,
    color: 'rgb(47, 54, 63)',
  },
  stockName: {
    fontSize: 12,
    color: 'rgb(129, 135, 140)',
    marginBottom: 12
  },
  itemText: {
    flex: 1,
  },
  item: {
    marginLeft: 36,
  },
  itemButton: {
    height: 24,
    width: 24,
    marginRight: 16,
    marginTop: 24,
  },
});

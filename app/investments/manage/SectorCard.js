import React, {PureComponent} from "react";
import {
  Image,
  Text,
  View,
  StyleSheet,
  Animated,
  Easing,
  TouchableWithoutFeedback
} from "react-native";
import {getColorByIndex, getExistingSimilarStocks} from "./ManageService";
import Divider from "../../component/Divider";
import ExpandCollapse from "../../component/ExpandCollapse";
import TouchHighlight from "../../component/TouchHighlight";

export default class SectorCard extends PureComponent {

  // get section details and map through elements to render view,
  toggleSector = (sector, index) => {
    const {setAccordionIndex, currentSelected} = this.props;

    // Sector expanded still false even after first expand
    if (currentSelected === index) {
      setAccordionIndex(-1, -1);
    } else {
      setAccordionIndex(index, currentSelected);
    }

    if (sector.expanded) {
      sector.startDeg = '180deg';
      sector.endDeg = '0deg';
    } else {
      sector.startDeg = '0deg';
      sector.endDeg = '180deg';
    }
    sector.spinValue.setValue(0);
    Animated.timing(
      sector.spinValue, {
        toValue: 1,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();

    this.setState({expanded: sector.expanded});
  };

  renderPrimarySectorContent = (item, index) => {
    const {stocks, getSectorDetailsArrow} = this.props;
    let detailsArrow = (<View style={styles.stockDetailsArrow}/>);
    if (item) detailsArrow = getSectorDetailsArrow(item);
    let constituents = getExistingSimilarStocks(stocks, item.sector);

    return (
      <TouchHighlight onClick={() => this.toggleSector(item, index)}>
        <View>
          <View style={{flexDirection: 'row', marginTop: 16,}}>
            <View style={[styles.circle, getColorByIndex(index)]}/>
            <View style={{flex: 1}}>
              <Text style={styles.sectionHeader}>{item.sector}</Text>
              <Text style={styles.sectionSubtitle}>You currently have {constituents}</Text>
            </View>
            {detailsArrow}
          </View>
          {item.expanded ? null : <Divider/>}
        </View>
      </TouchHighlight>
    );
  };

  render() {
    const {item, index, renderSector} = this.props;

    return (
      <View>
        {this.renderPrimarySectorContent(item, index)}
        <ExpandCollapse value={item.expanded} style={{backgroundColor: 'white'}}>
          {renderSector(item.stocks)}
        </ExpandCollapse>
      </View>
    );
  }

};

const styles = StyleSheet.create({
  sectionHeader: {
    fontSize: 16,
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
    marginBottom: 6,
  },
  sectionSubtitle: {
    fontSize: 12,
    color: 'rgb(129, 135, 140)',
    marginBottom: 16,
    lineHeight: 12 * 1.57
  },
  stockSymbol: {
    marginBottom: 6,
    marginTop: 12,
    fontSize: 14,
    fontWeight: 'bold',
    letterSpacing: 0.2,
    color: 'rgb(47, 54, 63)',
  },
  stockName: {
    fontSize: 12,
    color: 'rgb(129, 135, 140)',
    marginBottom: 12
  },
  itemText: {
    flex: 1,
  },
  item: {
    marginLeft: 36,
  },
  itemButton: {
    height: 24,
    width: 24,
    marginRight: 16,
    marginTop: 24,
  },
  circle: {
    height: 12,
    width: 12,
    marginRight: 8,
    marginLeft: 16,
    marginTop: 4,
    borderRadius: 100,
  },
});
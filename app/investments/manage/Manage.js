import React, {PureComponent} from "react";
import {Actions} from "react-native-router-flux";
import {
  Image,
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  TextInput,
  TouchableWithoutFeedback,
  AsyncStorage
} from "react-native";
import Search from "./Search";
import {connect} from "react-redux";
import CustomizeActionSheet from "../components/CustomizeActionSheet";
import {KeyboardAwareFlatList} from "react-native-keyboard-aware-scroll-view";
import Divider from "../../component/Divider";
import Navbar from "../../component/Navbar";
import SearchPage from "./SearchPage";
import {
  OrderLabel,
  OrderType,
  Routes,
  SnackBarTime,
  Styles,
  AnalyticsSource
} from "../../util/Utility";
import SnackBar from "../../component/SnackBar";
import {checkMarketStatus} from "../../helper/MarketHelper";
import {calculateTotalAmount, getUpdatedStocks} from "../../order/OrderService";
import {getUserFunds} from "../../user/UserService";
import StockWidget from '../../component/StockWidget';
import {BROKER_LOGIN} from "../../api/ApiRoutes";
import * as types from "../../constants/Constants";
import {getUserDetails} from "../../user/UserActions";
import {getJwt} from "../../helper/LoginHelper";
import {userLoggedIn} from "../../onboarding/LandingActions";
import * as Config from "../../../Config";
import TouchHighlight from "../../component/TouchHighlight";
import {MAX_STOCK_COUNT} from "../../constants/Constants";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {SnackBarMessages} from "../../util/Strings";

class Manage extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      stocks: [],
      fundsAvailable: true,
      searching: false,
      lastAddedStock: false,
      stockWidget: false,
      fetchingJwt: false
    };

    this.initialStocks = [];
    this.changeableStocks = [];

    props.smallcaseDetails.stocks.forEach(stock => {
      this.initialStocks.push(Object.assign({}, stock));
      this.changeableStocks.push(Object.assign({}, stock));
    });
  }

  componentWillReceiveProps(newProps) {
    if (newProps.loginReducer && (newProps.loginReducer.requestToken !== this.props.loginReducer.requestToken)) {
      this.fetchAuthDetails(newProps);
    }
  }

  fetchAuthDetails = (newProps) => {
    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    this.setState({fetchingJwt: true});
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      this.setState({fetchingJwt: false});
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {
        });
      } else {
        this.snackbar.show({
          title: SnackBarMessages.LOGIN_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    });
  }

  componentDidMount() {
    this.setState({stocks: this.changeableStocks});
    AnalyticsManager.track('Viewed Manage smallcase',this.props.analytics,[AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
  }

  showStockWidget = (sid) => {
    if (!sid) return;

    this.setState({stockWidget: true, sid});
  };

  onStockWidgetClosed = () => this.setState({stockWidget: false});

  isStockCountGreaterThanMax = (sid, changeValue) => {
    if (changeValue === 0) return false;

    const {stocks} = this.state;
    const validStocks = stocks.filter(stock => stock.newShares > 0 && stock.sid !== sid);

    return validStocks.length >= MAX_STOCK_COUNT;
  }

  onChange = (changedValue, item) => {
    if (changedValue < 0) {
      // user should not sell more than they hold
      return;
    }

    if (isNaN(changedValue)) {
      // only allow entering a valid number
      return;
    }

    if (this.isStockCountGreaterThanMax(item.sid, changedValue)) {
      this.snackbar.show({
        title: SnackBarMessages.EXCEEDED_MAX_STOCKS,
        duration: SnackBarTime.LONG
      });
      return;
    }

    let newStocks = [...this.state.stocks];

    newStocks.forEach((stock) => {
      if (stock.sid === item.sid) {
        stock.newShares = Math.round(changedValue);
      }
    });
    this.setState({stocks: newStocks});
  };

  resetChanges = () => {
    let newStocks = [];
    this.initialStocks.forEach(stock => {
      newStocks.push(Object.assign({}, stock));
    });
    this.setState({stocks: newStocks, lastAddedStock: false});
  };

  onPressConfirm = () => {
    const {stocks} = this.state;
    const {smallcaseDetails} = this.props

    let newStocks = [];
    let stocksGreaterThanZeroShares = 0;
    stocks.forEach(stock => {
      let newStock = Object.assign({}, stock);
      if (isNaN(stock.newShares)) {
        newStock.shares = 0;
        newStock.transactionType = OrderType.SELL;
        newStocks.push(newStock);
      } else if (stock.newShares !== stock.oldShares) {
        newStock.shares = Math.abs(newStock.newShares - newStock.oldShares);
        newStock.transactionType = (newStock.newShares - newStock.oldShares) > 0 ? OrderType.BUY : OrderType.SELL;
        newStocks.push(newStock);
      }

      if (newStock.newShares > 0) stocksGreaterThanZeroShares++;
    });

    if (stocksGreaterThanZeroShares < 2) {
      this.snackbar.show({
        title: SnackBarMessages.AT_LEAST_TWO_STOCKS,
        duration: SnackBarTime.LONG,
      });
      return;
    }

    if (newStocks.length === 0) {
      this.snackbar.show({
        title: SnackBarMessages.NO_MODIFICATIONS,
        duration: SnackBarTime.LONG,
      });
    } else {
      const {jwt, csrf} = this.props;

      checkMarketStatus(jwt, csrf)
        .then(isOpen => {
          if (isOpen) {
            getUpdatedStocks(jwt, csrf, newStocks)
              .then(stocks => {
                const amount = calculateTotalAmount(stocks);
                if (amount > 0) {
                  getUserFunds(jwt, csrf)
                    .then(funds => {
                      if (amount > funds) {
                        this.setState({fundsAvailable: false});
                      } else {
                        this.openReviewOrder(newStocks);
                      }
                    })
                    .catch((error) => {
                      if (error.data && error.data.status === 401) {
                        Actions.push(Routes.LOGIN);
                      }
                    });
                } else {
                  this.openReviewOrder(newStocks);
                }
              })
          } else {
            this.snackbar.show({
              title: SnackBarMessages.MARKET_CLOSED,
              duration: SnackBarTime.LONG,
            });
          }
        })
        .catch(error => {
          console.log(error)
        });
    }
    AnalyticsManager.track('Confirm Manage Changes',
      {
        noOfChanges : newStocks.length,
        iscid: smallcaseDetails.iscid,
        scid: smallcaseDetails.scid ,
        smallcaseName: smallcaseDetails.smallcaseName,
        smallcaseSource : smallcaseDetails.source,
        orderType : OrderLabel.MANAGE,
        smallcaseType: 'N/A',
      },
      [AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL]);

  };

  openReviewOrder = (newStocks) => {
    const {scid, iscid, smallcaseName, source, smallcaseImage} = this.props.smallcaseDetails;

    const newSmallcaseDetails = {
      scid: scid,
      iscid: iscid,
      smallcaseName: smallcaseName,
      source: source,
      smallcaseImage: smallcaseImage,
      stocks: newStocks,
      orderType: OrderLabel.MANAGE,
    };

    Actions.push(Routes.REVIEW_ORDER, {
      smallcaseDetails: newSmallcaseDetails,
      type: OrderLabel.MANAGE,
      repairing: false,
      analytics: {
        accessedFrom: 'Manage Order',
        sectionTag: 'N/A',
      }
    });
  };

  onPressSearch = () => {
    this.setState({searching: true});
  };

  onClose = () => {
    Actions.pop();
  };

  onCancel = () => {
    this.setState({searching: false});
  };

  addStock = (stock, handleStructure) => {
    const {searching, stocks} = this.state;

    if (this.isStockCountGreaterThanMax(stock.sid, 1)) {
      this.snackbar.show({
        title: SnackBarMessages.EXCEEDED_MAX_STOCKS,
        duration: SnackBarTime.LONG,
      });
      this.setState({searching: false});
      return;
    }

    if (handleStructure) {
      stock.sidInfo = stock.stock.info;
    }

    if (searching) {
      for (let i = 0; i < stocks.length; i++) {
        let existingStock = stocks[i];
        if (existingStock.sid === stock.sid) {
          this.snackbar.show({
            title: stock.sidInfo.ticker + ' already exists in your smallcase',
            duration: SnackBarTime.LONG,
          });
          this.setState({searching: false});
          return;
        }
      }

      stock.newShares = 1;
      stock.oldShares = 0;
      let newStocks = this.state.stocks.slice(0);
      newStocks.unshift(stock);
      this.setState({stocks: newStocks, searching: false, lastAddedStock: stock});
    }
  };

  renderItem = ({item, index}) => {
    let changeValue = item.newShares - item.oldShares;
    let change = (changeValue) > 0 ? "+" + (changeValue) : (changeValue);
    let color;
    if (changeValue === 0) {
      color = {color: 'rgb(47, 54, 63)'}
    } else {
      color = change > 0 ? {color: 'rgb(25, 175, 85)'} : {color: 'rgb(216, 47, 68)'};
    }

    return (
      <View>
        <Divider dividerStyles={{marginLeft: index === 0 ? 0 : 16}}/>
        <View style={styles.row}>
          <TouchableWithoutFeedback onPress={this.showStockWidget.bind(this, item ? item.sid : '')}>
            <View style={styles.stockNameContainer}>
              <Text style={styles.text} numberOfLines={1} ellipsizeMode='tail'>{item.sidInfo.name}</Text>
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.group}>
            <TouchHighlight styles={styles.sharesActionContainer}
                            onClick={() => this.onChange(item.newShares - 1, item)}>
              <Image source={require('../../../assets/minus.png')} style={styles.icon}/>
            </TouchHighlight>
            <View style={styles.box}>
              <TextInput
                multiline={true}
                style={styles.input}
                value={String(item.newShares)}
                keyboardType={"numeric"}
                onChangeText={(change) => this.onChange(change, item)}
              />
            </View>
            <TouchHighlight styles={styles.sharesActionContainer}
                            onClick={() => this.onChange(item.newShares + 1, item)}>
              <Image source={require('../../../assets/add.png')} style={styles.icon}/>
            </TouchHighlight>
          </View>
          <Text style={[styles.changeText, color]}>{change}</Text>
        </View>
      </View>
    );
  };

  keyExtractor = (item, index) => {
    return index;
  };

  renderHeader = () => {
    return (
      <View>
        <View style={{flexDirection: 'row', marginHorizontal: 16, marginVertical: 12}}>
          <Text style={{textAlign: 'left', flex: 6, color: Styles.DESCRIPTION_TEXT_COLOR}}>Stock</Text>
          <Text style={{textAlign: 'center', flex: 4, color: Styles.DESCRIPTION_TEXT_COLOR}}>Shares</Text>
          <Text style={{textAlign: 'right', flex: 2, color: Styles.DESCRIPTION_TEXT_COLOR}}>Change</Text>
        </View>
      </View>
    );
  };

  onFundsAvailableClicked = () => this.setState({fundsAvailable: true});

  render() {
    const {jwt, csrf} = this.props;
    const {stocks, searching, lastAddedStock, fundsAvailable, sid, stockWidget, fetchingJwt} = this.state;

    let titleBar = searching ? null : <Navbar title={"Manage"} showLeft={true} leftfunc={this.onClose}/>;

    let searchPage = (
      <SearchPage jwt={jwt} csrf={csrf} lastAddedStock={lastAddedStock} stocks={stocks} onCancel={this.onCancel}
                  addStock={this.addStock}/>
    );

    let flatList = (
      <View style={{flex: 1}}>
        <Search
          searchStyles={{height: 48, marginBottom: 24}}
          onFocus={this.onPressSearch}
          placeholder={"Add stocks by name or ticker"}
        />
        <KeyboardAwareFlatList
          style={styles.list}
          data={stocks}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          extraData={this.state}
          extraScrollHeight={80}
          ListHeaderComponent={this.renderHeader}
          enableAutomaticScroll={true}
        />
        <CustomizeActionSheet
          fundsAvailable={fundsAvailable}
          leftFunc={this.resetChanges}
          rightFunc={this.onPressConfirm}
          disabled={fetchingJwt}
          scene={Routes.MANAGE}
          onFundsAvailableClicked={this.onFundsAvailableClicked}
        />
        <StockWidget
          showWidget={stockWidget}
          stockWidgetClosed={this.onStockWidgetClosed}
          csrf={csrf}
          jwt={jwt}
          sid={sid}
        />
      </View>
    );

    let view = searching ? searchPage : flatList;

    if (stocks.length === 0) {
      return null;
    }

    return (
      <View style={styles.container}>
        {titleBar}
        {view}
        <SnackBar onRef={ref => this.snackbar = ref}/>
      </View>
    );
  }

}

const styles = StyleSheet.create({

  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  list: {
    flex: 1,
  },
  box: {
    borderColor: 'rgb(221, 224, 228)',
    borderWidth: 0.7,
    backgroundColor: 'white',
    height: 28,
    width: 60,
  },
  row: {
    flexDirection: 'row',
    height: 48,
    alignItems: 'center',
    marginHorizontal: 16
  },
  text: {
    fontSize: 14,
    color: 'rgb(47, 54, 63)',
  },
  stockNameContainer: {
    flex: 5,
    marginRight: 12,
    backgroundColor: 'white',
    height: 48,
    justifyContent: 'center'
  },
  group: {
    flexDirection: 'row',
    flex: 4,
    alignItems: 'center',
  },
  changeText: {
    fontSize: 14,
    flex: 2,
    textAlign: 'right'
  },
  input: {
    textAlign: 'right',
    paddingRight: 6,
    height: 27,
    width: 59,
    backgroundColor: 'white',
    fontWeight: 'bold'
  },
  icon: {
    height: 18,
    width: 18,
  },
  sharesActionContainer: {
    height: 48,
    paddingHorizontal: 8,
    marginHorizontal: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  loginReducer: state.loginReducer
});

module.exports = connect(mapStateToProps)(Manage);
import Api from "../../api/Api";
import {STOCKS_SEARCH, STOCKS_SIMILAR} from "../../api/ApiRoutes";

export const searchStocks = (jwt, csrf, string) => {

  let params = "text=" + string;

  return new Promise((resolve, reject) => {
    Api.get({route: STOCKS_SEARCH, jwt, csrf, params})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

export const fetchSimilarStocks = (jwt, csrf, stocks) => {
  let params = "";
  stocks.forEach(stock => params += ("stocks=" + stock.sid + "&"));

  return new Promise((resolve, reject) => {
    Api.get({route: STOCKS_SIMILAR, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const getExistingSimilarStocks = (existingStocks, sector) => {
  let constituents = "";
  let count = 0;
  for (let i = 0; i < existingStocks.length; i++) {
    let stock = existingStocks[i];
    if (stock.sidInfo.sector === sector) {
      if (count < 3) {
        constituents += stock.sidInfo.ticker +  ", ";
        count++;
      } else {
        constituents += ("+" + ((existingStocks.length - i) + " more, "));
        break;
      }
    }
  }
  return constituents.substring(0, constituents.length - 2);
};

export const getColorByIndex = (index) => ({backgroundColor: colors[index % 12]});

const colors = ["#42bb5c", "#31738B", "#13c2ce", "#fc7a21", "#164a74", "#ffc000", "#8f74f3", "#8056b6", "#17c490", "#ff4b7b", "#009ddc", "#b92b27", "#9f7aff"];

import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image, TextInput, Dimensions, Animated, TouchableWithoutFeedback
} from 'react-native';
import SmallcaseButton from "../../component/SmallcaseButton";
import Interactable from 'react-native-interactable';
import {getScreenSize} from "../../util/Utility";


const Screen = getScreenSize();

export class ConfirmPopup extends PureComponent{

  constructor(props){
    super(props);

    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';

  }

  componentWillReceiveProps = (newProps) => {
    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup) {
      this.confirmPopup.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.confirmPopup.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.confirmPopupClosed();
    } else {
      this.blurPointerEvent = 'auto';
    }

  };

  dismissConfirmPopup = () => {
    this.confirmPopup.snapTo({index: 1});
  };

  keepCustomizingPressed = () => {
    this.dismissConfirmPopup();
  };

  render(){

    let popup = (
        <View style={styles.container}>
          <Text style={styles.title}>Confirm Changes?</Text>
          <Text style={styles.text}>You are editing the Rebalance Update. Once rebalanced, your smallcase’s stocks, weights, & returns might not match those of the original smallcase.</Text>
          <View style={styles.buttonRow}>
            <SmallcaseButton buttonStyles={styles.continueButton} onClick={this.keepCustomizingPressed}>
              <Text style={styles.continueText}>Keep Customising</Text>
            </SmallcaseButton>
            <SmallcaseButton buttonStyles={styles.confirmButton} onClick={this.props.onConfirmPressed}>
              <Text style={styles.confirmText}>Confirm Changes</Text>
            </SmallcaseButton>
          </View>
        </View>
    );

    return(
        <View style={styles.panelContainer} pointerEvents={'box-none'}>
          <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent}
                                    onPress={() => {
                                      this.dismissConfirmPopup()
                                    }}>
            <Animated.View
                pointerEvents={this.blurPointerEvent}
                style={[styles.panelContainer, {
                  backgroundColor: 'black',
                  opacity: this.deltaY.interpolate({
                    inputRange: [Screen.height - 300, Screen.height],
                    outputRange: [0.5, 0],
                    extrapolateRight: 'clamp'
                  })
                }]}/>
          </TouchableWithoutFeedback>
          <Interactable.View
              ref={ele => this.confirmPopup = ele}
              verticalOnly={true}
              snapPoints={[{y: Screen.height - 300}, {y: Screen.height}]}
              onSnap={this.handelSnap.bind(this)}
              boundaries={{top: Screen.height - 320}}
              initialPosition={{y: Screen.height}}
              animatedNativeDriver={true}
              animatedValueY={this.deltaY}>

            {popup}
          </Interactable.View>
        </View>
    );
  }

}

const styles = StyleSheet.create({

  container : {
    height : 300,
    backgroundColor : 'white',
    paddingVertical: 32,
    paddingHorizontal: 24,
  },
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  title : {
    color : 'rgb(47, 54, 63)',
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  text : {
    color : 'rgb(83, 91, 98)',
    fontSize: 14,
    lineHeight : 20,
    marginBottom : 32,
    marginTop : 8,
  },
  continueText : {
    color : 'rgb(31, 122, 224)',
    fontSize : 14,
    fontWeight: 'bold',
  },
  confirmText : {
    color : 'white',
    fontSize : 14,
    fontWeight: 'bold',
  },
  buttonRow : {
    flexDirection: 'row',
    justifyContent : 'space-between',
    marginBottom : 24,
  },
  continueButton : {
    height : 48,
    width : 156,
    backgroundColor : 'white',
    borderRadius : 4,
    borderWidth: 0.7,
    borderColor : 'rgb(31, 122, 224)'
  },
  confirmButton : {
    height : 48,
    width : 156,
    backgroundColor : 'rgb(31, 122, 224)',
    borderRadius : 4,
  }

});
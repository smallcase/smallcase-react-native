import React, {PureComponent} from 'react';
import {Animated, Text, TouchableWithoutFeedback, View} from 'react-native';
import SmallcaseButton from "../../component/SmallcaseButton";
import {Actions} from "react-native-router-flux";
import Interactable from 'react-native-interactable';
import {getScreenSize, Styles} from "../../util/Utility";
import {MediaQueryStyleSheet} from "react-native-responsive";

const Screen = getScreenSize();


export class CancelPopup extends PureComponent {

  constructor(props) {
    super(props);

    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
  }

  componentWillReceiveProps = (newProps) => {
    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup) {
      this.cancelPopup.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.cancelPopup.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.cancelPopupClosed();
    } else {
      this.blurPointerEvent = 'auto';
    }

  };

  dismissCancelPopup = () => {
    this.cancelPopup.snapTo({index: 1});
  };

  leavePressed = () => {
    Actions.pop();
  };

  stayPressed = () => {
    this.dismissCancelPopup();
  };

  render() {
    let popup = (
      <View style={styles.container}>
        <Text style={styles.title}>Are you sure you want to cancel?</Text>
        <Text style={styles.text}>All your changes will not be saved if you leave. To stay on the page, tap on “Stay”,
          or if you want to discard the changes you made, tap on “Discard”.</Text>
        <View style={styles.buttonRow}>
          <SmallcaseButton buttonStyles={styles.stayButton} onClick={this.stayPressed}>
            <Text style={styles.stay}>Stay</Text>
          </SmallcaseButton>
          <SmallcaseButton buttonStyles={styles.leaveButton} onClick={this.leavePressed}>
            <Text style={styles.leave}>Discard</Text>
          </SmallcaseButton>
        </View>
      </View>
    );

    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback
          pointerEvents={this.blurPointerEvent}
          onPress={() => {
            this.dismissCancelPopup()
          }}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - 300, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.cancelPopup = ele}
          verticalOnly={true}
          snapPoints={[{y: Screen.height - 300}, {y: Screen.height}]}
          onSnap={this.handelSnap.bind(this)}
          boundaries={{top: Screen.height - 320}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          animatedValueY={this.deltaY}>
          {popup}
        </Interactable.View>
      </View>
    );
  }

}

const styles = MediaQueryStyleSheet.create({
  container: {
    height: 300,
    backgroundColor: 'white',
    paddingVertical: 32,
    paddingHorizontal: 24,
  },
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  title: {
    color: 'rgb(47, 54, 63)',
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  text: {
    color: 'rgb(83, 91, 98)',
    fontSize: 14,
    lineHeight: 20,
    marginBottom: 32,
    marginTop: 8,
  },
  buttonRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  leaveButton: {
    paddingVertical: 12,
    width: (Screen.width * 0.5) - 32,
    marginLeft: 8,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: Styles.RED_ALPHA,
    backgroundColor: 'white',
  },
  stayButton: {
    paddingVertical: 12,
    width: (Screen.width * 0.5) - 32,
    marginRight: 8,
    borderRadius: 4,
    borderColor: Styles.BLUE_ALPHA,
    borderWidth: 0.5
  },
  leave: {
    color: 'rgb(216, 47, 68)',
    fontSize: 14,
    fontWeight: 'bold'
  },
  stay: {
    color: Styles.BLUE,
    fontSize: 14,
    fontWeight: 'bold'
  }
});
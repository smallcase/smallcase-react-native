import React, {PureComponent} from 'react';
import {Animated, Text, TouchableWithoutFeedback, View} from 'react-native';
import SmallcaseButton from "../../component/SmallcaseButton";
import Interactable from 'react-native-interactable';
import {getScreenSize, Styles} from "../../util/Utility";
import {MediaQueryStyleSheet} from "react-native-responsive";
import {IPHONE_FIVE_AND_SMALLER} from "../../constants/Constants";


const Screen = getScreenSize();

export class SkipPopup extends PureComponent {

  constructor(props) {
    super(props);

    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
  }

  componentWillReceiveProps = (newProps) => {
    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup) {
      this.skipPopup.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.skipPopup.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.skipPopupClosed();
    } else {
      this.blurPointerEvent = 'auto';
    }
  };

  dismissSkipPopup = () => this.skipPopup.snapTo({index: 1});

  cancelPressed = () => this.dismissSkipPopup();

  render() {
    const popup = (
      <View style={styles.container}>
        <Text style={styles.title}>Skip rebalance update?</Text>
        <Text style={styles.text}>You are skipping Quarterly Update: Stocks Change for this smallcase. You will not be
          able to apply this rebalance again. Are you sure you want to skip this update?</Text>
        <View style={styles.buttonRow}>
          <SmallcaseButton buttonStyles={styles.continueButton} onClick={this.cancelPressed}>
            <Text style={styles.continueText}>Cancel</Text>
          </SmallcaseButton>
          <SmallcaseButton buttonStyles={styles.skipButton} onClick={this.props.skipPressed}>
            <Text style={styles.skipText}>Skip</Text>
          </SmallcaseButton>
        </View>
      </View>
    );

    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent} onPress={() => this.dismissSkipPopup()}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - 300, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.skipPopup = ele}
          verticalOnly={true}
          snapPoints={[{y: Screen.height - 300}, {y: Screen.height}]}
          onSnap={this.handelSnap.bind(this)}
          boundaries={{top: Screen.height - 320}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          animatedValueY={this.deltaY}>
          {popup}
        </Interactable.View>
      </View>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  container: {
    height: 300,
    backgroundColor: 'white',
    paddingVertical: 32,
    paddingHorizontal: 24,
  },
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  title: {
    color: 'rgb(47, 54, 63)',
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  text: {
    color: 'rgb(83, 91, 98)',
    fontSize: 14,
    lineHeight: 20,
    marginBottom: 32,
    marginTop: 8
  },
  continueText: {
    color: 'rgb(31, 122, 224)',
    fontSize: 14,
    fontWeight: 'bold',
  },
  skipText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
  buttonRow: {
    flexDirection: 'row',
    marginBottom: 24,
  },
  continueButton: {
    paddingVertical: 12,
    width: (Screen.width * 0.5) - 32,
    marginRight: 8,
    backgroundColor: 'white',
    borderRadius: 4,
    borderWidth: 0.7,
    borderColor: 'rgb(31, 122, 224)'
  },
  skipButton: {
    paddingVertical: 12,
    width: (Screen.width * 0.5) - 32,
    marginLeft: 8,
    backgroundColor: Styles.RED_FAILURE,
    borderRadius: 4,
  }
});
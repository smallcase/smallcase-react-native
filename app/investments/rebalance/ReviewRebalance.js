import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SectionList,
  ActivityIndicator,
  Image,
  ActionSheetIOS,
  TouchableWithoutFeedback,
  TouchableHighlight,
  AsyncStorage
} from 'react-native';
import OrderTitle from "../../order/components/OrderTitle";
import {
  OrderLabel,
  OrderType,
  Routes,
  SnackBarTime,
  Styles,
  AnalyticsSource
} from "../../util/Utility";
import SmallcaseButton from "../../component/SmallcaseButton";
import {getRebalanceChanges, noOrdersNeeded, skipRebalance} from "./RebalanceService";
import {connect} from "react-redux";
import CustomizeActionSheet from "../components/CustomizeActionSheet";
import Divider from "../../component/Divider";
import {Actions} from "react-native-router-flux"
import {SkipPopup} from "./SkipPopup";
import {calculateTotalAmount} from "../../order/OrderService";
import {getUserFunds} from "../../user/UserService";
import {checkMarketStatus, getUpdatedStocks} from "../../helper/MarketHelper";
import SnackBar from "../../component/SnackBar";
import StockWidget from '../../component/StockWidget';
import {BROKER_LOGIN} from "../../api/ApiRoutes";
import * as types from "../../constants/Constants";
import {getJwt} from "../../helper/LoginHelper";
import {userLoggedIn} from "../../onboarding/LandingActions";
import * as Config from "../../../Config";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {getUserDetails} from "../../user/UserActions";
import {getSmallcaseDetailsLite} from "../details/InvestmentDetailsService";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";

class ReviewRebalance extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      skipPopupShown: false,
      noOrders: false,
      fundsAvailable: true,
      showShares: true,
      stockWidget: false,
      fetchingJwt: false,
      rebalanceInfo: null
    }
  }

  componentDidMount() {
    const {jwt, csrf, iscid, user, analytics} = this.props;
    AnalyticsManager.track('Viewed Rebalance Update', analytics, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
    getRebalanceChanges(jwt, csrf, iscid)
      .then((response) => {
        getSmallcaseDetailsLite(jwt, csrf, response.scid)
          .then((response) => {
            for (let i = 0; i < response.updates.length; i++) {
              const update = response.updates[i];
              if (update.version === response.version) {
                this.setState({rebalanceInfo: {label: update.label, description: update.rationale}});
                break;
              }
            }
          })
          .catch((error) => console.log(error));
        this.setState({smallcaseDetails: response});
      })
      .catch((error) => console.log(error));
  }

  componentWillReceiveProps(newProps) {
    if (newProps.loginReducer && (newProps.loginReducer.requestToken !== this.props.loginReducer.requestToken)) {
      this.fetchAuthDetails(newProps);
    }
  }

  fetchAuthDetails = (newProps) => {
    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    this.setState({fetchingJwt: true});
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      this.setState({fetchingJwt: false});
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {
        });
      } else {
        this.snackbar.show({
          title: SnackBarMessages.LOGIN_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    });
  }

  onSkipPopupClosed = () => this.setState({skipPopupShown: false});

  skipActionPressed = () => {
    this.setState({skipPopupShown: true});
  };

  removeReBalanceAction = () => {
    const {iscid, jwt, csrf} = this.props;

    skipRebalance(jwt, csrf, iscid)
      .then((response) => {
        this.setState({rebalancePending: false, skipPopupShown: false});
        Actions.pop();
      })
      .catch((error) => {
        console.log(error)
      });
  };

  openActionSheet = () => {
    ActionSheetIOS.showActionSheetWithOptions({
        options: ['Shares', 'Weights', "Cancel"],
        cancelButtonIndex: 2,
      },
      (buttonIndex) => {
        switch (buttonIndex) {
          case 0 :
            this.setState({showShares: true});
            break;
          case 1 :
            this.setState({showShares: false});
            break;
        }
      });
  };

  onConfirm = () => {
    const {stocksBeingBought, stocksBeingSold, stocksUnchanged} = this.state.smallcaseDetails.reBalanceStockList;
    const {jwt, csrf} = this.props;

    if (stocksBeingBought.length === 0 && stocksBeingSold.length === 0) {
      this.setState({noOrders: true});
    } else {
      this.state.smallcaseDetails.stocks = [...stocksBeingBought,
        ...stocksBeingSold, ...stocksUnchanged];

      this.state.smallcaseDetails.stocks.forEach(stock => {
        if (stock.newShares - stock.oldShares > 0) {
          stock.transactionType = OrderType.BUY;
        } else {
          stock.transactionType = OrderType.SELL;
        }
        stock.shares = Math.abs(stock.newShares - stock.oldShares);
      });

      checkMarketStatus(jwt, csrf)
        .then(isOpen => {
          if (isOpen) {
            let sids = [];
            let stocks = [...this.state.smallcaseDetails.stocks];
            stocks.forEach(stock => sids.push(stock.sid));
            getUpdatedStocks(jwt, csrf, sids)
              .then(stockObj => {
                stocks.forEach(stock => stock.price = stockObj[stock.sid]);
                const amount = calculateTotalAmount(stocks);
                if (amount > 0) {
                  getUserFunds(jwt, csrf)
                    .then(funds => {
                      if (amount > funds) {
                        this.setState({fundsAvailable: false});
                      } else {
                        this.openReviewOrder();
                      }
                    })
                    .catch((error) => {
                      if (error.data && error.data.status === 401) {
                        Actions.push(Routes.LOGIN);
                      }
                    });
                } else {
                  this.openReviewOrder();
                }
              });
          } else {
            this.snackbar.show({
              title: SnackBarMessages.MARKET_CLOSED,
              duration: SnackBarTime.LONG,
            });
          }
        })
        .catch(error => {
          console.log(error)
        });
    }
  };

  openReviewOrder = () => {
    const {scid, iscid, smallcaseName, source, smallcaseImage, stocks} = this.state.smallcaseDetails;
    const updateLabel = this.state.rebalanceDescription
    let newStocks = [];
    stocks.forEach(stock => {
      if (stock.newShares !== stock.oldShares) {
        newStocks.push(Object.assign({}, stock));
      }
    });

    const newSmallcaseDetails = {
      scid: scid,
      iscid: iscid,
      smallcaseName: smallcaseName,
      source: source,
      smallcaseImage: smallcaseImage,
      stocks: newStocks,
      orderType: OrderLabel.REBALANCE,
    };

    AnalyticsManager.track('Applied Rebalance Update', {
      noOfChanges: newStocks.length,
      smallcaseName,
      smallcaseSource: source,
      scid,
      updateLabel,
      smallcaseType: 'N/A',
    }, [AnalyticsSource.MIXPANEL, AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])
    Actions.push(Routes.REVIEW_ORDER, {
      smallcaseDetails: newSmallcaseDetails,
      repairing: false,
      type: OrderLabel.REBALANCE,
      analytics: {
        accessedFrom: 'Review Rebalance',
        sectionTag: 'N/A',
      }
    });
  };

  noOrdersCall = () => {
    let {iscid, version, jwt, csrf} = this.props;
    console.log(this.props);
    noOrdersNeeded(jwt, csrf, version, iscid)
      .then(response => {
        Actions.pop();
      })
      .catch(error => console.log(error));

  };

  showStockWidget = (sid) => {
    if (!sid) return;

    this.setState({stockWidget: true, sid});
  };

  onStockWidgetClosed = () => this.setState({stockWidget: false});

  renderListHeader = () => {
    const {showShares, smallcaseDetails, rebalanceInfo} = this.state;
    return (
      <View>
        <OrderTitle repairing={false} type={OrderLabel.REBALANCE} smallcaseDetails={smallcaseDetails}/>
        <Text style={styles.titleText}>{rebalanceInfo ? rebalanceInfo.label : ''}</Text>
        <Text style={styles.rationaleText}>{rebalanceInfo ? rebalanceInfo.description : ''}</Text>
        <View style={styles.headingContainer}>
          <Text style={[styles.heading, {flex: 10, marginLeft: 16}]}>Stock</Text>
          <TouchableHighlight style={{flex: 7}} onPress={this.openActionSheet} underlayColor={'rgb(255, 255, 255)'}>
            <View style={{alignItems: 'flex-end'}}>
              <View style={{flexDirection: 'row', marginBottom: 12}}>
                <Text style={{
                  textAlign: 'center',
                  color: 'rgb(83, 91, 98)',
                  fontSize: 14
                }}>{showShares ? 'Shares' : 'Old Wts.'}</Text>
                <Image source={require('../../../assets/chevronLeft.png')} style={styles.dropIcon}/>
              </View>
              <Divider dividerStyles={{width: 60, height: 0.5, backgroundColor: Styles.PRIMARY_TEXT_COLOR}}/>
            </View>
          </TouchableHighlight>
          <Text style={[styles.heading, {
            flex: 5,
            marginRight: 16,
            textAlign: 'right'
          }]}>{showShares ? 'Change' : 'New Wts.'}</Text>
        </View>
        <Divider/>
      </View>
    );
  };

  sendUserToCustomizeRebalance = () => {
    const {smallcaseDetails, rebalanceInfo} = this.state
    Actions.push(Routes.CUSTOMIZE_REBALANCE, {smallcaseDetails,rebalanceInfo});
  }

  renderListFooter = () => (
    <SmallcaseButton buttonStyles={styles.button} onClick={this.sendUserToCustomizeRebalance}>
      <Image source={require('../../../assets/edit.png')} style={{padding: 6, marginRight: 6}}/>
      <Text style={styles.buttonText}>Customize Rebalance</Text>
    </SmallcaseButton>
  );

  renderHeader = (item) => {
    let color, title;
    switch (item.section.sectionNo) {
      case 0:
        color = {color: 'rgb(25, 175, 85)'};
        title = "Buying";
        break;
      case 1:
        color = {color: 'rgb(216, 47, 68)'};
        title = "Selling";
        break;
      case 2:
        color = {color: 'rgb(46, 54, 64)'};
        title = "Unchanged";
        break;
    }

    return (
      <View style={styles.header}>
        <Text style={[styles.headerTitle, color]}>{item.section.title}</Text>
      </View>
    );
  }
    ;


  renderItem = (item) => {
    const {showShares} = this.state;
    let change;

    if (showShares) {
      let changeValue = item.item.newShares - item.item.oldShares;
      change = (changeValue) > 0 ? "+" + (changeValue) : (changeValue);
    } else {
      change = ((item.item.newWeight) > 0 ? (item.item.newWeight.toFixed(2)) : (item.item.newWeight.toFixed(2))) + "%";
    }

    let color;

    switch (item.section.sectionNo) {
      case 0:
        color = {color: 'rgb(25, 175, 85)'};
        break;
      case 1:
        color = {color: 'rgb(216, 47, 68)'};
        break;
      case 2:
        color = {color: 'rgb(46, 54, 64)'};
        break;
    }

    return (
      <View>
        <View style={styles.row}>
          <TouchableWithoutFeedback onPress={this.showStockWidget.bind(this, item ? item.item.sid : '')}>
            <View style={styles.stockNameContainer}>
              <Text style={styles.stockName} numberOfLines={1} ellipsizeMode='tail'>{item.item.sidInfo.name}</Text>
            </View>
          </TouchableWithoutFeedback>
          <Text
            style={styles.stockQty}>{showShares ? item.item.newShares : item.item.oldWeight.toFixed(2) + "%"}</Text>
          <Text style={[styles.stockChange, showShares ? color : '#2e3640']}>{change}</Text>
        </View>
        <Divider dividerStyles={{marginLeft: 16}}/>
      </View>
    );
  };

  keyExtractor = (item) => item.sid;

  onFundsAvailableClicked = () => this.setState({fundsAvailable: true});

  render() {
    if (!("smallcaseDetails" in this.state)) return (
      <Loader loaderStyles={{marginTop: 64}} />
    );
    const {smallcaseDetails, skipPopupShown, noOrders, fundsAvailable, stockWidget, sid, fetchingJwt} = this.state;
    const {jwt, csrf} = this.props;

    let sections = [];
    if (smallcaseDetails.reBalanceStockList.stocksBeingBought.length) {
      sections.push({
        title: "Buying: " + smallcaseDetails.reBalanceStockList.stocksBeingBought.length,
        data: smallcaseDetails.reBalanceStockList.stocksBeingBought,
        sectionNo: 0
      });
    }
    if (smallcaseDetails.reBalanceStockList.stocksBeingSold.length) {
      sections.push({
        title: "Selling: " + smallcaseDetails.reBalanceStockList.stocksBeingSold.length,
        data: smallcaseDetails.reBalanceStockList.stocksBeingSold,
        sectionNo: 1
      });
    }
    if (smallcaseDetails.reBalanceStockList.stocksUnchanged.length) {
      sections.push({
        title: "Unchanged: " + smallcaseDetails.reBalanceStockList.stocksUnchanged.length,
        data: smallcaseDetails.reBalanceStockList.stocksUnchanged,
        sectionNo: 2
      });
    }

    return (
      <View style={styles.container}>
        <SectionList
          renderItem={this.renderItem}
          renderSectionHeader={this.renderHeader}
          keyExtractor={this.keyExtractor}
          sections={sections}
          ListHeaderComponent={this.renderListHeader}
          stickySectionHeadersEnabled={false}
          ListFooterComponent={this.renderListFooter}
        />
        <CustomizeActionSheet
          rightFunc={noOrders ? this.noOrdersCall : this.onConfirm}
          leftFunc={this.skipActionPressed}
          noOrders={noOrders}
          fundsAvailable={fundsAvailable}
          disabled={fetchingJwt}
          scene={Routes.REVIEW_REBALANCE}
          onFundsAvailableClicked={this.onFundsAvailableClicked}
        />
        <SkipPopup
          showPopup={skipPopupShown}
          skipPopupClosed={this.onSkipPopupClosed}
          skipPressed={this.removeReBalanceAction}
        />
        <StockWidget
          showWidget={stockWidget}
          stockWidgetClosed={this.onStockWidgetClosed}
          csrf={csrf}
          jwt={jwt}
          sid={sid}
        />
        <SnackBar onRef={ref => this.snackbar = ref}/>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  titleText: {
    marginTop: 24,
    marginHorizontal: 16,
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    color: 'rgb(47, 54, 63)',
  },
  rationaleText: {
    marginTop: 8,
    marginHorizontal: 16,
    marginBottom: 32,
    fontSize: 14,
    lineHeight: 21,
    color: 'rgb(83, 91, 98)'
  },
  headingContainer: {
    flexDirection: 'row',
  },
  heading: {
    fontSize: 14,
    marginBottom: 12,
    color: 'rgb(83, 91, 98)',
  },
  header: {
    height: 48,
    padding: 16,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    backgroundColor: 'rgb(249, 250, 251)',
  },
  headerTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgb(46, 54, 64)',
  },
  button: {
    height: 36,
    width: 200,
    borderColor: 'rgba(31, 122, 224, 0.7)',
    borderRadius: 4,
    borderWidth: 0.7,
    marginVertical: 24,
    marginLeft: 16,
  },
  stockNameContainer: {
    flex: 10,
    height: 48,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'rgb(31, 122, 224)',
    fontSize: 14,
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
    height: 48,
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  stockName: {
    color: 'rgb(47, 54, 63)',
  },
  stockQty: {
    flex: 5,
    textAlign: 'right',
    color: 'rgb(47, 54, 63)',
  },
  stockChange: {
    flex: 5,
    textAlign: 'right',
    color: 'rgb(47, 54, 63)',
  },
  dropIcon: {
    height: 18,
    width: 18,
    transform: [{rotate: '-90deg'}],
  }

});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  loginReducer: state.loginReducer,
  user: state.userReducer.user
});

module.exports = connect(mapStateToProps)(ReviewRebalance);
import Api from "../../api/Api";
import {
  GET_STOCK_PRICE,
  INVESTED_SMALLCASE,
  NO_ORDERS,
  SKIP_REBALANCE,
  SMALLCASE_DETAILS,
  SMALLCASE_LITE_DETAILS
} from "../../api/ApiRoutes";
import {SmallcaseImageURLs} from "../../util/Utility";
import {calculateSharesAndMinAmountFromWeight, calculateWeightageFromShares} from "../../helper/StockHelper";

const getSmallcaseDetails = (jwt, csrf, params) => {
  return new Promise((resolve, reject) => {
    Api.get({route: SMALLCASE_DETAILS, jwt, csrf, params})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

const getInvestedSmallcaseDetails = (jwt, csrf, params) => {
  return new Promise((resolve, reject) => {
    Api.get({route: INVESTED_SMALLCASE, jwt, csrf, params})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

export const getSmallcaseDetailsLite = (jwt, csrf, scid) => {
  let params = 'scid=' + scid;
  return new Promise((resolve, reject) => {
    Api.get({route: SMALLCASE_LITE_DETAILS, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const getRebalanceChanges = (jwt, csrf, iscid) => {

  let params = 'iscid=' + iscid;

  return new Promise((resolve, reject) => {
    getInvestedSmallcaseDetails(jwt, csrf, params)
      .then((response) => {

        let investedStocks = response.data.currentConfig.constituents;
        getPrices(investedStocks, jwt, csrf)
          .then((investedStocksWithPrices) => {
            investedStocks = investedStocksWithPrices;
            investedStocks = calculateWeightageFromShares(investedStocks);

            let smallcaseDetails = {
              scid: response.data.scid,
              iscid: response.data._id,
              source: response.data.source,
              smallcaseName: response.data.name,
              smallcaseImage: SmallcaseImageURLs.NON_CREATED + "80/" + response.data.scid + ".png",
            };

            let param = 'scid=' + response.data.scid;
            getSmallcaseDetails(jwt, csrf, param)
              .then((response) => {
                let requiredStocks = response.data.constituents;

                getPrices(requiredStocks, jwt, csrf)
                  .then((stocks) => {

                    requiredStocks = stocks;

                    let reBalanceStockList = [];
                    let stockListWithMinAmount = calculateSharesAndMinAmountFromWeight(requiredStocks, -1);
                    let maxAmount = 0;

                    investedStocks.forEach((investedStock) => {
                      maxAmount += investedStock.shares * investedStock.price
                    });
                    maxAmount = Math.max(maxAmount, stockListWithMinAmount.maxPByW);

                    requiredStocks.forEach((requiredStock) => {
                      requiredStock.shares = Math.round((requiredStock.weight * maxAmount) / requiredStock.price);
                    });
                    requiredStocks = calculateWeightageFromShares(requiredStocks);

                    requiredStocks.forEach((requiredStock) => {
                      let investedIndex = getStockIndex(investedStocks, requiredStock.sid);
                      if (investedIndex === -1) {
                        let rebalanceStock = {
                          sid: requiredStock.sid,
                          sidInfo: requiredStock.sidInfo,
                          newShares: requiredStock.shares,
                          newWeight: requiredStock.weight * 100,
                          oldShares: 0,
                          oldWeight: 0,
                        };
                        reBalanceStockList.push(rebalanceStock);
                      } else {
                        let presentStock = investedStocks[investedIndex];
                        let reBalanceStock = {
                          sid: presentStock.sid,
                          sidInfo: presentStock.sidInfo,
                        };

                        let valueDifference = Math.abs((requiredStock.shares - presentStock.shares) * requiredStock.price);
                        if (requiredStock.shares < presentStock.shares && valueDifference <= 500) {
                          reBalanceStock.newShares = presentStock.shares;
                          reBalanceStock.newWeight = presentStock.weight * 100;
                        } else {
                          reBalanceStock.newShares = requiredStock.shares;
                          reBalanceStock.newWeight = requiredStock.weight * 100;
                        }
                        reBalanceStock.oldShares = presentStock.shares;
                        reBalanceStock.oldWeight = presentStock.weight * 100;
                        reBalanceStockList.push(reBalanceStock);
                      }
                    });

                    investedStocks.forEach((investedStock) => {
                      let index = getStockIndex(requiredStocks, investedStock.sid);
                      if (index === -1) {
                        let rebalanceStock = {
                          sid: investedStock.sid,
                          sidInfo: investedStock.sidInfo,
                          newShares: 0,
                          newWeight: 0,
                          oldShares: investedStock.shares,
                          oldWeight: investedStock.weight * 100,
                        };
                        reBalanceStockList.push(rebalanceStock);
                      }
                    });

                    smallcaseDetails.reBalanceStockList = convertToPresentableLists(reBalanceStockList);
                    resolve(smallcaseDetails);

                  });

              })
              .catch((error) => {
                console.log(error)
              });

          })
          .catch((error) => reject(error));
      })
      .catch((error) => console.log(error));
  })


  /*
  *   fetch current smallcase profile
  *   compare user holdings and current smallcase profile
  *   create new rebalanced array with keys change and type
  *
  * */

};

const convertToPresentableLists = (reBalanceStockList) => {

  let stocksBeingBought = [], stocksBeingSold = [], stocksUnchanged = [];

  reBalanceStockList.forEach((reBalanceStock) => {
    if (reBalanceStock.oldShares < reBalanceStock.newShares) {
      stocksBeingBought.push(reBalanceStock)
    } else if (reBalanceStock.oldShares > reBalanceStock.newShares) {
      stocksBeingSold.push(reBalanceStock);
    } else {
      stocksUnchanged.push(reBalanceStock);
    }
  });

  return ({
    stocksBeingBought: stocksBeingBought,
    stocksBeingSold: stocksBeingSold,
    stocksUnchanged: stocksUnchanged,
  });

};

const getPrices = (stocks, jwt, csrf) => {
  let params = '';
  for (let i = 0; i < stocks.length; i++) {
    let stock = stocks[i];
    params += 'stocks=' + stock.sid + (i === stocks.length - 1 ? '' : '&');
  }

  return new Promise((resolve, reject) => {
    Api.get({route: GET_STOCK_PRICE, jwt, csrf, params})
      .then((response) => {
        stocks.forEach((stock) => {
          stock.price = response.data[stock.sid];
        });
        resolve(stocks);
      })
      .catch((error) => reject(error));
  });
};

export const noOrdersNeeded = (jwt, csrf, version, iscid) => {

  const body = {
    version: version,
    iscid: iscid,
  };

  return new Promise((resolve, reject) => {
    Api.post({route: NO_ORDERS, jwt, csrf, body})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  })
};

const getStockIndex = (stocks, sid) => {

  for (let i = 0; i < stocks.length; i++) {
    let stock = stocks[i];
    if (stock.sid === sid)
      return i;
  }

  return -1;

};

export const skipRebalance = (jwt, csrf, iscid) => {

  const body = {
    iscid: iscid,
  };

  return new Promise((resolve, reject) => {
    Api.post({route: SKIP_REBALANCE, jwt, csrf, body})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  })

};
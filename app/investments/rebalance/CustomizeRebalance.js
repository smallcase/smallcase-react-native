import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableHighlight,
  ActionSheetIOS,
  TouchableWithoutFeedback,
  AsyncStorage,
  Keyboard
} from 'react-native';
import CustomizeActionSheet from "../components/CustomizeActionSheet";
import {CancelPopup} from "./CancelPopup";
import {ConfirmPopup} from "./ConfirmPopup";
import {KeyboardAwareFlatList} from "react-native-keyboard-aware-scroll-view"
import {connect} from "react-redux";
import Divider from "../../component/Divider";
import {
  OrderLabel,
  OrderType,
  ReviewOrderTitles,
  Routes,
  SnackBarTime,
  Styles, AnalyticsSource
} from "../../util/Utility";
import {Actions} from "react-native-router-flux"
import {checkMarketStatus, getUpdatedStocks} from "../../helper/MarketHelper";
import {calculateTotalAmount} from "../../order/OrderService";
import {getUserFunds} from "../../user/UserService";
import StockWidget from '../../component/StockWidget';
import {BROKER_LOGIN} from "../../api/ApiRoutes";
import * as types from "../../constants/Constants";
import {getUserDetails} from "../../user/UserActions";
import {getJwt} from "../../helper/LoginHelper";
import {userLoggedIn} from "../../onboarding/LandingActions";
import * as Config from "../../../Config";
import TouchHighlight from "../../component/TouchHighlight";
import {MAX_STOCK_COUNT} from "../../constants/Constants";
import SnackBar from "../../component/SnackBar";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {calculateWeightageFromShares, getPrices} from "../../helper/StockHelper";
import {calculateMinInvestment} from "../../helper/SmallcaseHelper";
import {SnackBarMessages} from "../../util/Strings";

class CustomizeRebalance extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      stocks: [],
      weights: [],
      cancelPopupShown: false,
      confirmPopupShown: false,
      fundsAvailable: true,
      stockWidget: false,
      fetchingJwt: false,
      showChange: true,
    };

    this.initialStocks = [];
    this.changeableStocks = [];

    props.smallcaseDetails.reBalanceStockList.stocksBeingBought.forEach(stock => {
      this.initialStocks.push(Object.assign({}, stock));
      this.changeableStocks.push(Object.assign({}, stock));
    });
    props.smallcaseDetails.reBalanceStockList.stocksBeingSold.forEach(stock => {
      this.initialStocks.push(Object.assign({}, stock));
      this.changeableStocks.push(Object.assign({}, stock));
    });
    props.smallcaseDetails.reBalanceStockList.stocksUnchanged.forEach(stock => {
      this.initialStocks.push(Object.assign({}, stock));
      this.changeableStocks.push(Object.assign({}, stock));
    });

    this.cancelCustomize = this.cancelCustomize.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.loginReducer && (newProps.loginReducer.requestToken !== this.props.loginReducer.requestToken)) {
      this.fetchAuthDetails(newProps);
    }
  }

  fetchAuthDetails = (newProps) => {
    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    this.setState({fetchingJwt: true});
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      this.setState({fetchingJwt: false});
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {
        });
      } else {
        this.snackbar.show({
          title: SnackBarMessages.LOGIN_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    });
  }

  componentDidMount() {
    this.setState({stocks: this.changeableStocks});
    const {smallcaseDetails, rebalanceInfo} =this.props
    AnalyticsManager.track('Customized Rebalance Update', {
      iscid: smallcaseDetails.iscid ,
      scid: smallcaseDetails.scid ,
      smallcaseName: smallcaseDetails.name ,
      smallcaseSource: smallcaseDetails.source,
      smallcaseType: 'N/A',
      updateDate: 'N/A' ,
      updateLabel: rebalanceInfo.description,
      updateName: rebalanceInfo.label ,
    }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
  }

  isStockCountGreaterThanMax = (sid, changeValue) => {
    if (changeValue === 0) return false;

    const {stocks} = this.state;
    const validStocks = stocks.filter(stock => stock.newShares > 0 && stock.sid !== sid);

    return validStocks.length >= MAX_STOCK_COUNT;
  }


  calculateWeightageFromNewShares = (oldStocks) => {
    let stocks = [];
    oldStocks.forEach(stock => {
      const stockComponent = Object.assign({}, stock)
      stockComponent.shares = stock.newShares;
      stockComponent.weight = stock.newWeight;
      stocks.push(stockComponent);
    });
    stocks = calculateWeightageFromShares(stocks);
    stocks.forEach(stock => {
      stock.newShares = stock.shares;
      stock.newWeight = stock.weight * 100;
    });

    return stocks;
  }

  onChange = (change, item) => {
    const {jwt, csrf} = this.props;

    if (change < 0) {
      return;
    }

    if (isNaN(change)) {
      // only allow entering a valid number
      return;
    }

    if (this.isStockCountGreaterThanMax(item.sid, change)) {
      this.snackbar.show({
        title: SnackBarMessages.EXCEEDED_MAX_STOCKS,
        duration: SnackBarTime.LONG
      });
      return;
    }
    let newStocks = [...this.state.stocks];
    newStocks.forEach((stock) => {
      if (stock.sid === item.sid) {
        stock.newShares = Math.round(change);
      }
    });

    const sids = []
    newStocks.forEach((stock) => sids.push(stock.sid))
    getPrices(sids, jwt, csrf)
      .then(stocks => {
        const newStocksWithPrice = []
        newStocks.forEach(stock => {
          const stockComponent = Object.assign({}, stock);
          stockComponent.price = stocks[stock.sid];
          newStocksWithPrice.push(stockComponent);

        })
        const newStocksWithWeights = this.calculateWeightageFromNewShares(newStocksWithPrice);
        this.setState({stocks: newStocksWithWeights})

      })
      .catch((error) => console.log(error));
  };

  resetChanges = () => {
    let newStocks = [];
    this.initialStocks.forEach(stock => {
      newStocks.push(Object.assign({}, stock));
    });
    this.setState({stocks: newStocks});
  };

  onConfirm = () => {
    const {jwt, csrf, smallcaseDetails, rebalanceInfo} = this.props;

    const stocks = [];

    let stocksGreaterThanZeroShares = 0;
    this.state.stocks.forEach(stock => {
      let newStock = Object.assign({}, stock);
      if (newStock.newShares - newStock.oldShares > 0) {
        newStock.transactionType = OrderType.BUY;
      } else {
        newStock.transactionType = OrderType.SELL;
      }
      newStock.shares = Math.abs(newStock.newShares - newStock.oldShares);
      stocks.push(newStock);
      if (newStock.newShares > 0) stocksGreaterThanZeroShares++;
    });

    if (stocksGreaterThanZeroShares < 2) {
      this.snackbar.show({
        title: SnackBarMessages.AT_LEAST_TWO_STOCKS,
        duration: SnackBarTime.LONG,
      });
      return;
    }

    checkMarketStatus(jwt, csrf)
      .then(isOpen => {
        if (isOpen) {
          let sids = [];
          stocks.forEach(stock => sids.push(stock.sid));
          getUpdatedStocks(jwt, csrf, sids)
            .then(stockObj => {
              stocks.forEach(stock => stock.price = stockObj[stock.sid]);
              const amount = calculateTotalAmount(stocks);
              if (amount > 0) {
                getUserFunds(jwt, csrf)
                  .then(funds => {
                    if (amount > funds) {
                      this.setState({fundsAvailable: false, confirmPopupShown: false});
                    } else {
                      this.openReviewOrder(stocks);
                    }
                  })
                  .catch((error) => {
                    if (error.data && error.data.status === 401) {
                      Actions.push(Routes.LOGIN);
                    }
                  });
              } else {
                this.openReviewOrder(stocks);
              }
            })
            .catch((error) => console.log(error));
        } else {
          this.snackbar.show({
            title: SnackBarMessages.MARKET_CLOSED,
            duration: SnackBarTime.LONG,
          });
        }
      })
      .catch(error => {
        console.log(error)
      });
    AnalyticsManager.track('Confirmed Rebalance Changes',
      {
        noOfChanges: stocks.length,
        smallcaseName: smallcaseDetails.smallcaseName,
        smallcaseSource: smallcaseDetails.source,
        smallcaseType: 'N/A',
        scid: smallcaseDetails.scid,
         updateName: rebalanceInfo.label ,
        updateLabel: rebalanceInfo.description ,
         updateDate: 'N/A' ,
      },
      [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])
  };

  openReviewOrder = (stocks) => {
    const {scid, iscid, smallcaseName, source, smallcaseImage} = this.props.smallcaseDetails

    let newStocks = [];
    stocks.forEach(stock => {
      if (stock.newShares !== stock.oldShares) {
        newStocks.push(Object.assign({}, stock));
      }
    });

    let newSmallcaseDetails = {
      scid: scid,
      iscid: iscid,
      smallcaseName: smallcaseName,
      source: source,
      smallcaseImage: smallcaseImage,
      stocks: newStocks,
      orderType: OrderLabel.REBALANCE,
    };

    Actions.push(Routes.REVIEW_ORDER, {
      smallcaseDetails: newSmallcaseDetails,
      type: OrderLabel.REBALANCE,
      repairing: false,
      analytics: {
        accessedFrom: 'Customize Rebalance',
        sectionTag: 'N/A',
      }
    });
  };

  onConfirmPopupClosed = () => this.setState({confirmPopupShown: false});

  onCancelPopupClosed = () => this.setState({cancelPopupShown: false});

  onPressConfirm = () => {

    let stocksChanged = false;
    for (let i = 0; i < this.initialStocks.length; i++) {
      if (this.initialStocks[i].sid === this.state.stocks[i].sid && this.state.stocks[i].newShares !== this.initialStocks[i].newShares) {
        stocksChanged = true;
        break;
      }
    }

    if (!stocksChanged) {
      //goto review order
      this.onConfirm();
    }
    else {
      this.setState({confirmPopupShown: true});
    }
  }


  static onPressCancel() {
    const customizeRebalanceInstance = Actions.refs[Routes.CUSTOMIZE_REBALANCE].getWrappedInstance();
    customizeRebalanceInstance.cancelCustomize();
  };

  cancelCustomize() {
    let stocksChanged = false;

    Keyboard.dismiss();
    for (let i = 0; i < this.initialStocks.length; i++) {
      if (this.initialStocks[i].sid === this.state.stocks[i].sid && this.state.stocks[i].newShares !== this.initialStocks[i].newShares) {
        stocksChanged = true;
        break;
      }
    }

    if (!stocksChanged) {
      this.setState({confirmPopupShown: false, cancelPopupShown: false, stockWidget: false});
      Actions.pop()
    }
    else {
      this.setState({confirmPopupShown: false, cancelPopupShown: true, stockWidget: false});
    }
  }

  keyExtractor = (item, index) => index;

  showStockWidget = (sid) => {
    if (!sid) return;

    this.setState({stockWidget: true, sid});
  };

  onStockWidgetClosed = () => this.setState({stockWidget: false});

  openActionSheet = () => {
    ActionSheetIOS.showActionSheetWithOptions({
        options: ['Change', 'Weights', "Cancel"],
        cancelButtonIndex: 2,
      },
      (buttonIndex) => {
        switch (buttonIndex) {
          case 0 :
            this.setState({showChange: true});
            break;
          case 1 :
            this.setState({showChange: false});
            break;
        }
      });
  };

  renderHeader = () => {
    const {showChange} = this.state;
    return (
      <View>
        <View style={{flexDirection: 'row', marginHorizontal: 16, marginTop: 12}}>
          <Text style={{textAlign: 'left', flex: 6, color: Styles.DESCRIPTION_TEXT_COLOR, fontSize: 14}}>Stock</Text>
          <Text style={{textAlign: 'right', flex: 8, color: Styles.DESCRIPTION_TEXT_COLOR, fontSize: 14}}>Shares</Text>
          <TouchableHighlight style={{flex: 6}} onPress={this.openActionSheet} underlayColor={'rgb(255, 255, 255)'}>
            <View style={{alignItems: 'flex-end'}}>
              <View style={{flexDirection: 'row', marginBottom: 12}}>
                <Text style={{
                  textAlign: 'center',
                  color: 'rgb(83, 91, 98)',
                  fontSize: 14
                }}>{showChange ? 'Change' : 'Weights'}</Text>
                <Image source={require('../../../assets/chevronLeft.png')} style={styles.dropIcon}/>
              </View>
              <Divider dividerStyles={{width: 60, height: 0.5, backgroundColor: Styles.PRIMARY_TEXT_COLOR}}/>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  };

  renderItem = ({item, index}) => {
    const {showChange} = this.state;

    let change, changeValue;
    let color;

    if (showChange) {
      changeValue = item.newShares - item.oldShares;
      change = (changeValue) > 0 ? "+" + (changeValue) : (changeValue);
    }
    else {
      change = ((item.newWeight) > 0 ? (item.newWeight.toFixed(2)) : (item.newWeight.toFixed(2))) + "%";
    }

    if (changeValue === 0) {
      color = {color: 'rgb(47, 54, 63)'};
    } else {
      color = change > 0 ? {color: 'rgb(25, 175, 85)'} : {color: 'rgb(216, 47, 68)'};
    }

    return (
      <View>
        <Divider dividerStyles={{marginLeft: index === 0 ? 0 : 16}}/>
        <View style={styles.row}>
          <TouchableWithoutFeedback onPress={this.showStockWidget.bind(this, item ? item.sid : '')}>
            <View style={styles.stockNameContainer}>
              <Text style={styles.text} numberOfLines={1} ellipsizeMode='tail'>{item.sidInfo.name}</Text>
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.group}>
            <TouchHighlight styles={styles.sharesActionContainer}
                            onClick={() => this.onChange(item.newShares - 1, item)}>
              <Image source={require('../../../assets/minus.png')} style={styles.icon}/>
            </TouchHighlight>
            <View style={styles.box}>
              <TextInput
                multiline={true}
                style={styles.input}
                value={String(item.newShares)}
                keyboardType={"numeric"}
                onChangeText={(change) => this.onChange(change, item)}
              />
            </View>
            <TouchHighlight styles={styles.sharesActionContainer}
                            onClick={() => this.onChange(item.newShares + 1, item)}>
              <Image source={require('../../../assets/add.png')} style={styles.icon}/>
            </TouchHighlight>
          </View>
          <Text style={[styles.changeText, showChange ? color : '#2e3640']}>{change}</Text>
        </View>
      </View>
    )
  };

  onFundsAvailableClicked = () => this.setState({fundsAvailable: true});

  render() {
    const {stocks, cancelPopupShown, confirmPopupShown, fundsAvailable, sid, stockWidget, fetchingJwt} = this.state;
    const {jwt, csrf} = this.props;

    return (
      <View style={styles.container}>
        <KeyboardAwareFlatList
          style={styles.list}
          data={stocks}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          ListHeaderComponent={this.renderHeader}
          extraData={stocks}
          extraScrollHeight={80}
          enableAutomaticScroll={true}
        />
        <CustomizeActionSheet
          leftFunc={this.resetChanges}
          rightFunc={this.onPressConfirm}
          fundsAvailable={fundsAvailable}
          disabled={fetchingJwt}
          scene={Routes.CUSTOMIZE_REBALANCE}
          onFundsAvailableClicked={this.onFundsAvailableClicked}
        />
        <StockWidget
          showWidget={stockWidget}
          stockWidgetClosed={this.onStockWidgetClosed.bind(this)}
          csrf={csrf}
          jwt={jwt}
          sid={sid}
        />
        <CancelPopup
          showPopup={cancelPopupShown}
          cancelPopupClosed={this.onCancelPopupClosed.bind(this)}
        />
        <ConfirmPopup
          showPopup={confirmPopupShown}
          confirmPopupClosed={this.onConfirmPopupClosed.bind(this)}
          onConfirmPressed={this.onConfirm}
        />
        <SnackBar onRef={ref => this.snackbar = ref}/>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  actionContainer: {
    flexDirection: 'row',
  },
  box: {
    borderColor: 'rgb(221, 224, 228)',
    borderWidth: 0.7,
    backgroundColor: 'white',
    height: 28,
    width: 60,
  },
  list: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    height: 48,
    alignItems: 'center',
    marginHorizontal: 16
  },
  text: {
    fontSize: 14,
    color: 'rgb(47, 54, 63)',
  },
  group: {
    flexDirection: 'row',
    flex: 4,
    alignItems: 'center',
  },
  changeText: {
    fontSize: 14,
    flex: 2,
    textAlign: 'right'
  },
  stockNameContainer: {
    height: 48,
    flex: 5,
    backgroundColor: 'white',
    marginRight: 8,
    justifyContent: 'center',
  },
  input: {
    textAlign: 'right',
    paddingRight: 6,
    height: 27,
    width: 59,
    backgroundColor: 'white'
  },
  icon: {
    height: 18,
    width: 18,
  },
  sharesActionContainer: {
    height: 48,
    paddingHorizontal: 8,
    marginHorizontal: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dropIcon: {
    height: 18,
    width: 18,
    transform: [{rotate: '-90deg'}],
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  loginReducer: state.loginReducer
});

module.exports = connect(mapStateToProps, null, null, {withRef: true})(CustomizeRebalance);
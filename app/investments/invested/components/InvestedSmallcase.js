import {getFormattedCurrency, SmallcaseStatus, Styles} from "../../../util/Utility";
import React, {Component} from "react";
import {Image, Text, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import SmallcaseImage from "../../../component/SmallcaseImage";
import Divider from "../../../component/Divider";
import {EM_DASH, ReturnsComponentValueType} from "../../../constants/Constants";
import ReturnsComponent from "../../../component/ReturnsComponent";

export default class InvestedSmallcase extends Component {
  render() {
    let {item, showInvestedSmallcaseDetails} = this.props;

    let infoBar;
    switch (item.status) {
      case SmallcaseStatus.VALID:
        infoBar = null
        break;

      case SmallcaseStatus.INVALID:
        infoBar = (
          <View style={styles.infoBar}>
            <Text>Some orders are unfilled in this smallcase</Text>
          </View>
        );
        break;

      case SmallcaseStatus.PLACED:
        infoBar = (
          <View style={styles.infoBar}>
            <Text style={{color: 'rgb(129, 135, 140)'}}>Orders placed. Waiting for order status...</Text>
          </View>
        );
        break;

      default:
        infoBar = (
          <View style={styles.infoBar}>
            <Text style={{color: 'rgb(129, 135, 140)'}}>Orders placed. Waiting for order status...</Text>
          </View>
        );
        break;
    }

    return (
      <TouchableWithoutFeedback onPress={() => {
        showInvestedSmallcaseDetails(item)
      }}>
        <View style={styles.investmentCard}>
          <View style={styles.investmentInfo}>
            <View style={styles.smallcaseHorizontalContainer}>
              <SmallcaseImage source={item.source} imageUrl={item.smallcaseImage}
                              imageStyles={styles.smallcaseImage} textSize={8}/>
            </View>
            <View style={{marginRight: 56}}>
              <Text style={styles.smallcaseName} numberOfLines={1} ellipsizeMode='tail'>{item.smallcaseName}</Text>
              <Text style={styles.boughtOn}>Bought on {item.boughtOn}</Text>
            </View>
          </View>
          {infoBar}
          <View style={[styles.investmentInfo, {justifyContent: 'space-between'}]}>
            <View>
              <Text style={styles.investmentHeader}>Index</Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.investmentValue}>{item.index}</Text>
                <Image style={styles.indexTrend}
                       source={item.changeInIndexPercent >= 0 ? require('../../../../assets/iconCaretGreen.png') : require('../../../../assets/iconCaretRed.png')}/>
                <Text
                  style={[styles.changeInIndex, {color: item.changeInIndexPercent >= 0 ? 'rgb(25, 175, 85)' : 'rgb(216, 47, 68)'}]}>
                  {item.changeInIndexPercent === EM_DASH ? '' : Math.abs(item.changeInIndexPercent)}%
                </Text>
              </View>
            </View>
            <ReturnsComponent
              topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Value'}}
              bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: item.currentValue}}
              bottomLeftStyles={{marginBottom: 24, fontSize: 16}}
            />
            <ReturnsComponent
              topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Total Returns'}}
              bottomLeft={{type: ReturnsComponentValueType.AMOUNT, value: item.totalReturns}}
              bottomLeftStyles={{marginBottom: 24, fontSize: 16}}
              containerStyles={{alignItems: 'flex-end'}}
            />
          </View>
          <Divider dividerStyles={{marginLeft: 16}} />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  infoBar: {
    flex: 1,
    flexDirection: 'row',
    height: 36,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 16,
    marginBottom: 18,
    marginTop: -4,
    backgroundColor: 'rgb(249, 250, 251)'
  },
  investmentCard: {
    backgroundColor: '#FFFFFF',
  },
  investmentInfo: {
    flexDirection: 'row',
    flex: 1,
    marginHorizontal: 16
  },
  smallcaseHorizontalContainer: {
    flexDirection: 'row'
  },
  smallcaseImage: {
    height: 50,
    width: 50,
    marginTop: 16,
    marginBottom: 24,
    marginRight: 16,
    borderRadius: 4
  },
  smallcaseName: {
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 20,
    marginRight: 16,
    letterSpacing: -0.1,
  },
  boughtOn: {
    fontSize: 13,
    marginTop: 6,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  investmentHeader: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  indexTrend: {
    height: 7,
    width: 10,
    marginLeft: 4,
    marginTop: 16
  },
  changeInIndex: {
    marginTop: 12,
    marginLeft: 4,
    fontSize: 12,
    fontWeight: 'bold'
  },
  investmentValue: {
    fontSize: 16,
    marginTop: 8,
    letterSpacing: 0.4,
    marginBottom: 24,
  },
});
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import {Routes, Styles} from "../../../util/Utility";
import {Actions} from "react-native-router-flux";

const ExitedSmallcaseCountCard = (props) => (
  <TouchableOpacity style={{backgroundColor: 'white'}} onPress={() => Actions.push(Routes.EXITED_SMALLCASES)}>
    <View style={styles.exitedSmallcaseContainer}>
      <Text style={{color: Styles.PRIMARY_TEXT_COLOR}}>You have exited
        from {props.exitedSmallcaseCount} smallcases</Text>
      <Text style={styles.seeExitedSmallcase}>See exited smallcases</Text>
    </View>
  </TouchableOpacity>
);

export default ExitedSmallcaseCountCard;

const styles = StyleSheet.create({
  exitedSmallcaseContainer: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'rgb(221, 224, 228)',
    margin: 16,
    padding: 16,
    backgroundColor: 'white',
    borderRadius: 4
  },
  seeExitedSmallcase: {
    color: Styles.BLUE,
    marginTop: 8,
    fontWeight: 'bold'
  },
});
import React, {Component} from "react";
import {Animated, Easing, Text, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import {AnalyticsSource, getFormattedCurrency, Styles} from "../../../util/Utility";
import ExpandCollapse from "../../../component/ExpandCollapse";
import AnalyticsManager from "../../../analytics/AnalyticsManager";
import ReturnsComponent from "../../../component/ReturnsComponent";
import {ReturnsComponentValueType} from "../../../constants/Constants";

export default class InvestmentSummary extends Component {

  constructor() {
    super();
    this.state = {
      expandOverview: false,
      spinStartDegree: '0deg',
      spinEndDegree: '180deg',
      spinValue: new Animated.Value(0),
    }
  }
  componentDidMount() {
    AnalyticsManager.track('Viewed Investments',{accessedFrom: 'N/A', sectionTag:'N/A'},[AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])
  }

  toggleInvestmentOverview = () => {
    this.state.spinValue.setValue(0);
    Animated.timing(
      this.state.spinValue, {
        toValue: 1,
        duration: 200,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();

    this.setState({
      expandOverview: !this.state.expandOverview,
      spinStartDegree: this.state.expandOverview ? '180deg' : '0deg',
      spinEndDegree: this.state.expandOverview ? '0deg' : '180deg'
    });
  }

  getSeeDetailsArrow = () => {
    // Interpolate beginning and end values (in this case 0 and 1)
    const spin = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [this.state.spinStartDegree, this.state.spinEndDegree]
    });

    return (
      <Animated.Image style={[styles.detailsArrow, {transform: [{rotate: spin}]}]}
                      source={require('../../../../assets/iconArrowDownBlue.png')}/>
    );
  }

  shouldComponentUpdate = (nextProps, nextState) => nextProps !== this.props || nextState !== this.state

  render() {
    let {investmentOverview} = this.props;
    let {expandOverview} = this.state;
    return (
      <TouchableWithoutFeedback onPress={this.toggleInvestmentOverview.bind(this)}>
        <View>
          <View style={styles.investmentOverviewContainer}>
            <Text style={styles.investmentOverviewTitle}>Your Investments Summary</Text>
            <View style={styles.valueHeaders}>
              <ReturnsComponent
                topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Value'}}
                bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: investmentOverview.currentValue}}
                bottomLeftStyles={styles.totalReturns}
              />
              <ReturnsComponent
                topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Total Returns'}}
                topRight={{type: ReturnsComponentValueType.PERCENT, value: investmentOverview.totalPAndLPercent}}
                bottomRight={{type: ReturnsComponentValueType.AMOUNT, value: investmentOverview.totalPAndL}}
                topRightStyles={styles.totalReturnsPercentage}
                bottomRightStyles={styles.totalReturns}
                containerStyles={{alignItems: 'flex-end'}}
              />
            </View>
            <ExpandCollapse style={{marginTop: 16}} value={expandOverview}>
              <View style={styles.valueHeaders}>
                <ReturnsComponent
                  topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Investment'}}
                  bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: investmentOverview.currentInvestment}}
                  containerStyles={{marginBottom: 24}}
                  bottomLeftStyles={styles.totalReturns}
                />
                <ReturnsComponent
                  topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Returns'}}
                  topRight={{type: ReturnsComponentValueType.PERCENT, value: investmentOverview.currentPAndLPercent}}
                  bottomRight={{type: ReturnsComponentValueType.AMOUNT, value: investmentOverview.currentPAndL}}
                  topRightStyles={styles.totalReturnsPercentage}
                  bottomRightStyles={styles.totalReturns}
                  containerStyles={{alignItems: 'flex-end'}}
                />
              </View>
            </ExpandCollapse>
            <View style={styles.seeDetailsLayout}>
              <Text style={styles.seeDetailsText}>Details</Text>
              {this.getSeeDetailsArrow()}
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  detailsArrow: {
    marginLeft: 4,
    marginTop: 6,
    width: 11,
    height: 7
  },
  investmentOverviewContainer: {
    marginTop: 16,
    marginLeft: 16,
    marginRight: 16
  },
  investmentOverviewTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Styles.PRIMARY_TEXT_COLOR,
    marginBottom: 24,
    letterSpacing: -0.3
  },
  valueHeaders: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  currentValueText: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR,
    marginRight: 6
  },
  currentValue: {
    fontSize: 20,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginTop: 4,
    letterSpacing: 0.5
  },
  totalReturnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  pAndLContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  totalReturnsText: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR,
    marginRight: 6
  },
  totalReturnsPercentage: {
    fontSize: 12,
    fontWeight: 'bold',
    letterSpacing: 0.3
  },
  totalReturns: {
    fontSize: 20,
    letterSpacing: 0.5,
    alignSelf: 'flex-end'
  },
  currentPercentage: {
    fontSize: 12,
    fontWeight: 'bold',
    letterSpacing: 0.3
  },
  seeDetailsLayout: {
    marginTop: 26,
    flexDirection: 'row',
  },
  seeDetailsText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgb(31, 122, 224)',
    marginBottom: 24
  },
});
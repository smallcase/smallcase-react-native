import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native';
import {getFormattedCurrency, SmallcaseStatus, Styles} from "../../../util/Utility";
import SmallcaseButton from "../../../component/SmallcaseButton";
import SmallcaseImage from "../../../component/SmallcaseImage";
import {ReturnsComponentValueType} from "../../../constants/Constants";
import ReturnsComponent from "../../../component/ReturnsComponent";

export default class AwiInvested extends Component {
  render() {
    const {awiSmallcase, showInvestedSmallcaseDetails, showAwiOptions, primaryActionClicked} = this.props;

    return (
      <TouchableWithoutFeedback onPress={() => showInvestedSmallcaseDetails(awiSmallcase)}>
        <View style={styles.awiContainer}>
          <View style={{flexDirection: 'row'}}>
            <SmallcaseImage imageStyles={styles.smallcaseImage} imageUrl={awiSmallcase.smallcaseImage}
                            source={awiSmallcase.source}/>
            <View style={{flex: 1}}>
              <Text style={styles.smallcaseName}>{awiSmallcase.name}</Text>
              <Text style={styles.boughtOn}>Bought on {awiSmallcase.boughtOn}</Text>
            </View>
            <TouchableOpacity onPress={showAwiOptions} style={{padding: 12, justifyContent: 'center'}}>
              <View><Image source={require('../../../../assets/overflowVertical.png')}/></View>
            </TouchableOpacity>
          </View>
          <View style={styles.awiMoneyInfoContainer}>
            <ReturnsComponent
              topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Value'}}
              bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: awiSmallcase.currentValue}}
              topLeftStyles={{fontSize: 12}}
              containerStyles={{flex: 1}}
            />
            <ReturnsComponent
              topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Total Returns'}}
              bottomLeft={{type: ReturnsComponentValueType.AMOUNT, value: awiSmallcase.totalReturns}}
              topLeftStyles={{fontSize: 12}}
              containerStyles={{flex: 1, marginLeft: 6}}
            />
            {
             awiSmallcase.status === SmallcaseStatus.PLACED?
               null :
               <SmallcaseButton onClick={() => primaryActionClicked(awiSmallcase.iscid, awiSmallcase.status)} buttonStyles={styles.actionButton}>
                 <Text style={styles.actionText}>{awiSmallcase.status === SmallcaseStatus.VALID? 'Invest' : 'Repair'}</Text>
               </SmallcaseButton>
            }
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  awiContainer: {
    borderRadius: 4,
    shadowColor: 'rgba(47, 54, 63, 0.3)',
    shadowOffset: {
      width: 0,
      height: 4
    },
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Styles.SILVER,
    justifyContent: 'center',
    shadowRadius: 8,
    shadowOpacity: 0.2,
    backgroundColor: 'white',
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 16
  },
  smallcaseName: {
    marginTop: 17,
    color: Styles.PRIMARY_TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: 16
  },
  boughtOn: {
    marginTop: 4,
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  awiMoneyInfoContainer: {
    flexDirection: 'row',
    marginTop: 16,
    marginBottom: 18,
    marginHorizontal: 16,
    alignItems: 'center'
  },
  actionButton: {
    height: 36,
    width: 84,
    backgroundColor: Styles.BLUE,
    marginLeft: 6
  },
  actionText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white'
  },
  smallcaseImage: {
    margin: 16,
    height: 48,
    width: 48
  }
});

import React, {PureComponent} from 'react';
import {
  ActionSheetIOS,
  FlatList,
  interpolate,
  Linking,
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native';

import {
  getCurrentInvestments,
  getExitedSmallcases,
  getInvestedSmallcaseDetails,
  getInvestedSmallcases
} from './AllInvestmentsService';
import {
  getSmallcaseImage,
  OrderLabel,
  OrderType,
  Routes,
  SmallcaseStatus,
  SnackBarTime,
  Styles
} from '../../util/Utility';
import {Actions} from 'react-native-router-flux';
import SnackBar from "../../component/SnackBar";
import EmptyState from "../components/EmptyState";
import InvestmentSummary from "./components/InvestmentSummary";
import InvestedSmallcase from "./components/InvestedSmallcase";
import {connect} from "react-redux";
import {getUserDetails} from "../../user/UserActions";
import {logOutUser} from "../../user/UserService";
import AwiInvested from "./components/AwiInvested";
import Divider from "../../component/Divider";
import InvestMorePopup from "../../component/InvestMorePopup";
import {getClosestInvestableAmount} from "../../order/OrderService";
import {isSipDue} from "../../helper/SmallcaseHelper";
import NotificationService from "../../user/notification/NotificationService";
import {checkMarketStatus} from "../../helper/MarketHelper";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";
import ExitedSmallcaseCountCard from "./components/ExitedSmallcaseCountCard";
import {GETTING_STARTED_URL} from "../../constants/Constants";
import {DISCOVER_URL} from "../../constants/Constants";
import {calculateWeightageFromShares, getPrices} from "../../helper/StockHelper";

class AllInvestmentsComponent extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      investedSmallcases: [],
      fetchingInvestment: true,
      fetchingInvestmentDetails: true,
      fetchingExited: true,
      refreshing: false,
      index: 0,
      exitedSmallcaseCount: 0,
      awiSmallcase: null,
      showInvestPopUp: false,
      smallcaseDetails: null,
      isSipDue: false
    };

    props.dispatch(getUserDetails(props.jwt, props.csrf));
    this.fetchInvestments = this.fetchInvestments.bind(this);
  }

  componentDidMount() {
    this.fetchInvestments();
    NotificationService.registerForNotification(this.props.jwt, this.props.csrf);
  };

  fetchInvestments() {
    const {jwt, csrf} = this.props;

    getCurrentInvestments(jwt, csrf)
      .then(data => {
        const investmentOverview = Object.assign(this.state.investmentOverview ? this.state.investmentOverview : {}, data);
        this.setState({investmentOverview, fetchingInvestmentDetails: false});
      })
      .catch((error) => {
        if (error.logoutUser) {
          logOutUser(jwt, csrf);
          return;
        }
        this.snackbar.show({
          title: SnackBarMessages.INVESTMENTS_FAILED,
          action: 'Refresh',
          duration: SnackBarTime.LONG,
          onAction: () => this.fetchInvestments()
        });

        this.setState({fetchingInvestmentDetails: false, refreshing: false});
      });

    getInvestedSmallcases(jwt, csrf)
      .then((data) => {
        const investmentOverview = Object.assign(this.state.investmentOverview ? this.state.investmentOverview : {}, data.investmentOverview);

        this.setState({
          fetchingInvestment: false,
          investmentOverview,
          refreshing: false,
          investedSmallcases: data.investedSmallcases,
          awiSmallcase: data.awiSmallcase
        });
      })
      .catch(() => {
        this.snackbar.show({
          title: SnackBarMessages.EXITED_SMALLCASES_FAILED,
          action: 'Refresh',
          duration: SnackBarTime.LONG,
          onAction: () => this.fetchInvestments()
        });
        this.setState({fetchingInvestment: false});
      });

    getExitedSmallcases(jwt, csrf)
      .then((data) => {
        this.setState({fetchingExited: false, exitedSmallcaseCount: data.exitedSmallcases.length});
      })
      .catch(() => this.setState({fetchingExited: false}));
  }

  renderExitedSmallcase = () => {
    const {exitedSmallcaseCount} = this.state;
    return exitedSmallcaseCount? <ExitedSmallcaseCountCard exitedSmallcaseCount={exitedSmallcaseCount} /> : null;
  }

  renderInvestments = ({item}) => {
    if (!this.state.investedSmallcases.length) return (<EmptyState type='invested'/>);

    return (<InvestedSmallcase item={item} showInvestedSmallcaseDetails={this.showInvestedSmallcaseDetails}/>);
  }

  showInvestedSmallcaseDetails = (item) => {
    Actions.push(Routes.INVESTMENT_DETAILS, {
      iscid: item.iscid,
      analytics: {
        accessedFrom: 'Investments',
        sectionTag: 'N/A',
        iscid: item.iscid,
        scid: item.scid,
        smallcaseName: item.smallcaseName,
        smallcaseSource: item.source,
        smallcaseType: 'N/A',
      }
    });
  }

  showAwiOptions = () => {
    ActionSheetIOS.showActionSheetWithOptions({
        options: ['See Orders', 'Cancel'],
        cancelButtonIndex: 1,
      },
      (buttonIndex) => {
        if (buttonIndex === 0) {
          Actions.push(Routes.ORDER_BATCHES, {smallcase: this.state.awiSmallcase});
        }
      });
  };

  continueToReviewOrder = (amountEntered) => {
    const {smallcaseDetails, isSipDue} = this.state;
    const {jwt, csrf} = this.props;

    const sids = [];
    smallcaseDetails.stocks.forEach(stock => sids.push(stock.sid));

    getPrices(sids, jwt, csrf)
      .then((prices) => {
        let newStocks = smallcaseDetails.stocks.map(stock => Object.assign({}, stock));
        newStocks.forEach(stock => stock.price = prices[stock.sid]);
        newStocks = calculateWeightageFromShares(newStocks);
        getClosestInvestableAmount(jwt, csrf, newStocks, amountEntered)
          .then((updatedStocks) => {
            updatedStocks.stocks.forEach((stock) => {
              stock.transactionType = OrderType.BUY;
            });

            const newSmallcaseDetails = {
              smallcaseName: smallcaseDetails.smallcaseName,
              source: smallcaseDetails.source,
              smallcaseImage: smallcaseDetails.smallcaseImage,
              stocks: updatedStocks.stocks,
              iscid: smallcaseDetails.iscid,
              scid: smallcaseDetails.scid,
              orderType: isSipDue ? OrderLabel.SIP : OrderLabel.INVESTMORE,
            };
            Actions.push(Routes.REVIEW_ORDER, {
              smallcaseDetails: newSmallcaseDetails,
              investment: updatedStocks.closestInvestableAmount,
              type: isSipDue ? OrderLabel.SIP : OrderLabel.INVESTMORE,
              repairing: false,
              analytics: {
                accessedFrom: 'All Investments',
                sectionTag: 'N/A',
              }
            });
          }).catch((error) => console.log(error));
      })
      .catch(error => console.log(error));
  }

  primaryActionClicked = (iscid, status) => {
    const {jwt, csrf, user} = this.props;
    const {awiSmallcase} = this.state;

    if (status === SmallcaseStatus.INVALID) {
      Actions.push(Routes.ORDER_BATCHES, {smallcase: awiSmallcase});
      return;
    }

    checkMarketStatus(jwt, csrf)
      .then(isOpen => {
        if (isOpen) {
          this.setState({showInvestPopUp: true});
          getInvestedSmallcaseDetails(iscid, jwt, csrf)
            .then((response) => {
              let stocks = [];
              response.currentConfig.constituents.forEach((stock) => {
                stocks.push(Object.assign({}, stock));
              });
              const smallcaseDetails = {
                smallcaseName: response.name,
                scid: response.scid,
                source: response.source,
                smallcaseImage: getSmallcaseImage(response.scid, response.source, '80'),
                iscid,
                stocks
              }

              this.setState({smallcaseDetails, isSipDue: isSipDue(user.actions.sip, iscid)});
            })
            .catch(error => console.log(error));
        } else {
          this.snackbar.show({
            title: SnackBarMessages.MARKET_CLOSED,
            duration: SnackBarTime.LONG,
          });
        }
      })
      .catch((error) => {
        this.snackbar.show({
          title: SnackBarMessages.MARKET_CLOSED,
          duration: SnackBarTime.LONG,
        });
      });
  }

  onInvestMorePopupClosed = () => this.setState({showInvestPopUp: false});

  renderInvestmentsOverview = () => {
    const {awiSmallcase, investedSmallcases} = this.state;

    if (awiSmallcase || investedSmallcases) {
      return (
        <View>
          <InvestmentSummary investmentOverview={this.state.investmentOverview} />
          <Text style={styles.yourSmallcases}>Your smallcases</Text>
          {
            awiSmallcase ?
              <View style={{backgroundColor: 'white'}}>
                <AwiInvested
                  awiSmallcase={awiSmallcase}
                  showInvestedSmallcaseDetails={this.showInvestedSmallcaseDetails}
                  showAwiOptions={this.showAwiOptions}
                  primaryActionClicked={this.primaryActionClicked}
                />
                <Text style={styles.otherSmallcases}>Other smallcases</Text>
                <Divider/>
              </View> : null
          }
        </View>
      );
    }

    return null;
  }

  onRefresh = () => {
    this.setState({refreshing: true});
    this.fetchInvestments();
  }

  gettingStarted = () => this.openLink(gettingStartedUrl);

  openLink = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  redirectToDiscover = () => this.openLink(discoverUrl);

  render = () => {
    const {
      fetchingInvestment, fetchingInvestmentDetails, fetchingExited, investedSmallcases, awiSmallcase,
      showInvestPopUp, smallcaseDetails
    } = this.state;

    let content = null;
    if ((fetchingInvestment || fetchingInvestmentDetails || fetchingExited)) {
      content = (
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <Loader loaderStyles={{marginTop: 64}}/>
        </View>
      );
    } else if (!investedSmallcases.length && !awiSmallcase) {
      content = (<EmptyState type='awiInvested' gettingStarted={this.gettingStarted}/>);
    } else if (!investedSmallcases.length) {
      content = (
        <ScrollView style={{backgroundColor: 'rgb(249, 250, 251)'}}>
          {this.renderInvestmentsOverview()}
          <EmptyState type='invested'/>
          {this.renderExitedSmallcase()}
        </ScrollView>
      );
    } else {
      content = (
        <FlatList
          style={{backgroundColor: 'rgb(249, 250, 251)'}}
          data={investedSmallcases}
          renderItem={this.renderInvestments}
          ListHeaderComponent={this.renderInvestmentsOverview}
          keyExtractor={(item) => item.iscid}
          extraData={this.state}
          initialNumToRender={4}
          ListFooterComponent={this.renderExitedSmallcase}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        />
      );
    }

    return (
      <View style={{flex: 1}}>
        {content}
        <SnackBar onRef={ref => this.snackbar = ref}/>
        <InvestMorePopup
          showPopup={showInvestPopUp}
          orderPopupClosed={this.onInvestMorePopupClosed}
          startOffSet={60}
          smallcase={smallcaseDetails}
          stocks={smallcaseDetails ? smallcaseDetails.stocks.slice(0) : null}
          continueToReviewOrder={this.continueToReviewOrder}
        />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  emptyStateDescription: {
    marginTop: 8,
    lineHeight: 21,
    textAlign: 'center',
    fontSize: 14,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  emptyStateImage: {
    height: 200,
    marginHorizontal: 50,
    marginTop: 32,
  },
  emptyStateTitle: {
    marginTop: 32,
    letterSpacing: -0.1,
    fontSize: 16,
    fontWeight: 'bold',
    color: Styles.PRIMARY_TEXT_COLOR,
    lineHeight: 24,
    textAlign: 'center'
  },
  exitedSmallcaseContainer: {
    borderWidth: 0.5,
    borderColor: 'rgb(221, 224, 228)',
    margin: 16,
    padding: 16,
    backgroundColor: 'white',
    borderRadius: 4
  },
  investmentsContainer: {
    backgroundColor: 'white'
  },
  otherSmallcases: {
    marginTop: 24,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginLeft: 16,
    marginBottom: 8
  },

  seeExitedSmallcase: {
    color: Styles.BLUE,
    marginTop: 8,
    fontWeight: 'bold'
  },
  yourSmallcases: {
    fontSize: 20,
    fontWeight: 'bold',
    color: Styles.PRIMARY_TEXT_COLOR,
    letterSpacing: -0.3,
    paddingLeft: 16,
    paddingTop: 26,
    paddingBottom: 26,
    backgroundColor: 'white'
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  user: state.userReducer.user
});

module.exports = connect(mapStateToProps, null, null, {withRef: true})(AllInvestmentsComponent);
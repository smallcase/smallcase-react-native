import {
  EXITED_SMALLCASES,
  INVESTED_SMALLCASE_DETAILS,
  INVESTED_SMALLCASES,
  TOTAL_INVESTMENT,
} from '../../api/ApiRoutes';
import {getReadableDate, getSmallcaseImage} from '../../util/Utility';
import Api from "../../api/Api";
import {EM_DASH, SmallcaseType} from "../../constants/Constants";

const getPresentableInvestments = (totalInvestments) => {
  const currentPAndL = (totalInvestments.networth - totalInvestments.unrealizedInvestment).toFixed(2);
  const currentPAndLPercent = (((totalInvestments.networth - totalInvestments.unrealizedInvestment) / totalInvestments.unrealizedInvestment) * 100).toFixed(2);
  return {
    currentValue: totalInvestments.networth.toFixed(2),
    currentPAndL,
    currentPAndLPercent,
    currentInvestment: totalInvestments.unrealizedInvestment
  };
}

const getPresentableInvestedSmallcases = (investedSmallcases) => {
  let presentableInvestedSmallcases = [];
  let totalNetWorth = 0, totalUnrealizedInvestment = 0, totalRealizedReturn = 0,
    totalRealizedInvestment = 0, totalDivReturns = 0, totalReturnsPercent = 0;
  let awiSmallcase = null;

  for (let i = 0; i < investedSmallcases.length; i++) {
    let investedSmallcase = investedSmallcases[i];
    let totalReturns, totalPAndLPercent, changeInIndexPercent;
    if (investedSmallcase.returns.unrealizedInvestment === 0) {
      totalReturns = EM_DASH;
      totalPAndLPercent = EM_DASH;
      changeInIndexPercent = EM_DASH;
    } else {
      totalReturns = (investedSmallcase.returns.realizedReturns + (investedSmallcase.returns.networth - investedSmallcase.returns
        .unrealizedInvestment) + investedSmallcase.returns.divReturns + investedSmallcase.returns.creditedDivReturns
        + investedSmallcase.returns.accruedDivReturns).toFixed(0);
      totalReturnsPercent = (((investedSmallcase.returns.realizedReturns + (investedSmallcase.returns.networth - investedSmallcase.returns
        .unrealizedInvestment)) / investedSmallcase.returns.unrealizedInvestment + investedSmallcase.returns.realizedInvestment) * 100).toFixed(2);
      changeInIndexPercent = (((investedSmallcase.stats.indexValue - investedSmallcase.stats.lastCloseIndex) / investedSmallcase.stats.lastCloseIndex) * 100 ).toFixed(2);
    }

    totalNetWorth += investedSmallcase.returns.networth;
    totalRealizedReturn += investedSmallcase.returns.realizedReturns;
    totalUnrealizedInvestment += investedSmallcase.returns.unrealizedInvestment;
    totalRealizedInvestment += investedSmallcase.returns.realizedInvestment;
    totalDivReturns += (investedSmallcase.returns.divReturns + investedSmallcase.returns.creditedDivReturns
      + investedSmallcase.returns.accruedDivReturns);

    if (investedSmallcase.type === SmallcaseType.AWI) {
      awiSmallcase = {
        name: investedSmallcase.name,
        boughtOn: getReadableDate(investedSmallcase.date),
        currentValue: investedSmallcase.returns.networth ? investedSmallcase.returns.networth.toFixed(0) : EM_DASH,
        smallcaseImage: getSmallcaseImage(investedSmallcase.scid, investedSmallcase.source, '80'),
        scid: investedSmallcase.scid,
        iscid: investedSmallcase._id,
        source: investedSmallcase.source,
        status: investedSmallcase.status,
        totalReturns: totalReturns ? totalReturns : EM_DASH,
      }
      continue;
    }

    presentableInvestedSmallcases.push({
      index: investedSmallcase.stats.indexValue ? investedSmallcase.stats.indexValue.toFixed(2) : EM_DASH,
      currentValue: investedSmallcase.returns.networth ? investedSmallcase.returns.networth.toFixed(0) : EM_DASH,
      smallcaseName: investedSmallcase.name,
      boughtOn: getReadableDate(investedSmallcase.date),
      smallcaseImage: getSmallcaseImage(investedSmallcase.scid, investedSmallcase.source, '80'),
      scid: investedSmallcase.scid,
      iscid: investedSmallcase._id,
      totalReturns: totalReturns ? totalReturns : EM_DASH,
      totalReturnsPercent,
      source: investedSmallcase.source,
      changeInIndexPercent,
      status: investedSmallcase.status
    });
  }
  const totalPAndL = totalRealizedReturn + (totalNetWorth - totalUnrealizedInvestment) + totalDivReturns;
  const totalPAndLPercent = ((totalRealizedReturn + (totalNetWorth - totalUnrealizedInvestment) + totalDivReturns) * 100 /
    (totalRealizedInvestment + totalUnrealizedInvestment)).toFixed(2);
  return {
    awiSmallcase,
    investedSmallcases: presentableInvestedSmallcases,
    investmentOverview: {
      totalNetWorth: totalNetWorth.toFixed(0),
      totalRealizedReturn: totalRealizedReturn.toFixed(0),
      totalUnrealizedInvestment: totalUnrealizedInvestment.toFixed(0),
      totalRealizedInvestment: totalRealizedInvestment.toFixed(0),
      totalPAndLPercent,
      totalPAndL
    }
  };
}


export const getCurrentInvestments = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: TOTAL_INVESTMENT, jwt, csrf})
      .then((response) => resolve(getPresentableInvestments(response.data)))
      .catch((error) => {
        if (error && (error.data.status === 401)) {
          reject({logoutUser: true});
          return;
        }
        reject({logoutUser: false});
      });
  });
}

export const getInvestedSmallcases = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: INVESTED_SMALLCASES, jwt, csrf})
      .then((response) => resolve(getPresentableInvestedSmallcases(response.data.investedSmallcases)))
      .catch((error) => reject(error));
  });
};

export const getExitedSmallcases = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: EXITED_SMALLCASES, jwt, csrf})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const getInvestedSmallcaseDetails = (iscid, jwt, csrf) => {
  const params = 'iscid=' + iscid;

  return new Promise((resolve, reject) => {
    Api.get({route: INVESTED_SMALLCASE_DETAILS, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

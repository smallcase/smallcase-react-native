import React, {PureComponent} from "react";
import {Image, Text, View, StyleSheet} from "react-native";
import SmallcaseImage from "../../../component/SmallcaseImage";
import {Styles} from "../../../util/Utility";
import {EM_DASH} from "../../../constants/Constants";

export default class SmallcaseInfoCard extends PureComponent {
  render() {
    let {smallcaseDetails} = this.props;
    return (
      <View style={styles.smallcaseInfoContainer}>
        <SmallcaseImage source={smallcaseDetails.source} imageUrl={smallcaseDetails.smallcaseImage}
                        imageStyles={styles.smallcaseImage} textSize={8}/>
        <View style={styles.nameAndDateContainer}>
          <Text style={styles.smallcaseName}>{smallcaseDetails.smallcaseName}</Text>
          <Text style={styles.boughtOn}>Bought On: {smallcaseDetails.boughtOn}</Text>
        </View>
        <View style={styles.indexContainer}>
          <Text style={styles.indexText}>Index</Text>
          <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
            <Text style={styles.index}>{smallcaseDetails.index}</Text>
            <Image style={styles.indexTrend}
                   source={smallcaseDetails.indexChange >= 0 ? require('../../../../assets/iconCaretGreen.png') : require('../../../../assets/iconCaretRed.png')}/>
            <Text
              style={[styles.changeInIndex, {color: smallcaseDetails.indexChange >= 0 ? 'rgb(25, 175, 85)' : 'rgb(216, 47, 68)'}]}>
              {smallcaseDetails.indexChange === EM_DASH ? '' : Math.abs(smallcaseDetails.indexChange)}%
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  smallcaseInfoContainer: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: 'rgb(249, 250, 251)'
  },
  smallcaseImage: {
    width: 50,
    height: 50,
    margin: 16,
    borderRadius: 4,
  },
  nameAndDateContainer: {
    marginTop: 18,
    marginBottom: 24,
    flex: 2,
    marginRight: 12
  },
  smallcaseName: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: -0.2,
    lineHeight: 18 * 1.2,
    marginRight: 16
  },
  boughtOn: {
    marginTop: 6,
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR,
  },
  indexContainer: {
    marginTop: 20,
    marginBottom: 24,
    marginRight: 16,
    flex: 1,
  },
  indexText: {
    fontSize: 14,
    marginTop: 2,
    alignSelf: 'flex-end',
    color: Styles.SECONDARY_TEXT_COLOR
  },
  index: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 4,
    letterSpacing: 0.4,
    alignSelf: 'flex-end',
  },
  indexTrend: {
    height: 8,
    width: 10,
    marginLeft: 4,
    marginTop: 12
  },
  changeInIndex: {
    marginTop: 8,
    marginLeft: 4,
    fontSize: 12,
    fontWeight: 'bold',
  },
});

import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import {SmallcaseStatus, Styles} from "../../../util/Utility";
import SmallcaseButton from "../../../component/SmallcaseButton";

export const SipCard = (props) => {
  return (
    <View style={styles.sipContainer}>
      {props.isSipSet ?
        <TouchableOpacity onPress={props.setUpSip}>
          <View style={{flexDirection: 'row'}}>
            <Image style={styles.sipActiveImage} source={require('../../../../assets/sipSmall.png')}/>
            <View style={{marginLeft: 6}}>
              <Text style={styles.sipActive}>SIP Active</Text>
              <Text style={styles.sipActiveOn}>Your {props.frequency} SIP in this smallcase is next due on
                <Text style={{fontWeight: 'bold'}}> {props.sipDate}</Text></Text>
              <Text style={styles.editSip}>Edit SIP</Text>
            </View>
          </View>
        </TouchableOpacity> :
        <TouchableOpacity onPress={props.setUpSip}>
          <View style={{flexDirection: 'row'}}>
            <Image style={styles.sipImage} source={require('../../../../assets/sip.png')}/>
            <View style={{marginVertical: 12}}>
              <Text style={styles.sip}>Beat market volatility,{'\n'}Setup an SIP today</Text>
              <SmallcaseButton buttonStyles={styles.sipButton} onClick={props.setUpSip}>
                <Text style={styles.startSip}>Start SIP</Text>
              </SmallcaseButton>
            </View>
          </View>
        </TouchableOpacity>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  sipContainer: {
    flexDirection: 'row',
    padding: 12,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgb(221, 224, 228)',
    shadowColor: 'rgba(0, 0, 0, 0.09)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 0.2,
    shadowOpacity: 0.25
  },
  sipActiveImage: {
    height: 24,
    width: 24,
  },
  sipActive: {
    marginTop: 4,
    color: Styles.SECONDARY_TEXT_COLOR,
    backgroundColor: 'transparent'
  },
  sipActiveOn: {
    flex: 1,
    marginTop: 8,
    lineHeight: 21,
    marginRight: 48,
  },
  editSip: {
    marginTop: 12,
    fontWeight: 'bold',
    fontSize: 13,
    color: Styles.BLUE,
    backgroundColor: 'transparent',
    marginBottom: 8
  },
  sipImage: {
    width: 90,
    height: 100,
    marginRight: 24,
    alignItems: 'center'
  },
  sip: {
    color: 'rgb(83, 91, 98)',
    lineHeight: 22,
    fontSize: 16,
    marginBottom: 12
  },
  sipButton: {
    height: 34,
    width: 89,
    borderWidth: 1,
    borderColor: 'rgba(31, 122, 224, 0.3)',
    shadowOpacity: 0,
    alignItems: 'center'
  },
  startSip: {
    color: 'rgb(31, 122, 224)',
    fontWeight: 'bold',
  },
});

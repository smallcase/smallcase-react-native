import {Animated, Easing, Text, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import React, {PureComponent} from "react";
import ExpandCollapse from "../../../component/ExpandCollapse";
import Divider from "../../../component/Divider";
import {getFormattedCurrency, Styles} from "../../../util/Utility";
import ReturnsNumber from "../../../component/ReturnsNumber";
import {LIQUID_BEES, ReturnsComponentValueType} from "../../../constants/Constants";
export default class StockCard extends PureComponent {

  expandStock = (stock) => {
    stock.expanded = !stock.expanded;
    if (stock.expanded) {
      stock.startDeg = '0deg';
      stock.endDeg = '180deg';
    } else {
      stock.startDeg = '180deg';
      stock.endDeg = '0deg';
    }

    stock.spinValue.setValue(0);
    Animated.timing(
      stock.spinValue, {
        toValue: 1,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();

    this.setState({expanded: stock.expanded});
  };

  renderPrimaryStockContent = ({title, pAndL, stock}) => {
    let {getStockDetailsArrow, showStockWidget} = this.props;
    let detailsArrow = (<View style={styles.stockDetailsArrow}/>);
    if (stock.sid !== -1) detailsArrow = getStockDetailsArrow(stock);
    return (
      <TouchableWithoutFeedback onPress={this.expandStock.bind(this, stock)}>
        <View style={styles.mainStockCardContainer}>
          <TouchableWithoutFeedback onPress={showStockWidget.bind(this, stock ? stock.sid : '')}>
            <View style={styles.stockNameContainer}>
              <Text style={[styles.stockName, {
                color: stock ? 'rgb(47, 54, 63)' : 'rgb(83, 91, 98)',
              }]} numberOfLines={1} ellipsizeMode={'tail'}>
                {title}
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <Text style={[styles.pAndL, {
            color: stock ? stock.pAndL >= 0 ? 'rgb(25, 175, 85)' : 'rgb(216, 47, 68)' : 'rgb(83, 91, 98)',
          }]}>{ stock.sid === LIQUID_BEES ? 'Div.' : pAndL}</Text>
          {detailsArrow}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    let {item, index} = this.props;
  const dividendsText = item.divReturns > 0 ?  (<Text style={{marginBottom:16, fontSize:12,color: Styles.SECONDARY_TEXT_COLOR, lineHeight:19}}>
    This is a fixed income ETF which gives dividend (Div.) returns only. You have received <ReturnsNumber value={item.divReturns} type={ReturnsComponentValueType.AMOUNT} /> in dividends. Check details below. </Text>) : (<Text style={{marginBottom:16, fontSize:12,color: Styles.SECONDARY_TEXT_COLOR, lineHeight:19}}> This is a fixed income ETF which gives dividend (Div.) returns only.</Text>)



    return (
      <View>
        <Divider dividerStyles={{opacity: index === 1? 1 : 0.4, marginLeft: index === 1? 0 : 16}}/>
        {this.renderPrimaryStockContent({
          title: item.sidInfo.name,
          pAndL: item.pAndL + '%',
          stock: item,
        })}
        <ExpandCollapse value={item.expanded} style={{paddingTop: 12, backgroundColor: 'rgb(249, 250, 251)'}}>
          <View style={{marginHorizontal: 16}}>
            {item.sid === LIQUID_BEES && dividendsText}
            <View style={styles.stockSecondaryInfo}>
              <Text style={styles.stockConfig}>Weightage: </Text>
              <Text style={styles.stockConfigValue}>{(item.weight * 100).toFixed(2)}%</Text>
              <View style={styles.totalReturnsContainer}>
                <Text style={styles.stockConfig}>Current Price: </Text>
                <Text style={styles.stockConfigValue}>&#8377;{item.price}</Text>
              </View>
            </View>
            <View style={[styles.stockSecondaryInfo, {marginBottom: 16, marginTop: 8}]}>
              <Text style={styles.stockConfig}>Shares: </Text>
              <Text style={styles.stockConfigValue}>{item.shares}</Text>
              <View style={styles.totalReturnsContainer}>
                <Text style={styles.stockConfig}>Average Buy Price: </Text>
                <Text style={styles.stockConfigValue}>&#8377;{item.averagePrice}</Text>
              </View>
            </View>
          </View>
        </ExpandCollapse>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  stockSecondaryInfo: {
    flex: 1,
    flexDirection: 'row'
  },
  stockConfig: {
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  stockConfigValue: {
    fontSize: 12,
    color: Styles.PRIMARY_TEXT_COLOR
  },
  totalReturnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  mainStockCardContainer: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  stockNameContainer: {
    flex: 4,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  stockName: {
    flex: 4,
    fontSize: 14
  },
  pAndL: {
    fontSize: 14,
    justifyContent: 'center',
    marginLeft: 24
  },
  stockDetailsArrow: {
    marginLeft: 24,
    width: 10,
    height: 7
  },
});
import React, {PureComponent} from "react";
import {Actions} from "react-native-router-flux";
import {Image, Text, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import {AnalyticsSource, Routes, Styles} from "../../../util/Utility";
import {MediaQueryStyleSheet} from "react-native-responsive";
import {IPAD_PRO_SMALLEST} from "../../../constants/Constants";
import AnalyticsManager from "../../../analytics/AnalyticsManager";

export default class News extends PureComponent {

  openNews = (newsObject) => {

    AnalyticsManager.track('Viewed News Article',
      {
        accessedFrom: 'Investment Details',
        sectionTag: 'N/A',
        newsDate: newsObject.date,
        newsId: newsObject._id,
        source: newsObject.publisher
      },
      [AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])

    const link = newsObject.link
     Actions.push(Routes.SHOW_NEWS, {url:link});
  }


  render() {
    let {item} = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.openNews.bind(this, item)}>
        <View style={{flexDirection: 'row', flex: 1, marginRight: 16}}>
          <Image style={styles.newsImage} source={{uri: item.imageUrl}}/>
          <View style={styles.newsInfoContainer}>
            <Text style={styles.newsHeadline} numberOfLines={3} ellipsizeMode='tail'>{item.headline}</Text>
            <View style={{flexDirection: 'row', marginBottom: 12}}>
              <Text style={styles.newsDate}>{item.date} on </Text>
              <Text style={styles.publisher}>{item.publisher}</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  newsImage: {
    margin: 16,
    borderRadius: 3,
    width: 64,
    height: 64,
    backgroundColor: 'rgb(249, 250, 251)'
  },
  newsInfoContainer: {
    flexDirection: 'column',
    marginRight: 16
  },
  newsHeadline: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Styles.PRIMARY_TEXT_COLOR,
    lineHeight: 20,
    marginVertical: 16,
    alignItems: 'stretch',
    marginRight: 86
  },
  newsDate: {
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  publisher: {
    fontSize: 12,
    color: 'rgb(83, 91, 98)',
    marginRight: 56,
    flex: 1,
  },
}, {
  [IPAD_PRO_SMALLEST]: {
    newsHeadline: {
      maxWidth: 600
    }
  }
});
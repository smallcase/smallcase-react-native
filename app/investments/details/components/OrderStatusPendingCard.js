import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native';
import {Styles} from "../../../util/Utility";

const OrderStatusPendingCard = (props) => {
  const {viewOrderClicked} = props;

  return (
    <TouchableWithoutFeedback onPress={viewOrderClicked}>
      <View style={styles.container}>
        <Image source={require('../../../../assets/warning.png')} style={styles.warningImage}/>
        <View style={{marginVertical: 16}}>
          <Text style={{color: Styles.PRIMARY_TEXT_COLOR}}>Your last order's status is pending</Text>
          <Text style={styles.viewOrder}>View Order</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    margin: 16,
    borderWidth: 0.5,
    borderColor: 'rgb(221, 224, 228)',
    backgroundColor: 'white'
  },
  warningImage: {
    height: 18,
    width: 18,
    marginLeft: 16,
    marginTop: 16,
    marginRight: 8
  },
  viewOrder: {
    color: Styles.BLUE,
    fontWeight: 'bold',
    marginTop: 8,
  }
});

module.exports = OrderStatusPendingCard;

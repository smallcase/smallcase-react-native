import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Animated, Easing,
} from 'react-native';
import SmallcaseButton from "../../../component/SmallcaseButton";
import {getBottomBarPaddingBottom, getFormattedCurrency, SmallcaseStatus, Styles} from "../../../util/Utility";
import {getPrices} from '../../../helper/StockHelper'
import {skipRebalance} from "../../rebalance/RebalanceService";
import {calculateMinInvestment} from "../../../helper/SmallcaseHelper";
import {EM_DASH} from "../../../constants/Constants";

const ActionType = {
  REPAIR: 'repair',
  SIP: 'SIP',
  REBALANCE: 'rebalance'
};

export default class ActionSheet extends PureComponent {
  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(0);
    this.state = {
      minimumInvestmentAmount: EM_DASH
    }
  }

  componentDidMount() {
    const sids = this.props.stocks.map(stock => stock.sid)
    this.refreshPrices(sids)

  }

  componentWillReceiveProps(newProps) {
    if (this.props.repairOrderPending !== newProps.repairOrderPending) {
      this.forceUpdate();
    }
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 500,
        easing: Easing.easeOut
      }
    ).start();
  }

  refreshPrices = (sids) => {
    getPrices(sids, this.props.jwt, this.props.csrf)
      .then(stocks => {
        const newStocks = []
        this.props.stocks.forEach(stock => {
          const stockComponent = Object.assign({}, stock);
          stockComponent.price = stocks[stock.sid];
          newStocks.push(stockComponent);
        })
        this.setState({minimumInvestmentAmount: calculateMinInvestment(newStocks).minAmount});
      })
  }

  onRefreshPress = () => {
    const sids = this.props.stocks.map(stock => stock.sid);
    this.refreshPrices(sids);
    this.spin();
  };

  render() {
    const {smallcaseStatus, showInvestMorePopUp, rebalancePending, isSipDue, fixAction, rebalanceInfo} = this.props;

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    });
    let actionType = null;
    let order = null;
    if (smallcaseStatus !== SmallcaseStatus.VALID) {
      actionType = ActionType.REPAIR;
      let requiredSmallcase;
      fixAction.fix.forEach((smallcase) => {
        if (smallcase.iscid === fixAction.iscid) {
          requiredSmallcase = smallcase;
        }
      });
      if (requiredSmallcase) {
        order = {
          unfilled: requiredSmallcase.quantity - requiredSmallcase.filled,
          total: requiredSmallcase.quantity
        };
      } else {
        return null;
      }
    } else if (rebalancePending) {
      if (!rebalanceInfo) return <View/>;

      actionType = ActionType.REBALANCE;
    } else if (isSipDue) {
      actionType = ActionType.SIP;
    }

    if (actionType) {
      return (
        <ActionLayout
          {...this.props}
          actionType={actionType}
          order={order}
        />
      );
    }

    return (
      <View style={[styles.actionSheetContainer, {paddingBottom: getBottomBarPaddingBottom()}]}>
        <TouchableWithoutFeedback style={styles.minInvestContainer} onPress={this.onRefreshPress}>
          <View style={{flex: 1}}>
            <Text style={styles.minInvestment}>Min. Investment Amt.</Text>
            <View style={styles.investMoreContainer}>
              <Text style={styles.minInvestmentAmount}>{getFormattedCurrency(this.state.minimumInvestmentAmount)}</Text>
              <Animated.Image style={[styles.refreshIcon, {transform: [{rotate: spin}]}]}
                              source={require('../../../../assets/refresh.png')}/>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <SmallcaseButton disabled={smallcaseStatus !== SmallcaseStatus.VALID}
                         onClick={() => showInvestMorePopUp()}
                         buttonStyles={styles.investButton}>
          <Text style={styles.investText}>Invest More</Text>
        </SmallcaseButton>
      </View>
    );
  }
}

const ActionLayout = (props) => {
  let icon, title, description, primaryAction, secondaryAction, primaryActionText, secondaryActionText;
  switch (props.actionType) {
    case ActionType.REBALANCE:
      icon = require('../../../../assets/rebalance.png');
      title = props.rebalanceInfo.label;
      description = props.rebalanceInfo.description;
      primaryAction = props.openRebalance;
      secondaryAction = props.skipRebalance;
      primaryActionText = 'See Update';
      secondaryActionText = 'Skip';
      break;

    case ActionType.REPAIR:
      icon = require('../../../../assets/repair.png');
      title = props.order.unfilled + ' of ' + props.order.total + ' orders are unfilled';
      description = 'Please try repairing your last order batch';
      primaryAction = props.repair;
      secondaryAction = props.repair;
      primaryActionText = 'Repair Order';
      secondaryActionText = 'See Errors';
      break;

    case ActionType.SIP:
      icon = require('../../../../assets/sipSmall.png');
      title = 'SIP Installment due';
      description = 'Systematic investments help you beat market volatility';
      primaryAction = props.showInvestMorePopUp;
      secondaryAction = props.skipSip;
      primaryActionText = 'Invest Now';
      secondaryActionText = 'Skip';
      break;
  }

  return (
    <View style={[styles.fixContainer, {paddingBottom: getBottomBarPaddingBottom()}]}>
      <Image style={styles.fixIcon} source={icon}/>
      <View style={styles.fixDetailsContainer}>
        <Text style={styles.orderStatus}>{title}</Text>
        <Text style={{marginRight: 62}} numberOfLines={2} ellipsizeMode={'tail'}>{description}</Text>
        <View style={styles.actionsContainer}>
          <SmallcaseButton onClick={primaryAction}
                           buttonStyles={[styles.actions, {marginRight: 16, backgroundColor: Styles.BLUE}]}>
            <Text style={styles.repair}>{primaryActionText}</Text>
          </SmallcaseButton>
          <SmallcaseButton onClick={secondaryAction} buttonStyles={styles.actions}>
            <Text style={styles.secondaryText}>{secondaryActionText}</Text>
          </SmallcaseButton>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  actionSheetContainer: {
    backgroundColor: 'white',
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 0.8,
    flexDirection: 'row',
    alignItems: 'stretch',
    paddingVertical: 16,
    paddingLeft: 16,
    justifyContent: 'center'
  },
  minInvestContainer: {
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 16,
    alignItems: 'flex-start',
  },
  investButton: {
    backgroundColor: 'rgb(39, 188, 148)',
    height: 48,
    paddingHorizontal: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 16,
    alignSelf: 'center',
  },
  investText: {
    color: 'white',
    fontWeight: 'bold'
  },
  minInvestment: {
    color: 'rgb(129, 135, 140)',
    fontSize: 12,
    marginTop: -4
  },
  minInvestmentAmount: {
    color: 'rgb(47, 54, 63)',
    letterSpacing: 0.4,
    fontSize: 22
  },
  investMoreContainer: {
    marginTop: 4,
    flexDirection: 'row',
    alignItems: 'center'
  },
  refreshIcon: {
    height: 18,
    width: 18,
    marginLeft: 4,

  },
  fixIcon: {
    width: 24,
    height: 24,
    marginLeft: 16,
    marginTop: 16
  },
  fixDetailsContainer: {
    marginLeft: 8,
    marginRight: 16,
    marginTop: 20
  },
  orderStatus: {
    fontWeight: 'bold',
    color: 'rgb(28, 34, 42)',
    marginBottom: 8
  },
  actionsContainer: {
    marginTop: 16,
    flexDirection: 'row',
  },
  fixContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 0.8,
  },
  actions: {
    alignItems: 'center',
    width: 130,
    height: 34
  },
  repair: {
    color: 'white',
    fontWeight: 'bold'
  },
  secondaryText: {
    color: Styles.BLUE,
    fontWeight: 'bold'
  }
});

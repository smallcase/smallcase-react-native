import {Animated, Easing, Text, TouchableWithoutFeedback, View, StyleSheet} from "react-native";
import React, {PureComponent} from "react";
import {AnalyticsSource, getFormattedCurrency, Styles} from "../../../util/Utility";
import ExpandCollapse from "../../../component/ExpandCollapse";
import Divider from "../../../component/Divider";
import ReturnsComponent from '../../../component/ReturnsComponent'
import AnalyticsManager from "../../../analytics/AnalyticsManager";
import {ReturnsComponentValueType} from "../../../constants/Constants";

export default class SmallcaseNumbers extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      spinValue: new Animated.Value(0),
      spinStartDegree: '0deg',
      spinEndDegree: '180deg',
      expandOverview: false
    }
  }

  getSeeDetailsArrow = () => {
    // Interpolate beginning and end values (in this case 0 and 1)
    const spin = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [this.state.spinStartDegree, this.state.spinEndDegree]
    });

    return (
      <Animated.Image style={[styles.detailsArrow, {transform: [{rotate: spin}]}]}
                      source={require('../../../../assets/iconArrowDownBlue.png')}/>
    );
  };

  toggleInvestmentOverview = () => {
    this.state.spinValue.setValue(0);
    Animated.timing(
      this.state.spinValue, {
        toValue: 1,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();
    this.setState({expandOverview: !this.state.expandOverview});
    this.setState({
      spinStartDegree: this.state.expandOverview ? '180deg' : '0deg',
      spinEndDegree: this.state.expandOverview ? '0deg' : '180deg'
    });

    if (this.state.expandOverview) {
      AnalyticsManager.track('Toggled P&L Overview',
        {
          smallcaseName: this.props.smallcaseDetails.smallcaseName,
          smallcaseSource: this.props.smallcaseDetails.source,
          smallcaseType: 'N/A',
        },
        [AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])
    }

  };

  render() {
    let {smallcaseDetails, getPAndLColor} = this.props;
    let {expandOverview} = this.state;
    return (
      <View>
        <Text style={styles.performanceTitle}>Performance</Text>
        <TouchableWithoutFeedback onPress={this.toggleInvestmentOverview.bind(this)}>
          <View style={styles.investmentsContainer}>
            <View style={styles.valueHeaders}>
              <ReturnsComponent
                topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Value'}}
                bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: smallcaseDetails.currentValue}}
                topLeftStyles={styles.currentValueText}
                bottomLeftStyles={styles.currentValue}
              />
              <ReturnsComponent
                topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Total Returns'}}
                topRight={{type: ReturnsComponentValueType.PERCENT, value: smallcaseDetails.totalPAndLPercent}}
                bottomRight={{type: ReturnsComponentValueType.AMOUNT, value: smallcaseDetails.totalPAndL}}
                containerStyles={{alignItems: 'flex-end'}}
                topLeftStyles={styles.currentValueText}
                topRightStyles={styles.percent}
                bottomRightStyles={styles.currentValue}
                />
            </View>
            <ExpandCollapse style={{marginTop: 16}} value={expandOverview} easing='easeInOut'>
              <View>
                <View style={styles.valueHeaders}>
                  <ReturnsComponent
                    topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Investment'}}
                    bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: smallcaseDetails.currentInvestment}}
                    topLeftStyles={styles.currentValueText}
                    bottomLeftStyles={styles.currentValue}
                    containerStyles={{marginBottom: 24}}
                  />
                  <ReturnsComponent
                    topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Current Returns'}}
                    topRight={{type: ReturnsComponentValueType.PERCENT, value: smallcaseDetails.currentPAndLPercent}}
                    bottomRight={{type: ReturnsComponentValueType.AMOUNT, value: smallcaseDetails.currentPAndL}}
                    containerStyles={{alignItems: 'flex-end'}}
                    topLeftStyles={styles.currentValueText}
                    topRightStyles={styles.percent}
                    bottomRightStyles={styles.currentValue}
                  />
                </View>
                <View style={styles.valueHeaders}>
                  <ReturnsComponent
                    topLeft={{type: ReturnsComponentValueType.NORMAL, value: 'Money Put In'}}
                    bottomLeft={{type: ReturnsComponentValueType.UNCOLORED_AMOUNT, value: smallcaseDetails.moneyPutIn}}
                    topLeftStyles={styles.currentValueText}
                    bottomLeftStyles={styles.currentValue}
                    containerStyles={{marginBottom: 24}}
                  />
                  <View style={styles.totalReturnsContainer}>
                    <View style={{alignItems: 'flex-end'}}>
                      <Text style={styles.totalReturnsText}>Realized Returns + Dividends</Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={[styles.totalReturns, {color: getPAndLColor(smallcaseDetails.realizedPAndL)}]}>
                          {getFormattedCurrency(smallcaseDetails.realizedPAndL)}
                        </Text>
                        <Text style={styles.divReturns}> + {getFormattedCurrency(smallcaseDetails.divReturns)}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ExpandCollapse>
            <View style={styles.seeDetailsLayout}>
              <Text style={styles.seeDetailsText}>Details</Text>
              {this.getSeeDetailsArrow()}
            </View>
          </View>
        </TouchableWithoutFeedback>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  performanceTitle: {
    marginLeft: 16,
    fontSize: 18,
    marginTop: 24,
    letterSpacing: -0.2,
    fontWeight: 'bold',
    marginBottom: 20
  },
  investmentsContainer: {
    marginLeft: 16,
    marginRight: 16
  },
  valueHeaders: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  currentValueText: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR,
  },
  currentValue: {
    fontSize: 20,
    letterSpacing: 0.5
  },
  totalReturnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  pAndLContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  totalReturnsText: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR,
  },
  currentPercentage: {
    fontSize: 12,
    fontWeight: 'bold',
    letterSpacing: 0.3,
    marginLeft: 4
  },
  totalReturns: {
    fontSize: 20,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginTop: 8,
    letterSpacing: 0.5
  },
  seeDetailsLayout: {
    marginTop: 26,
    flexDirection: 'row'
  },
  seeDetailsText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgb(31, 122, 224)',
    marginBottom: 24
  },
  detailsArrow: {
    width: 11,
    height: 7,
    marginLeft: 4,
    marginTop: 7
  },
  divReturns: {
    fontSize: 13,
    marginTop: 14,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  percent: {
    fontWeight: 'bold',
    fontSize: 12
  }
});
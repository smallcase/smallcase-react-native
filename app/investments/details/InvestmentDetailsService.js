import {
  INVESTED_SMALLCASE_DETAILS,
  GET_STOCK_PRICE,
  GET_NEWS,
  GET_SIP_DETAILS, SMALLCASE_LITE, SKIP_SIP, SMALLCASE_LITE_DETAILS, PAYOUT_DATE
} from '../../api/ApiRoutes';
import {calculateWeightageFromShares} from '../../helper/StockHelper';
import {getReadableDate, getSmallcaseImage, SmallcaseSource} from '../../util/Utility';
import Api from "../../api/Api";
import {EM_DASH} from "../../constants/Constants";

let offSet = 0;
const count = 5;

let ids = [];
let newsItems = [];
let smallcaseSource = SmallcaseSource.PROFESSIONAL;

const getPrices = (data, jwt, csrf) => {
  let params = '';
  for (let i = 0; i < data.currentConfig.constituents.length; i++) {
    let stock = data.currentConfig.constituents[i];
    params += 'stocks=' + stock.sid + (i === data.currentConfig.constituents.length - 1 ? '' : '&');
  }

  return new Promise((resolve, reject) => {
    Api.get({route: GET_STOCK_PRICE, jwt, csrf, params})
        .then((response) => resolve(extractPrices(response.data, data)))
        .catch((error) => reject(error));
  });
};

const extractPrices = (priceData, data) => {
  for (let i = 0; i < data.currentConfig.constituents.length; i++) {
    let stock = data.currentConfig.constituents[i];
    stock.price = +priceData[stock.sid].toFixed(2);
    stock.averagePrice = stock.averagePrice ? +stock.averagePrice.toFixed(2) : EM_DASH;
    stock.pAndL = (((stock.price - stock.averagePrice) / stock.averagePrice) * 100).toFixed(2);
    stock.expanded = false;
    stock.startDeg = '0deg';
    stock.endDeg = '180deg';
  }
  return getPresentableInvestmentDetails(data, calculateWeightageFromShares(data.currentConfig.constituents));
};

const getPresentableNews = (data) => {
  data.map((news) => {
    newsItems.push({
      _id: news._id,
      link: news.link,
      headline: news.headline,
      imageUrl: news.imageUrl,
      date: getReadableDate(news.date),
      publisher: news.publisher.name,
    });
  });
  offSet += newsItems.length;
  // Return new array to enable re-rendering of news component
  return newsItems.slice(0, newsItems.length);
};

const getPresentableInvestmentDetails = (data, stocksWithWeightage) => {
  stocksWithWeightage.sort((a, b) => {
    return (parseFloat(b.pAndL) - parseFloat(a.pAndL));
  });

  smallcaseSource = data.source;
  ids = [];
  if (SmallcaseSource.PROFESSIONAL === data.source) {
    ids.push(data.scid);
  } else {
    data.currentConfig.constituents.forEach((stock) => {
      ids.push(stock.sid);
    });
  }

  stocksWithWeightage.unshift({sid: '-1'}) // For displaying header (sid is required as key in flat list)
  const totalPAndL = data.returns.networth ? +(data.returns.realizedReturns + (data.returns.networth - data.returns.unrealizedInvestment)
    + data.returns.divReturns + data.returns.accruedDivReturns + data.returns.creditedDivReturns).toFixed(0) : EM_DASH;
  const totalPAndLNumber = data.returns.networth?  data.returns.realizedReturns + (data.returns.networth - data.returns.unrealizedInvestment)
    + data.returns.divReturns + data.returns.accruedDivReturns + data.returns.creditedDivReturns : null;
  const totalPAndLPercent = totalPAndLNumber ? +((totalPAndLNumber / (data.returns.unrealizedInvestment + data.returns.realizedInvestment))
    * 100).toFixed(2) : EM_DASH;
  return {
    smallcaseName: data.name,
    boughtOn: getReadableDate(data.date),
    index: data.stats.indexValue ? +data.stats.indexValue.toFixed(2) : EM_DASH,
    isIndexUp: data.stats.indexValue ? true : data.stats.indexValue > data.stats.lastCloseIndex,
    indexChange: data.stats.indexValue ? +(((data.stats.indexValue - data.stats.lastCloseIndex) / data.stats.lastCloseIndex) * 100).toFixed(2) : EM_DASH,
    currentValue: data.returns.networth ? +data.returns.networth.toFixed(0) : EM_DASH,
    totalPAndLPercent,
    totalPAndL,
    credited: data.returns.creditedDivReturns,
    accrued: data.returns.accruedDivReturns,
    currentInvestment: data.returns.unrealizedInvestment ? data.returns.unrealizedInvestment : EM_DASH,
    currentPAndL: data.returns.networth ? +(data.returns.networth - data.returns.unrealizedInvestment).toFixed(0) : EM_DASH,
    currentPAndLPercent: +(((data.returns.networth - data.returns.unrealizedInvestment) / data.returns.unrealizedInvestment) * 100).toFixed(2),
    moneyPutIn: data.returns.unrealizedInvestment + data.returns.realizedInvestment,
    realizedPAndL: data.returns.realizedReturns,
    divReturns: (data.returns.divReturns + data.returns.creditedDivReturns + data.returns.accruedDivReturns).toFixed(0),
    stocks: stocksWithWeightage,
    scid: data.scid,
    source: data.source,
    smallcaseImage: getSmallcaseImage(data.scid, data.source, '80'),
    status: data.status,
    iscid: data._id,
    isSipSet: data.flags && data.flags.sip
  }
};

export const getInvestedSmallcaseDetails = (iscid, jwt, csrf) => {
  const params = 'iscid=' + iscid;

  return new Promise((resolve, reject) => {
    Api.get({route: INVESTED_SMALLCASE_DETAILS, jwt, csrf, params})
      .then((response) => getPrices(response.data, jwt, csrf))
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

export const getSmallcaseNews = (jwt, csrf) => {
  let params = '';
  if (SmallcaseSource.PROFESSIONAL === smallcaseSource) {
    params += 'scids=' + ids + '&';
  } else {
    ids.map((id) => {
      params += ('sids=' + id + '&');
    });
  }
  params += 'offset=' + offSet + '&count=' + count;
  return new Promise((resolve, reject) => {
    Api.get({route: GET_NEWS, jwt, csrf, params})
      .then((response) => resolve({
        response: getPresentableNews(response.data),
        newsRemaining: response.data.length === count
      }))
      .catch((error) => reject(error));
  });
};


export const getLiquidBeesPayoutDate = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: PAYOUT_DATE, jwt, csrf})
      .then( (response) => {  resolve(response.data)})
      .catch((error) => { console.log(error);
      reject(error) });
  })

}

export const clearSavedItems = () => {
  offSet = 0;
  ids = [];
  newsItems = [];
  smallcaseSource = SmallcaseSource.PROFESSIONAL;
};

export const getSmallcaseDetailsLite = (jwt, csrf, scid) => {
  let params = 'scid=' + scid;
  return new Promise((resolve, reject) => {
    Api.get({route: SMALLCASE_LITE_DETAILS, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const getSipDetails = (jwt, csrf, iscid) => {
  const params = 'iscid=' + iscid;
  return new Promise((resolve, reject) => {
    Api.get({route: GET_SIP_DETAILS, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error))
  });
};

export const skipSip = (jwt, csrf, iscid) => {
  const body = {iscid};
  return new Promise((resolve, reject) => {
    Api.post({route: SKIP_SIP, jwt, csrf, body})
      .then((response) => resolve())
      .catch((error) => reject(error));
  });
};

import React, {Component} from 'react';
import {
  ActivityIndicator,
  View,
  WebView
} from 'react-native'
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {AnalyticsSource} from "../../util/Utility";
import Loader from "../../component/Loader";

const LITE_URL = 'http://googleweblight.com/?lite_url=';

export default class ShowNews extends Component {

  constructor(props) {
    super();
    this.state= {
      showLoader: true
    }
  }

  hideLoader = () => this.setState({ showLoader: false });

  render() {
    let loader = null;
    if (this.state.showLoader) {
      loader = (<Loader loaderStyles={{ marginTop: 64 }}/>);
    }

    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        {loader}
        <WebView
          style={{flex: 1, opacity: this.state.showLoader? 0 : 1}}
          source={{uri: LITE_URL + this.props.url }}
          onLoadEnd={this.hideLoader}
        />
      </View>
    );
  }
}
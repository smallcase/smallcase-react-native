import React, {PureComponent} from 'react';
import {
  ActivityIndicator,
  Animated,
  Easing,
  FlatList,
  Image,
  interpolate,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

import {
  clearSavedItems,
  getInvestedSmallcaseDetails, getLiquidBeesPayoutDate,
  getSipDetails, getSmallcaseDetailsLite,
  getSmallcaseNews,
  skipSip
} from './InvestmentDetailsService';
import Divider from '../../component/Divider';
import StockWidget from '../../component/StockWidget';
import SnackBar from "../../component/SnackBar";
import News from "./components/News";
import SmallcaseInfoCard from "./components/SmallcaseInfoCard";
import SmallcaseNumbers from "./components/SmallcaseNumbers";
import StockCard from "./components/StockCard";
import {
  getScreenSize,
  OrderLabel,
  OrderType,
  Routes,
  SmallcaseStatus,
  SnackBarTime, Styles as Style,
  Styles, AnalyticsSource
} from "../../util/Utility";
import SmallcaseButton from "../../component/SmallcaseButton";
import ActionSheet from "./components/ActionSheet";
import {calculateMinInvestment, isSipDue} from "../../helper/SmallcaseHelper";
import InvestMorePopup from "../../component/InvestMorePopup";
import ExitPopup from "../../order/components/ExitPopup";
import {Actions} from 'react-native-router-flux';
import moment from "moment";
import {SipCard} from "./components/SipCard";
import {connect} from "react-redux";
import {getUserDetails} from "../../user/UserActions";
import {skipRebalance} from "../rebalance/RebalanceService";
import {SkipPopup} from "../rebalance/SkipPopup";
import {checkMarketStatus} from "../../helper/MarketHelper";
import BottomPopUpContainer from "../../component/BottomPopUpContainer";
import {SimplePopUp} from "../../component/SimplePopUp";
import {frequencyMap} from "../sip/SipService";
import {getClosestInvestableAmount} from "../../order/OrderService";
import OrderStatusPending from "./components/OrderStatusPendingCard";
import {EM_DASH, LIQUID_BEES} from "../../constants/Constants";
import TouchHighlight from "../../component/TouchHighlight";
import ErrorState from "../../component/ErrorState";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import DividendsInfoComponent from "../../component/DividendsInfoComponent";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";

const Screen = getScreenSize();

let stocks = [];

class InvestmentDetailsComponent extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      stocksData: [],
      newsData: [],
      fetchingInvestmentDetails: true,
      stockWidget: false,
      smallcaseDetails: null,
      refreshing: false,
      loadingNews: false,
      newsRemaining: false,
      status: SmallcaseStatus.PLACED,
      investPopupShown: false,
      exitPopupShown: false,
      maxExitAmount: null,
      sip: null,
      storeUpdated: false,
      rebalancePending: false,
      skipPopupShown: false,
      isSipDue: false,
      skipSipShown: false,
      loadingSkipSip: false,
      repairOrderPending: false,
      rebalanceInfo: null,
      hasError: false
    };
    this.deltaY = new Animated.Value(Screen.height - 100);
    this.blurPointerEvent = 'box-none';
    this.fetchInvestments = this.fetchInvestments.bind(this);

  }

  componentDidMount() {
    this.fetchInvestments(false);
    AnalyticsManager.track('Viewed Investment Details', this.props.analytics, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
  }

  componentWillUnmount() {
    clearSavedItems();
  }

  componentWillReceiveProps(newProps) {
    this.setState({storeUpdated: true});

    if (!newProps.user.actions || !newProps.user.actions.rebalance) return;

    newProps.user.actions.rebalance.forEach(smallcase => {
      if (smallcase.iscid === newProps.iscid) {
        this.setState({rebalancePending: true, version: smallcase.version});
      }
    });

    const {smallcaseDetails} = this.state;
    newProps.user.actions.fix.forEach((smallcase) => {
      if (smallcaseDetails && smallcase.iscid === smallcaseDetails.iscid) {
        this.setState({repairOrderPending: true});
      }
    });
  }

  fetchInvestments(refreshing = false) {
    const {jwt, csrf, iscid, user} = this.props;

    this.props.dispatch(getUserDetails(jwt, csrf));

    getInvestedSmallcaseDetails(iscid, jwt, csrf)
      .then((response) => {
        response.stocks.forEach((stock) => {
          stock.spinValue = new Animated.Value(0);
        });
        stocks = response.stocks;
        const isRefresh = refreshing;
        const minInvestmentAmount = calculateMinInvestment(response.stocks.slice(1)).minAmount;

        this.setState({
          stocksData: response.stocks.slice(0, (stocks.length > 5 ? 6 : stocks.length)),
          smallcaseDetails: response,
          fetchingInvestmentDetails: false,
          refreshing: false,
          maxExitAmount: response.currentValue - minInvestmentAmount,
          status: response.status,
          isSipDue: isSipDue(user.actions.sip, iscid),
          hasError: false,
        });

        getSmallcaseDetailsLite(jwt, csrf, response.scid)
          .then((response) => {
            for (let i = 0; i < response.updates.length; i++) {
              const update = response.updates[i];
              if (update.version === response.version) {
                this.setState({rebalanceInfo: {label: update.label, description: update.rationale}});
                break;
              }
            }
          })
          .catch((error) => console.log(error));

        if (response.isSipSet) {
          getSipDetails(jwt, csrf, response.iscid)
            .then((response) => this.setState({sip: response}))
            .catch((error) => console.log(error));
        }

        if (isRefresh) return;

        getSmallcaseNews(jwt, csrf)
          .then(({response, newsRemaining}) => this.setState({newsData: response, newsRemaining}))
          .catch((error) => {
            this.snackbar.show({
              title: SnackBarMessages.NEWS_INVESTMENT_DETAILS_FAILED,
              action: 'Refresh',
              onAction: () => this.fetchInvestments(),
              duration: SnackBarTime.LONG
            });
          });

        if (response.accrued + response.credited > 0) {
          this.getLiquidBeesPayoutDate(jwt, csrf);
        }
      })
      .catch((error) => {
        this.setState({hasError: true, fetchingInvestmentDetails: false});
      });
  };

  static onEnter() {
    const investmentDetailsInstance = Actions.refs[Routes.INVESTMENT_DETAILS].getWrappedInstance();
    investmentDetailsInstance.onRefresh();
  };

  getPAndLColor = (value) => (value > 0 ? 'rgb(25, 175, 85)' : value === 0 ? 'rgb(47, 54, 63)' : 'rgb(216, 47, 68)');

  viewOrderClicked = () => {
    const {smallcaseDetails} = this.state;

    const smallcase = {
      name: smallcaseDetails.smallcaseName,
      source: smallcaseDetails.source,
      iscid: smallcaseDetails.iscid,
      scid: smallcaseDetails.scid,
      smallcaseImage: smallcaseDetails.smallcaseImage
    };
    Actions.push(Routes.ORDER_BATCHES, {smallcase});
  }

  renderHeader = () => {
    const {smallcaseDetails, status, isSipDue, rebalancePending} = this.state;

    return (
      <View>
        <SmallcaseInfoCard smallcaseDetails={smallcaseDetails}/>
        {status === SmallcaseStatus.PLACED ? <OrderStatusPending viewOrderClicked={this.viewOrderClicked}/> : null}
        <SmallcaseNumbers smallcaseDetails={smallcaseDetails} getPAndLColor={this.getPAndLColor}/>
        {status === SmallcaseStatus.VALID && (isSipDue || rebalancePending) ?
          <SmallcaseButton onClick={this.showInvestMorePopUp} buttonStyles={styles.investMorePrimaryButton}>
            <Text style={styles.investMorePrimary}>Invest More</Text>
          </SmallcaseButton> : null
        }
        <Text style={styles.stockConstituentsTitle}>smallcase Constituents</Text>
        {this.renderPrimaryStockContent({title: 'Stock', pAndL: 'Returns'})}
      </View>
    );
  };

  expandStock = (rowID) => {
    if (rowID === undefined) return;

    let newStocks = this.state.smallcaseDetails.stocks.slice(0);
    let newStock = newStocks[rowID];
    newStock.expanded = !newStock.expanded;
    if (newStock.expanded) {
      newStock.startDeg = '0deg';
      newStock.endDeg = '180deg';
    } else {
      newStock.startDeg = '180deg';
      newStock.endDeg = '0deg';
    }
    newStock.spinValue.setValue(0);
    Animated.timing(
      newStock.spinValue, {
        toValue: 1,
        duration: 100,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();
    this.setState({stocksData: newStock});
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    this.fetchInvestments(true);
  };

  getStockDetailsArrow = (stock) => {
    const spin = stock.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [stock.startDeg, stock.endDeg]
    });

    return (
      <Animated.Image style={[styles.stockDetailsArrow, {transform: [{rotate: spin}], marginLeft: 12}]}
                      source={require('../../../assets/iconArrowDownBlue.png')}/>
    );
  };

  showStockWidget = (sid) => {
    if (!sid) return;

    this.setState({stockWidget: true, sid});
  };

  openManage = () => {
    const {smallcaseDetails} = this.state;

    smallcaseDetails.stocks.forEach(stock => {
      stock.newShares = stock.shares;
      stock.oldShares = stock.shares;
      stock.newWeight = stock.weight;
      stock.oldWeight = stock.weight;
    });

    let newSmallcaseDetails = {
      smallcaseName: smallcaseDetails.smallcaseName,
      source: smallcaseDetails.source,
      smallcaseImage: smallcaseDetails.smallcaseImage,
      stocks: smallcaseDetails.stocks.slice(1),
      iscid: smallcaseDetails.iscid,
      scid: smallcaseDetails.scid,
    };

    Actions.push(Routes.MANAGE,
      {
        smallcaseDetails: newSmallcaseDetails,
        analytics: {
          accessedFrom: 'Investment Details',
          sectionTag: 'N/A',
          smallcaseName: smallcaseDetails.smallcaseName,
          smallcaseSource: smallcaseDetails.source,
          smallcaseType: 'N/A',
        }
      })
  };

  openRebalance = () => {
    const {iscid} = this.props;
    const {version, smallcaseDetails, rebalanceInfo} = this.state;
    let rebalance = null;
    const rebalanceArray = this.props.user.actions.rebalance.filter(smallcase => smallcase.iscid === iscid)
    if (rebalanceArray) {
      rebalance = rebalanceArray[0]
    }
    Actions.push(Routes.REVIEW_REBALANCE, {
      iscid,
      version,
      analytics: {
        accessedFrom: 'Investment Details',
        sectionTag: 'N/A',
        smallcaseName: rebalance.name,
        smallcaseSource: smallcaseDetails.source,
        updateDate: rebalance.date,
        updateName: rebalanceInfo.label,
        updateLabel: rebalanceInfo.description,
        scid: rebalance.scid,
        smallcaseType: 'N/A',
      }
    });
  };

  skipActionPressed = () => {
    this.setState({skipPopupShown: true});
  };

  removeRebalanceAction = () => {
    const {iscid, jwt, csrf} = this.props;
    const rebalance = this.props.user.actions.rebalance.filter(smallcase => smallcase.iscid === iscid)[0]
    if (rebalance) {
      AnalyticsManager.track('Skipped Rebalance Update',
        {
          smallcaseName: rebalance.name,
          smallcaseSource: this.state.smallcaseDetails.source,
          smallcaseType: 'N/A',
          updateDate: rebalance.date,
          updateLabel: rebalance.label,
          scid: rebalance.scid,
          iscid: iscid,

        }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
    }

    skipRebalance(jwt, csrf, iscid)
      .then(() => this.setState({rebalancePending: false, skipPopupShown: false}))
      .catch((error) => {
        console.log(error)
      });

  };

  renderPrimaryStockContent = ({title, pAndL, stock, rowID}) => {
    let detailsArrow = (<View style={styles.stockDetailsArrow}/>);
    if (stock) detailsArrow = this.getStockDetailsArrow(stock);
    return (
      <TouchableWithoutFeedback onPress={this.expandStock.bind(this, rowID)}>
        <View style={styles.mainStockCardContainer}>
          <TouchableWithoutFeedback onPress={this.showStockWidget.bind(this, stock ? stock.sid : '')}>
            <View style={styles.stockNameContainer}>
              <Text style={[styles.stockName, {
                color: stock ? 'rgb(31, 122, 224)' : 'rgb(83, 91, 98)',
                fontWeight: stock ? 'bold' : '400'
              }]} numberOfLines={1} ellipsizeMode={'tail'}>
                {title}
              </Text>
            </View>
          </TouchableWithoutFeedback>
          <Text style={[styles.pAndL, {
            color: stock ? stock.pAndL >= 0 ? 'rgb(25, 175, 85)' : 'rgb(216, 47, 68)' : 'rgb(83, 91, 98)',
            fontWeight: stock ? 'bold' : '400'
          }]}>{pAndL}</Text>
          {detailsArrow}
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderStocks = ({item, index}) => {
    if (index === 0) {
      return this.renderHeader();
    }

    if (item.sid === LIQUID_BEES) {
      item.divReturns = this.state.smallcaseDetails.divReturns
    }
    return (
      <StockCard item={item} index={index} getStockDetailsArrow={this.getStockDetailsArrow}
                 showStockWidget={this.showStockWidget}/>
    );
  };

  onStockWidgetClosed = () => this.setState({stockWidget: false});

  showAllStocks = () => this.setState({stocksData: stocks});

  renderNews = ({item}) => <News item={item}/>

  loadMoreItems = () => {
    const {jwt, csrf} = this.props;
    this.setState({loadingNews: true});
    getSmallcaseNews(jwt, csrf)
      .then(({response, newsRemaining}) => this.setState({newsData: response, loadingNews: false, newsRemaining}))
      .catch((error) => {
        this.snackbar.show({
          title: 'Oops, the news for' + this.state.smallcaseDetails.smallcaseName + ' could not be fetched',
          action: 'Refresh',
          onAction: () => this.fetchInvestments(),
          duration: SnackBarTime.LONG
        });
      });
  };

  getLiquidBeesPayoutDate = (jwt, csrf) => {
    getLiquidBeesPayoutDate(jwt, csrf)
      .then((response) => this.setState({payoutDate: response.date, liquidBeesName: response.name}))
      .catch((error) => console.log(error));
  }

  setUpSip = () => {
    const {smallcaseDetails, sip} = this.state;
    const {jwt, csrf} = this.props;
    if (!smallcaseDetails.isSipSet) {
      AnalyticsManager.track('Viewed Start SIP Popup', {
        accessedFrom: 'Investment Details',
        sectionTag: 'N/A',
        scid: smallcaseDetails.scid,
        smallcaseName: smallcaseDetails.smallcaseName,
        smallcaseSource: smallcaseDetails.source,
        smallcaseType: 'N/A',

      }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])
    }
    Actions.push(Routes.SIP, {smallcase: smallcaseDetails, isEdit: smallcaseDetails.isSipSet, jwt, csrf, sip});
  };

  renderFooter = () => {
    const {stocksData, newsData, loadingNews, newsRemaining, status, smallcaseDetails, sip} = this.state;
    let showStocks = (
      <TouchHighlight onClick={this.showAllStocks}>
        <View>
          <Divider/>
          <View style={{flexDirection: 'row', paddingVertical: 16, justifyContent: 'center'}}>
            <Text style={styles.seeDetailsText}>See all stocks</Text>
            <Image style={[styles.stockDetailsArrow, {marginTop: 7}]}
                   source={require('../../../assets/iconArrowDownBlue.png')}/>
          </View>
          <Divider/>
        </View>
      </TouchHighlight>
    );

    if (stocksData.length === stocks.length) showStocks = <View style={{padding: 16, paddingBottom: 0}}/>;

    let loadIcon = (
      <Image style={styles.loadArrow} source={require('../../../assets/iconArrowDownBlue.png')}/>
    );

    if (loadingNews) {
      loadIcon = (
        <Loader size='small'/>
      );
    }

    let loadNews = null;
    if (newsRemaining) {
      loadNews = (
        <View>
          <Divider/>
          <TouchHighlight onClick={this.loadMoreItems}>
            <View style={styles.loadMoreContainer}>
              <Text style={styles.loadMore}>{loadingNews ? 'Fetching' : 'Load more news items'}</Text>
              {loadIcon}
            </View>
          </TouchHighlight>
          <Divider dividerStyles={{marginBottom: 24}}/>
        </View>
      );
    }

    let newsTitle = null;
    if (newsData.length > 0) {
      newsTitle = (<Text style={styles.newsTitle}>News</Text>);
    }

    return (
      <View>
        {showStocks}
        {stocksData.length === stocks.length ?
          <SmallcaseButton disabled={status !== SmallcaseStatus.VALID} buttonStyles={styles.manageButton}
                           onClick={this.openManage}>
            <Text style={styles.manageText}>Manage Stocks</Text>
          </SmallcaseButton> : null
        }
        {smallcaseDetails.credited + smallcaseDetails.accrued > 0 &&
        <DividendsInfoComponent total={smallcaseDetails.divReturns} credited={smallcaseDetails.credited}
                                accrued={smallcaseDetails.accrued} payoutDate={this.state.payoutDate}
                                liquidBeesName={this.state.liquidBeesName}/>}

        {newsTitle}
        <FlatList
          style={{flex: 1}}
          data={newsData}
          extraData={this.state}
          renderItem={this.renderNews}
          keyExtractor={item => item._id}
        />
        {loadNews}
        <View style={styles.moreActionsContainer}>
          <Text style={styles.moreTitle}>More Actions</Text>
          <SipCard
            isSipSet={smallcaseDetails.isSipSet}
            setUpSip={this.setUpSip}
            frequency={sip ? frequencyMap(sip.frequency, true).toLowerCase() : ''}
            sipDate={sip ? moment(sip.scheduledDate).format("DD MMM, YYYY") : EM_DASH}
            status={status}
          />
          <SmallcaseButton
            disabled={status !== SmallcaseStatus.VALID}
            onClick={this.showExitSmallcasePopUp}
            buttonStyles={styles.exitButton}>
            <Text style={styles.exitText}>Exit smallcase</Text>
          </SmallcaseButton>
        </View>
      </View>
    );
  };

  showInvestMorePopUp = () => {
    const {smallcaseDetails} = this.state;

    checkMarketStatus(this.props.jwt, this.props.csrf)
      .then((isOpen) => {
        if (isOpen) {
          this.setState({investPopupShown: true});
        } else {
          this.setState({investPopupShown: false});
          this.snackbar.show({
            title: SnackBarMessages.MARKET_CLOSED,
            duration: SnackBarTime.LONG,
          });
        }
      })
      .catch((error) => console.log(error));
    AnalyticsManager.track('Viewed Invest Popup',
      {
        accessedFrom: 'Investment Details',
        sectionTag: 'N/A',
        iscid: smallcaseDetails.iscid,
        isInvested: true,
        isWatchlisted: false,
        isSIPDue: this.state.isSipDue,
        orderType: this.state.isSipDue ? OrderLabel.SIP : OrderLabel.INVESTMORE,
        scid: smallcaseDetails.scid,
        smallcaseName: smallcaseDetails.smallcaseName,
        smallcaseSource: smallcaseDetails.source,
        amountType: this.state.isSipDue ? 'N/A' : 'Minimum Investment',
        smallcaseType: 'N/A',
      },
      [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])
  };

  showExitSmallcasePopUp = () => {
    checkMarketStatus(this.props.jwt, this.props.csrf)
      .then((isOpen) => {
          if (isOpen) {
            this.setState({exitPopupShown: true});
          } else {
            this.setState({exitPopupShown: false});
            this.snackbar.show({
              title: SnackBarMessages.MARKET_CLOSED,
              duration: SnackBarTime.LONG,
            });
          }
        }
      ).catch((error) => {
    });
  };

  onInvestMorePopupClosed = () => this.setState({investPopupShown: false});

  repairSmallcase = () => {
    const {smallcaseDetails} = this.state;
    const smallcase = {
      source: smallcaseDetails.source,
      iscid: smallcaseDetails.iscid,
      scid: smallcaseDetails.scid,
      smallcaseImage: smallcaseDetails.smallcaseImage,
      name: smallcaseDetails.smallcaseName
    }
    Actions.push(Routes.ORDER_BATCHES, {smallcase});
  }

  onExitPopupClosed = () => this.setState({exitPopupShown: false});

  onSkipPopupClosed = () => this.setState({skipPopupShown: false});

  skipSipPressed = () => this.setState({skipSipShown: true});

  skipSipClosed = () => this.setState({skipSipShown: false});

  skipSip = () => {
    const {smallcaseDetails} = this.state;
    const {jwt, csrf, dispatch} = this.props;
    const sipDetails = this.props.user.actions.sip.filter(sip => sip.iscid === smallcaseDetails.iscid)[0]
    this.setState({loadingSkipSip: true});
    skipSip(jwt, csrf, smallcaseDetails.iscid)
      .then(() => {
        dispatch(getUserDetails(jwt, csrf));
        this.setState({isSipDue: false, loadingSkipSip: false});
        this.skipSipClosed();
        AnalyticsManager.track('Skipped SIP', {
          amount: sipDetails.amount,
          frequency: sipDetails.frequency,
          iscid: smallcaseDetails.iscid,
          scid: smallcaseDetails.scid,
          sipDate: sipDetails.scheduledDate,
          smallcaseName: smallcaseDetails.smallcaseName,
          smallcaseSource: smallcaseDetails.source,
          smallcaseType: 'N/A',
          sipType: 'Manual',
          amountType: 'Minimum Investment',

        }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);

      })
      .catch((error) => console.log(error));
  }

  continueToReviewOrder = (amountEntered) => {
    const {smallcaseDetails, isSipDue} = this.state;
    const {jwt, csrf} = this.props;

    getClosestInvestableAmount(jwt, csrf, smallcaseDetails.stocks.slice(1), amountEntered)
      .then((updatedStocks) => {
        updatedStocks.stocks.forEach((stock) => stock.transactionType = OrderType.BUY);

        const newSmallcaseDetails = {
          smallcaseName: smallcaseDetails.smallcaseName,
          source: smallcaseDetails.source,
          smallcaseImage: smallcaseDetails.smallcaseImage,
          stocks: updatedStocks.stocks,
          iscid: smallcaseDetails.iscid,
          scid: smallcaseDetails.scid,
          orderType: isSipDue ? OrderLabel.SIP : OrderLabel.INVESTMORE,
        };

        Actions.push(Routes.REVIEW_ORDER, {
          smallcaseDetails: newSmallcaseDetails,
          investment: updatedStocks.closestInvestableAmount,
          type: isSipDue ? OrderLabel.SIP : OrderLabel.INVESTMORE,
          repairing: false,
          analytics: {
            accessedFrom: 'Investment Details',
            sectionTag: 'N/A',
          }
        });
      }).catch((error) => console.log(error));
  }

  render = () => {
    const {
      fetchingInvestmentDetails, stocksData, stockWidget, sid, status, investPopupShown, exitPopupShown,
      smallcaseDetails, storeUpdated, rebalancePending, skipPopupShown, maxExitAmount, isSipDue,
      skipSipShown, loadingSkipSip, repairOrderPending, rebalanceInfo, hasError, payoutDate
    } = this.state;
    const {jwt, csrf, user} = this.props;

    let analytics = null
    if (smallcaseDetails) {
      analytics = {
        accessedFrom: 'Investment Details',
        sectionTag: 'N/A',
        iscid: smallcaseDetails.iscid,
        scid: smallcaseDetails.scid,
        boughtOn: smallcaseDetails.boughtOn,
        smallcaseName: smallcaseDetails.smallcaseName,
        smallcaseSource: smallcaseDetails.source,
        smallcaseType: 'N/A',
        index: smallcaseDetails.index,
      }
    }

    if (hasError) return (<ErrorState refresh={this.fetchInvestments}/>);

    const snackBar = <SnackBar onRef={ref => this.snackbar = ref}/>;

    if (fetchingInvestmentDetails) return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Loader loaderStyles={{marginTop: 64}}/>
        {snackBar}
      </View>
    );

    let actionView = storeUpdated ?
      <ActionSheet
        jwt={jwt}
        csrf={csrf}
        stocks={smallcaseDetails.stocks.slice(1)}
        smallcaseStatus={status}
        showInvestMorePopUp={this.showInvestMorePopUp}
        rebalancePending={rebalancePending}
        openRebalance={this.openRebalance}
        rebalanceInfo={rebalanceInfo}
        skipRebalance={this.skipActionPressed}
        isSipDue={isSipDue}
        repair={this.repairSmallcase}
        showInvestPopup={investPopupShown}
        skipSip={this.skipSipPressed}
        repairOrderPending={repairOrderPending}
        fixAction={{fix: user.actions.fix, iscid: smallcaseDetails.iscid}}
      /> : null;

    return (
      <View style={{flex: 1}}>
        <FlatList
          style={{flex: 1, backgroundColor: 'white'}}
          data={stocksData}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
          extraData={this.state}
          renderItem={this.renderStocks}
          keyExtractor={item => item.sid}
          ListFooterComponent={this.renderFooter}
        />
        {actionView}
        <StockWidget
          showWidget={stockWidget}
          stockWidgetClosed={this.onStockWidgetClosed.bind(this)}
          csrf={csrf}
          jwt={jwt}
          sid={sid}
        />
        <InvestMorePopup
          showPopup={investPopupShown}
          orderPopupClosed={this.onInvestMorePopupClosed}
          stocks={smallcaseDetails.stocks.slice(1)}
          continueToReviewOrder={this.continueToReviewOrder}
          smallcase={smallcaseDetails}
        />
        <ExitPopup
          showPopup={exitPopupShown}
          exitPopupClosed={this.onExitPopupClosed.bind(this)}
          csrf={csrf}
          jwt={jwt}
          smallcaseDetails={smallcaseDetails}
          maxExitAmount={maxExitAmount}
          analytics={analytics}
        />
        <SkipPopup
          showPopup={skipPopupShown}
          skipPopupClosed={this.onSkipPopupClosed.bind(this)}
          skipPressed={this.removeRebalanceAction}
        />
        <BottomPopUpContainer disableDismiss={loadingSkipSip} showPopup={skipSipShown} contentHeight={264}
                              popupClosed={this.skipSipClosed}>
          <SimplePopUp
            title={'Skip this instalment?'}
            description={'Skipping an instalment will impact your SIP schedule. Are you sure you want to skip?'}
            primaryActionText={'Skip'}
            secondaryActionText={'Cancel'}
            primaryAction={this.skipSip}
            secondaryAction={this.skipSipClosed}
            loadingPrimaryAction={loadingSkipSip}
            isDestructive={true}
          />
        </BottomPopUpContainer>
        {snackBar}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  newsTitle: {
    color: Styles.PRIMARY_TEXT_COLOR,
    letterSpacing: -0.2,
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 56,
    marginBottom: 20,
    marginLeft: 16
  },
  loadMoreContainer: {
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginRight: 16,
  },
  loadMore: {
    color: 'rgb(31, 122, 224)',
    fontWeight: 'bold',
    marginRight: 8
  },
  loadArrow: {
    marginLeft: 4,
    marginTop: 4
  },
  stockDetailsArrow: {
    marginLeft: 4,
    width: 10,
    height: 7
  },
  moreActionsContainer: {
    marginHorizontal: 16,
    marginVertical: 40,
  },
  seeDetailsText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgb(31, 122, 224)',
  },
  mainStockCardContainer: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16
  },
  stockNameContainer: {
    flex: 4,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  },
  moreTitle: {
    fontSize: 18,
    letterSpacing: -0.2,
    fontWeight: 'bold',
    marginBottom: 16
  },
  exitButton: {
    alignItems: 'center',
    marginTop: 24,
    backgroundColor: 'white',
    borderColor: 'rgba(216, 47, 68, 0.5)',
    borderWidth: 0.5
  },
  exitText: {
    fontWeight: 'bold',
    color: Style.RED_FAILURE
  },
  stockName: {
    flex: 4,
    fontSize: 14
  },
  pAndL: {
    fontSize: 14,
    justifyContent: 'center',
    marginLeft: 24
  },
  manageButton: {
    backgroundColor: 'white',
    borderColor: 'rgba(31, 122, 224, 0.3)',
    borderRadius: 4,
    borderWidth: 1,
    width: 130,
    marginLeft: 16,
    height: 34,
  },
  manageText: {
    color: 'rgb(31, 122, 224)',
    fontSize: 14,
    fontWeight: 'bold'
  },
  investMorePrimaryButton: {
    backgroundColor: Style.GREEN_SUCCESS,
    margin: 16
  },
  investMorePrimary: {
    fontWeight: 'bold',
    color: 'white'
  },
  stockConstituentsTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: -0.2,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginTop: 24,
    marginLeft: 16,
    marginBottom: 16
  },
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  user: state.userReducer.user,
});

module.exports = connect(mapStateToProps, null, null, {withRef: true})(InvestmentDetailsComponent);
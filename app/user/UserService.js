import {Actions} from "react-native-router-flux";
import Intercom from "react-native-intercom";
import {AnalyticsSource, Routes} from "../util/Utility";
import {AsyncStorage, NativeModules} from "react-native";
import Api from "../api/Api";
import {DE_REGISTER_FOR_PUSH_NOTIFICATION, GET_USER_FUNDS, REGISTER_FOR_PUSH_NOTIFICATION} from "../api/ApiRoutes";
import * as Constants from "../constants/Constants";
import AnalyticsManager from "../analytics/AnalyticsManager";

const CookieManager = NativeModules.CookieManager;

export const logOutUser = (jwt, csrf,accessedFrom) => {
  AnalyticsManager.track('Logged Out',{accessedFrom},[AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL])
  AsyncStorage.getItem(Constants.APPLE_PUSH_TOKEN, (error, token) => {
    if (token) deRegisterDeviceForPushNotification(jwt, csrf, token).then((response) ={}).catch(error => console.log(error));
  });
  AsyncStorage.clear((error) => {
    if (!error) {
      Intercom.reset();
      Actions.reset(Routes.LANDING);
    }
  });
  CookieManager.clearCookies();
};

export const registerDeviceForPushNotification = (jwt, csrf, deviceId) => {
  return new Promise((resolve, reject) => {
    Api.post({route: REGISTER_FOR_PUSH_NOTIFICATION, jwt, csrf, body: {iosDeviceId: deviceId}})
      .then((response) => resolve())
      .catch((error) => reject(error));
  });
};

const deRegisterDeviceForPushNotification = (jwt, csrf, deviceId) => {
  return new Promise((resolve, reject) => {
    Api.post({route: DE_REGISTER_FOR_PUSH_NOTIFICATION, jwt, csrf, body: {iosDeviceId: deviceId}})
      .then((response) => resolve())
      .catch((error) => reject(error));
  });
};

export const getUserFunds = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: GET_USER_FUNDS, jwt, csrf})
      .then((response) => resolve(response.data.net))
      .catch((error) => reject(error));
  });
};

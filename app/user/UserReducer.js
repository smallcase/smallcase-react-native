import {SET_USER_DATA} from "./UserActions";

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return Object.assign({}, state, {user: action.payload});

    default:
      return state;
  }
}
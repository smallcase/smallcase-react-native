
export const SET_NOTIFICATION_DATA = 'SET_NOTIFICATION_DATA';

const getNotificationStatus = (notificationsRead) => ({
  type: SET_NOTIFICATION_DATA ,
  payload: {notificationsRead}
})

export const updateNotificationStatus = (unreadCount) => (dispatch) => {
  if (unreadCount > 0){
    dispatch(getNotificationStatus(true));
    return
  }
  dispatch(getNotificationStatus(false));


}
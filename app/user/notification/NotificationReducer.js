import {SET_NOTIFICATION_DATA} from "./NotificationAction";

export default (state = {}, action) => {
  switch (action.type) {
    case SET_NOTIFICATION_DATA:
      return Object.assign({}, state, {notification: action.payload});

    default:
      return state;
  }
}
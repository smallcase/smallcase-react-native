import PushNotification from 'react-native-push-notification';
import * as Constants from "../../constants/Constants";
import {deRegisterDeviceForPushNotification, registerDeviceForPushNotification} from "../UserService";
import {Alert, AsyncStorage, PushNotificationIOS} from "react-native";
import Intercom from 'react-native-intercom';

import {Actions} from 'react-native-router-flux';
import {getSmallcaseImage, Routes} from "../../util/Utility";

const PushNotificationType = {
  WEEKLY_P_N_L: "weekly_pnl",
  MARKET_OPEN: "market_open",
  SIP: "sip",
  ORDER_UPDATE: "order_update",
  REBALANCE: "rebalance",
  ALERT: "alert",
  ACCOUNT: "account",
  WATCH_LIST: "watchlist",
  INVESTMENT_DETAILS: "investment_details",
  PLAYED_OUT: "played_out"
}

export default class NotificationService {

  static configure(gcm = "") {
    const that = this;
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: (token) => {
        AsyncStorage.setItem(Constants.APPLE_PUSH_TOKEN, token.token, (error) => {});
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: (notification) => {
        if (!notification.foreground) that.handleRedirection(notification.data);
        // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
      senderID: gcm,

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
       * (optional) default: true
       * - Specified if permissions (ios) and token (android and ios) will requested or not,
       * - if not, you must call PushNotificationsHandler.requestPermissions() later
       */
      requestPermissions: true,
    });
  }

  checkPermission = (cbk) => {
    return PushNotification.checkPermissions(cbk);
  }

  requestPermission = (cbk) => {
    return PushNotification.requestPermissions(cbk)
  }

  cancelAll = () => {
    PushNotification.cancelAllLocalNotifications();
  }

  // Tobe used as cbk in checkPermissions
  handlePerm = (perms) => {
    Alert.alert("Permissions", JSON.stringify(perms));
  }

  static registerForNotification = (jwt, csrf) => {
    AsyncStorage.getItem(Constants.APPLE_PUSH_TOKEN_SET, (error, response) => {
      if (!response) {
        AsyncStorage.getItem(Constants.APPLE_PUSH_TOKEN, (error, result) => {
          if (result) {
            Intercom.sendTokenToIntercom(result);
            //Make call to api with token and store flag in asyc storage to indicate that the token was registered
            registerDeviceForPushNotification(jwt, csrf, result)
              .then(() => AsyncStorage.setItem(Constants.APPLE_PUSH_TOKEN_SET, 'true', (error) => {}))
              .catch((error) => console.log(error));
          }
        });
      }
    });
  }

  static handleRedirection = (notificationData) => {
    switch (notificationData.type) {
      case PushNotificationType.ACCOUNT:

        break;

      case PushNotificationType.INVESTMENT_DETAILS:

        break;

      case PushNotificationType.ORDER_UPDATE:
        const smallcase = {
          ...notificationData.data,
          smallcaseImage: getSmallcaseImage(notificationData.data.scid, notificationData.data.source, '80')
        };
        Actions.push(Routes.ORDER_BATCHES, {smallcase});
        break;

      case PushNotificationType.REBALANCE:
        Actions.reset(Routes.ROOT);
        break;

      case PushNotificationType.SIP:
        Actions.reset(Routes.ROOT);
        break;

      case PushNotificationType.WEEKLY_P_N_L:
        Actions.reset(Routes.ROOT);
        break;
    }
  }
}
import Api from "../api/Api";
import {GET_USER_DETAILS} from "../api/ApiRoutes";
import Intercom from "react-native-intercom";
import {SmallcaseSource} from "../util/Utility";
import {Sentry} from "react-native-sentry";
import AnalyticsManager from "../analytics/AnalyticsManager";

export const SET_USER_DATA = 'SET_USER_DATA';

const setUserData = (data) => ({
  type: SET_USER_DATA,
  payload: data
});

const getSmallcaseScids = (smallcases) => {
  let scids = "";
  smallcases.forEach(smallcase => scids += (smallcase.scid + ","));
  return scids;
};

const getCreatedAndCustomizedCount = (investedSmallcases, exitedSmallcases) => {
  let count = 0;
  investedSmallcases.forEach((investedSmallcase) => {
    if (investedSmallcase.source === SmallcaseSource.CREATED || investedSmallcase.source === SmallcaseSource.CUSTOM) {
      count++;
    }
  });

  exitedSmallcases.forEach((exitedSmallcase) => {
    if (exitedSmallcase.source === SmallcaseSource.CREATED || exitedSmallcase.source === SmallcaseSource.CUSTOM) {
      count++;
    }
  });
  return count;
}


const getWatchlistedSmallcases = (watchlistedSmallcases) => {
  let watchlist = "";
  watchlistedSmallcases.forEach(smallcase => watchlist += (smallcase + ","));
  return watchlist;
}

const updateIntercomUser = (user) => {
  Intercom.registerIdentifiedUser({ userId: user._id });
  let firstName = user.broker.userName ? user.broker.userName.split(" ")[0].capitalize() : "";
  Intercom.updateUser({
    name: firstName,
    user_id: user._id,
    email: user.meta.profile.email,
    custom_attributes : {
      fullName: user.broker.userName.capitalize(" "),
      created_at: user.meta.dateCreated,
      investedSmallcasesCount: user.investedSmallcases.length,
      investedSmallcasesNames: getSmallcaseScids(user.investedSmallcases),
      exitedSmallcasesCount: user.exitedSmallcases.length,
      exitedSmallcasesNames: getSmallcaseScids(user.exitedSmallcases),
      broker_id: user.broker.userId,
      accountType: user.broker.name,
      createdAndCustomizedCount: getCreatedAndCustomizedCount(user.investedSmallcases, user.exitedSmallcases),
      draftsCount: user.draftSmallcases? user.draftSmallcases.length : 0,
      watchlistsCount: user.smallcaseWatchlist? user.smallcaseWatchlist.length : 0,
      watchlistedSmallcasesNames: getWatchlistedSmallcases(user.smallcaseWatchlist),
      oldExperience: user.meta.dna? user.meta.dna.q1 : "",
      experience: user.meta.dna? user.meta.dna.q2 : "",
      phone: user.meta.profile.phone
    }
  });
};

const updateCleverTapUser = (user) => {
  let firstName = user.broker.userName ? user.broker.userName.split(" ")[0].capitalize() : "";
  AnalyticsManager.loginCleverTapUser({
    name: firstName,
    user_id: user._id,
    email: user.meta.profile.email,
    custom_attributes : {
      fullName: user.broker.userName.capitalize(" "),
      createdAt: user.meta.dateCreated,
      investedSmallcasesCount: user.investedSmallcases.length,
      investedSmallcasesNames: getSmallcaseScids(user.investedSmallcases),
      exitedSmallcasesCount: user.exitedSmallcases.length,
      exitedSmallcasesNames: getSmallcaseScids(user.exitedSmallcases),
      brokerId: user.broker.userId,
      accountType: user.broker.name,
      createdAndCustomizedCount: getCreatedAndCustomizedCount(user.investedSmallcases, user.exitedSmallcases),
      draftsCount: user.draftSmallcases ? user.draftSmallcases.length : 0,
      watchlistsCount: user.smallcaseWatchlist ? user.smallcaseWatchlist.length : 0,
      watchlistedSmallcasesNames: getWatchlistedSmallcases(user.smallcaseWatchlist),
      oldExperience: user.meta.dna ? user.meta.dna.q1 : "",
      experience: user.meta.dna ? user.meta.dna.q2 : "",
      phone: user.meta.profile.phone,
    }
  });

};

const updateMixpanelUser = (user) => {
  let firstName = user.broker.userName ? user.broker.userName.split(" ")[0].capitalize() : "";
  const data = {
    $name: firstName,
    user_id: user._id,
    email: user.meta.profile.email,
    fullName: user.broker.userName.capitalize(" "),
    createdAt: user.meta.dateCreated,
    investedSmallcasesCount: user.investedSmallcases.length,
    investedSmallcasesNames: getSmallcaseScids(user.investedSmallcases),
    exitedSmallcasesCount: user.exitedSmallcases.length,
    exitedSmallcasesNames: getSmallcaseScids(user.exitedSmallcases),
    brokerId: user.broker.userId,
    accountType: user.broker.name,
    createdAndCustomizedCount: getCreatedAndCustomizedCount(user.investedSmallcases, user.exitedSmallcases),
    draftsCount: user.draftSmallcases ? user.draftSmallcases.length : 0,
    watchlistsCount: user.smallcaseWatchlist ? user.smallcaseWatchlist.length : 0,
    watchlistedSmallcasesNames: getWatchlistedSmallcases(user.smallcaseWatchlist ? user.smallcaseWatchlist : 'N/A'),
    oldExperience: user.meta.dna ? user.meta.dna.q1 : "",
    experience: user.meta.dna ? user.meta.dna.q2 : "",
    phone: user.meta.profile.phone,

  }
  AnalyticsManager.loginUserOnMixPanel(data);

}

const updateSentryUser = (user) => {
  Sentry.setUserContext({
    email: user.meta.profile.email,
    id: user._id,
    username: user.broker.userIdß
  });
}

export const getUserDetails = (jwt, csrf) => (dispatch) => {
  Api.get({route: GET_USER_DETAILS, jwt, csrf})
    .then((response) => {
      updateIntercomUser(response.data);
      updateSentryUser(response.data);
      updateCleverTapUser(response.data);
      updateMixpanelUser(response.data);
      dispatch(setUserData(response.data));
    })
    .catch((error) => dispatch(setUserData({})));
};

// Actions
export const CREATE_ACCOUNT = 'CREATE_ACCOUNT';
export const LOGIN_KITE = 'LOGIN_KITE';
export const LOGIN_LEPRECHAUN = 'LOGIN_LEPRECHAUN';
export const USER_LOGGED_IN = 'USER_LOGGED_IN';
export const GET_INVESTMENTS = 'GET_INVESTMENTS';
export const GET_INVESTMENT_DETAILS = 'GET_INVESTMENT_DETAILS';
export const SET_REQUEST_TOKEN = 'SET_REQUEST_TOKEN';
export const CLEAR_REQUEST_TOKEN = 'CLEAR_REQUEST_TOKEN';
export const LOGOUT_USER = 'LOGOUT_USER';

// Async storage keys
export const AUTH_INFO = 'AUTH_INFO';
export const APPLE_PUSH_TOKEN = 'APPLE_PUSH_TOKEN';
export const APPLE_PUSH_TOKEN_SET = 'APPLE_PUSH_TOKEN_SET';

// Utils
export const SMALLCASE_IMAGE_PROFESSIONAL = 'SMALLCASE_IMAGE_PROFESSIONAL';
export const SMALLCASE_IMAGE_CREATED = 'SMALLCASE_IMAGE_CREATED';
export const ETF_DIVIDENDS = "ETF Dividends";
export const LIQUID_BEES = "LBES";

// App related
export const KITE_PROD_KEY = '12hpqpbfwnxyvud8';
export const KITE_DEV_KEY = 'sncldvjxlnphwqt9';
export const MAX_STOCK_COUNT = 50

//Symbols
export const EM_DASH = '––';

// Scree sizes
export const IPAD_PRO_SMALLEST = `@media (min-device-width: 768) and (min-device-height: 1024)`;
export const IPHONE_FIVE_AND_SMALLER = `@media (max-device-width: 320) and (max-device-height: 568)`;
export const NORMAL_PHONES = `@media ((min-device-width: 375) and (min-device-height: 667)) @media ((max-device-width: 410) and (max-device-height: 730))`

// Kite
export const ADD_FUNDS_URL = "https://kite.zerodha.com/funds/";


// Urls
export const GETTING_STARTED_URL = 'http://help.smallcase.com/welcome-to-smallcase';
export const DISCOVER_URL = 'https://www.smallcase.com/discover';

export const SmallcaseType = {
  AWI: 'All Weather Investing'
};

export const ReturnsComponentValueType = {
  TITLE: 'TITLE',
  AMOUNT: 'AMOUNT',
  PERCENT: 'PERCENT',
  NORMAL: 'NORMAL',
  UNCOLORED_AMOUNT: 'UNCOLORED_AMOUNT'
};
import {MAX_STOCK_COUNT} from "../constants/Constants";

export const SnackBarMessages = {
  SESSION_EXPIRED: 'Your session has expired. Please login again',
  MARKET_CLOSED: 'Markets are closed, please try again later',
  EXCEEDED_MAX_STOCKS: 'A smallcase cannot hold more than ' + MAX_STOCK_COUNT + ' stocks',
  LOGIN_FAILED: 'Something\'s wrong here. You can try logging in again or contact support@smallcase.com',
  SIGN_UP_FAILED: 'Looks like your request could not be submitted. Try again or contact us at: support@smallcase.com',
  INVESTMENTS_FAILED: 'Your investments could not be fetched. Refresh to try again or contact support@smallcase.com',
  EXITED_SMALLCASES_FAILED: 'Your exited investments could not be fetched. Refresh to try again or contact support@smallcase.com',
  NEWS_INVESTMENT_DETAILS_FAILED: 'Looks like news for this smallcase could not be fetched. Refresh to try again',
  SIP_SET_UP_FAILED: 'Looks like your SIP could not be started. Try again or contact us at: support@smallcase.com',
  SIP_END_FAILED: 'Looks like your SIP could not be ended. Try again or contact us at: support@smallcase.com',
  EDIT_SIP_FAILED: 'Looks like your SIP settings could not be saved. Try again or contact us at: support@smallcase.com',
  AT_LEAST_TWO_STOCKS: 'Your smallcase should have atleast 2 stocks with shares greater than 0',
  NO_MODIFICATIONS: 'You have not modified any constituent. No orders to place'
};

export const stockOrderErrorMap = {
  adapterError: 'Markets were closed when you placed the orders',
  checkFreezeQty: 'Looks like the quantity in order placed exceeds quantity allowed by the exchange. Check the NSE website for more details',
  checkHoldings: `Looks like shares of this stock may have been sold on the broker platform or short delivered`,
  circuitLimitExceeded: 'Looks like the price in order placed was outside range allowed by the exchange. Check the NSE website for more details',
  clientBlocked: 'Your account was not configured for placing orders for this stock',
  clientNotEnabled: 'Your account was not configured for placing CNC orders',
  exchgNotEnabled: 'Your account was not configured for placing orders in this exchange',
  marginExceeded: 'Funds available were not sufficient to complete the order',
  orderUnmatched: 'Looks like there were no buyers or sellers for this stock when you placed the orders',
  qtyNotMultipleOfLot: 'This stock is traded in lots. While placing orders, the quantity entered was not a multiple of the regular lot',
};

export const orderStatusErrorsMap = {
  adapterError: 'Markets are closed right now, try again when they are open',
  tradingSystemNotReady: 'Markets are closed right now, try again when they are open',
  clientBlocked: 'Your account is not configured for placing orders for stock ticker/tickers',
  clientNotEnabled: 'Your account is not configured for placing CNC orders',
  exchgNotEnabled: 'Your account was not configured for placing orders in this exchange',

  checkFreezeQty: ' order quantities exceed quantity limit allowed by the exchange',
  checkHoldings: ' shares may have been sold on the broker platform or short delivered',
  qtyNotMultipleOfLot: ' shares are traded in lots. Looks like when you placed the orders, the quantity was not a multiple of the regular lot',

  circuitLimitExceeded: 'Looks like price in the order placed was outside the range allowed by the exchange for ',
  marginExceeded: 'Funds available were not sufficient to complete the order(s) for ',
  orderUnmatched: 'Looks like there were no buyers or sellers when you placed order(s) for ',
};

String.prototype.capitalize = function (delimiter) {
  if (!this) return "";

  if (!delimiter) return this.charAt(0).toUpperCase() + this.substr(1).toLowerCase();

  if (this.split(delimiter).length < 2) return this.charAt(0).toUpperCase() + this.substr(1).toLowerCase();

  let finalString = '';
  let splitString = this.split(delimiter);
  for (let i = 0; i < splitString.length; i++) {
    finalString += splitString[i].charAt(0).toUpperCase() + splitString[i].substr(1).toLowerCase() + delimiter;
  }
  return finalString;
}

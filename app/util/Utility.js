import moment from 'moment';
import {Dimensions, Platform} from 'react-native';
import {EM_DASH} from "../constants/Constants";

export const SmallcaseSource = {
  PROFESSIONAL: 'PROFESSIONAL',
  CREATED: 'CREATED',
  CUSTOM: 'CUSTOM'
};

export const SmallcaseImageURLs = {
  CREATED: 'https://cimg.smallcase.com/images/user/smallcases/',
  NON_CREATED: 'https://assets.smallcase.com/images/smallcases/'
};

export const SmallcaseStatus = {
  VALID: 'VALID',
  INVALID: 'INVALID',
  PLACED: 'PLACED'
};

export const SmallcaseTier = {
  BASIC: 'BASIC',
  PREMIUM: 'PREMIUM'
}

export const SnackBarTime = {
  LONG: 4000,
  SHORT: 2000,
  INDEFINITE: -1
};

export const Styles = {
  PRIMARY_TEXT_COLOR: 'rgb(47, 54, 63)',
  DESCRIPTION_TEXT_COLOR: 'rgb(83, 91, 98)',
  SECONDARY_TEXT_COLOR: 'rgb(129, 135, 140)',
  BLUE_ALPHA: 'rgba(31, 122, 224, 0.7)',
  BLUE: 'rgb(31, 122, 224)',
  GREEN_SUCCESS: 'rgb(39, 188, 148)',
  GREEN: 'rgb(25, 175, 85)',
  RED: 'rgb(255, 89, 66)',
  RED_ALPHA: 'rgba(216, 47, 68, 0.7)',
  RED_FAILURE: 'rgb(216, 47, 68)',
  SILVER: 'rgb(221, 224, 228)'
};

export const MarketStatus = {
  OPEN: "open",
  CLOSED: "closed" // check if correct
};

export const OrderInputState = {
  INPUT: "INPUT",
  INCORRECT_FORMAT: "INCORRECT_FORMAT",
  INVALID_AMOUNT: "INVALID_AMOUNT",
  FUNDS_UNAVAILABLE: "FUNDS_UNAVAILABLE",
  INPUT_FOCUSED : "INPUT_FOCUSED",
};

export const getSmallcaseImage = (scid, source, res) => {
  return (source === SmallcaseSource.CREATED ? SmallcaseImageURLs.CREATED : SmallcaseImageURLs.NON_CREATED) + res + '/' + scid + '.png';
};

export const getFormattedCurrency = (val) => {
  if (val === null || val === undefined || val === EM_DASH) return val;

  let value = val || 0;
  let negative = false;
  if (value < 0) {
    negative = true;
    value = Math.abs(value);
  }
  value = (Math.floor(value * 100) / 100).toFixed(0).toString();

  value = Math.floor(value);
  value = value.toString();
  let lastThree = value.substring(value.length - 3);
  let otherNumbers = value.substring(0, value.length - 3);
  if (otherNumbers !== '') lastThree = ',' + lastThree;
  let result = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
  if (negative) {
    result = '-' + result;
  }
  return '\u20B9' + result;
};

export const OrderType = {
  BUY: 'BUY',
  SELL: 'SELL'
};

export const Routes = {
  LANDING: 'LANDING',
  INVESTMENTS: 'INVESTMENTS',
  INVESTMENT_DETAILS: 'INVESTMENT_DETAILS',
  LOGIN: 'LOGIN',
  INFO: 'INFO',
  ABOUT: 'ABOUT',
  LOGIN_WITH_LEPRECHAUN: 'LOGIN_WITH_LEPRECHAUN',
  SHOW_NEWS: 'SHOW_NEWS',
  ALL_ORDERS: 'ALL_ORDERS',
  ORDER_BATCHES: 'ORDER_BATCHES',
  REVIEW_ORDER: 'REVIEW_ORDER',
  ORDER_DETAILS: 'ORDER_DETAILS',
  SIP: 'SIP',
  ORDER_STATUS: 'ORDER_STATUS',
  ACCOUNT: 'ACCOUNT',
  FEES: 'FEES',
  NOTIFICATIONS: 'NOTIFICATIONS',
  ROOT : 'ROOT',
  REVIEW_REBALANCE : 'REVIEW_REBALANCE',
  CUSTOMIZE_REBALANCE : 'CUSTOMIZE_REBALANCE',
  MANAGE : 'MANAGE',
  MANAGE_SEARCH : 'MANAGE_SEARCH',
  EXITED_SMALLCASES: 'EXITED_SMALLCASES'
};

export const OrderLabel = {
  BUY: 'BUY',
  FIX: 'FIX',
  INVESTMORE: 'INVESTMORE',
  MANAGE: 'MANAGE',
  REBALANCE: 'REBALANCE',
  SELLALL: 'SELLALL',
  SIP: 'SIP',
  PARTIALEXIT: 'PARTIALEXIT',
};

export const BatchStatus = {
  COMPLETED: 'COMPLETED',
  MARKEDCOMPLETE: 'MARKEDCOMPLETE',
  FIXED: 'FIXED',
  PARTIALLYFILLED: 'PARTIALLYFILLED',
  PARTIALLYPLACED: 'PARTIALLYPLACED',
  UNFILLED: 'UNFILLED',
  PLACED: 'PLACED',
  UNPLACED: 'UNPLACED',
  ERROR: 'ERROR',
  UNKNOWN: 'UNKNOWN',
};

export const StockOrderStatus = {
  COMPLETE: 'COMPLETE',
  REJECTED: 'REJECTED',
};

export const orderLabelMap = (label) => {
  switch (label) {
    case OrderLabel.BUY:
      return 'Buy';

    case OrderLabel.FIX:
      return 'Partial';

    case OrderLabel.INVESTMORE:
      return 'Invest More';

    case OrderLabel.MANAGE:
      return 'Manage';

    case OrderLabel.REBALANCE:
      return 'Rebalance';

    case OrderLabel.SELLALL:
      return 'Exit';

    case OrderLabel.SIP:
      return 'SIP';

    case OrderLabel.PARTIALEXIT:
      return 'Partial Exit';

    default:
      return 'Unkown';
  }
};

export const batchStatusMap = (batchStatus) => {
  switch (batchStatus) {
    case BatchStatus.COMPLETED:
      return 'Filled';

    case BatchStatus.MARKEDCOMPLETE:
      return 'Archived';

    case BatchStatus.FIXED:
      return 'Repaired';

    case BatchStatus.PARTIALLYFILLED:
    case BatchStatus.PARTIALLYPLACED:
    case BatchStatus.UNFILLED:
      return 'Partial';

    case BatchStatus.PLACED:
      return 'Placed';

    default:
      return 'Unkown';
  }
};

export const errorKeys = {
  adapterError: 'adapterError',
  tradingSystemNotReady: 'tradingSystemNotReady',
  clientBlocked: 'clientBlocked',
  clientNotEnabled: 'clientNotEnabled',
  exchgNotEnabled: 'exchgNotEnabled',

  checkFreezeQty: 'checkFreezeQty',
  checkHoldings: 'checkHoldings',
  qtyNotMultipleOfLot: 'qtyNotMultipleOfLot',

  circuitLimitExceeded: 'circuitLimitExceeded',
  marginExceeded: 'marginExceeded',
  orderUnmatched: 'orderUnmatched',
};

export const AnalyticsSource = {
  MIXPANEL: 'MIXPANEL',
  CLEVER_TAP: 'CLEVER_TAP',
  INTERCOM: 'INTERCOM'
}

export const BenchmarkId = {
  NSE: '.NSEI'
};

export const BenchmarkType = {
  INDEX: 'index'
}

export const getReadableDate = (date) => {
  moment.locale('en');
  return moment(date).format('MMM D, YYYY');
};

export const areNumbersEqual = (first, second, acceptedErrorRate) => Math.abs(first - second) < acceptedErrorRate;

export const getTimeAgoDate = (stringDate) => {

  let date = (new Date(stringDate))*1;
  let now = (new Date())*1;

  let difference = (now - date) / 1000;
  let day = 3600 * 24;
  let time;

  if ((difference > (day * 365))) {
    time = difference / (day * 365);
    return time.toFixed() + "y ago";
  } else if ((difference > (day * 30))) {
    time = difference / (day * 30);
    return time.toFixed() + "mo ago";
  } else if ((difference > (day * 7))) {
    time = difference / (day * 7);
    return time.toFixed() + "w ago";
  } else if (difference > (day)) {
    time = difference / day;
    return time.toFixed() + "d ago";
  } else if (difference > (3600)) {
    time = difference / (3600);
    return time.toFixed() + "h ago";
  } else if (difference > 60) {
    time = difference / 60;
    return time.toFixed() + "m ago";
  } else {
    time = difference;
    return time.toFixed() + "s ago";
  }
};

const isIphoneX = () => {
  let d = Dimensions.get('window');
  const { height, width } = d;

  return (
      // This has to be iOS duh
      Platform.OS === 'ios' &&

      // Accounting for the height in either orientation
      (height === 812 || width === 812)
  );
}

Date.isLeapYear = function (year) {
  return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
  return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.getDaysInMonth = function () {
  return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
  let n = this.getDate();
  this.setDate(1);
  this.setMonth(this.getMonth() + value);
  this.setDate(Math.min(n, this.getDaysInMonth()));
  return this;
};

Date.prototype.addDays = function (value) {
  return new Date(this.getFullYear(), this.getMonth(), this.getDate() + value);
};

const Screen = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height
};

export const getScreenSize = () => {
  if (isIphoneX()) {
    return {
      width: Screen.width,
      height: Screen.height - 60
    }
  }

  return Screen;
};

export const getBottomBarPaddingBottom = () => isIphoneX()? 32 : 16;

export const getBottomBarPaddingTop = () => isIphoneX()? 24 : 0;

export const getDefaultDurationText = (duration) => {
  if (duration.length < 2) return "";

  let postFix = "";
  let number = duration.substring(0, duration.length - 1);
  let timePeriod = duration.substring(duration.length - 1);
  try {
    if (number > 1) postFix = "s";
  } catch (e) {
    console.log(e);
  }

  let durationText;
  switch (timePeriod.toLowerCase()) {
    case "d":
      durationText = "day";
      break;

    case "w":
      durationText = "week";
      break;

    case "m":
      durationText = "month";
      break;

    case "y":
      durationText = "year";
      break;

    default:
      durationText = "";
      break;
  }

  return (number + " " + durationText + postFix);
}

export const isDecimalPresent = (value) => value % 1 !== 0;

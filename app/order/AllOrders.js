import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Image,
  TouchableWithoutFeedback
} from 'react-native';
import SmallcaseImage from "../component/SmallcaseImage";
import SmallcaseButton from "../component/SmallcaseButton";
import {getOrders} from "./AllOrdersService";
import {AnalyticsSource, BatchStatus, batchStatusMap, OrderLabel, orderLabelMap, Routes, Styles} from "../util/Utility";
import Divider from "../component/Divider";
import OrderPercent from "../component/OrderPercent";
import {Actions} from 'react-native-router-flux';
import {connect} from "react-redux";
import {logOutUser} from "../user/UserService";
import {getBatchStatus} from "./OrderService";
import ErrorState from "../component/ErrorState";
import AnalyticsManager from "../analytics/AnalyticsManager";
import Loader from "../component/Loader";

class AllOrders extends Component {
  constructor() {
    super();
    this.state = {
      isFetching: true,
      refreshing: false,
      orders: [],
      empty: false,
      hasError: false
    }
    this.showOrders = this.showOrders.bind(this);
  }

  componentDidMount() {
    this.showOrders();
    AnalyticsManager.track('Viewed All Orders', this.props.analytics, [AnalyticsSource.MIXPANEL, AnalyticsSource.CLEVER_TAP]);

  };

  showOrders() {
    const {jwt, csrf} = this.props;
    getOrders(jwt, csrf)
      .then((orders) => {
        if (orders.length > 0) {
          this.setState({orders, isFetching: false, empty: false, refreshing: false,hasError: false});
        } else {
          this.setState({isFetching: false, empty: true, refreshing: false, hasError: false});
        }
      })
      .catch((error) => {

        if (error.data && error.data.status === 401) {
          logOutUser(jwt, csrf, "All Orders");

        }
        else {
          this.setState({hasError: true, isFetching: false,})
        }
      });
  }

  static onEnter() {
    const allOrdersInstance = Actions.refs[Routes.ALL_ORDERS].getWrappedInstance();
    allOrdersInstance.showOrders();
  }

  showBatches = (item) => {
    Actions.push(Routes.ORDER_BATCHES, {
      ...this.props,
      smallcase: item
    });
  };

  onRefresh = () => {
    this.setState({refreshing: true});
    this.showOrders();

  }

  renderOrders = ({item}) => {
    return (
      <TouchableWithoutFeedback onPress={() => this.showBatches(item)}>
        <View>
          <View style={styles.smallcaseContainer}>
            <SmallcaseImage source={item.source} imageUrl={item.smallcaseImage} imageStyles={styles.smallcaseImage}/>
            <View style={styles.smallcaseInfoContainer}>
              <Text style={styles.smallcaseName} numberOfLines={2}>{item.name}</Text>
              <Text style={styles.orderDate}>Order placed on {item.lastBatchDate}</Text>
            </View>
            <SmallcaseButton buttonStyles={styles.seeAllButton} onClick={() => this.showBatches(item)}><Text
              style={styles.seeAll}>See
              All</Text></SmallcaseButton>
          </View>
          <View style={styles.batchStatusContainer}>
            <View style={styles.batchContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.batchTitle}>Batch</Text>
                <Text style={styles.batchValue}>{item.lastBatch.label}</Text>
              </View>
              <View style={styles.orderStatusContainer}>
                <Text style={styles.batchTitle}>Status</Text>
                <Text style={styles.batchValue}>{item.lastBatch.status}</Text>
              </View>
              <View style={{flex: 1.25}}>
                <Text style={styles.batchTitle}>{item.lastBatch.filled} of {item.lastBatch.quantity} filled</Text>
                <OrderPercent
                  percent={(item.lastBatch.filled / item.lastBatch.quantity) * 100}
                  isRepaired={item.originalStatus === BatchStatus.FIXED || item.originalStatus === BatchStatus.MARKEDCOMPLETE}
                  containerStyle={styles.orderFilledDisplay}/>
              </View>
            </View>
            {item.isPartial ?
              (<View style={styles.unfilledBatchContainer}>
                <Image style={styles.errorIcon} source={require('../../assets/error.png')}/>
                <Text style={styles.unfilledBatch}>Last batch is not filled.</Text>
                <Text style={styles.repair}> Repair/Archive</Text>
                <Image style={styles.arrow} source={require('../../assets/iconArrowDownBlue.png')}/>
              </View>) : null
            }
          </View>
          <Divider/>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const {orders, refreshing, isFetching, empty, hasError} = this.state;

    if (isFetching) return (<Loader loaderStyles={{marginTop: 64}} />)
    if (hasError) return (<ErrorState refresh={this.showOrders}/>);

    let listView = (
      <FlatList
        style={{backgroundColor: 'white'}}
        data={orders}
        refreshing={refreshing}
        onRefresh={this.onRefresh}
        extraData={orders}
        renderItem={this.renderOrders}
        keyExtractor={item => item.iscid}
      />
    );

    let emptyView = (
      <View style={styles.empty}>
        <Image source={require("../../assets/ordersEmptyState.png")} style={styles.emptyImage}/>
        <Text style={styles.emptyTitle}>You don't have any orders yet</Text>
        <Text style={styles.emptyText}>Orders for your invested smallcases appear here. Discover some smallcases to
          buy</Text>
      </View>
    );

    let view = empty ? emptyView : listView;

    return (
      <View style={styles.container}>
        {view}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  smallcaseContainer: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginTop: 24
  },
  smallcaseImage: {
    height: 48,
    width: 48,
    borderRadius: 5
  },
  smallcaseInfoContainer: {
    flex: 1,
    marginLeft: 8,
    marginRight: 16
  },
  smallcaseName: {
    fontSize: 16,
    lineHeight: 20,
    marginTop: 2,
    color: Styles.PRIMARY_TEXT_COLOR,
    fontWeight: 'bold'
  },
  orderDate: {
    marginTop: 6,
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  seeAllButton: {
    height: 36,
    width: 74,
    borderWidth: 0.5,
    borderColor: Styles.BLUE_ALPHA,
    borderRadius: 4,
    alignItems: 'center',
    shadowOpacity: 0,
    marginTop: 4
  },
  seeAll: {
    color: Styles.BLUE,
    fontWeight: 'bold',
    fontSize: 14
  },
  batchContainer: {
    flexDirection: 'row',
  },
  orderStatusContainer: {
    flex: 1
  },
  orderFilledDisplay: {
    height: 12,
    marginTop: 10
  },
  batchTitle: {
    fontSize: 12,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  batchValue: {
    fontSize: 15,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginTop: 8
  },
  batchStatusContainer: {
    marginVertical: 24,
    marginHorizontal: 16,
  },
  unfilledBatchContainer: {
    marginTop: 16,
    height: 52,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgba(255, 89, 66, 0.7)',
    flexDirection: 'row',
    alignItems: 'center'
  },
  errorIcon: {
    width: 24,
    height: 24,
    marginLeft: 16
  },
  unfilledBatch: {
    color: Styles.PRIMARY_TEXT_COLOR,
    marginLeft: 8
  },
  repair: {
    color: Styles.BLUE,
    fontWeight: 'bold'
  },
  arrow: {
    marginLeft: 4,
    transform: [{rotate: '-90deg'}],
    marginTop: 2
  },
  empty: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'white'
  },
  emptyImage: {
    height: 200,
    width: 360,
    marginBottom: 16,
  },
  emptyTitle: {
    color: 'rgb(47, 54, 63)',
    fontSize: 16,
    lineHeight: 24,
    marginBottom: 8,
    textAlign: 'center'
  },
  emptyText: {
    fontSize: 14,
    lineHeight: 20,
    color: 'rgb(129, 135, 140)',
    marginHorizontal: 24,
    textAlign: 'center'
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps, null, null, {withRef: true})(AllOrders);
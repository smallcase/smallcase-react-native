import React, {PureComponent} from 'react';
import {View, StyleSheet, Image, Text, TouchableHighlight, Animated, Easing} from "react-native";
import SmallcaseButton from "../../component/SmallcaseButton";
import {Actions} from "react-native-router-flux";
import {AnalyticsSource, getBottomBarPaddingBottom, getFormattedCurrency, OrderLabel, Routes} from "../../util/Utility";
import {connect} from "react-redux";
import {MediaQueryStyleSheet} from "react-native-responsive";
import {IPHONE_FIVE_AND_SMALLER} from "../../constants/Constants";
import AnalyticsManager from "../../analytics/AnalyticsManager";

class ReviewOrderAction extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      amount: props.amount,
      stocks: props.stocks,
      type: props.type,
      isConfirm: false
    };

    this.spinValue = new Animated.Value(0);
  }

  componentWillReceiveProps(props) {
    this.setState({amount: props.amount, stocks: props.stocks});
  }

  componentDidMount() {
    this.props.refresh();
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 500,
        easing: Easing.easeOut
      }
    ).start();
  }

  onRefreshPress = () => {
    this.props.refresh();
    this.spin();
  };

  openOrderStatus = () => {
    const {smallcaseDetails, type, stocks, repairing, amount} = this.props;

    smallcaseDetails.stocks = stocks;


    AnalyticsManager.track('Confirm Place Order', {
      buyAmount: amount > 0 ? amount : 0,
      sellAmount: amount < 0 ? amount : 0,
      iscid: smallcaseDetails.iscid,
      orderType: type,
      originalLabel: smallcaseDetails.orderType || 'N/A',
      scid: smallcaseDetails.scid,
      smallcaseName: smallcaseDetails.smallcaseName,
      smallcaseSource: smallcaseDetails.source,
      smallcaseType: 'N/A',

    }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);

    Actions.replace(Routes.ORDER_STATUS, {
      smallcaseDetails: smallcaseDetails,
      type: type,
      repairing: repairing,
    });
  };

  getLabel = (orderType, amount) => {
    switch (orderType) {
      case OrderLabel.INVESTMORE:
      case OrderLabel.SIP:
        return 'You will Invest';

      case OrderLabel.PARTIALEXIT:
      case OrderLabel.SELLALL:
        return 'You will Receive';

      case OrderLabel.REBALANCE:
      case OrderLabel.MANAGE:
      case OrderLabel.FIX:
        if (amount > 0) {
          return 'You will Invest';
        } else {
          return 'You will Receive';
        }
      default:
        return '';
    }
  };

  render() {
    const {amount, stocks, type, isConfirm} = this.state;

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    });

    const view = isConfirm ?
      (
        <View style={{flexDirection: 'row', flex: 1, paddingBottom: getBottomBarPaddingBottom()}}>
          <Text style={styles.confirmText}>Placing IOC market orders for these {stocks.length} stocks</Text>
          <SmallcaseButton buttonStyles={styles.confirmButton} onClick={this.openOrderStatus}>
            <Text style={styles.buttonText}>Confirm Orders</Text>
          </SmallcaseButton>
        </View>
      ) :
      (
        <View style={[styles.actionSheetContainer, {paddingBottom: getBottomBarPaddingBottom()}]}>
          <TouchableHighlight style={styles.investContainer} onPress={this.onRefreshPress}
                              underlayColor={'rgb(255, 255, 255)'}>
            <View>
              <Text style={styles.text}>{this.getLabel(type, amount)}</Text>
              <View style={styles.amountContainer}>
                <Text style={styles.minInvestment}>{getFormattedCurrency(Math.abs(amount))}</Text>
                <Animated.Image style={[styles.refresh, {transform: [{rotate: spin}]}]}
                                source={require('../../../assets/refresh.png')}/>
              </View>
            </View>
          </TouchableHighlight>
          <SmallcaseButton buttonStyles={styles.investButton} onClick={() => this.setState({isConfirm: true})}>
            <Text style={styles.buttonText}>Place Orders</Text>
          </SmallcaseButton>
        </View>
      );

    return (
      <View style={styles.container}>
        {view}
      </View>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16
  },
  confirmText: {
    fontSize: 12,
    flex: 1,
    alignSelf: 'center',
    marginRight: 8,
    // TODO: is wrap required for confirmText overflow?
    color: 'rgb(83, 91, 98)'
  },
  refresh: {
    height: 18,
    width: 18,
    padding: 8,
    marginLeft: 10
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  exitText: {
    color: 'rgb(216, 47, 68)',
    fontSize: 14,
    fontWeight: 'bold'
  },
  actionSheetContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  investContainer: {
    flexDirection: 'column',
    flex: 1
  },
  amountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  investButton: {
    backgroundColor: 'rgb(39, 188, 148)',
    height: 48,
    width: 170,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch'
  },
  exitButton: {
    backgroundColor: 'white',
    height: 48,
    width: 170,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: "rgba(216, 47, 68, 0.7)"
  },
  confirmButton: {
    backgroundColor: 'rgb(31, 122, 224)',
    width: 170,
    marginLeft: 8,
  },
  minInvestment: {
    color: 'rgb(47, 54, 63)',
    fontSize: 22
  },
  text: {
    fontSize: 14,
    color: 'rgb(129, 135, 140)',
    paddingBottom: 10,
    justifyContent: 'flex-start'
  }
}, {
  [IPHONE_FIVE_AND_SMALLER]: {
    investButton: {
      width: 140,
    }
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps)(ReviewOrderAction);
import React, {PureComponent} from 'react';
import {
  ActivityIndicator,
  Animated,
  AsyncStorage,
  Image,
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import Divider from "../../component/Divider";
import Interactable from 'react-native-interactable';
import PropTypes from 'prop-types';
import SmallcaseButton from "../../component/SmallcaseButton"
import {
  AnalyticsSource,
  getFormattedCurrency,
  getScreenSize,
  OrderInputState,
  OrderLabel,
  OrderType,
  Routes, SnackBarTime,
  Styles
} from "../../util/Utility";
import {Actions} from "react-native-router-flux"
import {calculatePartialExitAmount} from "../../helper/SmallcaseHelper";
import {IPHONE_FIVE_AND_SMALLER} from "../../constants/Constants";
import {MediaQueryStyleSheet} from "react-native-responsive";
import {getUserFunds} from "../../user/UserService";
import {BROKER_LOGIN} from "../../api/ApiRoutes";
import * as types from "../../constants/Constants";
import {getUserDetails} from "../../user/UserActions";
import {getJwt} from "../../helper/LoginHelper";
import {userLoggedIn} from "../../onboarding/LandingActions";
import * as Config from "../../../Config";
import {connect} from "react-redux";
import {SnackBarMessages} from "../../util/Strings";
import Loader from "../../component/Loader";
import AnalyticsManager from "../../analytics/AnalyticsManager";

const Screen = getScreenSize();

class ExitPopup extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      exitError: {
        status: false,
        message: null
      },
      inputState: OrderInputState.INPUT,
      wholeExit: true,
      minExitAmount: 0,
      maxExitAmount: props.maxExitAmount,
      wholeExitAmount: props.smallcaseDetails.currentValue,
      exitAmount: +props.maxExitAmount.toFixed(2),
      keyboardShown: false,
      keyboardHeight: 0,
      fetchingJwt: false
    };
    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';

    this.switchExitMode = this.switchExitMode.bind(this);
  }

  componentWillMount() {
    // check if value available in async storage
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow(e) {
    let newSize = e.startCoordinates.height;

    this.setState({
      inputState: OrderInputState.INPUT_FOCUSED,
      keyboardHeight: newSize,
      keyboardShown: true,
    });
  }

  componentDidMount() {
    AnalyticsManager.track('Viewed Exit Popup', this.props.analytics, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
  }
  keyboardDidHide(e) {

    //this.keyboardDidShowListener.remove();  // probably not needed
    this.setState({
      inputState: OrderInputState.INPUT,
      keyboardShown: false,
      investPopupShown: false,
    });
  }

  componentWillReceiveProps = (newProps) => {
    if (newProps.loginReducer && (newProps.loginReducer.requestToken !== this.props.loginReducer.requestToken)) {
      this.fetchAuthDetails(newProps);
    }

    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup && this.state.keyboardShown) {
      this.exitPopup.snapTo({index: 2});
      this.blurPointerEvent = 'auto';
    } else if (newProps.showPopup && !this.state.keyboardShown) {
      this.exitPopup.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.exitPopup.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  fetchAuthDetails = (newProps) => {
    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      this.setState({fetchingJwt: false});
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {});
      } else {
        this.snackbar.show({
          title: SnackBarMessages.LOGIN_FAILED,
          duration: SnackBarTime.LONG
        });
      }
    });
  }

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.exitPopupClosed();
    } else if (event.nativeEvent.index === 2) {
      this.blurPointerEvent = 'auto';
    } else {
      this.blurPointerEvent = 'auto';
    }

    this.setState({exitPopupToggled: true});
  };

  dismissExitPopup = () => {
    this.exitPopup.snapTo({index: 1});
    Keyboard.dismiss();
  };

  switchExitMode = (wholeClicked) => {
    if ((wholeClicked && !this.state.wholeExit) || (!wholeClicked && this.state.wholeExit)) {
      this.setState({wholeExit: !this.state.wholeExit});
      if (wholeClicked) {
        this.exitPopup.snapTo({index: 0});
        this.blurPointerEvent = 'auto';
      }
    }
  };

  getLowestPrice = (stocks) => {
    if (!stocks.length) return null;

    let lowest = 0;
    stocks.forEach((stock) => {
      if (!lowest || lowest > stock.price) lowest = stock.price;
    });
    return lowest;
  }

  onConfirm = () => {
    const {jwt, csrf, smallcaseDetails} = this.props;
    const {exitAmount, maxExitAmount, minExitAmount, wholeExit} =this.state

    AnalyticsManager.track('Confirmed Amount',{
      amountConfirmed: exitAmount,
      maxExitAmount: maxExitAmount,
      minInvestAmount: minExitAmount,
      exitType: wholeExit ? 'complete': 'partial',
      iscid: smallcaseDetails.iscid ,
      scid: smallcaseDetails.scid ,
      smallcaseName: smallcaseDetails.smallcaseName,
      smallcaseSource: smallcaseDetails.source ,
      smallcaseType: 'N/A',
    },[AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])


    getUserFunds(jwt, csrf)
      .then(() => this.continueExit())
      .catch((error) => {
        if (error.data && error.data.status === 401) {
          this.dismissExitPopup();
          this.setState({fetchingJwt: true});
          Actions.push(Routes.LOGIN);
        }
      });
  };

  continueExit = () => {
    const {smallcaseDetails, jwt, csrf} = this.props;
    const {wholeExitAmount, wholeExit, maxExitAmount, exitAmount} = this.state;
    let smallcase = Object.assign({}, smallcaseDetails);
    const stocks = smallcase.stocks.slice(1);

    if (wholeExit) {
      const newSmallcaseDetails = {
        iscid: smallcaseDetails.iscid,
        scid: smallcaseDetails.scid,
        smallcaseName: smallcaseDetails.smallcaseName,
        source: smallcaseDetails.source,
        smallcaseImage: smallcaseDetails.smallcaseImage,
        stocks: stocks,
        orderType: OrderLabel.SELLALL,
      };
      newSmallcaseDetails.stocks.forEach((stock) => {
        stock.transactionType = OrderType.SELL;
      });
      this.exitPopup.snapTo({index: 1});

      Actions.push(Routes.REVIEW_ORDER, {
        smallcaseDetails: newSmallcaseDetails,
        investment: wholeExitAmount,
        type: OrderLabel.SELLALL,
        jwt,
        csrf,
        repairing: false,
        analytics: {
          accessedFrom: 'Exit Popup',
          sectionTag: 'N/A',
        }
      });
      return;
    }

    if ((exitAmount - maxExitAmount) > 0.1) {
      this.setState({
        exitError: {
          status: true,
          message: 'Enter an amount lesser than ' + maxExitAmount + ' to maintain the weighting scheme of your smallcase'
        }
      });
      return;
    }

    if (stocks.length > 0 && this.getLowestPrice(stocks) > exitAmount) {
      this.setState({
        exitError: {
          status: true,
          message: 'No stocks found to sell for the amount entered. Try increasing the amount'
        }
      });
      return;
    }

    const exitObj = calculatePartialExitAmount(stocks, maxExitAmount, exitAmount);
    const newSmallcaseDetails = {
      iscid: smallcaseDetails.iscid,
      scid: smallcaseDetails.scid,
      smallcaseName: smallcaseDetails.smallcaseName,
      source: smallcaseDetails.source,
      smallcaseImage: smallcaseDetails.smallcaseImage,
      stocks: exitObj.stocksToSell,
      orderType: OrderLabel.PARTIALEXIT,
    }
    this.exitPopup.snapTo({index: 1});
    Actions.push(Routes.REVIEW_ORDER, {
      smallcaseDetails: newSmallcaseDetails,
      investment: exitObj.finalAmount,
      type: OrderLabel.PARTIALEXIT,
      repairing: false,
      analytics: {
        accessedFrom: 'Exit Popup',
        sectionTag: 'N/A',
      },
    });
  }

  reactToKeyboard = () => {
    this.exitPopup.snapTo({index: 2});
  };

  render() {
    const {wholeExit, exitError, maxExitAmount, exitAmount, wholeExitAmount, keyboardHeight, fetchingJwt} = this.state;

    let kbHeight = keyboardHeight > 0 ? keyboardHeight : 250;

    const confirmButton = (
      <SmallcaseButton buttonStyles={styles.confirmButton} onClick={this.onConfirm}>
        <Text style={styles.confirmText}>Confirm Amount</Text>
      </SmallcaseButton>
    );

    const errorView = (
      <View style={styles.errorContainer}>
        <Image style={styles.errorImage} source={require('../../../assets/error.png')}/>
        <Text style={styles.errorText}>{exitError.message}</Text>
      </View>
    );
    let dividerFocused = this.state.inputState === OrderInputState.INPUT_FOCUSED ? {backgroundColor: Styles.BLUE} : null;
    let dividerStyle = wholeExit ? {backgroundColor: "white"} : exitError.status ? {backgroundColor: Styles.RED} : dividerFocused;

    let view = exitError.status ? errorView : confirmButton;
    let max = wholeExit ? null : (
      <Text style={styles.actionDetail}> (up to {getFormattedCurrency(maxExitAmount)})</Text>);
    let wholeButtonStyle = wholeExit ? styles.buttonSelected : styles.buttonNotSelected;
    let wholeTextStyle = wholeExit ? styles.textSelected : styles.textNotSelected;
    let partialButtonStyle = !wholeExit ? styles.buttonSelected : styles.buttonNotSelected;
    let partialTextStyle = !wholeExit ? styles.textSelected : styles.textNotSelected;
    const popup =
      fetchingJwt ?
        (<Loader loaderStyles={[styles.container, {justifyContent: 'center'}]} />) : (
          <View style={styles.container}>
            <Text style={styles.title}>Confirm Exit Type</Text>
            <View style={styles.buttonContainer}>
              <SmallcaseButton buttonStyles={[wholeButtonStyle, {borderTopRightRadius: 0, borderBottomRightRadius: 0}]}
                               onClick={() => this.switchExitMode(true)}>
                <Text style={wholeTextStyle}>Whole</Text>
              </SmallcaseButton>
              <SmallcaseButton disabled={maxExitAmount === null || Math.round(maxExitAmount) === 0}
                               buttonStyles={[partialButtonStyle, {borderTopLeftRadius: 0, borderBottomLeftRadius: 0}]}
                               onClick={() => this.switchExitMode(false)}>
                <Text style={partialTextStyle}>Partial</Text>
              </SmallcaseButton>
            </View>
            <View style={styles.actionTextContainer}>
              <Text style={styles.actionTitle}>Sell Amount</Text>
              {max}
            </View>
            <View style={styles.amountContainer}>
              <Text style={styles.amount}>{"\u20B9 "}</Text>
              <TextInput
                style={styles.amountInput}
                keyboardType={'numeric'}
                editable={!wholeExit}
                onChangeText={(value) => {
                  const exitAmount = parseFloat(value);
                  if (isNaN(exitAmount)) return;
                  this.setState({exitAmount, exitError: {status: false, message: ''}})
                }}
                onFocus={this.reactToKeyboard}
                value={wholeExit ? wholeExitAmount.toString() : exitAmount.toString()}
              />
            </View>
            <Divider dividerStyles={dividerStyle}/>
            {view}
          </View>
        );

    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent} onPress={() => this.dismissExitPopup()}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - 300, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.exitPopup = ele}
          verticalOnly={true}
          snapPoints={[{y: Screen.height - 380}, {y: Screen.height}, {y: Screen.height - 380 - kbHeight}]}
          onSnap={this.handelSnap.bind(this)}
          boundaries={{top: Screen.height - 380 - kbHeight - 15}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          animatedValueY={this.deltaY}>
          {popup}
        </Interactable.View>
      </View>
    );
  }
}

const styles = MediaQueryStyleSheet.create({
  container: {
    flexDirection: 'column',
    minHeight: 360,
    justifyContent: 'flex-start',
    padding: 24,
    backgroundColor: 'white'
  },
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  title: {
    marginTop: 8,
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
    marginBottom: 16,
    fontSize: 16
  },
  buttonContainer: {
    flexDirection: 'row',
    marginBottom: 32,
  },
  buttonSelected: {
    width: 160,
    height: 48,
    backgroundColor: 'rgb(31, 122, 224)',
  },
  buttonNotSelected: {
    width: 160,
    height: 48,
    backgroundColor: 'white',
    borderColor: 'rgb(31, 122, 224)',
    borderWidth: StyleSheet.hairlineWidth,
  },
  textSelected: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold'
  },
  textNotSelected: {
    color: 'rgb(31, 122, 224)',
    fontSize: 14,
    fontWeight: 'bold'
  },
  actionTextContainer: {
    marginBottom: 8,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  actionTitle: {
    fontSize: 14,
    color: 'rgb(129, 135, 140)',
    fontWeight: 'bold'
  },
  actionDetail: {
    fontSize: 14,
    color: 'rgb(129, 135, 140)'
  },
  amountContainer: {
    flexDirection: 'row',
  },
  divider: {
    backgroundColor: 'rgb(255, 89, 66)',
  },
  amount: {
    fontSize: 20,
    marginBottom: 8,
  },
  amountInput: {
    fontSize: 20,
    marginBottom: 8,
    flex: 1,
  },
  errorContainer: {
    padding: 16,
    marginTop: 2,
    flexDirection: 'row',
    borderColor: 'rgba(255, 89, 66, 0.7)',
    borderWidth: 0.5,
    borderRadius: 4,
  },
  errorText: {
    color: 'rgb(47, 54, 63)'
  },
  errorImage: {
    height: 20,
    width: 20,
    marginRight: 8
  },
  confirmButton: {
    marginBottom: 8,
    marginTop: 32,
    width: 330,
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48,
    justifyContent: 'center'
  },
  confirmText: {
    fontSize: 14,
    color: 'white',
    fontWeight: 'bold',
  }
}, {
  [IPHONE_FIVE_AND_SMALLER]: {
    buttonSelected: {
      width: 140,
    },
    buttonNotSelected: {
      width: 140,
    },
    confirmButton: {
      width: 280,
    }
  }
});

ExitPopup.propTypes = {
  showPopup: PropTypes.bool.isRequired,
  exitPopupClosed: PropTypes.func.isRequired,
  jwt: PropTypes.string.isRequired,
  csrf: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  loginReducer: state.loginReducer
});

module.exports = connect(mapStateToProps)(ExitPopup);


import React, {PureComponent} from 'react';
import {Animated, Dimensions, StyleSheet, View, Text, TouchableWithoutFeedback} from "react-native";
import SmallcaseButton from "../../component/SmallcaseButton";
import {Actions} from "react-native-router-flux";
import PropTypes from 'prop-types';
import Interactable from 'react-native-interactable';
import {AnalyticsSource, getScreenSize, OrderLabel, Routes} from "../../util/Utility";
import {calculateTotalAmount} from "../OrderService";
import {connect} from "react-redux";
import AnalyticsManager from "../../analytics/AnalyticsManager";


const Screen = getScreenSize();

class RepairPopup extends PureComponent {

  constructor(props) {
    super(props);

    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
  }

  componentWillReceiveProps = (newProps) => {
    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup) {
      this.repairPopup.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.repairPopup.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.repairPopupClosed();
    } else {
      this.blurPointerEvent = 'auto';
    }

    this.setState({repairPopupToggled: true});
  };

  dismissRepairPopup = () => {
    this.repairPopup.snapTo({index: 1});
  };

  openRepairOrder = () => {

    let {smallcaseDetails, csrf, jwt, type} = this.props;


    let amount = calculateTotalAmount(smallcaseDetails.stocks);

    Actions.replace(Routes.REVIEW_ORDER, {
      smallcaseDetails: smallcaseDetails,
      investment: amount,
      type: type,
      jwt: jwt,
      csrf: csrf,
      repairing : true,
      analytics: {
        accessedFrom: 'Repair Order',
        sectionTag: 'N/A',
      }
    });
  };

  render() {

    let popup = (
        <View style={styles.container}>
          <Text style={styles.title}>Repair Orders?</Text>
          <Text style={styles.text}>This will retry to order the unfilled orders that did not go through</Text>
          <SmallcaseButton buttonStyles={styles.button} onClick={() => this.openRepairOrder()}>
            <Text style={styles.buttonText}>Repair Orders</Text>
          </SmallcaseButton>
        </View>
    );

    return (
        <View style={styles.panelContainer} pointerEvents={'box-none'}>
          <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent}
                                    onPress={() => {
                                      this.dismissRepairPopup()
                                    }}>
            <Animated.View
                pointerEvents={this.blurPointerEvent}
                style={[styles.panelContainer, {
                  backgroundColor: 'black',
                  opacity: this.deltaY.interpolate({
                    inputRange: [Screen.height - 230, Screen.height],
                    outputRange: [0.5, 0],
                    extrapolateRight: 'clamp'
                  })
                }]}/>
          </TouchableWithoutFeedback>
          <Interactable.View
              ref={ele => this.repairPopup = ele}
              verticalOnly={true}
              snapPoints={[{y: Screen.height - 280}, {y: Screen.height}]}
              onSnap={this.handelSnap.bind(this)}
              boundaries={{top: Screen.height - 300}}
              initialPosition={{y: Screen.height}}
              animatedNativeDriver={true}
              animatedValueY={this.deltaY}>

            {popup}
          </Interactable.View>
        </View>
    );
  };

}

const styles = StyleSheet.create({

  container: {
    flexDirection: 'column',
    height: 300,
    paddingTop: 32,
    paddingBottom: 32,
    paddingRight: 24,
    paddingLeft: 24,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
  },
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  title: {
    fontSize: 20,
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
    color: 'rgb(83, 91, 98)',
    marginTop: 8,
    marginBottom: 32,
  },
  button: {
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  }
});

RepairPopup.propTypes = {
  showPopup: PropTypes.bool.isRequired,
  repairPopupClosed: PropTypes.func.isRequired,
  jwt: PropTypes.string.isRequired,
  csrf: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps)(RepairPopup);
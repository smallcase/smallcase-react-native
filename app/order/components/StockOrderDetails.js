import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView
} from 'react-native';
import SmallcaseButton from "../../component/SmallcaseButton";
import {getFormattedCurrency, getReadableDate, OrderType, Styles} from "../../util/Utility";
import {Actions} from 'react-native-router-flux';
import {connect} from "react-redux";
import {EM_DASH} from "../../constants/Constants";
import {stockOrderErrorMap} from "../../util/Strings";

class StockOrderDetails extends Component {

  render() {
    const {stockDetails} = this.props;
    const isBuy = OrderType.BUY === stockDetails.transactionType;

    return (
      <View style={styles.mainContainer}>
        <ScrollView>
          <View style={{marginBottom: 24}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.ticker}>{stockDetails.tradingsymbol}</Text>
              <Text style={[styles.orderLabel, {
                color: isBuy ? Styles.GREEN_SUCCESS : Styles.RED,
                backgroundColor: isBuy ? 'rgba(235, 247, 244, 0.7)' : 'rgba(216,47,68,0.1)'
              }]}>
                {stockDetails.transactionType}
              </Text>
            </View>
            { stockDetails.errorCode?
              <View style={styles.errorContainer}>
                <Image style={styles.errorIcon} source={require('../../../assets/error.png')}/>
                <Text style={styles.errorText}>{stockOrderErrorMap[stockDetails.errorCode]? stockOrderErrorMap[stockDetails.errorCode] :
                  'Order placement unsuccessful. Could you give it another try? If the issue persists, please write to support@smallcase.com'
                }</Text>
              </View> : null
            }
            <View style={styles.infoContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Date</Text>
                <Text style={styles.infoValue}>{getReadableDate(stockDetails.orderTimestamp)}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Order Status</Text>
                <Text style={styles.infoValue}>{stockDetails.status}</Text>
              </View>
            </View>
            <View style={styles.infoContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Avg. Price</Text>
                <Text
                  style={styles.infoValue}>{stockDetails.averagePrice ? getFormattedCurrency(stockDetails.averagePrice) : EM_DASH}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Quantity</Text>
                <Text style={styles.infoValue}>{stockDetails.filledQuantity} of {stockDetails.quantity}</Text>
              </View>
            </View>
            <View style={styles.infoContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Trigger Price</Text>
                <Text
                  style={styles.infoValue}>{stockDetails.price ? getFormattedCurrency(stockDetails.price) : EM_DASH}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Order Type</Text>
                <Text style={styles.infoValue}>Market</Text>
              </View>
            </View>
            <View style={styles.infoContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Product Validity</Text>
                <Text style={styles.infoValue}>IOC</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Order Placed By</Text>
                <Text style={styles.infoValue}>{this.props.user.broker.userId}</Text>
              </View>
            </View>
            <View style={styles.infoContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Order ID</Text>
                <Text style={[styles.infoValue, {marginRight: 12}]}>{stockDetails.orderId}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.infoHeader}>Exchange ID</Text>
                <Text style={styles.infoValue}>{stockDetails.exchangeOrderId}</Text>
              </View>
            </View>
            <Text style={[styles.infoHeader, {marginTop: 12}]}>Status Message</Text>
            <Text style={[styles.infoValue, {marginBottom: 72}]}>{stockDetails.statusMessage}</Text>
          </View>
        </ScrollView>
        <View style={styles.doneContainer}>
          <SmallcaseButton onClick={() => Actions.pop()} buttonStyles={styles.doneButton}><Text style={styles.done}>Done</Text></SmallcaseButton>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    paddingTop: 32,
    paddingHorizontal: 16,
    backgroundColor: 'white',
    flex: 1
  },
  ticker: {
    fontWeight: 'bold',
    fontSize: 20
  },
  orderLabel: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    backgroundColor: 'rgba(235, 247, 244, 0.7)',
    fontWeight: 'bold',
    fontSize: 12,
    marginLeft: 8
  },
  errorContainer: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: 'rgba(255, 89, 66, 0.7)',
    paddingTop: 12,
    paddingBottom: 14,
    paddingLeft: 14,
    paddingRight: 24,
    marginTop: 16,
    marginBottom: 4
  },
  errorIcon: {
    width: 24,
    height: 24
  },
  errorText: {
    lineHeight: 1.5 * 14,
    marginLeft: 10
  },
  infoContainer: {
    paddingVertical: 12,
    flexDirection: 'row',
  },
  infoHeader: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  infoValue: {
    fontSize: 16,
    marginTop: 8,
  },
  doneButton: {
    shadowOpacity: 0,
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48
  },
  done: {
    color: 'white',
    fontWeight: 'bold'
  },
  doneContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    borderTopColor: 'rgb(221, 224, 228)',
    borderTopWidth: 1,
    padding: 16,
    backgroundColor: 'white'
  }
});

const mapStateToProps = (state) => ({
  user: state.userReducer.user
});

export default connect(mapStateToProps)(StockOrderDetails);
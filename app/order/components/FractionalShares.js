import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import {Styles} from "../../util/Utility";

export default FractionalShares = () => (
  <View style={styles.container}>
    <Image source={require('../../../assets/info.png')} style={styles.infoIcon}/>
    <Text style={styles.description}>You will also redeem fractional units of ETF dividends and the amount will be
      credited to your account in 2 days</Text>
  </View>
);


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(249, 250, 252)',
    padding: 16,
    flexDirection: 'row'
  },
  infoIcon: {
    width: 18,
    height: 18
  },
  description: {
    fontSize: 12,
    color: Styles.DESCRIPTION_TEXT_COLOR,
    marginLeft: 8,
    flex: 1,
    lineHeight: 19,
    marginTop: -2
  }
});

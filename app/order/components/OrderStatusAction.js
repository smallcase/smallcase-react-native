import React, {PureComponent} from 'react';
import {
  Text,
  View,
} from 'react-native';
import SmallcaseButton from "../../component/SmallcaseButton";
import Divider from "../../component/Divider";
import {BatchStatus, getBottomBarPaddingBottom, Routes} from "../../util/Utility";
import {Actions} from "react-native-router-flux"
import {MediaQueryStyleSheet} from "react-native-responsive";
import {IPHONE_FIVE_AND_SMALLER} from "../../constants/Constants";

const SEE_ERRORS = "See Errors";
const REPAIR_ORDER = "Repair Order";
const ORDER_DETAILS = "Order Details";
const VIEW_INVESTMENTS = "View Investments";
const CHECK_ORDERS = "Check Orders";

export default class OrderStatusAction extends PureComponent {

  showOrdersPage = () => Actions.replace(Routes.ALL_ORDERS, {analytics : {accessedFrom: 'Order Status', sectionTag: 'N/A'}});

  showInvestmentsPage = () => Actions.reset(Routes.ROOT,{analytics : {accessedFrom: 'Order Status' }});

  render() {
    const {batchStatus, showRepairPopup} = this.props;

    const errorView = (
      <View style={{paddingBottom: getBottomBarPaddingBottom()}}>
        <Divider/>
        <View style={styles.container}>
          <SmallcaseButton buttonStyles={styles.leftButton} onClick={() => this.showOrdersPage()}>
            <Text style={styles.leftText}>{SEE_ERRORS}</Text>
          </SmallcaseButton>
          <SmallcaseButton buttonStyles={styles.rightButton} onClick={() => showRepairPopup()}>
            <Text style={styles.rightText}>{REPAIR_ORDER}</Text>
          </SmallcaseButton>
        </View>
      </View>
    );

    const unknownView = (
      <View style={{paddingBottom: getBottomBarPaddingBottom()}}>
        <Divider/>
        <View style={styles.unknownContainer}>
          <SmallcaseButton buttonStyles={styles.unknownButton} onClick={() => this.showOrdersPage()}>
            <Text style={styles.rightText}>{CHECK_ORDERS}</Text>
          </SmallcaseButton>
        </View>
      </View>
    );

    const successView = (
      <View style={{paddingBottom: getBottomBarPaddingBottom()}}>
        <Divider/>
        <View style={styles.container}>
          <SmallcaseButton buttonStyles={styles.leftButton} onClick={() => this.showOrdersPage()}>
            <Text style={styles.leftText}>{ORDER_DETAILS}</Text>
          </SmallcaseButton>
          <SmallcaseButton buttonStyles={styles.rightButton} onClick={() => this.showInvestmentsPage()}>
            <Text style={styles.rightText}>{VIEW_INVESTMENTS}</Text>
          </SmallcaseButton>
        </View>
      </View>
    );

    const disabledSuccessView = (
      <View style={{opacity: 0.3, paddingBottom: getBottomBarPaddingBottom()}} pointerEvents="none">
        <Divider/>
        <View style={styles.container}>
          <SmallcaseButton buttonStyles={styles.leftButton}>
            <Text style={styles.leftText}>{ORDER_DETAILS}</Text>
          </SmallcaseButton>
          <SmallcaseButton buttonStyles={styles.rightButton}>
            <Text style={styles.rightText}>{VIEW_INVESTMENTS}</Text>
          </SmallcaseButton>
        </View>
      </View>
    );

    let buttonView = null;

    switch (batchStatus) {
      case BatchStatus.COMPLETED :
      case BatchStatus.MARKEDCOMPLETE :
      case BatchStatus.FIXED :
        buttonView = successView;
        break;

      case BatchStatus.UNPLACED :
      case BatchStatus.PARTIALLYFILLED :
      case BatchStatus.UNFILLED :
      case BatchStatus.ERROR :
      case BatchStatus.PARTIALLYPLACED :
        buttonView = errorView;
        break;

      case BatchStatus.UNKNOWN :
        buttonView = unknownView;
        break;

      case BatchStatus.PLACED :
      default:
        buttonView = disabledSuccessView;
        break;
    }

    return (
      <View>
        {buttonView}
      </View>
    );
  }

}

const styles = MediaQueryStyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingTop: 16,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  leftButton: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(31, 122, 224)',
    backgroundColor: 'white',
    height: 48,
    width: 164,
  },
  leftText: {
    color: 'rgb(31, 122, 224)',
    fontWeight: 'bold',
  },
  rightButton: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(31, 122, 224)',
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48,
    width: 164,
  },
  rightText: {
    color: 'white',
    fontWeight: 'bold',
  },
  unknownContainer: {
    height: 80,
    flexDirection: 'row',
    padding: 16,
    justifyContent: 'center'
  },
  unknownButton: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(31, 122, 224)',
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48,
    width: 330,
  }
}, {
  [IPHONE_FIVE_AND_SMALLER]: {
    leftButton: {
      width: 140
    },
    rightButton: {
      width: 140
    }
  }
});
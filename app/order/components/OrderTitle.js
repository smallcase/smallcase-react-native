import React, {PureComponent} from 'react';
import {View, StyleSheet, Text} from "react-native";
import SmallcaseImage from "../../component/SmallcaseImage";
import {OrderLabel} from "../../util/Utility";

export default class OrderTitle extends PureComponent {

  getTitleFromType = () => {
    const {type, repairing} = this.props;
    if (repairing) return 'Repairing';

    switch (type) {
      case OrderLabel.INVESTMORE:
      case OrderLabel.SIP:
        return 'Investing more in';

      case OrderLabel.SELLALL:
        return 'Exiting';

      case OrderLabel.REBALANCE:
        return 'Rebalancing';

      case OrderLabel.PARTIALEXIT:
        return 'Partially exiting';

      case OrderLabel.MANAGE:
        return 'Managing';

      case OrderLabel.FIX:
        return 'Repairing';
    }
  };

  render() {
    const {smallcaseDetails} = this.props;

    return (
      <View style={styles.container}>
        <SmallcaseImage source={smallcaseDetails.source} imageUrl={smallcaseDetails.smallcaseImage} imageStyles={styles.smallcaseImage} textSize={8}/>
        <View style={styles.titleText}>
          <Text style={styles.orderTypeText}>{this.getTitleFromType()}</Text>
          <Text style={styles.scNameText}>{smallcaseDetails.smallcaseName}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'rgb(249, 250, 251)'
  },
  smallcaseImage: {
    height: 48,
    width: 48,
    marginLeft: 16,
    marginTop: 16,
    marginBottom: 16,
    marginRight: 16,
  },
  titleText: {
    flexDirection: 'column',
    marginVertical: 16,
    flex: 1
  },
  orderTypeText: {
    color: 'rgb(129, 135, 140)',
    fontSize: 14,
    marginBottom: 6,
    fontWeight: 'bold',

  },
  scNameText: {
    color: 'rgb(47, 54, 63)',
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 24,
  },
  list: {
    justifyContent: 'center'
  }
});
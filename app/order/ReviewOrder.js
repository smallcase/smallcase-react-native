import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList
} from 'react-native';
import OrderTitle from "./components/OrderTitle";
import ReviewOrderAction from "./components/ReviewOrderAction";
import Divider from "../component/Divider";
import {AnalyticsSource, isDecimalPresent, OrderType, Styles} from "../util/Utility";
import PropTypes from 'prop-types';
import {calculateTotalAmount} from "./OrderService";
import {getUpdatedStocks} from "../helper/MarketHelper";
import {connect} from "react-redux";
import FractionalShares from "./components/FractionalShares";
import {ETF_DIVIDENDS} from "../constants/Constants";
import AnalyticsManager from "../analytics/AnalyticsManager";

class ReviewOrder extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      stocks: props.smallcaseDetails.stocks,
      amount: props.investment,
      areFractionalSharesPresent: false
    };
  }

  componentDidMount() {
    this.state.stocks.forEach((stock) => {
      if (stock.sidInfo.name === ETF_DIVIDENDS && isDecimalPresent(stock.shares)) {
        this.setState({areFractionalSharesPresent: true});
      }
    });
    AnalyticsManager.track('Viewed Order Batches',this.props.analytics,[AnalyticsSource.CLEVER_TAP,AnalyticsSource.MIXPANEL]);
  }

  refresh = () => {
    const {jwt, csrf} = this.props;
    const stocks = [...this.state.stocks];
    let sids = [];
    stocks.forEach(stock => sids.push(stock.sid));
    getUpdatedStocks(jwt, csrf, sids)
      .then(stockObj => {
        stocks.forEach(stock => stock.price = stockObj[stock.sid]);
        let sum = calculateTotalAmount(stocks);
        this.setState({amount: sum, stocks})
      }).catch(error => reject(error));
  };

  renderItem = ({item, index}) => (
    <View>
      <Divider dividerStyles={{marginLeft: index === 0? 0 : 16}} />
      <View style={styles.listItem}>
        <Text numberOfLines={1} ellipsizeMode={'tail'} style={styles.scName}>{item.sidInfo.name}</Text>
        <Text style={styles.price}>{item.price}</Text>
        <Text style={styles.shares}>{isDecimalPresent(item.shares)? item.shares.toFixed(3) : item.shares}</Text>
        <Text style={[styles.type, {color: OrderType.BUY === item.transactionType ? Styles.GREEN_SUCCESS : Styles.RED}]}>{item.transactionType}</Text>
      </View>
    </View>
  );

  render() {
    const {type, jwt, csrf, smallcaseDetails, repairing} = this.props;
    const {stocks, amount, areFractionalSharesPresent} = this.state;

    return (
      <View style={styles.container}>
        <OrderTitle smallcaseDetails={smallcaseDetails} type={type}/>
        <FlatList
          style={styles.list}
          data={stocks}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index}
          ListHeaderComponent={<ListHeader/>}
        />
        {areFractionalSharesPresent? <FractionalShares /> : null}
        <Divider/>
        <ReviewOrderAction
          type={type}
          stocks={stocks}
          amount={amount}
          jwt={jwt}
          csrf={csrf}
          refresh={this.refresh}
          smallcaseDetails={smallcaseDetails}
          repairing={repairing}/>
      </View>
    )
  }
}

class ListHeader extends PureComponent {
  render() {
    return (
      <View style={{flexDirection: 'row', padding: 16}}>
        <Text style={styles.listStockText}>Stock</Text>
        <Text style={styles.listPriceText}>Price</Text>
        <Text style={styles.listSharesText}>Shares</Text>
        <Text style={styles.listTypeText}>Type</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    backgroundColor: 'white'
  },
  listItem: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 13,
    paddingBottom: 13,
    alignItems: 'center',
  },
  scName: {
    flex: 6,
    fontSize: 13,
    color: 'rgb(47, 54, 63)',
  },
  price: {
    flex: 3,
    fontSize: 13,
    textAlign: 'right',
    marginRight: 8,
    color: 'rgb(47, 54, 63)'
  },
  shares: {
    flex: 3,
    fontSize: 13,
    textAlign: 'right',
    marginRight: 8,
    color: 'rgb(47, 54, 63)'
  },
  type: {
    flex: 2,
    fontSize: 12,
    textAlign: 'right',
    fontWeight: 'bold'
  },
  listStockText: {
    fontSize: 14,
    color: 'rgb(83, 91, 98)',
    flex: 6
  },
  listPriceText: {
    fontSize: 14,
    color: 'rgb(83, 91, 98)',
    flex: 3,
    textAlign: 'right',
    marginRight: 8,
  },
  listSharesText: {
    fontSize: 14,
    color: 'rgb(83, 91, 98)',
    flex: 3,
    textAlign: 'right',
    marginRight: 8,
  },
  listTypeText: {
    fontSize: 14,
    color: 'rgb(83, 91, 98)',
    flex: 2,
    textAlign: 'right',
  }
});

ReviewOrder.propTypes = {
  smallcaseDetails: PropTypes.exact({
    source: PropTypes.string.isRequired,
    smallcaseName: PropTypes.string.isRequired,
    smallcaseImage: PropTypes.string.isRequired,
    stocks: PropTypes.array.isRequired,
    iscid : PropTypes.string.isRequired,
    scid : PropTypes.string.isRequired,
    batchId: PropTypes.string
  }),
  type: PropTypes.string.isRequired,
  jwt: PropTypes.string.isRequired,
  csrf: PropTypes.string.isRequired,
  repairing: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps)(ReviewOrder);
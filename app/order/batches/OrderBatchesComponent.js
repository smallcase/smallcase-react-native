import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableWithoutFeedback,
  Animated,
  Easing
} from 'react-native';
import SmallcaseImage from "../../component/SmallcaseImage";
import SmallcaseButton from "../../component/SmallcaseButton";
import {
  BatchStatus,
  getBottomBarPaddingBottom,
  getFormattedCurrency,
  OrderLabel,
  OrderType,
  Routes,
  SnackBarTime,
  Styles, AnalyticsSource
} from "../../util/Utility";
import {archiveBatch, getOrderBatches} from "./OrderBatchesService";
import {Actions} from 'react-native-router-flux';
import OrderPercent from "../../component/OrderPercent";
import Divider from "../../component/Divider";
import {connect} from "react-redux";
import {checkMarketStatus} from "../../helper/MarketHelper";
import SnackBar from "../../component/SnackBar";
import BottomPopUpContainer from "../../component/BottomPopUpContainer";
import {EM_DASH} from "../../constants/Constants";
import AnalyticsManager from "../../analytics/AnalyticsManager";
import {SnackBarMessages} from "../../util/Strings";

class OrderBatchesComponent extends Component {

  constructor() {
    super();
    this.state = {
      batches: [],
      toggle: false,
      showRepairPopUp: false,
      showArchivePopUp: false,
      popUpMessage: null,
      archiving: false
    }
  }

  componentWillReceiveProps(newProps) {
    this.getBatches();
  }

  componentDidMount = () => {
    this.getBatches();
  };

  getBatches = () => {
    const {jwt, csrf, smallcase} = this.props;
    getOrderBatches(jwt, csrf, smallcase.iscid)
      .then((batches) => this.setState({batches}))
      .catch((error) => console.log(error));
  }

  showInvestmentDetails = () => {
    const {smallcase} = this.props
    Actions.push(Routes.INVESTMENT_DETAILS, {
      iscid: smallcase.iscid, analytics: {
        accessedFrom: 'smallcase Batches',
        sectionTag: 'N/A',
        iscid: smallcase.iscid,
        smallcaseName: smallcase.name,
        smallcaseSource: smallcase.source,
        scid: smallcase.scid,
        smallcaseType: 'N/A',
      }
    });

  }

  renderHeader = () => {
    const {smallcase, user} = this.props;

    return (
      <View style={styles.smallcaseInfoContainer}>
        <SmallcaseImage
          imageUrl={smallcase.smallcaseImage}
          source={smallcase.source}
          imageStyles={styles.smallcaseImage}
        />
        <Text style={styles.smallcaseName}>{smallcase.name}</Text>
        {
          this.isSmallcaseExited(user.exitedSmallcases, smallcase.iscid) ?
            null :
            <SmallcaseButton onClick={() => this.showInvestmentDetails()} buttonStyles={styles.trackButton}>
              <Text style={styles.track}>Track</Text>
            </SmallcaseButton>
        }
      </View>
    );
  };

  isSmallcaseExited = (exitedSmallcases, iscid) => {
    for (let i = 0; i < exitedSmallcases.length; i++) {
      if (iscid === exitedSmallcases[i]._id) return true;
    }
    return false;
  }

  showStockOrderDetails = (stockDetails) => Actions.push(Routes.ORDER_DETAILS, {stockDetails});

  renderOrder = ({item}) => {
    const isTitle = item.quantityTitle != null;
    const orderLabelColor = isTitle ? {color: Styles.DESCRIPTION_TEXT_COLOR} : {
      color: item.transactionType === OrderType.BUY ? Styles.GREEN_SUCCESS : Styles.RED,
      fontWeight: 'bold'
    };
    const infoColor = {color: isTitle ? Styles.DESCRIPTION_TEXT_COLOR : Styles.PRIMARY_TEXT_COLOR};
    return (
      <TouchableWithoutFeedback onPress={() => this.showStockOrderDetails(item)}>
        <View>
          <View
            style={[styles.stockContainer, {backgroundColor: (!isTitle && item.filledQuantity !== item.quantity) ? 'rgba(216, 47, 68, 0.04)' : 'white'}]}>
            <Text style={[styles.tradingSymbol, infoColor]}>{item.tradingsymbol}</Text>
            <Text
              style={[styles.valuesOfStock, infoColor]}>{typeof item.averagePrice === 'string' ? item.averagePrice : item.averagePrice ? item.averagePrice.toFixed(2) : EM_DASH}</Text>
            <Text
              style={[styles.valuesOfStock, infoColor]}>{isTitle ? item.quantityTitle : (item.filledQuantity ? item.filledQuantity : 0) + '/' + item.quantity}</Text>
            <Text style={[styles.valuesOfStock, orderLabelColor]}>{item.transactionType}</Text>
          </View>
          <Divider dividerStyles={{marginLeft: item.quantityTitle ? 0 : 16}}/>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  toggleStockOrders = (stock) => {
    stock.isExpanded = !stock.isExpanded;
    if (stock.isExpanded) {
      stock.startDeg = '0deg';
      stock.endDeg = '180deg';
    } else {
      stock.startDeg = '180deg';
      stock.endDeg = '0deg';
    }
    stock.spinValue.setValue(0);

    Animated.timing(
      stock.spinValue, {
        toValue: 1,
        duration: 300,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();
    this.setState({toggle: !this.state.toggle});
  };

  renderBuyAndSell = (item) => (
    <Text style={styles.buySellText}>For this order your total buy value is <Text
      style={{fontWeight: 'bold'}}>{getFormattedCurrency(item.buyAmount)}
    </Text> & total sell value is <Text
      style={{fontWeight: 'bold'}}>{getFormattedCurrency(item.sellAmount)}</Text></Text>
  );

  getBatchArrow = (stock) => {
    const spin = stock.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [stock.startDeg, stock.endDeg]
    });

    return (
      <Animated.Image style={{transform: [{rotate: spin}]}} source={require('../../../assets/iconArrowDownBlue.png')}/>
    );
  };

  renderBatch = ({item, index}) => {
    return (
      <View>
        {index === 0 ? null : <Divider dividerStyles={{marginLeft: item.date ? 0 : 16}}/>}
        <View style={styles.batchStatusContainer}>
          {item.date ? <Text style={styles.date}>{item.date}</Text> : null}
          <TouchableWithoutFeedback onPress={() => this.toggleStockOrders(item)}>
            <View style={styles.batchContainer}>
              <View style={{flex: 1}}>
                <Text style={styles.batchTitle}>Batch</Text>
                <Text style={styles.batchValue}>{item.label}</Text>
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.batchTitle}>Status</Text>
                <Text style={styles.batchValue}>{item.status}</Text>
              </View>
              <View style={{flex: 1.25}}>
                <Text style={styles.batchTitle}>{item.filled} of {item.quantity} filled</Text>
                <OrderPercent
                  percent={(item.filled / item.quantity) * 100}
                  containerStyle={styles.orderFilledDisplay}
                  isRepaired={item.originalStatus === BatchStatus.FIXED || item.originalStatus === BatchStatus.MARKEDCOMPLETE}
                />
              </View>
              <View style={{flex: 0.45, alignItems: 'flex-end', justifyContent: 'center'}}>
                {this.getBatchArrow(item)}
              </View>
            </View>
          </TouchableWithoutFeedback>
          {item.isRepair ?
            (
              <View style={styles.unfilledBatchContainer}>
                <SmallcaseButton onClick={() => this.archiveBatch(item)}
                                 buttonStyles={[styles.actionButton, {
                                   borderWidth: 0.5,
                                   borderColor: Styles.BLUE_ALPHA
                                 }]}>
                  <Text style={styles.archive}>Archive Orders</Text>
                </SmallcaseButton>
                <SmallcaseButton onClick={() => this.repairBatch(item)}
                                 buttonStyles={[styles.actionButton, {backgroundColor: Styles.BLUE}]}>
                  <Text style={styles.repair}>Repair Batch</Text>
                </SmallcaseButton>
              </View>
            ) : null
          }
          {item.isExpanded ?
            (
              <FlatList
                style={{marginTop: 24}}
                ListHeaderComponent={() => this.renderOrder({
                  item: {
                    tradingsymbol: 'Stock',
                    averagePrice: 'Avg Price',
                    quantityTitle: 'Qty Filled',
                    transactionType: 'Type'
                  }
                })}
                ListFooterComponent={() => this.renderBuyAndSell(item)}
                data={item.orders}
                extraData={item.orders}
                renderItem={this.renderOrder}
                keyExtractor={item => item.orderId}
              />
            ) : null}

        </View>
      </View>
    );
  };

  archiveBatch = (batch) => {

    this.batchId = batch.batchId;
    this.setState({
      showArchivePopUp: true,
      popUpMessage: (batch.originalLabel === OrderLabel.BUY && [BatchStatus.UNFILLED, BatchStatus.UNPLACED].indexOf(batch.originalStatus) > -1) ?
        'This will not place new orders for unfilled stocks. Since all order in this batch are unfilled, your smallcase will' +
        ' also be archived. You can buy this smallcase again' : 'This will not place new ' +
        'orders for unfilled stocks. Your returns may not match that of the original smallcase'
    });
    AnalyticsManager.track('Viewed Archive Order Popup',
      {
        accessedFrom: 'Order Batches',
        sectionTag: 'N/A',
        smallcaseName: this.props.smallcase.name,
        smallcaseSource: this.props.smallcase.source,
        orderType: batch.originalStatus,
        originalLabel: batch.originalLabel,
        iscid: batch.iscid,
        scid: this.props.smallcase.scid,
        smallcaseType: 'N/A',
      },

      [AnalyticsSource.MIXPANEL, AnalyticsSource.CLEVER_TAP])


  };

  repairBatch = (batch) => {
    const {jwt, csrf} = this.props;

    checkMarketStatus(jwt, csrf)
      .then((isOpen) => {
        if (isOpen) {
          this.showRepairPopUp(batch);
        } else {
          this.snackbar.show({
            title: SnackBarMessages.MARKET_CLOSED,
            duration: SnackBarTime.LONG
          });
        }
      })
      .catch((error) => console.log(error));
  };

  showRepairPopUp = (batch) => {
    const {smallcase, jwt, csrf} = this.props;

    const stocks = [];
    batch.orders.forEach((order) => {
      if (order.quantity === order.filledQuantity) return;

      stocks.push({
        sidInfo: {
          name: order.tradingsymbol
        },
        sid: order.sid,
        shares: order.quantity - order.filledQuantity,
        price: order.price,
        transactionType: order.transactionType
      });
    });

    this.batchDetails = {
      smallcaseDetails: {
        source: smallcase.source,
        smallcaseName: smallcase.name,
        iscid: smallcase.iscid,
        scid: smallcase.scid,
        smallcaseImage: smallcase.smallcaseImage,
        batchId: batch.batchId,
        orderType: batch.originalLabel,
        stocks,
      },
      type: OrderLabel.FIX,
      jwt,
      csrf,
      repairing: true,
      analytics: {
        sectionTag: 'N/A',
        accessedFrom: 'Order Batches',
      }
    };
    this.setState({
      showRepairPopUp: true,
      popUpMessage: 'This will retry placing unfilled orders that did not go through'
    });

    AnalyticsManager.track('Viewed Repair Order Popup',{
      accessedFrom: 'Order Batches',
      sectionTag: 'N/A',
      smallcaseName: smallcase.name,
      smallcaseSource: smallcase.source,
      scid: smallcase.scid,
      orderType: OrderLabel.FIX,
      smallcaseType: 'N/A',
    },[AnalyticsSource.MIXPANEL, AnalyticsSource.CLEVER_TAP]);
  };

  confirmAction = () => {
    const {jwt, csrf, smallcase} = this.props;

    const {showRepairPopUp} = this.state;

    if (showRepairPopUp) {
      this.setState({showRepairPopUp: false, showArchivePopUp: false, popUpMessage: null});
      Actions.push(Routes.REVIEW_ORDER, this.batchDetails);
      return;
    }

    this.setState({archiving: true});
    archiveBatch(jwt, csrf, smallcase.iscid, this.batchId)
      .then((response) => {
        this.getBatches();
        console.log(this.state.batches)


        const batch = this.state.batches.filter(batch => batch.batchId === this.batchId)[0]
        AnalyticsManager.track('Archived Order',
          {
            accessedFrom: 'smallcase Batches',
            iscid: smallcase.iscid,
            scid: smallcase.scid,
            smallcaseName: smallcase.name,
            smallcaseSource: smallcase.source,
            smallcaseType: 'N/A',
            orderType: batch.originalStatus,
            originalLabel: batch.originalLabel
          },
          [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
        this.setState({archiving: false, showRepairPopUp: false, showArchivePopUp: false, popUpMessage: null});
      })
      .catch((error) => console.log(error));
  };

  render() {
    const {batches, showRepairPopUp, showArchivePopUp, popUpMessage, archiving} = this.state;
    return (
      <View style={{flex: 1, paddingBottom: getBottomBarPaddingBottom(), backgroundColor: 'white'}}>
        <FlatList
          style={{backgroundColor: 'white'}}
          ListHeaderComponent={this.renderHeader}
          data={batches}
          extraData={batches}
          renderItem={this.renderBatch}
          keyExtractor={item => item.batchId}
        />
        <SnackBar onRef={ref => this.snackbar = ref}/>
        <BottomPopUpContainer
          showPopup={showRepairPopUp || showArchivePopUp}
          disableDismiss={archiving}
          popupClosed={() => this.setState({showRepairPopUp: false, showArchivePopUp: false, popUpMessage: null})}
          contentHeight={300}>
          <View style={styles.repairPopUp}>
            <Text style={styles.repairTitle}>{showArchivePopUp ? 'Archive Orders?' : 'Repair Orders?'}</Text>
            <Text style={styles.repairDescription}>{popUpMessage}</Text>
            <SmallcaseButton disabled={archiving} buttonStyles={styles.repairActionButton} onClick={this.confirmAction}>
              <Text style={styles.repairAction}>
                {showArchivePopUp ? (archiving ? 'Archiving...' : 'Archive Orders') : 'Repair Orders'}
              </Text>
            </SmallcaseButton>
          </View>
        </BottomPopUpContainer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  smallcaseImage: {
    width: 48,
    height: 48
  },
  smallcaseInfoContainer: {
    padding: 16,
    flexDirection: 'row',
    backgroundColor: 'rgb(249, 250, 252)',
    alignItems: 'center'
  },
  smallcaseName: {
    flex: 1,
    fontSize: 18,
    color: Styles.PRIMARY_TEXT_COLOR,
    fontWeight: 'bold',
    marginHorizontal: 16
  },
  trackButton: {
    height: 36,
    width: 78,
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: Styles.BLUE_ALPHA,
    alignItems: 'center'
  },
  track: {
    fontWeight: 'bold',
    color: Styles.BLUE
  },
  batchStatusContainer: {
    paddingVertical: 24,
  },
  batchContainer: {
    flexDirection: 'row',
    paddingHorizontal: 16
  },
  batchTitle: {
    fontSize: 12,
    color: Styles.DESCRIPTION_TEXT_COLOR
  },
  batchValue: {
    fontSize: 15,
    color: Styles.PRIMARY_TEXT_COLOR,
    marginTop: 8
  },
  date: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 16,
    marginBottom: 24,
  },
  orderFilledDisplay: {
    height: 12,
    marginTop: 10
  },
  stockContainer: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
    paddingHorizontal: 16
  },
  buySellText: {
    paddingHorizontal: 16,
    marginTop: 16,
    lineHeight: 14 * 1.5
  },
  valuesOfStock: {
    flex: 1,
    textAlign: 'right',
    fontSize: 13
  },
  unfilledBatchContainer: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginTop: 16,
    justifyContent: 'space-between',
  },
  actionButton: {
    height: 36,
    width: 164,
    shadowOpacity: 0
  },
  archive: {
    color: Styles.BLUE,
    fontSize: 13,
    fontWeight: 'bold'
  },
  repair: {
    color: 'white',
    fontSize: 13,
    fontWeight: 'bold'
  },
  repairPopUp: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 32,
    height: 300
  },
  repairTitle: {
    color: Styles.PRIMARY_TEXT_COLOR,
    fontWeight: 'bold',
    fontSize: 20
  },
  repairDescription: {
    marginTop: 8,
    color: Styles.DESCRIPTION_TEXT_COLOR,
    lineHeight: 22,
    fontSize: 16
  },
  repairAction: {
    color: 'white',
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  repairActionButton: {
    backgroundColor: Styles.BLUE,
    marginTop: 32
  },
  tradingSymbol: {
    flex: 1.4,
    fontSize: 13
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  user: state.userReducer.user,
});

module.exports = connect(mapStateToProps)(OrderBatchesComponent);
import {CANCEL_ORDER, GET_ORDERS} from "../../api/ApiRoutes";
import Api from "../../api/Api";
import {batchStatusMap, BatchStatus, getReadableDate, orderLabelMap} from "../../util/Utility";
import {Animated} from "react-native";

const getPresentableBatches = (response) => {
  let batches = []
  let previousDate = null;
  response.forEach((data) => {
    data.batches.forEach((batch) => {
      let date = getReadableDate(batch.date);
      if (date === previousDate) {
        date = null;
      } else {
        previousDate = date;
      }

      batches.push({
        batchId: batch.batchId,
        buyAmount: batch.buyAmount,
        filled: batch.filled? batch.filled : 0,
        iscid: batch.iscid,
        label: orderLabelMap(batch.label),
        status: batchStatusMap(batch.status),
        isRepair: batch.status === BatchStatus.UNPLACED || batch.status === BatchStatus.PARTIALLYFILLED ||
          batch.status === BatchStatus.UNFILLED,
        sellAmount: batch.sellAmount,
        orders: batch.orders,
        quantity: batch.quantity,
        isExpanded: false,
        originalStatus: batch.status,
        originalLabel: batch.originalLabel,
        spinValue: new Animated.Value(0),
        startDeg: '0deg',
        endDeg: '180deg',
        date
      });
    });
  });

  return batches;
}

export const getOrderBatches = (jwt, csrf, iscid) => {
  const params = 'iscid=' + iscid;
  return new Promise((resolve, reject) => {
    Api.get({route: GET_ORDERS, jwt, csrf, params})
      .then((response) => resolve(getPresentableBatches(response.data)))
      .catch((error) => reject(error));
  });
}

export const archiveBatch = (jwt, csrf, iscid, batchId) => {
  const body = {iscid, batchId};
  return new Promise((resolve, reject) => {
    Api.post({route: CANCEL_ORDER, jwt, csrf, body})
      .then((response) => {
        if (response.success) resolve();
        else reject();
      })
      .catch((error) => reject(error))
  });
}
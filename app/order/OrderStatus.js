import React, {PureComponent} from 'react';
import {Image, Text, View,} from 'react-native';
import OrderTitle from "./components/OrderTitle";
import OrderStatusAction from "./components/OrderStatusAction";
import Spinner from 'react-native-spinkit'
import OrderPercent from "../component/OrderPercent";
import RepairPopup from "./components/RepairPopup"
import {finalSuccessStateCopy, getBatchStatus, groupErrors, placeBatchOrders, repairBatchOrders} from "./OrderService";
import {BatchStatus, errorKeys, orderLabelMap, Styles} from "../util/Utility";
import {connect} from "react-redux";
import {MediaQueryStyleSheet} from "react-native-responsive";
import {IPHONE_FIVE_AND_SMALLER} from "../constants/Constants";
import {orderStatusErrorsMap} from "../util/Strings";

const STATUS_RE_FETCH_INTERVAL = 5000;
const StatusStrings = {
  PARTIAL: 'Partial',
  FILLED: 'Filled',
  WAITING: 'Waiting',
  UNFILLED: 'Unfilled',
  PLACED: 'Placed',
};

class OrderStatus extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      batchStatus: "PLACING",
      continuePolling: true,
      filledQuantity: 0,
      statusText: StatusStrings.WAITING,
      repairPopupShown: false,
      smallcaseDetails: props.smallcaseDetails,
      errorStocks: [],
      stockLength: props.smallcaseDetails.stocks.length,
      sellAmount: 0,
    };
  }

  componentDidMount() {
    const {jwt, csrf, type, repairing} = this.props;
    const {smallcaseDetails} = this.state;

    if (repairing) {
      const body = {
        iscid: smallcaseDetails.iscid,
        batchId: smallcaseDetails.batchId,
      };

      repairBatchOrders(jwt, csrf, body)
        .then((response) => {
          if (response.success) {
            let newBatchId = response.data.batchId;
            this.checkStatusAndUpdateView(newBatchId);
          }
        })
        .catch((error) => console.log(error));
    } else {
      const body = {
        iscid: smallcaseDetails.iscid,
        source: smallcaseDetails.source,
        scid: smallcaseDetails.scid,
        label: type,
        orders: this.stocksWithOrders(smallcaseDetails.stocks)
      };

      placeBatchOrders(jwt, csrf, body)
        .then((response) => {

          this.setState({batchStatus: BatchStatus.PLACED});
          this.checkStatusAndUpdateView(response.data.batchId);
        })
        .catch((error) => {
          console.log(error);
          this.setState({batchStatus: BatchStatus.UNKNOWN})
        });
    }
  };

  checkStatusAndUpdateView = (batchId) => {
    const {jwt, csrf} = this.props;
    let {smallcaseDetails, errorStocks} = this.state;

    let poll = setInterval(() => {
      if (this.state.continuePolling) {
        getBatchStatus(jwt, csrf, batchId)
          .then((response) => {
            const status = response.data[0].batches[0].status;

            let orderErrors = groupErrors(response.data[0].batches[0].orders);
            smallcaseDetails.batchId = response.data[0].batches[0].batchId;
            errorStocks = this.removeFilled(orderErrors.filled);
            smallcaseDetails.stocks = errorStocks;

            this.setState({
              batchStatus: status,
              filledQuantity: orderErrors.filledCount,
              smallcaseDetails,
              errorStocks
            });

            delete orderErrors["filled"];
            delete orderErrors["filledCount"];
            this.setState({orderErrors: orderErrors});

            switch (status) {
              case BatchStatus.COMPLETED :
              case BatchStatus.MARKEDCOMPLETE :
              case BatchStatus.FIXED :
                this.setState({
                  continuePolling: false,
                  statusText: StatusStrings.FILLED,
                  sellAmount: response.data[0].batches[0].sellAmount
                });
                break;

              case BatchStatus.PARTIALLYFILLED :
                this.setState({continuePolling: false, statusText: StatusStrings.PARTIAL});
                break;

              case BatchStatus.UNPLACED :
              case BatchStatus.UNFILLED :
                this.setState({continuePolling: false, statusText: StatusStrings.UNFILLED});
                break;

              case BatchStatus.ERROR :
              case BatchStatus.PLACED :
              case BatchStatus.PARTIALLYPLACED :
                this.setState({continuePolling: true, statusText: StatusStrings.PLACED});
                break;

            }
          }).catch((error) => {
          console.log(error)
        });
      } else {
        clearInterval(poll)
      }
    }, STATUS_RE_FETCH_INTERVAL);
  };

  removeFilled = (filled) => {
    const {smallcaseDetails} = this.state;

    return smallcaseDetails.stocks.filter((stock) => {
      if (!filled.includes(stock.sid)) return stock;
    });
  };

  /*
    modify the stocks array to have only sid, quantity, transactionType
  */
  stocksWithOrders = (stocks) => {
    let orderStocks = [];

    stocks.forEach((stock) => {
      let orderStock = {};
      orderStock.sid = stock.sid;
      orderStock.quantity = stock.shares;
      orderStock.transactionType = stock.transactionType;
      orderStocks.push(orderStock);
    });

    return orderStocks;
  };

  showRepairPopup = () => this.setState({repairPopupShown: true});

  onRepairPopupClosed = () => this.setState({repairPopupShown: false});

  displayErrors = (errorTypes) => {
    return Object.keys(errorTypes).map((type, id) => {
      let text = '';
      switch (type) {
        case errorKeys.checkFreezeQty:
        case errorKeys.checkHoldings:
        case errorKeys.qtyNotMultipleOfLot:
          text = errorTypes[type].join(', ') + orderStatusErrorsMap[type];
          break;

        case errorKeys.circuitLimitExceeded:
        case errorKeys.marginExceeded:
        case errorKeys.orderUnmatched:
          text = orderStatusErrorsMap[type] + errorTypes[type].join(', ');
          break;

        case errorKeys.adapterError:
        case errorKeys.clientBlocked:
        case errorKeys.clientNotEnabled:
        case errorKeys.tradingSystemNotReady:
        case errorKeys.exchgNotEnabled:
          text = orderStatusErrorsMap[type];
          break;

        default :
          text = "Order placement unsuccessful. Could you give it another try? If the issue persists, write to support@smallcase.com";

      }

      return (
        <View style={styles.errorMessageContainer} key={id}>
          <Image style={styles.smallIcon} source={require('../../assets/error.png')}/>
          <Text style={styles.messageText}>{text}</Text>
        </View>
      );
    });
  };

  renderSuccessMessage = (text) => (
    <View style={styles.successMessageContainer}>
      <Image style={styles.smallIcon} source={require('../../assets/success.png')}/>
      <Text style={styles.messageText}>{text}</Text>
    </View>
  );

  renderIncompleteMessage = (text) => (
    <View style={styles.statusRow}>
      <Image style={styles.largeIcon} source={require('../../assets/errorLarge.png')}/>
      <Text style={styles.largeStatusText}>{text}</Text>
    </View>
  );

  renderUnsuccessfulMessage = (message) => (
    <View style={styles.plainMessageContainer}>
      <Text style={[styles.messageText, {marginLeft: 14}]}>{message}</Text>
    </View>
  );

  render() {
    const {type, smallcaseDetails, jwt, csrf, repairing} = this.props;
    const {batchStatus, filledQuantity, statusText, repairPopupShown, stockLength, orderErrors, sellAmount} = this.state;
    const  analytics = {
      accessedFrom: 'Order Status',
      sectionTag: 'N/A',
      smallcaseName: smallcaseDetails.smallcaseName,
      smallcaseSource : smallcaseDetails.source,
      scid : smallcaseDetails.scid,
      orderType : '',
      smallcaseType: 'N/A',

    }

    let successView = (
      <View style={styles.statusRow}>
        <Image style={styles.largeIcon} source={require('../../assets/success.png')}/>
        <Text style={styles.largeStatusText}>All Orders filled</Text>
      </View>
    );

    let placedView = (
      <View style={[styles.statusRow, {marginTop: 16}]}>
        <Image style={styles.largeIcon} source={require('../../assets/success.png')}/>
        <Text style={styles.largeStatusText}>Orders Placed</Text>
      </View>
    );

    let placingView = (
      <View style={styles.statusRow}>
        <View style={styles.spinnerContainer}>
          <Spinner type={'Arc'} style={styles.spinner} size={40}/>
          <Text style={[styles.placingText, {color: Styles.BLUE}]}> 1</Text>
        </View>
        <Text style={styles.largeStatusText}>Placing Orders</Text>
      </View>
    );

    let waitingForStatusView = (
      <View style={styles.statusRow}>
        <View style={styles.spinnerContainer}>
          <Spinner type={'Arc'} style={styles.spinner} size={40}/>
          <Text style={[styles.placingText, {color: Styles.BLUE,}]}> 2</Text>

        </View>
        <Text style={styles.largeWaitingStatusText}>Waiting for Order Status</Text>
      </View>
    );

    let waitingToPlaceView = (
      <View style={styles.statusRow}>
          <View style={styles.circle}>
          <Text style={[styles.placingText, {color: 'rgb(129, 135, 140)'}]}> 2</Text>
          </View>
        <Text style={styles.largeWaitingStatusText}>Waiting to place orders</Text>
      </View>
    );

    let placedStatusView = null;
    let orderStatusView = null;
    let messageView = null;

    switch (batchStatus) {
      case BatchStatus.FIXED :
      case BatchStatus.COMPLETED :
      case BatchStatus.MARKEDCOMPLETE :
        placedStatusView = placedView;
        orderStatusView = successView;
        let text = finalSuccessStateCopy(type, sellAmount);
        messageView = this.renderSuccessMessage(text);
        break;

      case BatchStatus.PLACED :
      case BatchStatus.ERROR :
      case BatchStatus.PARTIALLYPLACED :
        placedStatusView = placedView;
        orderStatusView = waitingForStatusView;
        messageView = (typeof orderErrors !== "undefined" ? this.displayErrors(orderErrors) : null);
        break;

      case BatchStatus.PARTIALLYFILLED :
        text = "Orders partially filled";
        placedStatusView = placedView;
        orderStatusView = this.renderIncompleteMessage(text);
        messageView = (typeof orderErrors !== "undefined" ? this.displayErrors(orderErrors) : null);
        break;

      case BatchStatus.UNFILLED :
        text = "Orders not filled";
        placedStatusView = placedView;
        orderStatusView = this.renderIncompleteMessage(text);
        messageView = (typeof orderErrors !== "undefined" ? this.displayErrors(orderErrors) : null);
        break;

      case "PLACING" :
        placedStatusView = placingView;
        orderStatusView = waitingToPlaceView;
        messageView = null;
        break;

      case BatchStatus.UNPLACED :   // only text changes, allowing fallthrough
        text = "Orders not placed";
        let message = "Order placement unsuccessful. You could give it another try. If the issue persists, write to support@smallcase.com";
        break;

      default :
        text = "Order status is unknown";
        message = "Sorry, your order could status could not be fetched due to an internal server error. You can check your Orders and see if the order exists there or not.";
        placedStatusView = this.renderIncompleteMessage(text);
        orderStatusView = null;
        messageView = this.renderUnsuccessfulMessage(message);
        break;
    }

    const batchView = (
      <View style={styles.batchContainer}>
        <View style={styles.batchColumn}>
          <Text style={styles.batchTitleText}>Batch</Text>
          <Text style={styles.batchTypeText}>{orderLabelMap(type)}</Text>
        </View>
        <View style={styles.statusColumn}>
          <Text style={styles.batchTitleText}>Status</Text>
          <Text style={styles.batchTypeText}>{statusText}</Text>
        </View>
        <View style={styles.filledColumn}>
          <Text style={styles.batchTitleText}>{filledQuantity} of {stockLength} filled</Text>
          <OrderPercent percent={filledQuantity / stockLength * 100}
                        containerStyle={styles.batchStatusProgress}/>
        </View>
      </View>
    );

    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <OrderTitle type={type} smallcaseDetails={smallcaseDetails} repairing={repairing}/>
          <View style={{marginBottom: 16}}/>
          {placedStatusView}
          {orderStatusView}
          {batchStatus === BatchStatus.UNKNOWN ? null : batchView}
          {messageView}
        </View>
        <OrderStatusAction batchStatus={batchStatus} showRepairPopup={this.showRepairPopup}/>
        <RepairPopup
          showPopup={repairPopupShown}
          repairPopupClosed={this.onRepairPopupClosed.bind(this)}
          csrf={csrf}
          jwt={jwt}
          type={type}
          smallcaseDetails={smallcaseDetails}
          analytics={analytics}

        />
      </View>
    )
  }
}

const styles = MediaQueryStyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  statusRow: {
    flexDirection: 'row',
    height: 36,
    alignItems: 'center',
    marginLeft: 16,
    marginTop: 8,
    marginBottom: 12
  },
  largeIcon: {
    height: 36,
    width: 36,
  },
  largeStatusText: {
    padding: 16,
    fontSize: 18,
    fontWeight: 'bold',
    color: 'rgb(47, 54, 63)',
  },
  largeWaitingStatusText: {
    padding: 16,
    fontSize: 18,
    color: 'rgb(47, 54, 63)',
  },
  batchStatusProgress: {
    width: 134,
    height: 12,
    marginTop: 9
  },
  batchContainer: {
    marginTop: 20,
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 32,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  batchColumn: {
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  statusColumn: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginLeft: 16
  },
  filledColumn: {
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  batchTitleText: {
    fontSize: 14,
    color: 'rgb(129,135,140)',
    width: 90,
  },
  batchTypeText: {
    fontSize: 16,
    color: 'rgb(47,54,63)',
    marginTop: 9,
  },
  successMessageContainer: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(25, 175, 85)',
    marginHorizontal: 16,
    marginVertical: 9,
    flexDirection: 'row',
  },
  errorMessageContainer: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(255, 89, 66)',
    marginHorizontal: 16,
    marginVertical: 9,
    flexDirection: 'row',
  },
  plainMessageContainer: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'rgb(221, 224, 228)',
    marginLeft: 16,
    marginRight: 16,
    flexDirection: 'row',
  },
  smallIcon: {
    marginLeft: 14,
    marginTop: 14,
    marginRight: 10,
    height: 20,
    width: 20,
  },
  messageText: {
    marginTop: 12,
    marginBottom: 14,
    marginRight: 14,
    fontSize: 14,
    color: 'rgb(83, 91, 98)',
    flex: 1
  },
  spinner: {
    position: 'absolute',
    color: 'rgb(31, 122, 224)',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    width: 44,
    height: 44,
  },
  placingText: {
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    marginRight: 5,
  },
  spinnerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
  },
  circle: {

      width: 40,
      padding:3,
      height: 40 ,
      borderRadius:20,
      borderWidth: 2,
      borderColor: 'rgb(129, 135, 140)',
      alignItems: 'center',
      justifyContent : 'center',
  },
}, {
  [IPHONE_FIVE_AND_SMALLER]: {
    batchStatusProgress: {
      width: 92,
    }
  }
});

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps)(OrderStatus);

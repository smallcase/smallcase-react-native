import Api from "../api/Api";
import {GET_USER_FUNDS, GET_STOCK_PRICE, PLACE_ORDERS, GET_ORDERS, FIX_BATCH, REPAIR_BATCH} from "../api/ApiRoutes";
import {OrderLabel, OrderType, StockOrderStatus} from "../util/Utility";
import {calculateClosestInvestableAmount} from "../helper/SmallcaseHelper";

export const getClosestInvestableAmount = (jwt, csrf, stocklist, inputAmount) => {
  return new Promise((resolve, reject) => {
      getUpdatedStocks(jwt, csrf, stocklist)
        .then(stocks => resolve(calculateClosestInvestableAmount(stocks, inputAmount)))
        .catch(error => reject(error));
    }
  )
};

/*
*   Take stocks from investments page and fetch new prices.
*/
export const getUpdatedStocks = (jwt, csrf, stocklist) => {
  let params = "";
  for (let i = 0; i < stocklist.length; i++) {
    params += "stocks=" + stocklist[i].sid + "&";
  }
  return new Promise((resolve, reject) => {
    Api.get({route: GET_STOCK_PRICE, jwt, csrf, params})
      .then((response) => resolve(updateStockPrices(stocklist, response)))
      .catch((error) => reject(error))
  });
};

const updateStockPrices = (stocks, prices) => {
  stocks.forEach((stock) => {
    stock.price = prices.data[stock.sid];
  });

  return stocks;
};

export const placeBatchOrders = (jwt, csrf, body) => {
  return new Promise((resolve, reject) => {
    Api.post({route: PLACE_ORDERS, jwt, csrf, body})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

export const getBatchStatus = (jwt, csrf, batchId) => {
  const params = "batchId=" + batchId + "&noDetails=false&onlyOne=false";
  return new Promise((resolve, reject) => {
    Api.get({route: GET_ORDERS, jwt, csrf, params})
      .then((response) => resolve(response))
      .catch((error) => {
        reject(error)
      });
  });
};

export const repairBatchOrders = (jwt, csrf, body) => {
  return new Promise((resolve, reject) => {
    Api.post({route: REPAIR_BATCH, jwt, csrf, body})
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};

export const groupErrors = (orders) => {
  let errorMap = {
    filledCount: 0,
    filled: [],
    // more arrays based on error types
    // eg. checkFreezeQty : [] ...
  };

  orders.forEach((order) => {
    if (order.status === StockOrderStatus.COMPLETE) {
      errorMap.filledCount++;
      errorMap.filled.push(order.sid);
    } else {
      let code = order.errorCode;
      if (errorMap.hasOwnProperty(code)) {
        errorMap[code].push(order.sid);
        // add a new stock to a particular error type
      } else {
        errorMap[code] = [order.sid];
        // add a new array of a particular error type
      }
    }
  });
  return errorMap;
};

export const calculateTotalAmount = (stocks) => {
  let sum = 0;
  stocks.forEach((stock) => {
    if (stock.transactionType === OrderType.SELL) {
      sum -= stock.shares * stock.price;
    } else {
      sum += stock.shares * stock.price;
    }
  });
  return sum;
};

export const finalSuccessStateCopy = (type, amount) => {
  switch (type) {
    case OrderLabel.INVESTMORE:
      return "Topped up. Since the additional amount you have invested is yet to generate returns, your index value would drop initially.";
    case OrderLabel.SELLALL:
      return "Thats a wrap: You will receive " + amount + " in your broker funds";
    case OrderLabel.PARTIALEXIT:
      return "Make hay while the sun shines. You've withdrawn Rs. " + amount;
    case OrderLabel.MANAGE:
      return "Fine tuned. Your smallcase has been updated with all the changes. ";
    case OrderLabel.REBALANCE:
      return "Great your smallcase is shiny and new. Your smallcase now matches the original smallcase's composition";
    case OrderLabel.SIP:
      return "Well done. Since the additional amount you have invested is yet to generate returns, your index value would drop initially";
    case OrderLabel.BUY:
      return "Future you will thank you for this. smallcase's index value has been set to 100 for easier tracking.";
    case OrderLabel.FIX:
      return "Fixed: Your smallcase is now all set";
  }
};
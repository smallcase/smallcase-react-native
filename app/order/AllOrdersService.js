import Api from "../api/Api";
import {GET_ORDERS} from "../api/ApiRoutes";
import {
  batchStatusMap,
  BatchStatus,
  getReadableDate,
  getSmallcaseImage,
  orderLabelMap,
  SmallcaseTier
} from "../util/Utility";

const getPresentableOrders = (data) => {
  return data.map((smallcase) => {
    return {
      name: smallcase.name,
      scid: smallcase.scid,
      source: smallcase.source,
      tier: smallcase.tier || SmallcaseTier.PREMIUM,
      iscid: smallcase._id,
      smallcaseImage: getSmallcaseImage(smallcase.scid, smallcase.source, '80'),
      lastBatchDate: getReadableDate(smallcase.lastBatchDate),
      lastBatch: {
        filled: smallcase.lastBatch.filled? smallcase.lastBatch.filled : 0,
        quantity: smallcase.lastBatch.quantity,
        label: orderLabelMap(smallcase.lastBatch.label),
        originalStatus: smallcase.lastBatch.status,
        status: batchStatusMap(smallcase.lastBatch.status),
        isPartial: smallcase.lastBatch.status === BatchStatus.UNFILLED || smallcase.lastBatch.status === BatchStatus.PARTIALLYFILLED
          || smallcase.lastBatch.status === BatchStatus.PARTIALLYPLACED,
      }
    }
  });
};

export const getOrders = (jwt, csrf) => {
  const params = 'onlyOne=true&noDetails=true';
  return new Promise((resolve, reject) => {
    Api.get({route: GET_ORDERS, jwt, csrf, params})
      .then((response) => resolve(getPresentableOrders(response.data)))
      .catch((error) => reject(error));
  });
};

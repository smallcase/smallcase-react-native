import React, {Component} from 'react';
import {
  View,
} from 'react-native';
import {Styles} from "../util/Utility";
import PropTypes from 'prop-types';

export default class OrderPercent extends Component {
  render() {
    const {percent, containerStyle, isRepaired} = this.props;
    let backgroundColor = Styles.RED;
    if (percent === 100 || isRepaired) {
      backgroundColor = Styles.GREEN_SUCCESS;
    }

    return (
      <View style={[{flexDirection: 'row', backgroundColor: 'rgb(230, 231, 232)', borderRadius: 8}, containerStyle]}>
        <View style={{flex: percent, backgroundColor, borderRadius: 8}}/>
        <View style={{flex: 100 - percent, backgroundColor: 'rgb(230, 231, 232)', borderTopRightRadius: 8, borderBottomRightRadius: 8,
          borderTopLeftRadius: percent? 0 : 8, borderBottomLeftRadius: percent? 0 : 8 }}/>
      </View>
    );
  }
}

OrderPercent.protoTypes = {
  percent: PropTypes.number.isRequired
};
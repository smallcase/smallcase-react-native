import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

export default class SmallcaseButton extends Component {
  render() {
    let {children, onClick, disabled, buttonStyles} = this.props;
    return (
      <TouchableOpacity disabled={disabled} onPress={onClick}>
        <View style={[styles.button, {opacity: disabled ? 0.3 : 1}, buttonStyles]}>
          {children}
        </View>
      </TouchableOpacity>
    );
  }
}

SmallcaseButton.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: 'white',
    alignSelf: 'stretch',
    alignItems: 'center',
    borderRadius: 4,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    justifyContent: 'center',
    shadowRadius: 0,
    shadowOpacity: 0
  }
});
import React, { Component } from 'react';
import {
  Image,
  View,
  Text,
  StyleSheet
} from 'react-native';
import {SmallcaseSource} from "../util/Utility";
import PropTypes from 'prop-types';

export default class SmallcaseImage extends Component {

  render() {
    let {imageUrl, source, imageStyles, textSize} = this.props;
    let customTag = null;
    if (SmallcaseSource.CUSTOM === source) {
      customTag = (<Text style={[styles.customTag, {fontSize: textSize || 7}]}>CUSTOM</Text>);
    }

    return (
      <View style={imageStyles}>
        <Image style={[styles.smallcaseImage]} source={{uri: imageUrl}}/>
        {customTag}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  smallcaseImage: {
    flex: 1,
    backgroundColor: 'rgb(249, 250, 251)',
    borderRadius: 4
  },
  customTag: {
    alignItems: 'stretch',
    fontWeight: 'bold',
    backgroundColor: 'rgb(31, 122, 224)',
    color: 'white',
    paddingVertical: 2,
    marginBottom: 4,
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    textAlign: 'center'
  }
});

SmallcaseImage.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  source: PropTypes.string.isRequired,
  textSize: PropTypes.number
}
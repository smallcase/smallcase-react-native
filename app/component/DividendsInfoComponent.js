import React from 'react'
import {TouchableWithoutFeedback, View, Image, StyleSheet, Text, Animated, Easing, Linking} from "react-native";
import ExpandCollapse from "./ExpandCollapse";
import {getFormattedCurrency, getReadableDate, Styles} from "../util/Utility";
import ReturnsNumber from "./ReturnsNumber";
import {ReturnsComponentValueType} from "../constants/Constants";
const DIVIDENDS_INFO_SMALLCASE = "https://zerodha-help.smallcase.com/smallcase-for-ios/track-and-manage-your-smallcases/where-can-i-check-the-dividends-earned-on-the-ios-app"
const DescriptionComponent = (props) => {

  return (
    <View style={styles.descriptionContainer}>
      <Text style={styles.titleText}>{props.title}</Text>
      <Text style={styles.valueText}>{props.value}</Text>
    </View>
  )
}



export default class DividendsInfoComponent extends React.Component {

  constructor(props) {
    super()
    this.state = {
      expandOverview: false,
      spinStartDegree: '0deg',
      spinEndDegree: '180deg',
      spinValue: new Animated.Value(0),
    }
  }

  getSeeDetailsArrow = () => {
    // Interpolate beginning and end values (in this case 0 and 1)
    const spin = this.state.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: [this.state.spinStartDegree, this.state.spinEndDegree]
    });

    return (
      <Animated.Image style={[styles.detailsArrow, {transform: [{rotate: spin}]}]}
                      source={require('../../assets/iconArrowDownBlue.png')}/>
    );
  }


  redirectToDividendsInfo = () => {
    Linking.canOpenURL(DIVIDENDS_INFO_SMALLCASE).then(supported => {
      if (supported) {
        Linking.openURL(DIVIDENDS_INFO_SMALLCASE);
      } else {
        console.log("Couldn't open url");
      }
    });
  }

  toggleSeeDetails = () => {
    this.state.spinValue.setValue(0);
    Animated.timing(
      this.state.spinValue, {
        toValue: 1,
        duration: 200,
        easing: Easing.linear,
        useNativeDriver: true
      }
    ).start();

    this.setState({
      expandOverview: !this.state.expandOverview,
      spinStartDegree: this.state.expandOverview ? '180deg' : '0deg',
      spinEndDegree: this.state.expandOverview ? '0deg' : '180deg'
    });
  }


  render() {
    const {credited, accrued, total, payoutDate, liquidBeesName} = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.toggleSeeDetails}>
        <View style={[styles.topContainer, styles.containerBorder]}>
          <View style={styles.innerContainer}>
            <Image source={require('../../assets/combined-shape.png')} style={styles.image}/>
            <View >
              <Text style={{paddingRight: 16}}>You have received <ReturnsNumber type={ReturnsComponentValueType.AMOUNT} value={total}/> in dividends from {liquidBeesName}</Text>
            </View>
          </View>
          <ExpandCollapse style={{margin: 8}} value={this.state.expandOverview} maxHeight='490'>
            <View style={styles.detailsContainer}>
              <View style={styles.investmentDetailsContainer}>
                <DescriptionComponent title='Total Dividends' value={getFormattedCurrency(total)}/>
                <DescriptionComponent title='Credited' value={getFormattedCurrency(credited)}/>
                <DescriptionComponent title='Not Credited' value={getFormattedCurrency(accrued)}/>
              </View>
              <Text style={styles.creditDescription}>Dividends will be next credited to your demat account on <Text style={{fontWeight: 'bold',color:Styles.PRIMARY_TEXT_COLOR}}>{getReadableDate(payoutDate)}.</Text><TouchableWithoutFeedback onPress={this.redirectToDividendsInfo}><Text style={{color:Styles.BLUE,fontWeight:'bold'}}> Learn More</Text></TouchableWithoutFeedback></Text>
            </View>
          </ExpandCollapse>
          <View style={styles.seeDetailsLayout}>
            <Text style={styles.seeDetailsText}>{this.state.expandOverview ? 'Hide Details': 'Show Details'}</Text>
            {this.getSeeDetailsArrow()}
          </View>

        </View>
      </TouchableWithoutFeedback>
    )
  }
}


styles = StyleSheet.create({
  containerBorder: {
    borderRadius: 5,
    borderColor: '#dde0e4',
    borderWidth: 1,
  },
  descriptionContainer: {
    flexDirection: 'column',
    marginBottom: 16,
  },
  detailsContainer: {
    marginBottom: 8,
    marginLeft: 22,
    flexDirection: 'column',
  },
  detailsArrow: {
    marginLeft: 4,
    marginTop: 6,
    width: 11,
    height: 7
  },
  image: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  innerContainer: {
    flexDirection: 'row',
    paddingRight: 8,

  },
  topContainer: {
    marginLeft: 16,
    marginRight: 16,
    marginTop: 32,
    flex: 1,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
    paddingBottom: 16
  },
  titleText: {
    fontSize: 12,
    color: '#81878c',
    marginBottom: 8,
  },
  investmentDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  valueText: {
    fontSize: 15,
    color: Styles.PRIMARY_TEXT_COLOR,
  },
  seeDetailsLayout: {
    marginTop: 16,
    marginLeft: 30,
    flexDirection: 'row',
  },
  seeDetailsText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: Styles.BLUE,
    //marginBottom: 16
  },
  creditDescription : {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR,
    fontWeight: '500',
    lineHeight: 19,
  }


})
import React from 'react'
import {View,Text,StyleSheet} from 'react-native';
import { Styles} from "../util/Utility";

 const  DateBoxComponent = (props) => (
    <View style={styles.topContainer}>
         <Text style={styles.titleText}>{props.title}</Text>
         <Text style={styles.dateText}>{props.date}</Text>
    </View>)
export default DateBoxComponent


const styles = StyleSheet.create({
    topContainer : {
       flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center',

    },
    titleText : {
        fontSize : 12,
        color :Styles.SECONDARY_TEXT_COLOR,
        marginBottom: 8,

    },
    dateText : {
        fontSize: 15,
        letterSpacing: 0.3,
        color: Styles.PRIMARY_TEXT_COLOR,

    }
});
import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  Animated,
  Easing
} from 'react-native';
import PropTypes from 'prop-types';

export default class ExpandCollapse extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {height: this.props.animatedValue || new Animated.Value(this.props.minHeight || 0)};
  }

  componentWillReceiveProps = (newProps) => {
    if (this.props.value !== newProps.value) {
      newProps.value ? this.open() : this.close();
    }
  };

  close = () => {
    Animated.timing(this.state.height, {
      easing: Easing.ease,
      duration: 300,
      toValue: this.props.minHeight || 0,
    }).start();
  };

  open = () => {
    Animated.timing(this.state.height, {
      bounciness: this.props.bounciness || 1,
      duration: 300,
      toValue: this.state.maxHeight,
      easing: Easing.ease,
    }).start();
  };

  setMaxHeight = (event) => {
    const layoutHeight = event.nativeEvent.layout.height;
    this.setState({
      maxHeight: Math.min((this.props.maxHeight || layoutHeight), layoutHeight)
    });
  };

  render() {
    let {style, children, containerStyle} = this.props;
    let {height} = this.state;
    return (
      <View style={[styles.containerStyle]}>
        <Animated.View style={[styles.menuStyle, {height}, containerStyle]}>
          <View ref="expand" onLayout={this.setMaxHeight} style={style}>
            {children}
          </View>
        </Animated.View>
      </View>
    );
  }
}

ExpandCollapse.propTypes = {
  animatedValue: PropTypes.number,
  minHeight: PropTypes.number,
  maxHeight: PropTypes.number,
  value: PropTypes.bool.isRequired,
  bounciness: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]).isRequired,
};

const styles = StyleSheet.create({
  containerStyle: {
    overflow: 'hidden'
  },
  menuStyle: {
    overflow: 'scroll'
  }
});
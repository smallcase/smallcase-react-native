import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import SmallcaseButton from "./SmallcaseButton";
import {Styles as Style} from "../util/Utility";

export default ErrorState = (props) => (
  <View style={styles.container}>
    <Image source={require('../../assets/error500.png')} style={styles.errorImage} />
    <Text style={styles.title}>This should not have happened</Text>
    <Text style={styles.description}>We are working on squashing the bug. Meanwhile, you can refresh this page or head back to your or try contacting
      <Text style={styles.mail}>support@smallcase.com</Text></Text>
    <SmallcaseButton buttonStyles={styles.refreshButton} onClick={props.refresh}><Text style={styles.refresh}>Refresh</Text></SmallcaseButton>
  </View>
);

const styles = StyleSheet.create({
  errorImage: {
    width: 320,
    height: 220
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 16,
    color: Style.PRIMARY_TEXT_COLOR,
    marginTop: 16,
    marginHorizontal: 24,
    textAlign: 'center'
  },
  description: {
    lineHeight: 20,
    marginTop: 8,
    marginHorizontal: 24,
    textAlign: 'center',
    color: Style.SECONDARY_TEXT_COLOR
  },
  mail: {
    fontWeight: 'bold',
    color: Style.BLUE
  },
  refresh: {
    color: 'white'
  },
  refreshButton: {
    marginTop: 16,
    height: 48,
    width: 156,
    backgroundColor: Style.BLUE
  }
});

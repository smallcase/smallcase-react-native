import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native';
import {Styles} from "../util/Utility";
import PropTypes from 'prop-types';

export default class RadioButton extends Component {

  componentWillReceiveProps = (newProps) => {
    if (newProps.isSelected !== this.props.isSelected) this.setState({isSelected: newProps.isSelected});
  }

  render() {
    const {title, isSelected, containerStyles, onSelected} = this.props;
    const checkBoxImage = isSelected ? require('../../assets/selectedCheckBox.png') : require('../../assets/checkBox.png');
    return (
      <TouchableWithoutFeedback onPress={() => onSelected()}>
        <View style={[{flexDirection: 'row'}, containerStyles]}>
          <Image style={styles.checkBox} source={checkBoxImage}/>
          <Text style={[styles.title, {color: isSelected ? Styles.BLUE : Styles.PRIMARY_TEXT_COLOR}]}>{title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

RadioButton.propTypes = {
  title: PropTypes.string.isRequired,
  isSelected: PropTypes.bool.isRequired,
}

const styles = StyleSheet.create({
  checkBox: {
    width: 20,
    height: 20
  },
  title: {
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: 2
  }
});

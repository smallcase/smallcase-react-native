import {View, StyleSheet} from "react-native";
import React from "react";
import PropTypes from 'prop-types';
import ReturnsNumber from "./ReturnsNumber";
import {Styles} from "../util/Utility";

const ReturnsComponent = (props) => (
  <View style={props.containerStyles}>
    <View style={{flexDirection: 'row'}}>
      {props.topLeft ? <ReturnsNumber value={props.topLeft.value} type={props.topLeft.type}
                                      textStyle={[styles.topLeftStyles, props.topLeftStyles]}/> : null}
      {props.topRight ? <ReturnsNumber value={props.topRight.value} type={props.topRight.type}
                                       textStyle={[styles.topRightStyles, props.topRightStyles]}/> : null}
    </View>
    <View style={{flexDirection: 'row', marginTop: 8}}>
      {props.bottomLeft ? <ReturnsNumber value={props.bottomLeft.value} type={props.bottomLeft.type}
                                         textStyle={[styles.bottomLeftStyles, props.bottomLeftStyles]}/> : null}
      {props.bottomRight ? <ReturnsNumber value={props.bottomRight.value} type={props.bottomRight.type}
                                          textStyle={props.bottomRightStyles}/> : null}
    </View>
  </View>
);

export default ReturnsComponent;

ReturnsComponent.protoTypes = {
  topLeft: PropTypes.object,
  topRight: PropTypes.object,
  bottomLeft: PropTypes.object,
  bottomRight: PropTypes.object,
}

const styles = StyleSheet.create({
  topLeftStyles: {
    fontSize: 13,
    color: Styles.SECONDARY_TEXT_COLOR
  },
  topRightStyles: {
    fontSize: 12,
    fontWeight: 'bold',
    marginLeft: 6,
    marginTop: 1
  },
  bottomLeftStyles: {
    fontSize: 15
  }
});

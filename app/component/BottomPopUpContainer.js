import React, {Component} from 'react';
import {
  Dimensions,
  TouchableWithoutFeedback,
  View,
  StyleSheet,
  Animated,
  interpolate
} from 'react-native';
import Interactable from 'react-native-interactable';
import PropTypes from 'prop-types';
import {getScreenSize} from "../util/Utility";

const Screen = getScreenSize();

export default class BottomPopUpContainer extends Component {

  constructor() {
    super();
    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
  }

  componentWillReceiveProps = (newProps) => {
    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup) {
      this.popUp.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.popUp.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.popupClosed();
    } else {
      this.blurPointerEvent = 'auto';
    }
  };

  dismissPopup = () => {
    if (this.props.disableDismiss) return;

    this.popUp.snapTo({index: 1});
  }

  render () {
    const {children, contentHeight, disableDismiss} = this.props;

    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback
          pointerEvents={this.blurPointerEvent}
          onPress={this.dismissPopup}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - contentHeight, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.popUp = ele}
          verticalOnly={true}
          snapPoints={[{y: Screen.height - contentHeight}, {y: Screen.height}]}
          onSnap={this.handelSnap}
          boundaries={{top: Screen.height - (contentHeight + 20)}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          dragEnabled={!disableDismiss}
          animatedValueY={this.deltaY}>
          {children}
        </Interactable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
});

BottomPopUpContainer.protoTypes = {
  showPopup: PropTypes.bool.isRequired,
  popupClosed: PropTypes.func,
  contentHeight: PropTypes.number.isRequired,
  disableDismiss: PropTypes.bool
};

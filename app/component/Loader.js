import React from 'react';
import {ActivityIndicator} from 'react-native';
import {Styles} from "../util/Utility";

export default Loader = function (props) {
  let color = Styles.SECONDARY_TEXT_COLOR;
  let size = 'large';

  if (props.color) color = props.color;

  if (props.size) size = props.size;

  return (
    <ActivityIndicator size={size} color={color} style={props.loaderStyles} />
  );
};

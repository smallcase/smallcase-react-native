import React, {PureComponent} from 'react';
import {
  ActivityIndicator,
  Animated,
  AsyncStorage,
  Dimensions,
  Image,
  Keyboard,
  Linking,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import Divider from "./Divider";
import Interactable from 'react-native-interactable';
import PropTypes from 'prop-types';
import SmallcaseButton from "./SmallcaseButton"
import {getUserFunds} from "../user/UserService"
import {
  AnalyticsSource,
  getFormattedCurrency,
  getScreenSize,
  OrderInputState, OrderLabel, OrderType,
  Routes,
  SnackBarTime,
  Styles
} from "../util/Utility";
import {Actions} from "react-native-router-flux"
import {ADD_FUNDS_URL} from "../constants/Constants.js"
import * as types from "../constants/Constants";
import * as Config from "../../Config";
import {getUserDetails} from "../user/UserActions";
import {BROKER_LOGIN} from "../api/ApiRoutes";
import {getJwt} from "../helper/LoginHelper";
import {userLoggedIn} from "../onboarding/LandingActions";
import {getStockPrices} from "../investments/sip/SipService";
import {calculateMinInvestment} from "../helper/SmallcaseHelper";
import {calculateWeightageFromShares} from "../helper/StockHelper";
import {connect} from "react-redux";
import TouchHighlight from "./TouchHighlight";
import AnalyticsManager from "../analytics/AnalyticsManager";
import {SnackBarMessages} from "../util/Strings";
import Loader from "./Loader";

const Screen = getScreenSize();

class InvestMorePopup extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      amount: '',
      orderError: false,
      inputState: OrderInputState.INPUT,
      minInvestmentAmount: '',
      fetchingMinAmount: true,
      keyboardShown: false,
      keyboardHeight: 0,
      availableFunds: null,
    };
    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
  }

  componentWillMount() {
    // check if value available in async storage
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow(e) {
    let newSize = e.startCoordinates.height;
    this.setState({
      inputState: OrderInputState.INPUT_FOCUSED,
      keyboardHeight: newSize,
      keyboardShown: true,
    });
  }

  keyboardDidHide(e) {
    this.setState({
      keyboardShown: false,
      investPopupShown: false,
      inputState: OrderInputState.INPUT,

    });
  }

  componentWillReceiveProps = (newProps) => {
    if (newProps.loginReducer && (newProps.loginReducer.requestToken !== this.props.loginReducer.requestToken)) {
      this.fetchAuthDetails(newProps);
    }

    if (!this.props.stocks && newProps.stocks !== null) {
      this.calculateMinAmount();
    }

    if (this.props.showPopup === newProps.showPopup) return;

    if (newProps.showPopup && this.state.keyboardShown) {
      this.orderPopup.snapTo({index: 2});
      this.blurPointerEvent = 'auto';
    } else if (newProps.showPopup && !this.state.keyboardShown) {
      this.orderPopup.snapTo({index: 0});
      this.blurPointerEvent = 'auto';
    } else {
      this.orderPopup.snapTo({index: 1});
      this.blurPointerEvent = 'box-none';
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1) {
      this.blurPointerEvent = 'box-none';
      this.props.orderPopupClosed();
    } else if (event.nativeEvent.index === 2) {
      this.blurPointerEvent = 'auto';
    } else {
      this.blurPointerEvent = 'auto';
      this.calculateMinAmount();
    }

    this.setState({orderPopupToggled: true});
  };

  calculateMinAmount = () => {
    const {jwt, csrf, stocks} = this.props;
    const sids = [];

    if (!stocks || !stocks.length) return;

    stocks.forEach(stock => sids.push(stock.sid));
    getStockPrices(jwt, csrf, sids)
      .then((prices) => {
        let newStocks = stocks.map(stock => Object.assign({}, stock));
        newStocks.forEach(stock => stock.price = prices[stock.sid]);
        newStocks = calculateWeightageFromShares(newStocks);
        const minAmount = calculateMinInvestment(newStocks).minAmount;
        this.setState({
          minInvestmentAmount: minAmount,
          amount: minAmount.toFixed(2),
          fetchingMinAmount: false,
          inputState: OrderInputState.INPUT,
          availableFunds: null
        });
      })
      .catch(error => console.log(error));
  }


  dismissOrderPopup = () => {
    this.blurPointerEvent = 'box-none';
    this.setState({orderPopupToggled: true});

    this.props.orderPopupClosed();
    Keyboard.dismiss();
  };

  dismissError = () => this.setState({inputState: OrderInputState.INPUT_FOCUSED});

  openFundsPage = () => {
    const {smallcase} = this.props;
    AnalyticsManager.track('Proceeded To Adding Funds',
      {
        accessedFrom: "Investment Details",
        sectionTag: 'N/A',
        amountConfirmed: this.state.amount,
        minInvestAmount: this.state.minInvestmentAmount,
        smallcaseName: smallcase.smallcaseName,
        smallcaseSource: smallcase.source,
        smallcaseType: 'N/A',

      },
      [AnalyticsSource.MIXPANEL, AnalyticsSource.CLEVER_TAP]);
    Linking.canOpenURL(ADD_FUNDS_URL).then(supported => {
      if (supported) {
        this.dismissOrderPopup();
        Linking.openURL(ADD_FUNDS_URL);
        this.setState({inputState: OrderInputState.INPUT})
      } else {
        console.log("Couldn't open url");
      }
    });
  };

  fetchAuthDetails = (newProps) => {
    const body = {broker: 'kite', reqToken: newProps.loginReducer.requestToken, app: 'platform'};
    this.setState({fetchingJwt: true});
    getJwt(BROKER_LOGIN, body, Config.AUTH_BASE_URL, (jwt, csrf) => {
      this.setState({fetchingJwt: false});
      if (jwt && csrf) {
        this.props.dispatch(getUserDetails(jwt, csrf));
        this.props.dispatch(userLoggedIn(jwt, csrf));
        AsyncStorage.setItem(types.AUTH_INFO, JSON.stringify({jwt, csrf}), (error) => {
        });
        //this.onConfirmAmountClicked(jwt, csrf);
      }
      else {
        console.log("Login failed");
      }
    });
  }

  onConfirmAmountClicked = (jwt, csrf) => {
    const {minInvestmentAmount} = this.state;
    const {continueToReviewOrder, smallcase} = this.props;
    let amountEntered = 0;
    let minInvestment = minInvestmentAmount.toFixed(2);
    jwt = jwt || this.props.jwt;
    csrf = csrf || this.props.csrf;

    if (this.state.amount) {
      amountEntered = this.state.amount;
    } else {
      amountEntered = minInvestmentAmount.toFixed(2);
    }

    if (isNaN(amountEntered.replace(/\,/g, ""))) {
      this.setState({inputState: OrderInputState.INCORRECT_FORMAT});
      return;
    }

    amountEntered = parseFloat(amountEntered);
    minInvestment = parseFloat(minInvestment);

    if (amountEntered < minInvestment) {
      this.setState({inputState: OrderInputState.INVALID_AMOUNT});
    } else {
      getUserFunds(jwt, csrf)
        .then((funds) => {
          if (funds < amountEntered) {
            this.setState({inputState: OrderInputState.FUNDS_UNAVAILABLE, availableFunds: funds});
          } else {
            this.dismissOrderPopup();
            continueToReviewOrder(amountEntered);
          }
        })
        .catch((error) => {
          if (error.data && error.data.status === 401) {
            this.dismissOrderPopup();
            Actions.push(Routes.LOGIN);
          }
        });
    }

    AnalyticsManager.track('Confirmed Amount', {
      amountConfirmed: amountEntered,
      maxExitAmount: this.state.availableFunds,
      minInvestAmount: minInvestment,
      exitType: 'N/A',
      orderType: OrderLabel.INVESTMORE,
      iscid: smallcase.iscid,
      scid: smallcase.scid,
      smallcaseName: smallcase.smallcaseName,
      smallcaseSource: smallcase.source,
      smallcaseType: 'N/A',
    }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])

  };

  reactToKeyboard = () => {
    this.orderPopup.snapTo({index: 2});

    this.setState({
      inputState: OrderInputState.INPUT_FOCUSED,
    })
  }


  render() {
    const {inputState, minInvestmentAmount, amount, fetchingMinAmount, keyboardHeight, availableFunds} = this.state;
    const {startOffSet, smallcase} = this.props;
    const offSet = startOffSet ? startOffSet : 0;
    const kbHeight = keyboardHeight > 0 ? keyboardHeight : 250;
    console.log(this.blurPointerEvent);

    const buttonView = (
      <SmallcaseButton buttonStyles={styles.button} onClick={() => this.onConfirmAmountClicked()}>
        <Text style={styles.buttonText}>Confirm Amount</Text>
      </SmallcaseButton>
    );

    const incorrectFormatView = (
      <TouchHighlight onClick={this.dismissError}>
        <View style={styles.errorContainer}>
          <Image style={styles.errorImage} source={require('../../assets/error.png')}/>
          <View style={styles.errorDetails}>
            <Text style={styles.errorText}>Please enter a valid amount</Text>
          </View>
        </View>
      </TouchHighlight>
    );

    const fundsUnavailableView = (
      <TouchHighlight onClick={this.openFundsPage}>
        <View style={styles.errorContainer}>
          <Image style={styles.errorImage} source={require('../../assets/error.png')}/>
          <View style={styles.errorDetails}>
            <Text style={styles.errorText}>
              You don't have enough funds for this order. Available funds: {getFormattedCurrency(availableFunds)}
            </Text>
            <Text style={styles.fundsButton}>Add funds</Text>
          </View>
        </View>
      </TouchHighlight>
    );

    const invalidAmountView = (
      <TouchHighlight onClick={this.dismissError}>
        <View style={styles.errorContainer}>
          <Image style={styles.errorImage} source={require('../../assets/error.png')}/>
          <View style={styles.errorDetails}>
            <Text style={styles.errorText}>
              Enter an amount greater than {minInvestmentAmount ? minInvestmentAmount.toFixed(2) : ""}
            </Text>
          </View>
        </View>
      </TouchHighlight>
    );

    let view = buttonView;
    let dividerColor = 'rgb(221, 224, 228)';


    switch (inputState) {
      case OrderInputState.INCORRECT_FORMAT:
        view = incorrectFormatView;
        dividerColor = Styles.RED_FAILURE;
        break;
      case OrderInputState.INVALID_AMOUNT:
        view = invalidAmountView;
        dividerColor = Styles.RED_FAILURE;
        break;
      case OrderInputState.FUNDS_UNAVAILABLE:
        view = fundsUnavailableView;
        dividerColor = Styles.RED_FAILURE;
        AnalyticsManager.track('Viewed Add Funds Error', {
          iscid: smallcase.iscid,
          orderType: OrderLabel.INVESTMORE,
          originalLabel: OrderLabel.INVESTMORE,
          scid: smallcase.scid,
          smallcaseName: smallcase.smallcaseName,
          smallcaseSource: smallcase.source,
          smallcaseType: 'N/A',
        }, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL])
        break;
      case OrderInputState.INPUT_FOCUSED :
        view = buttonView;
        dividerColor = Styles.BLUE;
        break;

      default :
        view = buttonView;
      break;
    }

    const popup = (
      fetchingMinAmount ?
        <Loader loaderStyles={styles.container}/> :
        <View style={styles.container}>
          <Text style={styles.title}>Confirm Investment Amount</Text>
          <Text style={styles.details}>Minimum investment
            is {minInvestmentAmount ? minInvestmentAmount.toFixed(2) : ""}</Text>
          <View style={styles.amountRow}>
            <Text style={styles.amount}>{"\u20B9 "}</Text>
            <TextInput
              style={styles.amountInput}
              keyboardType={'numeric'}
              defaultValue={minInvestmentAmount ? minInvestmentAmount.toFixed(2) : ""}
              onChangeText={(value) => this.setState({amount: value})}
              onChange={this.dismissError}
              value={amount}
              onFocus={this.reactToKeyboard}
            />
          </View>
          <Divider dividerStyles={{backgroundColor: dividerColor}}/>
          {view}
        </View>
    );


    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent} onPress={this.dismissOrderPopup}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - 300 - offSet, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.orderPopup = ele}
          verticalOnly={true}
          style={{backgroundColor: 'white'}}
          snapPoints={[{y: Screen.height - 300 - offSet}, {y: Screen.height}, {y: Screen.height - 300 - kbHeight}]}
          onSnap={this.handelSnap}
          dragEnabled={false}
          boundaries={{top: Screen.height - 300 - kbHeight - 15 - offSet}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          animatedValueY={this.deltaY}>
          {popup}
        </Interactable.View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    padding: 32,
    minHeight: 300,
    backgroundColor: 'white'
  },
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  title: {
    color: 'rgb(47, 54, 63)',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8
  },
  details: {
    color: 'rgb(129, 135, 140)',
    marginBottom: 32,
  },
  amount: {
    fontSize: 20,
    color: 'rgb(47, 54, 63)',
    marginBottom: 8,
  },
  amountInput: {
    fontSize: 20,
    marginBottom: 8,
    flex: 1,

  },
  amountRow: {
    flexDirection: 'row',
    justifyContent: 'flex-start',

  },
  button: {
    backgroundColor: 'rgb(31, 122, 224)',
    height: 48,
    marginTop: 32
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  errorContainer: {
    marginTop: 2,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: Styles.RED_FAILURE,
    padding: 16
  },
  errorDetails: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  errorImage: {
    height: 20,
    width: 20,
    marginRight: 8
  },
  errorText: {
    color: Styles.PRIMARY_TEXT_COLOR,
    fontSize: 14,
    marginTop: 2,
    marginRight: 56
  },
  fundsButton: {
    color: Styles.BLUE,
    fontSize: 14,
    marginTop: 8
  }
});

InvestMorePopup.propTypes = {
  showPopup: PropTypes.bool.isRequired,
  orderPopupClosed: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf,
  loginReducer: state.loginReducer
});

module.exports = connect(mapStateToProps)(InvestMorePopup);

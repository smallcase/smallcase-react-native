import React, {Component} from 'react';
import ErrorState from "./ErrorState";


class ErrorBoundary extends Component {
  state = {hasError: false};

  componentDidCatch() {
    this.setState({hasError: true});
  }

  render() {
    if (this.state.hasError) {
      return <ErrorState />;
    }
    return this.props.children;
  }
}
import React, {Component} from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  processColor
} from 'react-native';
import update from 'immutability-helper';
import Interactable from 'react-native-interactable';
import {LineChart} from 'react-native-charts-wrapper';
import PropTypes from 'prop-types';
import {GET_STOCK_INFO_AND_HISTORICAL, GET_STOCK_PRICE_AND_CHANGE} from '../api/ApiRoutes';
import Api from "../api/Api";
import {connect} from "react-redux";
import {getScreenSize} from "../util/Utility";
import {EM_DASH} from "../constants/Constants";
import Loader from "./Loader";

const Screen = getScreenSize();

class StockWidget extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fetchingStockInfo: true,
      data: {},
      legend: {
        enabled: false,
      },
      marker: {
        enabled: false
      },
      noData: false
    };
    this.deltaY = new Animated.Value(Screen.height);
    this.blurPointerEvent = 'box-none';
    this.stockInfo = null;
    this.priceAndChange = null;
  }

  setUpChart = () => {
    const values = this.stockInfo.historical.map((point) => {
      return {y: point.price};
    });

    this.setState(
      update(this.state, {
        data: {
          $set: {
            dataSets: [{
              label: '',
              values: values,
              config: {
                lineWidth: 3,
                drawCircles: false,
                highlightColor: processColor('#1f7ae0'),
                drawHorizontalHighlightIndicator: false,
                drawVerticalHighlightIndicator: false,
                color: processColor('#1f7ae0'),
                drawFilled: false,
                valueTextSize: 15,
                valueFormatter: "##.000",
                drawValues: false
              }
            }],
          }
        },
        xAxis: {
          $set: {
            enabled: false
          }
        },
        yAxis: {
          $set: {
            left: {
              enabled: false
            },
            right: {
              enabled: false
            }
          }
        },
        axisRight: {
          $set: {
            enabled: false,
            drawLabels: false,
            drawAxisLine: false,
            drawGridLines: false
          }
        }
      })
    );
  }

  componentWillReceiveProps = (newProps) => {
    if (newProps.showWidget) {
      this.blurPointerEvent = 'auto';
      this.setState({fetchingStockInfo: true});

      this.stockWidget.snapTo({index: 0});
      const infoParams = 'sid=' + newProps.sid;
      const priceParams = 'stocks=' + newProps.sid;
      const {jwt, csrf} = this.props;

      Api.get({route: GET_STOCK_INFO_AND_HISTORICAL, jwt, csrf, params: infoParams})
        .then((response) => {
          this.stockInfo = response.data[newProps.sid];
          this.setUpChart();
          if (this.priceAndChange) this.setState({fetchingStockInfo: false});
        }).catch((error) => {
        this.setState({noData: true});
      });

      Api.get({route: GET_STOCK_PRICE_AND_CHANGE, jwt, csrf, params: priceParams})
        .then((response) => {
          this.priceAndChange = response.data[newProps.sid];
          if (this.stockInfo) this.setState({fetchingStockInfo: false});
        }).catch((error) => this.setState({noData: true}));
    } else {
      this.dismissStockWidget();
    }
  };

  handelSnap = (event) => {
    if (event.nativeEvent.index === 1 && !this.state.fetchingStockInfo) this.dismissStockWidget();

    this.setState({stockWidgetToggled: true});
  };

  dismissStockWidget = () => {
    this.blurPointerEvent = 'box-none';
    this.stockWidget.snapTo({index: 1});
    this.props.stockWidgetClosed();
  };

  getColor = (value) => value ? (value > 0 ? '#19af55' : '#f34b57') : '#535b62';

  render = () => {
    const {fetchingStockInfo, noData} = this.state;
    let stockInfo = (
      <View style={styles.panel}>
        <Loader loaderStyles={{flex: 1}} />
      </View>
    );

    if (noData) {
      stockInfo = (
        <View style={[styles.panel, {justifyContent: 'center'}]}>
          <Text style={{alignSelf: 'center', marginTop: -64}}>No data available</Text>
        </View>
      );
    }

    if (!fetchingStockInfo) {
      stockInfo = (
        <View style={styles.panel}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.stockName} numberOfLines={1}
                    ellipsizeMode='tail'>{this.stockInfo.stock.info.name}</Text>
              <Text style={styles.marketCap}>{this.stockInfo.stock.info.ticker}</Text>
            </View>
            <Text style={[styles.mainText, {marginTop: 8, fontWeight: 'bold'}]}>
              {this.stockInfo.stock.gic.sector} → <Text style={[styles.mainText, {marginTop: 8, fontWeight: '400'}]}>
              {this.stockInfo.stock.info.sector}</Text>
            </Text>
            <View style={styles.divider}/>
            <Text style={styles.stockDescription}>{this.stockInfo.stock.info.description}</Text>
            <Text style={styles.priceTrend}>1Y Price Trend</Text>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <LineChart
                style={{flex: 1.85, height: 50, marginLeft: -4}}
                data={this.state.data}
                chartDescription={{text: ''}}
                legend={this.state.legend}
                marker={this.state.marker}
                xAxis={this.state.xAxis}
                yAxis={this.state.yAxis}
                drawGridBackground={false}
                drawBorders={false}

                touchEnabled={false}
                dragEnabled={false}
                scaleEnabled={false}
                scaleXEnabled={false}
                scaleYEnabled={false}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}

                dragDecelerationEnabled={true}
                dragDecelerationFrictionCoef={0.99}

                keepPositionOnRotation={false}
              />
              <View style={[styles.highLowContainer, {marginRight: 18}]}>
                <Text style={styles.infoTitle}>52W High</Text>
                <Text
                  style={styles.infoValue}>{this.stockInfo.stock.ratios['52wHigh'] ? this.stockInfo.stock.ratios['52wHigh'].toFixed(2) : EM_DASH}</Text>
              </View>
              <View style={styles.highLowContainer}>
                <Text style={styles.infoTitle}>52W Low</Text>
                <Text
                  style={styles.infoValue}>{this.stockInfo.stock.ratios['52wLow'] ? this.stockInfo.stock.ratios['52wLow'].toFixed(2) : EM_DASH}</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 4}}>
              <View style={[styles.highLowContainer, {flex: 1.75, alignItems: 'flex-start'}]}>
                <Text style={styles.infoTitle}>1M Returns</Text>
                <Text
                  style={[styles.infoValue, {color: this.getColor(this.stockInfo.stock.ratios['4wpct'])}]}>{this.stockInfo.stock.ratios['4wpct'] ? this.stockInfo.stock.ratios['4wpct'].toFixed(2) : EM_DASH}%</Text>
              </View>
              <View style={[styles.highLowContainer, {flex: 1.75, alignItems: 'flex-start'}]}>
                <Text style={styles.infoTitle}>1Y Returns</Text>
                <Text
                  style={[styles.infoValue, {color: this.getColor(this.stockInfo.stock.ratios['52wpct'])}]}>{this.stockInfo.stock.ratios['52wpct'] ? this.stockInfo.stock.ratios['52wpct'].toFixed(2) : EM_DASH}%</Text>
              </View>
              <View style={styles.highLowContainer}>
                <Text style={styles.infoTitle}>PE</Text>
                <Text
                  style={styles.infoValue}>{this.stockInfo.stock.ratios.pe ? this.stockInfo.stock.ratios.pe.toFixed(2) : EM_DASH}</Text>
              </View>
              <View style={styles.highLowContainer}>
                <Text style={styles.infoTitle}>Beta</Text>
                <Text
                  style={styles.infoValue}>{this.stockInfo.stock.ratios.beta ? this.stockInfo.stock.ratios.beta.toFixed(2) : EM_DASH}</Text>
              </View>
              <View style={styles.highLowContainer}>
                <Text style={styles.infoTitle}>Div Yld</Text>
                <Text
                  style={styles.infoValue}>{this.stockInfo.stock.ratios.divYield ? this.stockInfo.stock.ratios.divYield.toFixed(2) : EM_DASH}</Text>
              </View>
            </View>
          </View>
        </View>
      );
    }

    return (
      <View style={styles.panelContainer} pointerEvents={'box-none'}>
        <TouchableWithoutFeedback pointerEvents={this.blurPointerEvent} onPress={this.dismissStockWidget.bind(this)}>
          <Animated.View
            pointerEvents={this.blurPointerEvent}
            style={[styles.panelContainer, {
              backgroundColor: 'black',
              opacity: this.deltaY.interpolate({
                inputRange: [Screen.height - 390, Screen.height],
                outputRange: [0.5, 0],
                extrapolateRight: 'clamp'
              })
            }]}/>
        </TouchableWithoutFeedback>
        <Interactable.View
          ref={ele => this.stockWidget = ele}
          verticalOnly={true}
          snapPoints={[{y: Screen.height - 390}, {y: Screen.height}]}
          onSnap={this.handelSnap}
          boundaries={{top: Screen.height - 410}}
          initialPosition={{y: Screen.height}}
          animatedNativeDriver={true}
          animatedValueY={this.deltaY}>
          {stockInfo}
        </Interactable.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  panel: {
    height: 410,
    shadowColor: '#000000',
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 5,
    shadowOpacity: 0.4,
    padding: 16,
    backgroundColor: 'white'
  },
  stockName: {
    fontSize: 16,
    fontWeight: 'bold',
    letterSpacing: -0.2,
    maxWidth: 250
  },
  mainText: {
    fontSize: 12,
    color: '#535b62',
  },
  stockDescription: {
    fontSize: 12,
    color: '#535b62',
    lineHeight: 12 * 1.6,
    marginTop: 12
  },
  divider: {
    height: 1,
    backgroundColor: '#dddddd',
    marginTop: 16,
    marginHorizontal: -16
  },
  marketCap: {
    fontSize: 12,
    color: '#81878c',
    marginHorizontal: 8,
    marginTop: 4,
    fontWeight: 'bold'
  },
  priceTrend: {
    fontSize: 10,
    color: '#81878c',
    marginTop: 24
  },
  highLowContainer: {
    flex: 1,
    alignItems: 'flex-end'
  },
  infoTitle: {
    fontSize: 10,
    color: '#81878c'
  },
  infoValue: {
    fontSize: 12,
    color: '#2f363f',
    fontWeight: 'bold',
    marginTop: 8
  }
});

StockWidget.propTypes = {
  showWidget: PropTypes.bool.isRequired,
  stockWidgetClosed: PropTypes.func.isRequired,
  jwt: PropTypes.string.isRequired,
  csrf: PropTypes.string.isRequired,
  sid: PropTypes.string
};


const mapStateToProps = (state) => ({
  jwt: state.landingReducer.jwt,
  csrf: state.landingReducer.csrf
});

module.exports = connect(mapStateToProps)(StockWidget);
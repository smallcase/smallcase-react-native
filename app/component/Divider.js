import React, {PureComponent} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

export default class Divider extends PureComponent {

  render() {
    return (
        <View style={[styles.divider, this.props.dividerStyles]}/>
    )
  }
}

const styles = StyleSheet.create({
  divider: {
    height: 1,
    transform: [{scaleY :0.5}],
    backgroundColor: 'rgb(221, 224, 228)'
  }
});
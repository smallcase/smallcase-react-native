import React from "react";
import {Text} from "react-native";
import {getFormattedCurrency, Styles} from "../util/Utility";
import {ReturnsComponentValueType} from "../constants/Constants";

const getPAndLColor = (number) => {
  if (number === 0) return Styles.PRIMARY_TEXT_COLOR;

  return number > 0 ? Styles.GREEN : Styles.RED_FAILURE;
}

const getColor = (type, number) => {
  switch (type) {
    case ReturnsComponentValueType.NORMAL:
      return Styles.SECONDARY_TEXT_COLOR;

    case ReturnsComponentValueType.TITLE:
    case ReturnsComponentValueType.UNCOLORED_AMOUNT:
      return Styles.PRIMARY_TEXT_COLOR;

    case ReturnsComponentValueType.AMOUNT:
    case ReturnsComponentValueType.PERCENT:
      return getPAndLColor(number);

    default:
      return Styles.SECONDARY_TEXT_COLOR;
  }
}

export default ReturnsNumber = (props) => {
  let content;
  switch (props.type) {
    case ReturnsComponentValueType.PERCENT:
      content = Math.abs(props.value) + '%';
      break;

    case ReturnsComponentValueType.AMOUNT:
    case ReturnsComponentValueType.UNCOLORED_AMOUNT:
      content = getFormattedCurrency(props.value);
      break;

    case ReturnsComponentValueType.TITLE:
    case ReturnsComponentValueType.NORMAL:
    default:
      content = props.value;
      break;
  }
  return (
    <Text style={[{
      color: getColor(props.type, props.value),
      fontWeight: ReturnsComponentValueType.NORMAL? 'normal' : 'bold'
    }, props.textStyle]}>
      {content}
    </Text>
  );
}
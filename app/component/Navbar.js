import React, {PureComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {getBottomBarPaddingBottom, getBottomBarPaddingTop, Styles} from "../util/Utility";
import Divider from "./Divider";

export default class Navbar extends PureComponent {

  render() {

    let {title, right, showLeft, rightfunc, leftfunc} = this.props;

    let leftView = (
      <TouchableOpacity style={styles.leftContainer} onPress={leftfunc}>
        <Text style={styles.cancel}>Cancel</Text>
      </TouchableOpacity>
    );

    let view = showLeft ? leftView : null;

    return (
      <View style={{paddingTop: getBottomBarPaddingTop(), backgroundColor: 'white'}}>
        <View style={styles.statusBar}/>
        <View style={styles.container}>
          <View style={styles.titleContainer}><Text style={styles.title}>{title}</Text></View>
          {view}
          <Text style={styles.right} onPress={rightfunc}>{right}</Text>
        </View>
        <Divider dividerStyles={{backgroundColor: Styles.SILVER}}/>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  statusBar: {
    height: 20,
  },
  container: {
    height: 44,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  leftIcon: {
    height: 24,
    width: 24,
    marginTop: 8,
    marginLeft: 16,
  },
  left: {
    color: 'rgb(31, 122, 224)',
    fontSize: 16,
    marginLeft: 8,
    marginTop: 8,
  },
  titleContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  leftContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    color: 'rgb(3,3,3)',
    fontWeight: 'bold',
    fontSize: 17,
  },
  right: {
    color: 'rgb(31, 122, 224)',
    fontSize: 16,
    marginRight: 16,
    marginTop: 8,
    letterSpacing: -0.4,
    width: 24
  },
  cancel: {
    marginHorizontal: 16,
    justifyContent: 'center',
    fontSize: 16,
    color: Styles.BLUE
  }
});
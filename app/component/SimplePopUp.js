import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import SmallcaseButton from "./SmallcaseButton";
import {Styles} from "../util/Utility";

export const SimplePopUp = (props) => (
  <View style={styles.container}>
    <Text style={styles.title}>{props.title}</Text>
    <Text style={styles.description}>{props.description}</Text>
    <View style={styles.buttonRow}>
      <SmallcaseButton buttonStyles={styles.secondaryActionButton} onClick={props.secondaryAction}>
        <Text style={styles.secondaryActionText}>{props.secondaryActionText}</Text>
      </SmallcaseButton>
      <SmallcaseButton
        disabled={props.loadingPrimaryAction}
        buttonStyles={[styles.primaryActionButton, {backgroundColor: props.isDestructive? Styles.RED_FAILURE : Styles.BLUE}]}
        onClick={props.primaryAction}>
        <Text style={styles.primaryActionText}>{props.primaryActionText}</Text>
      </SmallcaseButton>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container : {
    height : 300,
    backgroundColor : 'white',
    paddingVertical: 32,
    paddingHorizontal: 24,
  },
  title : {
    color : Styles.PRIMARY_TEXT_COLOR,
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 22,
  },
  description : {
    color : Styles.DESCRIPTION_TEXT_COLOR,
    fontSize: 14,
    lineHeight : 21,
    marginBottom : 24,
    marginTop: 8
  },
  primaryActionText : {
    color : 'white',
    fontSize : 14,
    fontWeight: 'bold',
  },
  secondaryActionText: {
    color: Styles.BLUE,
    fontSize: 14,
    fontWeight: 'bold'
  },
  buttonRow : {
    flexDirection: 'row',
    justifyContent : 'space-between',
    marginBottom : 24,
  },
  secondaryActionButton : {
    height : 48,
    width : 156,
    backgroundColor : 'white',
    borderRadius : 4,
    borderWidth: 0.7,
    borderColor : Styles.BLUE
  },
  primaryActionButton : {
    height : 48,
    width : 156,
    backgroundColor : Styles.BLUE,
    borderRadius : 4,
  }
});

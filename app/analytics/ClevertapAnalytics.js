const CleverTap = require('clevertap-react-native');

export default class ClevertapAnalytics {

  constructor() {
    console.log(CleverTap);

  }

  track = (eventName,props) => {
    CleverTap.recordEvent(eventName,props);
  }

  onUserLogin = (profile) => {
    CleverTap.onUserLogin(profile);
  }
}
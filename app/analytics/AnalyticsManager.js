import MixpanelAnalytics from "./MixpanelAnalytics";
import {AnalyticsSource} from "../util/Utility";
import ClevertapAnalytics from "./ClevertapAnalytics";

const mixpanel = new MixpanelAnalytics();
const clevertap = new ClevertapAnalytics();

export default class AnalyticsManager {

  static track(event, properties, sources) {
    sources.forEach((source) => {
      switch (source) {
        case AnalyticsSource.MIXPANEL:
          if (properties) {
            mixpanel.trackWithProperties(event, properties);
          } else {
            mixpanel.track(event);
          }
          break;

        case AnalyticsSource.CLEVER_TAP:
          clevertap.track(event,properties);
          break;

        case AnalyticsSource.INTERCOM:
          break;
      }
    });
  }

  static  loginUserOnMixPanel(user) {
    mixpanel.identifyUser(user.user_id);
    mixpanel.setUserDetails(user);

  }

  static loginCleverTapUser(profile){
    clevertap.onUserLogin(profile);
  }
}
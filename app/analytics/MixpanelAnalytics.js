import * as Config from "../../Config";

const Mixpanel = require('react-native-mixpanel');

export default class MixpanelAnalytics {
  constructor() {

    this.mixpanel = cb => Mixpanel.default.sharedInstanceWithToken(Config.MIXPANEL_TOKEN)
      .then(() => cb())
      .catch((error) => console.log(error));
  }

  track = (event) => {
    this.mixpanel(() => Mixpanel.default.track(event));
  }

  trackWithProperties = (event, properties) => {
    this.mixpanel(() => Mixpanel.default.trackWithProperties(event, properties));
  }

  identifyUser = (brokerId) => {

    this.mixpanel( () => Mixpanel.default.identify(brokerId));
  }

  setUserDetails = (user) => {

    this.mixpanel( () => Mixpanel.default.set(user));
  }
}
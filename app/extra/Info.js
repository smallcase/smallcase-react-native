import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  ScrollView,
  View
} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class Info extends Component {

  constructor(props) {
    super();
    switch (props.content) {
      case 'terms':
        Actions.refresh({title: 'How It Works'});
        break;

      case 'privacy':
        Actions.refresh({title: 'Privacy'});
        break;

      case 'disclosures':
        Actions.refresh({title: 'Disclosures'});
        break;
    }
  }

  render() {
    switch (this.props.content) {
      case 'terms':
        return (<Terms />);

      case 'privacy':
        return (<Privacy />);

      case 'disclosures':
        return (<Disclosures />);

      default:
        return (<View />);
    }
  }
}

class Terms extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.mainTitle}>How It Works! </Text>
        <Text style={styles.subTitle}>Introduction</Text>
        <Text style={styles.description}>It's easy to buy & manage smallcases. All you have to do is find your favourite smallcases and click buy. The smallcase platform helps you place orders for each of them, track each smallcase as a separate portfolio & assist you with rebalancing your smallcase in two clicks!</Text>
        <Text style={styles.subTitle}>Choose smallcase</Text>
        <Text style={styles.description}>Choose from the line-up of 60+ professionally created smallcases from the Discover section. Understand the rationale behind the theme, view the constituents & weights and check past performance. </Text>
        <Text style={styles.subTitle}>Buy smallcase</Text>
        <Text style={styles.description}>Once you have chosen a smallcase, confirm the amount you want to invest and place orders in a few clicks.</Text>
        <Text style={styles.subTitle}>Track smallcase</Text>
        <Text style={styles.description}>Track your smallcases using the custom index value. A smallcase's index starts from 100, at its inception (when you buy it) and moves up & down based on the smallcase's performance </Text>
        <Text style={styles.subTitle}>Manage smallcase</Text>
        <Text style={styles.description}>Receive and apply rebalance updates each quarter to ensure your smallcase stays updated & continues to reflect the underlying theme </Text>
      </ScrollView>
    );
  }
}

class Privacy extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <Text style={styles.mainTitle}>Privacy Policy</Text>
        <Text style={styles.purpleTitle}>Privacy matters. Privacy is what allows us to determine who we are and what we do - Edward Snowden</Text>
        <Text style={styles.description}>smallcase.com is a product by smallcase Technologies Pvt. Ltd. (called smallcase from here onwards).</Text>
        <Text style={styles.subTitle}>User Consent</Text>
        <Text style={styles.description}>By submitting Personal Information through our Site or Services, you agree to the terms of this Privacy Policy and you expressly consent to the collection, use and disclosure of the Personal Information in accordance with this Privacy Policy. </Text>
        <Text style={styles.subTitle}>Information you provide to us</Text>
        <Text style={styles.description}> • If you provide us feedback or contact us via e-mail, we will collect your name and e-mail address, as well as any other content included in the e-mail, in order to send you a reply.{'\n'}
          • When you participate in one of our surveys, we may collect additional information.{'\n'}
          • We may also collect Personal Information on the Site or through the Services when we make clear that are we are collecting it and you voluntarily provide it, for example when you submit an application for employment.{'\n'}
          • We may collect Personal Information from you, such as your first and last name, phone, e-mail address, age when you create an Account. </Text>
        <Text style={styles.subTitle}>Information collected via technology</Text>
        <Text style={styles.description}>• To make our Site and Services more useful to you, our servers (which may be hosted by a third party service provider) collect information from you, including your browser type, operating system, Internet Protocol (IP) address (a number that is automatically assigned to your computer when you use the Internet, which may vary from session to session), and domain name.{'\n'}
          • We may use third party service providers to help us analyze certain online activities. For example, these service providers may help us analyze visitor activity on the Site. We may permit these service providers to use cookies and other technologies to perform these services for us.{'\n'}
          • If you are logged into the Site or Services, the information collected via technology as described in this Section may be associated with your Personal Information and will, accordingly, be treated as Personal Information in accordance with this Privacy Policy. </Text>
        <Text style={styles.subTitle}>Use of personal information</Text>
        <Text style={styles.description}>In general, Personal Information you submit to us regarding you or your company is used either to respond to requests that you make, or to aid us in serving you better. We use such Personal Information in the following ways:{'\n'}
          • to facilitate the creation of and secure your Account on our network;{'\n'}
          • to identify you as a user in our system;{'\n'}
          • to provide improved administration of our Site and Services;{'\n'}
          • to provide the Services you request;{'\n'}
          • to improve the quality of experience when you interact with our Site and Services;{'\n'}
          • to send you transactional e-mail notifications{'\n'}
          • to send newsletters, surveys, offers, and other promotional materials related to our Services and for other marketing purposes; </Text>
        <Text style={styles.subTitle}>Creation of Anonymous Information</Text>
        <Text style={styles.description}>We may create Anonymous Information records from Personal Information by excluding information (such as the name) that makes the information personally identifiable to the data subject. We use this Anonymous Information to analyze request and usage patterns so that we may enhance the content of our Services and improve Site navigation. smallcase reserves the right to use Anonymous Information for any purpose and disclose Anonymous Information to third parties at its sole discretion. </Text>
        <Text style={styles.subTitle}>Third Party Websites</Text>
        <Text style={styles.description}>When you click on a link to any other website or location, you will leave our Site and go to another site and another entity may collect Personal Information or Anonymous Information from you. We have no control over, do not review, and cannot be responsible for, these outside websites or their content. Please be aware that the terms of this Privacy Policy do not apply to these outside websites or content, or to any collection of information after you click on links to such outside websites. </Text>
        <Text style={styles.subTitle}>Cookies</Text>
        <Text style={styles.description}>Cookies" are pieces of information that a browser can record after visiting a web site. We may use cookies for technical purposes such as to enable better navigation through our site, or to store user preferences for interacting with the site. If you turn off the option to have cookies stored on your browser, it will affect your experience of using the smallcase platform. </Text>
        <Text style={styles.subTitle}>Security of your information</Text>
        <Text style={styles.description}>smallcase is committed to protecting the security of your Personal Information. We use a variety of industry-standard security technologies and procedures to help protect your Personal Information from unauthorized access, use, or disclosure. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while smallcase uses reasonable efforts to protect your Personal Information, smallcase cannot guarantee its absolute security. </Text>
        <Text style={styles.subTitle}>Contact Information</Text>
        <Text style={styles.description}>smallcase welcomes your comments or questions regarding this Privacy Policy. Please e-mail us at support@smallcase.com or contact us at the following address:
          {'\n'}Smallcase Technologies Private Limited{'\n'}
          Regus-EGL, 1st Floor, Pine Valley Building{'\n'}
          Embassy Golf Links, Domlur{'\n'}
          Bangalore - 560008{'\n'}
          Karnataka, India{'\n'} </Text>
        <Text style={styles.subTitle}>Changes to this Privacy policy</Text>
        <Text style={styles.description}>Please note that this Privacy Policy may change from time to time. We will post any Privacy Policy changes on this page</Text>
        <Text style={styles.subTitle}>Last updated on 18th May 2017</Text>
      </ScrollView>
    );
  }
}

class Disclosures extends Component {
  render() {
    return(
      <ScrollView style={styles.container}>
        <Text style={styles.mainTitle}>Disclosures</Text>
        <Text style={styles.subTitle}>Investing involves risks and investments may lose value{'\n'}
          Past performance does not guarantee future returns and performance of smallcases are subject to market risks.</Text>
        <Text style={styles.purpleTitle}>How we calculate returns</Text>
        <Text style={styles.subTitle}>Methodology</Text>
        <Text style={styles.description}>smallcase performance does not include transaction fees and other related costs. No actual money was invested or trades were executed while calculating smallcase performances. Returns are based on end of day prices of stocks in a smallcase, provided by exchange approved third party vendors. All smallcases are reviewed and rebalanced as per a schedule depending on the smallcase. As a result of this review process, some stocks may be added, some may be removed and some may undergo weight changes. smallcase returns reflect all these changes
          {'\n'}
          We assign an index value of 100 at the smallcase inception date. This means that we assume a hypothetical investment of INR 100 in the smallcase at the inception date. Individual stock investments are calculated as per their weights in the smallcase. Thus, a stock having a weight of 10% in the smallcase will receive a hypothetical investment of INR 10 ( 10% * INR 100) on the inception date. After calculating individual stock investments, we calculate the individual number of shares that could be bought using hypothetical investments determined in the last step. If the same stock which received a hypothetical investment of INR 10 has an end of day stock price of INR 2 on the inception date, it will have 5 shares in the smallcase (INR 10/ INR 2). In this way we determine the individual number of shares of each stock in the smallcase, on the inception date. Multiplication of individual number of shares of different stocks in the smallcase with respective end of day share prices on the inception date, results in a total hypothetical investment of INR 100 (index value at the inception date)
          {'\n'}
          Once the individual number of shares are decided for a stock in the smallcase, it remains constant unless changed in a review/rebalance process. Every day index value is calculated by multiplying no of shares of each stock with respective end of day prices of stocks on that particular day. If the index value of the smallcase after 30 days is 110, it means that your hypothetical investment of INR 100 is worth INR 110 now. Thus, smallcase is worth INR 10 more compared to the investment date and has generated a return on 10% (INR 10/ INR 100). smallcase returns for any time period (week/ quarter/ annual) is determined in the same manner by comparing beginning and end of period index values </Text>
        <Text style={styles.subTitle}>Historical Time Periods including pre-inception</Text>
        <Text style={styles.description}> Assuming a hypothetical investment of INR 100 at the beginning of the period under focus, we calculate the individual stock investments and number of shares in the same manner as described earlier. This is done, assuming the same weighting scheme as it was on the inception date. Returns are calculated in the similar fashion using index values. For a time period extending before the inception date, changes resulting from review/rebalance process are incorporated only for the period after inception date </Text>
        <Text style={styles.subTitle}>General Investment Disclosure</Text>
        <Text style={styles.description}>The content and data available on the smallcase platform, including but not limited to smallcase index value, smallcase return numbers and smallcase rationale are for information and illustration purposes only. Charts and performance numbers are backtested/simulated results calculated via a standard methodology and do not include the impact of transaction fee and other related costs. Data used for calculation of historical returns and other information is provided by exchange approved third party data vendors, and has neither been audited nor validated by smallcase
          {'\n'}
          All information present on smallcase platform is to help investors in their decision making process and shall not be considered as a recommendation or solicitation of an investment or investment strategy. Investors are responsible for their investment decisions and are responsible to validate all the information used to make the investment decision. Investor should understand that his/her investment decision is based on personal investment needs and risk tolerance, and performance information available on smallcase platform is one among many other things that should be considered while making an investment decision. Past performance does not guarantee future returns and performance of smallcases are subject to market risk </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainTitle: {
    fontSize: 26,
    color: 'rgb(47, 54, 63)'
  },
  subTitle: {
    fontSize: 16,
    color: 'rgb(47, 54, 63)',
    fontWeight: 'bold',
    marginTop: 16
  },
  purpleTitle: {
    fontSize: 18,
    color: '#8f74f3',
    marginTop: 16
  },
  description: {
    color: 'rgb(47, 54, 63)',
    marginTop: 8
  },
  container: {
    flex: 1,
    paddingLeft: 36,
    paddingRight: 36,
    paddingTop: 36,
    backgroundColor: 'white',
    paddingBottom: 96
  }
});
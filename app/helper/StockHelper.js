// This method assumes that shares and price of stocks are already presents and gives an array
// with weightage of each stock added.
import {GET_STOCK_PRICE} from "../api/ApiRoutes";
import Api from "../api/Api";

export const calculateWeightageFromShares = (stocks) => {
  let totalAmount = 0;
  const stocksWithInvestmentAmount = stocks.map((stock) => {
    stock.investmentAmount = stock.shares < 0 ? 0 : stock.shares * stock.price;
    totalAmount += stock.investmentAmount;
    return stock;
  });

  return stocksWithInvestmentAmount.map((stock) => {
    if (stock.investmentAmount <= 0) stock.weight = 0;
    else stock.weight = stock.investmentAmount / totalAmount;
    return stock;
  });
};

const calculateMaxPByW = (stocks) => {

  let maxPByW = 0;
  stocks.forEach((stock) => {
    let pByW = (stock.price <= 0 || stock.weight <= 0) ? 0 : (stock.price / stock.weight);
    stock.pByW = pByW;
    if (maxPByW < pByW) maxPByW = pByW;
  });

  return maxPByW;

};

export const getPrices = (sids, jwt, csrf) => {
  let params = '';

  for (let i = 0; i < sids.length; i++) {
    let sid = sids[i];
    params += 'stocks=' + sid + (i === sids.length - 1 ? '' : '&');
  }

  return new Promise((resolve, reject) => {
    Api.get({route: GET_STOCK_PRICE, jwt, csrf, params})
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
}

export const calculateSharesAndMinAmountFromWeight = (stocks, maximumPByW) => {

  let stockListWithShares = [], minAmount = 0;
  let maxPByW = (maximumPByW === -1) ? calculateMaxPByW(stocks) : maximumPByW;

  stocks.forEach((stock) => {
    let stockWithShares = stock;

    stockWithShares.shares = (stockWithShares.pByW === 0) ? 0 : Math.round(maxPByW / stockWithShares.pByW);
    stockWithShares.investmentAmount = stockWithShares.price * stockWithShares.shares;
    minAmount += stockWithShares.investmentAmount;
    stockListWithShares.push(stockWithShares);
  });

  return ({
    stockListWithShares: stockListWithShares,
    minAmount: minAmount,
    maxPByW: maxPByW,
  })
};

const addDataPoint = (date, price, datesMap, shares, pointList, i) => {
  let isoDate = new Date(date).toISOString();
  const amount = price * shares;
  if (datesMap.hasOwnProperty(isoDate)) {
    datesMap[isoDate] += amount;
    pointList[i].index = datesMap[isoDate];
  } else {
    datesMap[isoDate] = amount;
    pointList.push({index: amount, date});
  }
};

/**
 * Creates a normalized list which has the same number of data points as niftyHistorical
 * @param historicalList historical list of stocks
 * @param shares object with {sid: {shares: Number}}
 * @param niftyHistorical list of nifty historical points, which is an array of {x: Time stamp, y: Index}
 * @returns {Array} {index: Number, date: Time stamp}
 */
export const createHistoricalForStocks = (historicalList, shares, niftyHistorical) => {
  if (!historicalList.length) return [];

  const expectedSize = niftyHistorical.length;

  if (expectedSize === 0) return [];

  const dates = niftyHistorical.map((historical) => historical.x);

  let pointsList = [];
  let datesMap = {};
  // Calculate price x shares for all points
  historicalList.forEach((stock) => {
    let i = 0, j = 0;
    let historical = stock.point;
    while (i < dates.length) {
      let price = 0;
      let date = dates[i];
      if (historical[j]) {
        if (historical[j].date === date) {
          addDataPoint(date, historical[j].price, datesMap, shares[stock._id].shares, pointsList, i);
          i++;
          j++;
        } else if (historical[j].date < date) {
          j++;
        } else {
          if (historical[j - 1])
            price = historical[j - 1].price;
          else
            price = historical[j].price;
          addDataPoint(date, price, datesMap, shares[stock._id].shares, pointsList, i);
          i++;
        }
      } else {
        price = j - 1 > historical.length ? historical[historical.length - 1].price : historical[j - 1].price;
        addDataPoint(date, price, datesMap, shares[stock._id].shares, pointsList, i);
        i++;
        j++;
      }
    }
  });

  return pointsList;
};

import {GET_REQUEST_TOKEN, LOGIN_SMALLCASE} from "../api/ApiRoutes";
import Api from "../api/Api";
import * as Config from "../../Config";
import AnalyticsManager from "../analytics/AnalyticsManager";
import {AnalyticsSource} from "../util/Utility";

const getRequestToken = (tradingJwt, cb) => {
  const params = 'app=platform';
  Api.get({route: GET_REQUEST_TOKEN, baseUrl: Config.AUTH_BASE_URL, jwt: tradingJwt, params})
    .then((response) => loginToSmallcase(response.data.reqToken, cb))
    .catch((error) => cb(null));
};

const loginToSmallcase = (reqToken, cb) => {
  const body = {reqToken}

  Api.post({route: LOGIN_SMALLCASE, body})
    .then((response) => {
      AnalyticsManager.track('Logged In', null, [AnalyticsSource.CLEVER_TAP, AnalyticsSource.MIXPANEL]);
      cb(response.data.jwt, response.data['x-csrf-token']);
    })
    .catch((error) => {
      cb(null);
    });
};

export const getJwt = (route, body, baseUrl, cb) => {
  Api.post({route, body, baseUrl})
    .then((response) => getRequestToken(response.data.jwt_trd, cb))
    .catch((error) => cb(null));
}



import {areNumbersEqual, OrderType} from "../util/Utility";
import {sipFrequency} from "../investments/sip/SipService";

const getPByW = (weight, price) => (price && weight) ? price / weight : 0;

const calculateMaxPByW = (stocks) => {
  let maxPByW = 0;
  stocks.forEach((stock) => {
    const pByW = getPByW(stock.weight, stock.price);
    if (maxPByW < pByW) maxPByW = pByW;
  });
  return maxPByW;
};

/**
 * Calculates minInvestment amount given from a set of stocks
 * @param stocks should contain an array of stock objects, with mandatory weight and price
 * @param maximumPByW null if this needs to be calculated
 * @returns {{stocks: *, minAmount: number}} returns updated stocks array with an addition of shares to each stock object along with minAmount
 */
export const calculateMinInvestment = (stocks, maximumPByW) => {
  const maxPByW = maximumPByW ? maximumPByW : calculateMaxPByW(stocks);
  let minAmount = 0;
  let updatedStocks = stocks.map((stock) => {
    let updatedStock = Object.assign({}, stock);
    const pByW = getPByW(updatedStock.weight, updatedStock.price);
    updatedStock.shares = pByW ? Math.round(maxPByW / pByW) : 0;
    minAmount += updatedStock.price * updatedStock.shares;
    return updatedStock;
  });
  return {stocks: updatedStocks, minAmount};
};

/**
 * Calculates closest investable amount from a given set of stocks
 * @param stocks should contain an array of stock objects, with mandatory weight and price
 * @param desiredAmount amount entered by the user (should be greater than minAmount)
 * @returns {{stocks: *, closestInvestableAmount: number}} returns updated stocks array with an addition of shares to each stock object along with closestInvestableAMount
 */
export const calculateClosestInvestableAmount = (stocks, desiredAmount) => {
  let closestInvestableAmount = 0;
  let updatedStocks = stocks.map((stock) => {
    let updatedStock = Object.assign({}, stock);
    updatedStock.shares = Math.round((updatedStock.weight * desiredAmount) / updatedStock.price);
    closestInvestableAmount += (updatedStock.shares * updatedStock.price);
    return updatedStock;
  });
  return {stocks: updatedStocks, closestInvestableAmount};
};

export const calculatePartialExitAmount = (stocks, maxExitAmount, exitAmountEntered) => {
  stocks.sort((a, b) => a.price - b.price);

  let desiredAmountToExit, amountRemaining;
  let newStocks;
  let netWorth = 0;
  stocks.forEach((stock) => netWorth += (stock.price * stock.shares));
  if (areNumbersEqual(maxExitAmount, exitAmountEntered, 1)) {
    desiredAmountToExit = calculateMaxPByW(stocks);
    const result = calculateMinInvestment(stocks, desiredAmountToExit);
    amountRemaining = result.minAmount;
    newStocks = result.stocks;
  } else {
    desiredAmountToExit = netWorth - exitAmountEntered;
    const closestInvestableObj = calculateClosestInvestableAmount(stocks, desiredAmountToExit);
    amountRemaining = closestInvestableObj.closestInvestableAmount;
    newStocks = closestInvestableObj.stocks;
  }
  let calculatedExitAmount = netWorth - amountRemaining;

  const exitRangeMaximum = exitAmountEntered * 1.05;
  const exitRangeMinimum = exitAmountEntered * 0.95;

  const listSize = newStocks.length;
  if (calculatedExitAmount < exitRangeMinimum) {
    for (let i = 0; i < listSize; i++) {
      let stock = newStocks[i];
      let newAmount = calculatedExitAmount + stock.price;
      if (newAmount > exitRangeMinimum && newAmount < exitRangeMaximum) {
        // If amount is in the min, max exit range then break
        stock.shares--;
        break;
      } else if (exitRangeMinimum > newAmount) {
        // Need to add more stocks
        stock.shares--;
        calculatedExitAmount = newAmount;
      } else if (newAmount > exitRangeMaximum) {
        if (i === 0) break;
        // Start from beginning as this stock exceeds max range
        i = -1;
      } else {
        // Should not come here, but meh!
        break;
      }
    }
  } else if (calculatedExitAmount > exitRangeMaximum) {
    for (let i = 0; i < listSize; i++) {
      let stock = newStocks[i];
      let newAmount = calculatedExitAmount - stock.price;
      if (newAmount > exitRangeMinimum && newAmount < exitRangeMaximum) {
        // If amount is in the min, max exit range then break
        stock.shares++;
        break;
      } else if (newAmount > exitRangeMaximum) {
        // Need to remove more stocks
        stock.shares++;
        calculatedExitAmount = newAmount;
      } else if (exitRangeMinimum > newAmount) {
        if (i === 0) break;
        // Start from beginning as this stock falls below min range
        i = -1;
      } else {
        // Should not come here, but meh! ( Yeah I copy past comments as well 8-) )
        break;
      }
    }
  }

  let finalAmount = 0;
  let stocksToSell = [];
  try {
    for (let i = 0; i < newStocks.length; i++) {
      let modifiedStock = newStocks[i];
      let existingStock = stocks[i];
      let shares = existingStock.shares - modifiedStock.shares;
      if (shares !== 0) {
        finalAmount += (modifiedStock.price * shares);
        modifiedStock.shares = shares;
        modifiedStock.transactionType = OrderType.SELL;
        stocksToSell.push(modifiedStock);
      }
    }
  } catch (e) {
  }

  return {finalAmount, stocksToSell};
};

const getPreviousSipEntryDate = (date, index, frequency) => {
  let checkableDate = new Date(date);
  switch (frequency) {
    case sipFrequency.WEEKLY.display:
      return Date.parse(checkableDate.addDays(-7 * index));
    case sipFrequency.FORTNIGHTLY.display:
      return Date.parse(checkableDate.addDays(-14 * index));
    case sipFrequency.MONTHLY.display:
      return Date.parse(checkableDate.addMonths(-1 * index));
    case sipFrequency.QUARTERLY.display:
      return Date.parse(checkableDate.addMonths(-3 * index));
  }
};

export const getHistoricalInIndexFormat = (historicalList) => {
  let baseIndex = 100 / (historicalList[0].index || historicalList[0].price);
  return historicalList.map((point) => {
    const y = parseFloat(((point.index || point.price) * baseIndex).toFixed(2));
    const x = typeof point.date === 'string' ? Date.parse(point.date) : point.date;
    return {x, y};
  });
};

export const getSipFromHistorical = ({smallcaseHistorical, niftyHistorical, frequency, selectedDate, minInvestmentAmount}) => {
  smallcaseHistorical.map((point, id) => {
      if (id) point.returns = (point.y - smallcaseHistorical[id - 1].y) / smallcaseHistorical[id - 1].y;
    }
  );

  if (niftyHistorical) {
    niftyHistorical.map((point, id) => {
        if (id) point.returns = (point.y - niftyHistorical[id - 1].y) / niftyHistorical[id - 1].y;
      }
    );
  }

  let sipEntryDates = [];
  let i = smallcaseHistorical.length - 1;
  let index = 1;
  while (i > -1) {
    if (getPreviousSipEntryDate(selectedDate, index, frequency) >= smallcaseHistorical[i].x) {
      index++;
      sipEntryDates.push(smallcaseHistorical[(i === smallcaseHistorical.length - 1) ? i : i + 1].x);
    }
    i--;
  }

  const lastSipDate = sipEntryDates[sipEntryDates.length - 1];
  let userInvestmentAmount = 0, smallcaseAmount = 0, niftyAmount = 0;
  let userSipHistorical = [], smallcaseSipHistorical = [], niftySipHistorical = [];

  smallcaseHistorical.map((point, id) => {
    if (point.x >= lastSipDate) {
      if (sipEntryDates.indexOf(point.x) > -1) {
        userInvestmentAmount += minInvestmentAmount;
        smallcaseAmount += minInvestmentAmount;
        niftyAmount += minInvestmentAmount;
      }

      userSipHistorical.push({x: point.x, y: userInvestmentAmount});
      smallcaseSipHistorical.push({x: point.x, y: smallcaseAmount});
      smallcaseAmount = smallcaseAmount * (1 + (point.returns || 0));

      if (niftyHistorical) {
        niftySipHistorical.push({x: point.x, y: niftyAmount});
        niftyAmount = niftyAmount * (1 + (niftyHistorical[id].returns || 0));
      }
    }
  });
  // Update last smallcase amount
  smallcaseSipHistorical[smallcaseSipHistorical.length - 1].y = smallcaseAmount;

  return {
    userInvestmentAmount,
    smallcaseAmount,
    niftyAmount,
    userSipHistorical,
    smallcaseSipHistorical,
    niftySipHistorical
  };
}

export const isSipDue = (sips, iscid) => {
  for (let i = 0; i < sips.length; i++) {
    if (sips[i].iscid === iscid) return true;
  }
  return false;
};
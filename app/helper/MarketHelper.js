import {MarketStatus} from "../util/Utility";
import {GET_MARKET_STATUS, GET_STOCK_PRICE} from "../api/ApiRoutes";
import Api from "../api/Api";

export const checkMarketStatus = (jwt, csrf) => {
  return new Promise((resolve, reject) => {
    Api.get({route: GET_MARKET_STATUS, jwt, csrf})
      .then((response) => resolve(response.data === MarketStatus.OPEN))
      .catch((error) => reject(error));
  })
};

export const getUpdatedStocks = (jwt, csrf, sids) => {
  let params = "";
  for (let i = 0; i < sids.length; i++) {
    params += "stocks=" + sids[i] + "&";
  }
  return new Promise((resolve, reject) => {
    Api.get({route: GET_STOCK_PRICE, jwt, csrf, params})
      .then((response) => resolve(updateStockPrices(sids, response)))
      .catch((error) => reject(error))
  });
};

const updateStockPrices = (sids, prices) => {
  let priceObj = {};
  sids.forEach((sid) => {
    priceObj[sid] = prices.data[sid];
  });

  return priceObj;
};

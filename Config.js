import {AUTH_BASE_URL_DEV, AUTH_BASE_URL_PROD, BASE_URL_DEV, BASE_URL_PROD} from "./app/api/ApiRoutes";
import {KITE_DEV_KEY, KITE_PROD_KEY} from "./app/constants/Constants";

let loggingEnabled;
let BASE_URL;
let AUTH_BASE_URL;
let KITE_TOKEN;
let MIXPANEL_TOKEN;
let SENTRY_DSN;

if (__DEV__) {
  loggingEnabled = true;
  BASE_URL = BASE_URL_PROD;
  AUTH_BASE_URL = AUTH_BASE_URL_PROD;
  KITE_TOKEN = KITE_PROD_KEY;
  MIXPANEL_TOKEN = '7b1e06d0192feeac86689b5599a4b024';
  SENTRY_DSN = 'https://ad9d8ec05f0641ad8eab11927fdfc10f:faa3313351c94581869fdafc58babf87@sentry.smallcase.com/13';
} else {
  // Don't change this
  loggingEnabled = false;
  BASE_URL = BASE_URL_PROD;
  AUTH_BASE_URL = AUTH_BASE_URL_PROD;
  KITE_TOKEN = KITE_PROD_KEY;
  MIXPANEL_TOKEN = '7b1e06d0192feeac86689b5599a4b024';
  SENTRY_DSN = 'https://b6db8bb914fb40c9aae73f272d59476c:15c52dfba85a4aa4ab460fee7188df8c@sentry.smallcase.com/14';
}

module.exports = {
  loggingEnabled,
  BASE_URL,
  AUTH_BASE_URL,
  KITE_TOKEN,
  MIXPANEL_TOKEN,
  SENTRY_DSN
}